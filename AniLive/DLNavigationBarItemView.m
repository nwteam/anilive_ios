//
//  DLNavigationBarItemView.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/23.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLNavigationBarItemView.h"

const static CGFloat kDefaultMargin = 8;
const static CGFloat kDefaultFontSize = 14;

@interface DLNavigationBarItem ()

@property (readwrite, nonatomic, strong) NSString *title;
@property (readwrite, nonatomic, strong) UIColor *enabledTitleColor;
@property (readwrite, nonatomic, strong) UIColor *disabledTitleColor;
@property (readwrite, nonatomic, strong) UIImage *image;
@property (readwrite, nonatomic, assign) CGFloat itemWidth;

@end

@implementation DLNavigationBarItem

- (instancetype)initWithImage:(UIImage *)image {
    return [self initWithTitle:nil
         withEnabledTitleColor:nil
        withDisabledTitleColor:nil
                     withImage:image];
}

- (instancetype)initWithTitle:(NSString *)title
        withEnabledTitleColor:(UIColor *)enabledTitleColor {
    return [self initWithTitle:title
         withEnabledTitleColor:enabledTitleColor
        withDisabledTitleColor:nil
                     withImage:nil];
}

- (instancetype)initWithTitle:(NSString *)title
        withEnabledTitleColor:(UIColor *)enabledTitleColor
       withDisabledTitleColor:(UIColor *)disabledTitleColor {
    return [self initWithTitle:title
         withEnabledTitleColor:enabledTitleColor
        withDisabledTitleColor:disabledTitleColor
                     withImage:nil];
}

- (instancetype)initWithTitle:(NSString *)title
        withEnabledTitleColor:(UIColor *)enabledTitleColor
       withDisabledTitleColor:(UIColor *)disabledTitleColor
                    withImage:(UIImage *)image
{
    self = [super init];
    if (self) {
        self.title = title;
        self.enabledTitleColor = enabledTitleColor;
        
        if (NOT_NULL(disabledTitleColor)) {
            self.disabledTitleColor = disabledTitleColor;
        } else {
            self.disabledTitleColor = self.enabledTitleColor;
        }
        
        self.image = image;
        
        if (NOT_NULL(title)) {
            UILabel *sizeLabel = [[UILabel alloc]init];
            sizeLabel.text = title;
            sizeLabel.font = BASE_FONT(kDefaultFontSize);
            [sizeLabel sizeToFit];
            
            self.itemWidth = sizeLabel.width + kDefaultMargin;
        } else {
            self.itemWidth = kDefaultItemViewSide;
        }
    }
    return self;
}

@end

@interface DLNavigationBarItemView ()

@property (readwrite, nonatomic, strong) UILabel *label;
@property (readwrite, nonatomic, strong) UIImageView *imageView;
@property (readwrite, nonatomic, strong) DLNavigationBarItem *item;

@end

@implementation DLNavigationBarItemView

- (instancetype)initWithItem:(DLNavigationBarItem *)item
{
    self = [super init];
    if (self) {
        
        self.item = item;
        [self initView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
}

#pragma mark - Private Methods

- (void)initView {
    
    self.imageView = [[UIImageView alloc]init];
    self.imageView.backgroundColor = [UIColor clearColor];
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.image = self.item.image;
    [self addSubview:self.imageView];
    
    self.label = [[UILabel alloc]init];
    self.label.backgroundColor = [UIColor clearColor];
    self.label.textColor = self.item.enabledTitleColor;
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.font = BASE_FONT(kDefaultFontSize);
    [self addSubview:self.label];
    
    self.button = [DLHighlightedButton buttonWithType:UIButtonTypeSystem];
    self.button.isNoNeedHighlight = YES;
    [self addSubview:self.button];
}

- (void)updateFrame {
    
    self.label.frame = self.bounds;
    self.imageView.frame = CGRectMake(kDefaultMargin, kDefaultMargin, self.width - (kDefaultMargin * 2), self.height - (kDefaultMargin * 2));
    self.button.frame = self.bounds;
}

@end
