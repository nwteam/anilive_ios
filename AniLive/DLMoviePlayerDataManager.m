//
//  DLMoviePlayerDataManager.m
//  AniLive
//
//  Created by ykkc on 2017/01/24.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLMoviePlayerDataManager.h"

static NSString *kForumIndexKeyPath = @"currentForumIndex";
static NSString *kCommentListKeyPath = @"currentForumCommentList";


@interface DLMoviePlayerDataManager ()

// Forum情報
@property (nonatomic, assign) NSInteger currentForumIndex;
@property (nonatomic, strong) NSMutableArray<DLForum *> *movieForumList;

// Comment情報
@property (nonatomic, strong) NSMutableArray<DLComment *> *currentForumCommentList;

@end

@implementation DLMoviePlayerDataManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.currentForumIndex = 0;
        self.movieForumList = [NSMutableArray<DLForum *> new];
        [self addObserver:self
               forKeyPath:kForumIndexKeyPath
                  options:NSKeyValueObservingOptionNew
                  context:nil];

        self.currentForumCommentList = [NSMutableArray<DLComment *> new];
        [self addObserver:self
               forKeyPath:kCommentListKeyPath
                  options:NSKeyValueObservingOptionNew
                  context:nil];

    }
    return self;
}

- (DLForum *)getCurrentForum {
    return [self.movieForumList objectAtIndex:self.currentForumIndex];
}

- (NSMutableArray<DLForum *> *)getForumList {
    return [self.movieForumList mutableCopy];
}

- (void)movePreviousForum {
    if(self.currentForumIndex > 0){
        self.currentForumIndex -= 1;
    }
}

- (void)moveNextForum {
    if(self.currentForumIndex < [self.movieForumList count]){
        self.currentForumIndex += 1;
    }
}

- (void)updateForums:(NSMutableArray<DLForum *> *)forums {
    self.movieForumList = [forums mutableCopy];
    NSInteger index = self.currentForumIndex;
    self.currentForumIndex = index;
}

- (void)updateComments:(NSMutableArray<DLComment *> *)comments {
    // Forum_idのチェックをした方がいいような気がしないでもない。
    self.currentForumCommentList = [comments mutableCopy];
}

#pragma KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:kForumIndexKeyPath]) {
        // ForumのIndexが更新された際に、ForumListのCurrentにあたるものをdelegateで通知する。

        if(([self.movieForumList count] > self.currentForumIndex) && (self.delegate != nil)){
            [self.delegate onUpdateCurrentForum:[self.movieForumList objectAtIndex:self.currentForumIndex]
                                  existPrevious:(self.currentForumIndex > 0)
                                      existNext:(self.currentForumIndex < [self.movieForumList count])];
        }
    }else if([keyPath isEqualToString:kCommentListKeyPath]) {
        // Comment情報が更新された際に、CommentListを通知する。
        [self.delegate onUpdateCurrentForumComments:self.currentForumCommentList];
    }
}

- (void)dealloc
{
    self.delegate = nil;
    [self removeObserver:self forKeyPath:kForumIndexKeyPath];
    [self removeObserver:self forKeyPath:kCommentListKeyPath];
}

@end
