//
//  DLSettingsSectionHeader.h
//  AniLive
//
//  Created by Isao Kono on 2016/12/01.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DLSettingSectionHeader : UIView

+ (CGFloat)getSectionHeaderHeight;

- (void)setTitle:(NSString *)title;

@end

