//
//  DLLiveMovieCell.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/17.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLSuperCollectionViewCell.h"
#import "DLMovie.h"
#import "DLLive.h"
#import "DLPlayerBaseView.h"

@protocol DLLiveMovieCellDelegate;

@interface DLLiveMovieCell : DLSuperCollectionViewCell

@property (nonatomic, strong) DLPlayerBaseView *playerBaseView;
@property (nonatomic, weak) id<DLLiveMovieCellDelegate> delegate;

+ (CGSize)getCellSizeWithParantWidth:(CGFloat)parantWidth;

- (void)setCellWithMovieData:(DLMovie *)movieData;
- (void)setCellWithLiveData:(DLLive *)liveData;

- (void)addPlayerBaseView;

- (void)removePlayerBaseView;

@end

@protocol DLLiveMovieCellDelegate <NSObject>

@optional
- (void)didTapLiveMovieCell:(DLLiveMovieCell *)cell withMovieData:(DLMovie *)movieData;
- (void)didTapLiveMovieCell:(DLLiveMovieCell *)cell withLiveData:(DLLive *)liveData;

- (BOOL)scrollViewIsScrolling;

@end
