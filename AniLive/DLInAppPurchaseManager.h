//
//  DLInAppPurchaseManager.h
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/11/06.
//  Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//



@class SKProductsRequest;
@class SKPaymentQueue;

@protocol InAppPurchaseManagerDelegate <NSObject>
@optional
- (void)succeedInAppPurchase:(NSString *)productId;

- (void)succeedInAppPurchaseReceipt:(NSString *)receipt;

- (void)failedInAppPurchase:(NSString *)productId withMessage:(NSString *)message;

- (void)purchasing;

@end

@interface DLInAppPurchaseManager : NSObject
@property(nonatomic, assign) id <InAppPurchaseManagerDelegate> delegate;

+ (void)setDelegate:(id <InAppPurchaseManagerDelegate>)delegate;
+ (void)purchaseDone;
+ (void)validateByApple:(NSURL *)receiptURL;
+ (void)restore;

+ (BOOL)canMakePurchases;

+ (void)requestProductData:(NSString *)pID;
+ (void)requestProductDatas;
@end
