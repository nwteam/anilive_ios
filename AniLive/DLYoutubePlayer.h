//
// Created by akasetaichi on 2017/01/17.
// Copyright (c) 2017 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>
#import "DLPlayerViewIsolated.h"

@interface DLYoutubePlayer : UIView<DLPlayerViewIsolatedProtocol, WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler>

- (instancetype)initIsLive:(BOOL)flag;

- (void)embedYouTube:(NSString*)code;

@end