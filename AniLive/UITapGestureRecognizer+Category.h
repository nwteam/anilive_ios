//
//  UITapGestureRecognizer+Category.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITapGestureRecognizer (Category)

// UILabelの指定された文言の領域をタップしたかどうかを判定する
- (BOOL)didTapAttributedTextInLabel:(UILabel*)label withTargetRange:(NSRange)targetRange;

@end
