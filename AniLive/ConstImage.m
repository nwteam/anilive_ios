//
//  ConstImage.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "ConstImage.h"

@implementation ConstImage


#pragma mark - Images

NSString * const DLConstImageComment = @"img-comment";
NSString * const DLConstImageMovieLoading = @"img-movie_loading";
NSString * const DLConstImagePremium = @"img-premium";
NSString * const DLConstImageSwipeBig = @"img-swipe-big";
NSString * const DLConstImageSwipeSmall = @"img-swipe-small";
NSString * const DLConstImageTap = @"img-tap";

#pragma mark - Labels

NSString * const DLConstLabelLive = @"label-live";
NSString * const DLConstLabelPremium = @"label-premium";

#pragma mark - Icons

NSString * const DLConstIconArrowBack = @"ic-arrow-back";

NSString * const DLConstIconCheckBoxOff = @"ic-check-box-off";
NSString * const DLConstIconCheckBoxOn = @"ic-check-box-on";

NSString * const DLConstIconChevronLeftDisable = @"ic-chevron-left-disable";
NSString * const DLConstIconChevronLeft = @"ic-chevron-left";
NSString * const DLConstIconChevronRightDisable = @"ic-chevron-right-disable";
NSString * const DLConstIconChevronRight = @"ic-chevron-right";

NSString * const DLConstIconClose = @"ic-close";

NSString * const DLConstIconFullScreenExit = @"ic-fullscreen-exit";
NSString * const DLConstIconFullScreen = @"ic-fullscreen";

NSString * const DLConstIconKeyboadArrowDown = @"ic-keyboard-arrow-down";

NSString * const DLConstIconList = @"ic-list";

NSString * const DLConstIconMakeForum = @"ic-make-forum";

NSString * const DLConstIconModeEditDisable = @"ic-mode-edit-disable";

NSString * const DLConstIconMoreVert = @"ic-more-vert";

NSString * const DLConstIconNotificationsOff = @"ic-notifications-off";
NSString * const DLConstIconNotificationsOn = @"ic-notifications-on";

NSString * const DLConstIconPause = @"ic-pause";

NSString * const DLConstIconPlayArrow = @"ic-play-arrow";

NSString * const DLConstIconSchedule = @"ic-schedule";

NSString * const DLConstIconStarOff = @"ic-star-off";
NSString * const DLConstIconStarOn  = @"ic-star-on";

#pragma mark TabBarView
NSString * const DLConstIconTabBarCatalogOff = @"ic-tab-catalog-off";
NSString * const DLConstIconTabBarCatalogOn = @"ic-tab-catalog-on";
NSString * const DLConstIconTabBarForumOff = @"ic-tab-forum-off";
NSString * const DLConstIconTabBarForumOn = @"ic-tab-forum-on";
NSString * const DLConstIconTabBarLiveOff = @"ic-tab-live-off";
NSString * const DLConstIconTabBarLiveOn = @"ic-tab-live-on";
NSString * const DLConstIconTabBarNoticeOff = @"ic-tab-notice-off";
NSString * const DLConstIconTabBarNoticeOn = @"ic-tab-notice-on";
NSString * const DLConstIconTabBarMyPageOff = @"ic-tab-mypage-off";
NSString * const DLConstIconTabBarMyPageOn = @"ic-tab-mypage-on";

NSString * const DLConstIconWhatsHot = @"ic-whatshot";

NSString * const DLConstIconInfoOutline = @"ic_info_outline";


// ---------------------------------------------------- //
#pragma mark - xxx Deprecated xxx
// ---------------------------------------------------- //

NSString * const DLConstImageTopLogo = @"img_top_logo";
NSString * const DLConstImageSplashGIF = @"logo_animation_2";

NSString * const DLConstIconTotalView = @"icon_total_view";
NSString * const DLConstIconTotalComment = @"icon_total_comment";
NSString * const DLConstIconBack = @"icon_back";
NSString * const DLConstIconWhiteClose = @"icon_close_white";
NSString * const DLConstIconLiveOff = @"ic-tab-live-off";
NSString * const DLConstIconAddForum = @"icon_add_forum";
NSString * const DLConstIconPlay = @"icon_play";
NSString * const DLConstIconShrink = @"icon_shrink";
NSString * const DLConstIconSetting = @"icon_setting";

NSString * const DLConstIconForumList        = @"icon_forum_list";
NSString * const DLConstIconForumNext        = @"icon_forum_next";
NSString * const DLConstIconForumPrev        = @"icon_forum_prev";
NSString * const DLConstIconForumPostComment = @"icon_forum_post_comment";
NSString * const DLConstIconForumOther       = @"icon_forum_other";

@end
