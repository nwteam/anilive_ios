//
//  DLUnityAdsButton.h
//  AniLive
//
//  Created by 辰己佳祐 on 2017/01/14.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^DLUnityButtonDidTapBlock)(UIButton *button);

@interface DLUnityAdsButton : UIView

@property (nonatomic, assign, readonly) BOOL enabled;

+ (CGFloat)buttonHeight;
+ (CGFloat)buttonWidth;

- (void)buttonDidTap:(DLUnityButtonDidTapBlock)completion;
- (void)setIsEnabled:(BOOL)isEnabled;
- (void)setButtonTitle:(NSString *)title;

@end
