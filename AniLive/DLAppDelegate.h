//
//  DLAppDelegate.h
//  AniLive
//
//  Created by ykkc on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLNavigationController.h"
#import "DLMainTabBarController.h"
#import "DLSplashViewController.h"
#import "DaisukiContext.h"

@class HTTPServer;

@interface DLAppDelegate : UIResponder <UIApplicationDelegate> {
    HTTPServer *_httpServer;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) DaisukiContext*	context;
@property (strong, nonatomic) DLNavigationController *rootNavigationController;
@property (strong, nonatomic) DLMainTabBarController *rootTabBarController;

- (void)openSplashViewController: (NSObject*) notificationTransitionObject;
- (void)setTabBarViewIsHidden:(BOOL)isHidden;

@end

