//
//  UIAlertController+Category.m
//  AniLive
//
//  Created by 辰己佳祐 on 2017/01/14.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import "UIAlertController+Category.h"

@implementation UIAlertController (Category)

+ (void)showSingleAlertWithTitle:(NSString *)title
                      withTarget:(UIViewController *)controller {
    [UIAlertController showSingleAlertWithTitle:title withMessage:@"" withStyle:UIAlertControllerStyleAlert withTarget:controller];
}

+ (void)showSingleAlertWithMessage:(NSString *)message
                        withTarget:(UIViewController *)controller {
    [UIAlertController showSingleAlertWithTitle:@"" withMessage:message withStyle:UIAlertControllerStyleAlert withTarget:controller];
}

+ (void)showSingleAlertWithTitle:(NSString *)title
                     withMessage:(NSString *)message
                      withTarget:(UIViewController *)controller {
    [UIAlertController showSingleAlertWithTitle:title withMessage:message withStyle:UIAlertControllerStyleAlert withTarget:controller];
}

+ (void)showSingleAlertWithTitle:(NSString *)title
                     withMessage:(NSString *)message
                       withStyle:(UIAlertControllerStyle)style
                      withTarget:(UIViewController *)controller {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                             message:message
                                                                      preferredStyle:style];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:nil]];
    [controller presentViewController:alertController animated:YES completion:nil];
}

@end
