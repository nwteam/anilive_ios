//
//  DLTransitionManager.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DLMovie, DLLive, DLWork;
@class DLForumGroup;
@class DLForum;

@interface DLTransitionManager : NSObject

// 最前面のUIViewControllerを取得
+ (UIViewController*)getMostTopController;

+ (void)showMoviePlayViewControllerWithMovieData:(DLMovie *)movieData withTarget:(UIViewController*)target;

+ (void)showMoviePlayViewControllerWithMovieData:(DLMovie *)movieData withTarget:(UIViewController*)target withIsModal:(BOOL)isModal;

+ (void)showMoviePlayViewControllerWithLiveData:(DLLive *)liveData withTarget:(UIViewController*)target;

+ (void)showMoviePlayViewControllerWithLiveData:(DLLive *)liveData withTarget:(UIViewController*)target withIsModal:(BOOL)isModal;

+ (void)showForumListViewControllerWithForumGroup:(DLForumGroup *)group withTarget:(UIViewController *)target;

+ (void)showForumListViewControllerWithForum:(DLForum *)froum withTarget:(UIViewController *)target;

+ (void)showForumDetailViewControllerWithForum:(DLForum *)forum withTarget:(UIViewController *)target;

+ (void)showWorkDetailViewControllerWithWorkData:(DLWork *)workData withTarget:(UIViewController *)target;

+ (void)showWorkInfoViewControllerWithWorkData:(DLWork *)workData withTarget:(UIViewController *)target;

+ (void)showSettingsViewComtroller:(UIViewController *)target withIsModal:(BOOL)isModal;

+ (void)showRegisterViewController:(UIViewController *)target withIsModal:(BOOL)isModal;
@end
