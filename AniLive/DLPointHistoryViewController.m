//
//  DLPointHistoryViewController.m
//  AniLive
//
//  Created by 市川 俊介 on 2016/12/24.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPointHistoryViewController.h"

#import "DLPointHistoryCell.h"
#import "DLNavigationBarView.h"
#import "DLAppFrameManager.h"

@interface DLPointHistoryViewController ()
@property(nonatomic, strong) DLNavigationBarView *navigationBarView;
@property(nonatomic, strong) UITextView *txvPointHistoryNotice;         //ポイント履歴注意
@property(nonatomic, strong) UITableView *tvPointHistory;               //ポイント履歴表示
@property(nonatomic, strong) NSMutableArray *points;                    //ポイント履歴情報

@end

@implementation DLPointHistoryViewController

//共通事項
const static CGFloat commonMargin                       = 10.0f;
//ポイント履歴注意事項に関する属性値
const static CGFloat vPointHistoryNoticeViewHeight      = 50.0f;
const static CGFloat vPointHistoryNoticeViewFontSize    = 11.0f;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    [self loadData:NO];

    //ナビゲーションはupdateFrameをする必要がある
    [self updateFrame];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateFrame {
    self.navigationBarView.frame = CGRectMake(0,
                                              CGRectGetMinY([DLAppFrameManager sharedInstance].applicationFrame),
                                              CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame),
                                              DLConstNavigationBarHeight);
    /*
    _tableViewTopConstraint.constant = self.navigationBarView.maxY - [UIApplication sharedApplication].statusBarFrame.size.height;
     */
    [self.view needsUpdateConstraints];
}


#pragma mark - Private Methods

- (void)initView {

    //背景色の設定
    self.view.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkGray];

    //ナビゲーションの設定
    self.navigationBarView = [[DLNavigationBarView alloc] initWithType:DLNavigationBarViewTypeForumList];
    self.navigationBarView.delegate = self;
    [self.navigationBarView setTitle:Localizer(@"Point History")];
    [self.view addSubview:self.navigationBarView];
    
    //ポイントに関する注意事項の配置(固定)
    UIView *vPointHistoryNotice     =   [[UIView alloc] init];
    vPointHistoryNotice.frame       =   CGRectMake(0.0f,
                                           DLConstNavigationBarHeight,
                                           self.view.bounds.size.width,
                                           vPointHistoryNoticeViewHeight);
    [self.view addSubview:vPointHistoryNotice];
    
    self.txvPointHistoryNotice                  =   [[UITextView alloc] init];
    self.txvPointHistoryNotice.frame            =   vPointHistoryNotice.bounds;
    self.txvPointHistoryNotice.textColor        =   [UIColor whiteColor];
    self.txvPointHistoryNotice.backgroundColor  =    [UIColor clearColor];
    self.txvPointHistoryNotice.editable         =   NO;
    self.txvPointHistoryNotice.userInteractionEnabled   =   NO;
    self.txvPointHistoryNotice.font             =   BASE_FONT(vPointHistoryNoticeViewFontSize);
    self.txvPointHistoryNotice.text             =   @"Your points will be lost unless you use or get any point for 45 days";
    self.txvPointHistoryNotice.textContainerInset = UIEdgeInsetsMake(commonMargin,
                                                                     commonMargin,
                                                                     commonMargin,
                                                                     commonMargin);
    [self.txvPointHistoryNotice sizeToFit];
    [vPointHistoryNotice addSubview:self.txvPointHistoryNotice];
    
    //ポイント履歴のテーブル作成、配置
    #pragma mark Table View
    self.tvPointHistory = [[UITableView alloc] init];
    [self.tvPointHistory registerNib:[DLPointHistoryCell getDefaultNib]
              forCellReuseIdentifier:@"point-history-cell"];
    self.tvPointHistory.delegate                =   self;
    self.tvPointHistory.dataSource              =   self;
    self.tvPointHistory.separatorColor          =   [UIColor clearColor];
    self.tvPointHistory.frame                   =   CGRectMake(0.0f,
                                                               DLConstNavigationBarHeight + vPointHistoryNoticeViewHeight,
                                                               self.view.bounds.size.width,
                                                               self.view.bounds.size.height - DLConstNavigationBarHeight - vPointHistoryNoticeViewHeight    );
    self.tvPointHistory.backgroundColor         =   [UIColor colorWithHex:DLConstColorCodeDarkGray];
    [self.view addSubview:self.tvPointHistory];
}


- (void)loadData:(BOOL) shouldScrollToBottom{
    @weakify(self)
    [DLAPIHelper fetchPointsUsed:^(NSMutableArray *points) {
        @strongify(self)
        self.points = points;
        [self.tvPointHistory reloadData];
        if (shouldScrollToBottom && self.points.count > 0) {
            [self.tvPointHistory scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.points.count - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
    }];
}


#pragma mark - DLNavigation Bar View Delegate

- (void)navigationBarViewDidTapLeftButton:(UIButton *)button {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.points ? self.points.count : 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *CellIdentifier = @"point-history-cell";
    DLPointHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[DLPointHistoryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor        =   [UIColor blackColor];
    }
    [cell setPoint:self.points[indexPath.row]];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [DLPointHistoryCell getCellHeight];
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
