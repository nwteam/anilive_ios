//
//  DLTabBarView.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLTabBarView.h"
#import "DLTabBarItemView.h"
#import "DLNotice.h"

@interface DLTabBarView () <DLTabBarItemViewDelegate>

@property (nonatomic, strong) NSMutableArray *tabBarItemViews;
@property (nonatomic, assign) NSInteger numberOfTabs;
@property (nonatomic, assign) NSInteger startTab;
@property (readwrite, nonatomic, strong) RLMNotificationToken *realmToken;
@property (readwrite, nonatomic, strong) DLTabBarItemView *noticeItemView;
@end

@implementation DLTabBarView

- (instancetype)initWithNumberOfTabs:(NSInteger)numberOfTabs withStartTab:(NSInteger)startTab
{
    self = [super init];
    if (self) {
        self.numberOfTabs = numberOfTabs;
        self.startTab = startTab;
        self.tabBarItemViews = [[NSMutableArray alloc]init];
        [self initView];
        [self updateFrame];

        /*
         * Realmの監視
         */
        RLMRealm* realm = [RLMRealm defaultRealm];
        self.realmToken = [realm addNotificationBlock:^(NSString *notification, RLMRealm* realm) {
            [self reflectNoticeBadge];
        }];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    [self.realmToken stop];
    FUNC_LOG
    for (DLTabBarItemView *itemView in self.tabBarItemViews) {
        itemView.delegate = nil;
    }
}

#pragma mark - Private Methods

- (void)initView {
    self.backgroundColor = [UIColor colorWithHex:DLConstColorLightBlue];
    
    for (NSInteger i=0; i<self.numberOfTabs; i++) {
        
        DLTabBarItem *item = [[DLTabBarItem alloc] init];
        item.normalTitleColor = [UIColor colorWithHex:DLConstColorCodeThinGray];
        item.highlightedTitleColor = [UIColor colorWithHex:DLConstColorCodeYellow];
        item.tag = i;

        NSLog(@"item:%@", item);
        switch (i) {
            case 0:
            {
                item.title = Localizer(@"tab_menu_catalog");
                item.normalIconImage = [UIImage imageNamed:DLConstIconTabBarCatalogOff];
                item.highlightedIconImage = [UIImage imageNamed:DLConstIconTabBarCatalogOn];
            }
                break;
            case 1:
            {
                item.title = Localizer(@"tab_menu_forum");
                item.normalIconImage = [UIImage imageNamed:DLConstIconTabBarForumOff];
                item.highlightedIconImage = [UIImage imageNamed:DLConstIconTabBarForumOn];
            }
                break;
            case 2:
            {
                item.title = Localizer(@"tab_menu_live");
                item.normalIconImage = [UIImage imageNamed:DLConstIconTabBarLiveOff];
                item.highlightedIconImage = [UIImage imageNamed:DLConstIconTabBarLiveOn];
            }
                break;
            case 3:
            {
                item.title = Localizer(@"tab_menu_notification");
                item.normalIconImage = [UIImage imageNamed:DLConstIconTabBarNoticeOff];
                item.highlightedIconImage = [UIImage imageNamed:DLConstIconTabBarNoticeOn];
            }
                break;
            case 4:
            {
                item.title = Localizer(@"tab_menu_mypage");
                item.normalIconImage = [UIImage imageNamed:DLConstIconTabBarMyPageOff];
                item.highlightedIconImage = [UIImage imageNamed:DLConstIconTabBarMyPageOn];
            }
                break;
                
            default:
                break;
        }
        
        DLTabBarItemView *itemView = [[DLTabBarItemView alloc]initWithTabBarItem:item];
        itemView.delegate = self;

        if (i == 3) {
            self.noticeItemView = itemView;
            [self reflectNoticeBadge];
        }

        if (self.startTab == i) {
            [itemView setButtonSelected:YES];
        }
        
        [self addSubview:itemView];
        [self.tabBarItemViews addObject:itemView];
    }
}

- (void)reflectNoticeBadge {
    if ([DLNotice isExistYetRead]) {
        self.noticeItemView.badgeImageView.image = [UIImage imageNamed:@"icon_badge"];
    } else {
        self.noticeItemView.badgeImageView.image = nil;
    }
}

- (void)updateFrame {
    
    CGFloat tabWidth = self.width / self.tabBarItemViews.count;
    
    for (int i = 0; i< self.tabBarItemViews.count; i++) {
        
        DLTabBarItemView *itemView = self.tabBarItemViews[i];
        itemView.frame = CGRectMake(tabWidth * i, 0, tabWidth, self.height);
    }
}

#pragma mark - DLTabBarItemViewDelegate

- (void)tabBarItemViewDidSelect:(DLTabBarItemView *)tabBarItemView {
    
    for (DLTabBarItemView *itemView in self.tabBarItemViews) {
        
        if (itemView.tabBarItem.tag == tabBarItemView.tabBarItem.tag) {

            
            [itemView setButtonSelected:YES];
        } else {
            [itemView setButtonSelected:NO];
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(tabBarView:didSelectIndex:)]) {
        [self.delegate tabBarView:self didSelectIndex:tabBarItemView.tabBarItem.tag];
    }
}

@end
