//
//  DLOpenPlayMovieService.m
//  AniLive
//
//  Created by Kotaro Itoyama on 2017/01/14.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//
#import <UnityAds/UnityAds.h>
#import <Toast/UIView+Toast.h>
#import "DLOpenPlayMovieService.h"
#import "DLLive.h"
#import "DLUserDataManager.h"
#import "DLMovie.h"
#import "DLWork.h"
#import "DLTransitionManager.h"
#import "DLPayingPointManager.h"
#import "DLMoviePlayViewController.h"
#import "DLContinueToPlayView.h"


typedef NS_ENUM(NSInteger, DLOpenPlayMovieServiceCallbackCase) {
    UNKNOWN_ERROR,
    PLAY_IMMEDIATELY,
    NEED_POINT_OR_REGISTER,
    NEED_1POINT_OR_REGISTER,
    REGISTER_ONLY,
    NEED_BUY,
    MOVIE_GONE
};

typedef NS_ENUM(NSInteger, DLLeadRegistrationDialogResultCase) {
    CANCELLED,
    BE_PREMIUM,
    USE_POINT
};

@interface DLOpenPlayMovieService () <UnityAdsDelegate>
@property(nonatomic, copy) DLOpenPlayMovieServiceCallback callback;
@property(nonatomic, strong) DLLive *mLive;
@property(nonatomic, strong) DLMovie *mMovie;
@property(nonatomic, strong) UIViewController *callerViewController;
@property(nonatomic) NSInteger needPointAmount;

- (void)tryPlayMovie:(DLOpenPlayMovieServiceCallback)callback;
@end

@implementation DLOpenPlayMovieService
+ (instancetype)sharedInstance {
    static DLOpenPlayMovieService *service;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = DLOpenPlayMovieService.new;
    });

    if([UnityAds isInitialized]){
        [UnityAds setDelegate:service];
    }else{
        [UnityAds initialize:APP_STORE_ID_FOR_UNITY_ADS delegate:service testMode:YES];
    }

    service.callerViewController = nil;
    service.mMovie = nil;
    service.mLive = nil;
    service.needPointAmount = 25;

    return service;
}

+ (void)tryPlayMovie:(DLMovie *)movie
                live:(DLLive *)live
  fromViewController:(UIViewController *)controller
            callback:(DLOpenPlayMovieServiceCallback)callback {
    DLOpenPlayMovieService *service = [DLOpenPlayMovieService sharedInstance];
    if([controller isKindOfClass:[DLMoviePlayViewController class]]){
        service.callerViewController = controller;
    }else{
        service.callerViewController = controller.view.window.rootViewController;
    }
    service.mMovie = movie;
    service.mLive = live;
    [service tryPlayMovie:callback];
}

- (void)tryPlayMovie:(DLOpenPlayMovieServiceCallback)callback {
    self.callback = callback;
    [self determineCase:^(DLOpenPlayMovieServiceCallbackCase caseEnum) {
        switch (caseEnum) {
            case PLAY_IMMEDIATELY:
                [self playMovie];
                break;
            default:
                [self openDialog:caseEnum];
                break;
        }
    }];
}

- (void)openDialog:(DLOpenPlayMovieServiceCallbackCase)caseEnum {
    // ダイアログを起動してユーザーにアクションを問い合わせる
    NSString *representation;
    switch (caseEnum) {
        case NEED_POINT_OR_REGISTER:
            representation = @"To watch this video, you need to use 25 points for unlocking it or register as a premium member.";
            break;
        case NEED_1POINT_OR_REGISTER:
            representation = @"To watch this video, you need to use 1 points for unlocking it or register as a premium member.";
            break;
        case REGISTER_ONLY:
            representation = @"To watch this video, you need to register as a premium member.";
            break;
        default:
            break;
    }
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"プレミアムエピソード解禁" message:representation preferredStyle:UIAlertControllerStyleAlert];

    [alertController addAction:[UIAlertAction actionWithTitle:@"課金" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        // TODO 課金画面へ遷移
        [DLTransitionManager showRegisterViewController:self.callerViewController withIsModal:YES];
    }]];
    if (caseEnum != REGISTER_ONLY) {
        [alertController addAction:[UIAlertAction actionWithTitle:@"ポイントを使う" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self consumePoint];
        }]];
    }
    [alertController addAction:[UIAlertAction actionWithTitle:@"いいえ" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        self.callback();
    }]];

    [self.callerViewController presentViewController:alertController animated:YES completion:nil];

}

- (void)showToast:(NSString *)content {
    if (self.callerViewController) {
        [self.callerViewController.view makeToast:content];
    }
}

- (void)consumePoint {
    BOOL hasPointEnough = [[DLUserDataManager sharedInstance].user.point integerValue] >= self.needPointAmount;
    if (hasPointEnough) {
        [self showToast:[NSString stringWithFormat:Localizer(@"forum_15"), self.needPointAmount]];

        @weakify(self)
        [DLAPIHelper fetchWorksByWorkID:[self.mMovie.workId integerValue] callback:^(DLWork *work, NSError *error) {
            NSString *reason = [NSString stringWithFormat:@"Unlocked %@ %@", work.title, self.mMovie.title];
            [DLAPIHelper usePointWithMovieId:self.mMovie.movieId reason:reason callback:^(BOOL flag) {
                @strongify(self)
                [self playMovie];
            }];
        }];
        return;
    }

    [DLAPIHelper checkAdsShowingTimesCallback:^(BOOL canShowAd) {
        if (!canShowAd) {
            [self showToast:Localizer(@"forum_8")];
            return;
        }

        // TODO UnityAdsを表示
        [UnityAds show:self.callerViewController];
    }];

}

- (void)playMovie {
    dispatch_async(
            dispatch_get_main_queue(),
            ^{
                if (self.mLive) {
                    [DLTransitionManager showMoviePlayViewControllerWithLiveData:self.mLive withTarget:self.callerViewController withIsModal:YES];
                } else {
                    if([self.callerViewController isKindOfClass:[DLMoviePlayViewController class]]){
                        [((DLMoviePlayViewController *)self.callerViewController) showNextMovie: self.mMovie];
                    }else{
                        [DLTransitionManager showMoviePlayViewControllerWithMovieData:self.mMovie withTarget:self.callerViewController withIsModal:YES];
                    }
                }
            }
    );

}

- (BOOL)canShow {
    if (self.mLive) {
        return YES;
    }

    if ([[DLUserDataManager sharedInstance].user getBillingType] == DLUserBillingTypeFree) {
        switch (self.mMovie.salesType.intValue) {
            case 0://AVOD
                return self.mMovie.pointFlag.intValue == 0;
            case 1://パック
                break;
            case 2://TVOD
                break;
            case 3://SVOD
                break;
            default:
                return NO;
        }
        return NO;
    }
    return YES;
}

- (void)determineCase:(void (^)(DLOpenPlayMovieServiceCallbackCase caseEnum))callback {
    DLUser *user = [DLUserDataManager sharedInstance].user;
    if (self.mLive) {
        callback(PLAY_IMMEDIATELY);
        return;
    }

    @weakify(self)
    [DLAPIHelper fetchWatch:self.mMovie.movieId.integerValue
                     liveId:0
                   callback:^(DLWatch *watch, NSError *error) {
                       @strongify(self)
                       callback([self determineCore:user movie:self.mMovie watch:watch]);
                   }];
}

- (DLOpenPlayMovieServiceCallbackCase)determineCore:(DLUser *)user movie:(DLMovie *)movie watch:(DLWatch *)watch {
    if (watch) {
        return PLAY_IMMEDIATELY;
    }
    int salesType = movie.salesType.intValue;
    int pointFlag = movie.pointFlag.intValue;
    int status = user.userStatus.intValue;
    if (status == 1) {
        if (salesType == 0) {
            if (pointFlag == 0) {
                if (movie.chapter.intValue > 2) {
                    self.needPointAmount = 1;
                    return NEED_1POINT_OR_REGISTER;
                } else {
                    return PLAY_IMMEDIATELY;
                }
            } else if (pointFlag == 1) {
                return NEED_POINT_OR_REGISTER;
            }
        } else if (salesType == 3) {
            return REGISTER_ONLY;
        } else if (salesType == 1 || salesType == 2) {
            return NEED_BUY;
        }
    } else if (status == 2) {//有料会員
        if (salesType == 0 || salesType == 3) {
            return PLAY_IMMEDIATELY;
        } else {
            return NEED_BUY;
        }
    }
    return UNKNOWN_ERROR;
}

# pragma mark - Unity Ads Delegate

- (void)unityAdsReady:(NSString *)placementId {

}

- (void)unityAdsDidError:(UnityAdsError)error withMessage:(NSString *)message {
    switch (error) {
        case kUnityAdsErrorNotInitialized:
            break;
        case kUnityAdsErrorInitializedFailed:
            break;
        case kUnityAdsErrorInvalidArgument:
            break;
        case kUnityAdsErrorVideoPlayerError:
            break;
        case kUnityAdsErrorInitSanityCheckFail:
            break;
        case kUnityAdsErrorAdBlockerDetected:
            break;
        case kUnityAdsErrorFileIoError:
            break;
        case kUnityAdsErrorDeviceIdError:
            break;
        case kUnityAdsErrorShowError:
            break;
        case kUnityAdsErrorInternalError:
            break;
    }
}

- (void)unityAdsDidStart:(NSString *)placementId {
    [self showToast:Localizer(@"point_ad_title")];
}

- (void)unityAdsDidFinish:(NSString *)placementId withFinishState:(UnityAdsFinishState)state {
    switch (state) {
        case kUnityAdsFinishStateError: {
            break;
        }
        case kUnityAdsFinishStateSkipped: {
            break;
        }
        case kUnityAdsFinishStateCompleted: {
            @weakify(self)
            [DLAPIHelper addPoint:^(DLPoint *point) {
                @strongify(self)
                if (point) {
                    [self consumePoint];
                }
            }];
            break;
        }
    }
}


@end
