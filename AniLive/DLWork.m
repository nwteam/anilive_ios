//
//  DLWork.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/10/01.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLWork.h"

@interface DLWork ()

@property (readwrite, nonatomic, strong) NSNumber *workId;
@property (readwrite, nonatomic, strong) NSString *title;
@property (readwrite, nonatomic, strong) NSNumber *studioId;
@property (readwrite, nonatomic, strong) NSString *studioName;
@property (readwrite, nonatomic, strong) NSString *licensor;
@property (readwrite, nonatomic, strong) NSNumber *creationYear;
@property (readwrite, nonatomic, strong) NSString *synopsis;
@property (readwrite, nonatomic, strong) NSString *staff;
@property (readwrite, nonatomic, strong) NSString *cast;
@property (readwrite, nonatomic, strong) NSString *youtube;
@property (readwrite, nonatomic, strong) NSDate *releaseOpen;
@property (readwrite, nonatomic, strong) NSDate *releaseClose;
@property (readwrite, nonatomic, strong) NSString *image1;
@property (readwrite, nonatomic, strong) NSString *image2;
@property (readwrite, nonatomic, strong) NSString *image3;
@property (readwrite, nonatomic, strong) NSString *image4;
@property (readwrite, nonatomic, strong) NSString *image5;
@property (readwrite, nonatomic, strong) NSNumber *totalMovies;
@property (readwrite, nonatomic, strong) NSNumber *popularity;

@end

@implementation DLWork

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.workId = CHECK_NULL_DATA_NUMBER(attributes[@"work_id"]);
    self.title = CHECK_NULL_DATA_STRING(attributes[@"title"]);
    self.studioId = CHECK_NULL_DATA_NUMBER(attributes[@"studio_id"]);
    self.studioName =  CHECK_NULL_DATA_STRING(attributes[@"studio_name"]);
    self.licensor = CHECK_NULL_DATA_STRING(attributes[@"licensor"]);
    self.creationYear =  CHECK_NULL_DATA_NUMBER(attributes[@"creation_year"]);
    self.synopsis = CHECK_NULL_DATA_STRING(attributes[@"synopsis"]);
    
    // TODO: 必要でなくなったら消す
    // synopsisの改行コードが \\n で入ってくるので、\n に置換する
    self.synopsis = [self.synopsis stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    
    self.staff = CHECK_NULL_DATA_STRING(attributes[@"staff"]);
    
    // TODO: 必要でなくなったら消す
    // staffの改行コードが \\n で入ってくるので、\n に置換する
    self.staff = [self.staff stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    
    self.cast = CHECK_NULL_DATA_STRING(attributes[@"cast"]);
    
    // TODO: 必要でなくなったら消す
    // castの改行コードが \\n で入ってくるので、\n に置換する
    self.cast = [self.cast stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    
    self.youtube = CHECK_NULL_DATA_STRING(attributes[@"youtube"]);
    self.releaseOpen = [DLAPIHelper dateFromString:attributes[@"release_open"]];
    self.releaseClose = [DLAPIHelper dateFromString:attributes[@"release_close"]];
    self.image1 = CHECK_NULL_DATA_STRING(attributes[@"image1"]);
    self.image2 = CHECK_NULL_DATA_STRING(attributes[@"image2"]);
    self.image3 = CHECK_NULL_DATA_STRING(attributes[@"image3"]);
    self.image4 = CHECK_NULL_DATA_STRING(attributes[@"image4"]);
    self.image5 = CHECK_NULL_DATA_STRING(attributes[@"image5"]);
    self.totalMovies =  CHECK_NULL_DATA_NUMBER(attributes[@"total_movies"]);
    self.popularity = CHECK_NULL_DATA_NUMBER(attributes[@"popularity"]);
    
    return self;
}

@end
