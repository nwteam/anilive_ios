//
//  DLAlertViewSetPassword.m
//  AniLive
//
//  Created by suisun on 1/13/17.
//  Copyright © 2017 AnimeConsortiumJapan. All rights reserved.
//

#import "DLAlertViewSetPassword.h"

@implementation DLAlertViewSetPassword

@synthesize messageView;
@synthesize titleLabel;
@synthesize msgLabel;
@synthesize txtPassword;
@synthesize lblPassword;
@synthesize btnOK;
@synthesize btnCancel;

- (id)initDLAlertViewSetPassword {
    NSArray* bundle = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    self = [bundle objectAtIndex:0];
    if (self) {
        // init
        [self initMainView];
    }
    return self;
}

- (void)initMainView {
    messageView.layer.cornerRadius = 5;
    messageView.clipsToBounds = true;
    
    titleLabel.text     = @"Set your password";
    msgLabel.text       = @"Prease set your password. You can link to another device by entering your UserID and this password.";
    lblPassword.text    = @"Password";
    txtPassword.text    = @"";
    txtPassword.keyboardType = UIKeyboardTypeNumberPad;
    
    [btnOK setTitle:@"OK" forState:UIControlStateNormal];
    btnOK.layer.masksToBounds   = YES;
    btnOK.layer.cornerRadius    = 3.0;

    [btnCancel  setTitle:@"Cancel" forState:UIControlStateNormal];
    
}

#pragma marker - dismiss keyboard when press return key
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField isFirstResponder]) {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    textField.delegate = self;
    textField.keyboardType = UIKeyboardTypeDefault;
    textField.returnKeyType = UIReturnKeyDone;
    return YES;
}

- (IBAction)tapBtnDLAlertViewSetPassword:(UIButton *)sender {
    //バリデーション
    if (txtPassword.text.length == 12) {
        [self.delegate didTapButtonDLAlertViewSetPassword:[sender tag]];
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"alert"
                                                                                 message:@"Please enter 12 digits number."
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

        }]];

        [self.delegate presentViewController:alertController animated:YES completion:nil];
    }
}

@end
