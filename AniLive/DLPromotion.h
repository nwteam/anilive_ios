//
//  DLPromotion.h
//  AniLive
//
//  Created by ykkc on 2016/12/30.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLModelBase.h"

@interface DLPromotion : DLModelBase
@property (readonly, nonatomic, strong) NSNumber *liveId;
@property (readonly, nonatomic, strong) NSNumber *promotionId;
@property (readonly, nonatomic, strong) NSString *promotionTitle;
@property (readonly, nonatomic, strong) NSString *moviePath;
@end
