//
//  DLWorkDetailViewController.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/11/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UnityAds/UnityAds.h>

@class DLWork;
@interface DLWorkDetailViewController : UIViewController <UnityAdsDelegate>

- (instancetype)initWithWorkData:(DLWork *)workData;

@end
