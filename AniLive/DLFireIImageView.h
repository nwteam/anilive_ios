//
//  DLFireIImageView.h
//  AniLive
//
//  Created by isaoeka on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DLFireIImageView : UIView

- (instancetype)initWithFireCount:(NSInteger)count;

- (void)setFireCount:(NSInteger)count;

@end
