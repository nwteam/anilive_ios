//
//  DLTimeScheduleViewController.m
//  AniLive
//
//  Created by Loyal Lauzier on 12/21/16.
//  Copyright © 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLTimeScheduleViewController.h"
#import "DLScheduleSectionHeader.h"
#import <UIImageView+AFNetworking.h>
#import "DLTransitionManager.h"
#import "DLAPIHelper.h"
#import <AFNetworking/AFNetworking.h>
#import "MBProgressHUD.h"
#import "UIView+Toast.h"
#import "DLLive.h"
#import "UIImageView+Category.h"


@interface DLTimeScheduleViewController () {
    BOOL isReserved;
    NSString *title;
    NSString *broadcastOpen;
    NSString *broadcastDate;
    NSString *mediumImage;
    NSMutableArray *arrayOut;
    
}

@end

@implementation DLTimeScheduleViewController
@synthesize table;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self initMainView];
    
    //タイムスケジュール取得
    @weakify(self)
    [DLAPIHelper fetchTimeSchedules: ^(NSMutableArray *timeSchedulesData) {
        @strongify(self)
        if (timeSchedulesData != nil) {
            [self makeTimeScheduleArray:timeSchedulesData];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initMainView {
    //    table.allowsSelection = NO;
    arrayOut = [[NSMutableArray alloc] init];
    //    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}





#pragma marker - make array
- (void)makeTimeScheduleArray:(NSMutableArray *)arrayInput {
    //    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSMutableArray *listOut = [[NSMutableArray alloc] init];
    NSString *date_i = @"";
    NSString *date_o = @"";
    BOOL flag = false;
    for (int i = 0; i < [arrayInput count]; i ++) {
        if ([arrayInput[i] isKindOfClass:[DLLive class]]) {
            DLLive *tmpDLLive = arrayInput[i];
            NSDate *tmpBroadcastOpen = tmpDLLive.broadcastOpen;
            if (tmpBroadcastOpen) {
                date_i = [self getBroadcastOpenDate:tmpDLLive.broadcastOpen];
                if ([arrayOut count] == 0) {
                    NSMutableDictionary *dicOut = [[NSMutableDictionary alloc] init];
                    
                    [dicOut setObject:date_i forKey:@"date"];
                    [dicOut setObject:listOut forKey:@"list"];
                    
                    [listOut addObject:tmpDLLive];
                    [arrayOut addObject:dicOut];
                }
                else {
                    flag = false;
                    for (int j = 0; j < [arrayOut count]; j ++) {
                        date_o = [arrayOut[j] valueForKey:@"date"];
                        if ([date_i isEqualToString:date_o]) {
                            [[arrayOut[j] valueForKey:@"list"] addObject:arrayInput[i]];
                            flag = true;
                            break;
                        }
                    }
                    if (!flag) {
                        NSMutableDictionary *dicOut1 = [[NSMutableDictionary alloc] init];
                        NSMutableArray *listOut1 = [[NSMutableArray alloc] init];
                        [dicOut1 removeAllObjects];
                        [dicOut1 setObject:date_i forKey:@"date"];
                        [dicOut1 setObject:listOut1 forKey:@"list"];
                        [listOut1 addObject:arrayInput[i]];
                        [arrayOut addObject:dicOut1];
                    }
                }
            }
        }
    }
    
    if ([arrayOut count] > 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [table reloadData];
        });
    }
}

#pragma marker - get TimeSchedules

#pragma marker - get Moviews

- (NSString *)formatHeaderTitle:(NSString *)headerTitle {
    NSString *result;
    
    // get tommorrow
    int daysToAdd = 1;
    NSDateComponents *commpnents = [[NSDateComponents alloc] init];
    [commpnents setDay:daysToAdd];
    
    NSCalendar *greorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *nextDay = [greorian dateByAddingComponents:commpnents toDate:[NSDate date] options:0];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd"];
    NSString *strNow = [formatter stringFromDate:[NSDate date]];
    NSString *strNext = [formatter stringFromDate:nextDay];
    
    if ([headerTitle isEqualToString:strNow]) {                            // is today ?
        result = [NSString stringWithFormat:@"Today (%@)", headerTitle];
    } else if ([headerTitle isEqualToString:strNext]) {                     // is tommorrow ?
        result = [NSString stringWithFormat:@"Tommorrow (%@)", headerTitle];
    } else result = headerTitle;
    
    return result;
}

#pragma mark Section
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return [DLScheduleSectionHeader getSectionHeaderHeight];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    DLScheduleSectionHeader *sectionHeader = [DLScheduleSectionHeader new];
    NSString *strTitle = arrayOut[section][@"date"];
    [sectionHeader setTitle:[self formatHeaderTitle:strTitle]];
    sectionHeader.backgroundColor = [UIColor colorWithHex:@"151515"];
    
    return sectionHeader;
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [arrayOut count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrayOut[section][@"list"] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0f;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DLScheduleCell * cell = [tableView dequeueReusableCellWithIdentifier:@"DLScheduleCell"];
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:@"DLScheduleCell" bundle:nil] forCellReuseIdentifier:@"DLScheduleCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"DLScheduleCell"];
    }
    
    //************************
    cell.delegate = self;
    cell.itemIdx = indexPath.row;
    cell.itemPosition = indexPath;
    //************************
    
    [self setUpWhithSectionCell:cell cellData:arrayOut[indexPath.section][@"list"] atIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)setUpWhithSectionCell:(DLScheduleCell *)cell cellData:(NSMutableArray *)dicData atIndexPath:(NSIndexPath *)indexPath {
    if (dicData[indexPath.row] != nil && dicData.count > indexPath.row) {
        DLLive *dic = dicData[indexPath.row];
        if (dic) {
            // get
            mediumImage = dic.image;
            title = dic.title;
            isReserved = dic.isReserved;
            broadcastOpen = [self getStartTime:dic.broadcastOpen];
            
            // set
            cell.lblTitle.text = title;
            cell.lblStartTime.text = broadcastOpen;
            
            //現在時刻とLIVE開始時刻を比較
            //現在時刻が開始時刻を過ぎていた場合、通知する必要がないのでベルを非表示にする
            NSDate *nowDateTime = [NSDate date];
            NSComparisonResult dateTimeCompareResult = [nowDateTime compare:dic.broadcastOpen];
            
            switch (dateTimeCompareResult) {
                case NSOrderedSame:
                    // 同一時刻
                    // 通知ベル表示
                    [cell.btnNoti setHidden:YES];
                    break;
                case NSOrderedAscending:
                    // nowよりotherDateのほうが未来
                    // 通知ベル表示
                    [cell.btnNoti setHidden:NO];
                    break;
                case NSOrderedDescending:
                    // nowよりotherDateのほうが過去
                    // 通知ベル非表示
                    [cell.btnNoti setHidden:YES];
                    break;
            }
            
            if (isReserved) {
                [cell.btnNoti setBackgroundImage:[UIImage imageNamed:DLConstIconNotificationsOn] forState:UIControlStateNormal];
            } else {
                [cell.btnNoti setBackgroundImage:[UIImage imageNamed:DLConstIconNotificationsOff] forState:UIControlStateNormal];
            }
            [cell.imageThumb setImage:[UIImage imageNamed:mediumImage]];
            
            if (NOT_NULL(mediumImage)) {
                [UIImageView loadImageWithURLString:mediumImage
                                    targetImageView:cell.imageThumb];
            }
            
            //    if ([isOnAir boolValue]) {
            //        cell.imageMark.image = [UIImage imageNamed:@"label_live"];
            //        [cell.btnNoti setEnabled:NO];
            //        [cell.btnNoti setHidden:YES];
            //    }
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
//    NSDictionary *selectedDic = arrayOut[indexPath.section][@"list"][indexPath.row];
    DLLive *selectedDic = arrayOut[indexPath.section][@"list"][indexPath.row];
    if (selectedDic) {
        NSString *liveID = [selectedDic.liveId stringValue];
//        NSString *liveID = selectedDic[@"live_id"];
        NSInteger liveState = [self getLiveOpenState:selectedDic];
        //    NSString *highlightMovieUrl = selectedDic[@"highlight_movie_url"];

        // Modify the following code for your need.
        NSString *message;

        switch (liveState) {
            case 1:         // alert
                message = @"This live stream is not\n available right now.";
                break;
            case 2:         // adv
                message = @"Play Advertising";
                break;
            case 3:         // play
                message = @"Play Movie";
                break;
                
            default:
                break;
        }
        [[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"%@\nlive_id:%@", message, liveID]
                                  message:nil
                                 delegate:self
                        cancelButtonTitle:nil
                        otherButtonTitles:@"OK", nil] show];
    }
}

- (NSInteger)getLiveOpenState:(DLLive *)live {
//    NSString *strOpen = [live valueForKey:@"broadcast_open"];
//    NSString *strClose = [live valueForKey:@"broadcast_close"];
    NSDate *openDate = live.broadcastOpen;
    NSDate *closeDate = live.broadcastClose;
    
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//    cal.timeZone = [NSTimeZone timeZoneWithName:@"Asia/Tokyo"];
//    formatter = NSDateFormatter.new;
//    formatter.calendar = cal;
//    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
//    NSDate *openDate = [formatter dateFromString:strOpen];
//    NSDate *closeDate = [formatter dateFromString:strClose];
    NSDate *nowDate = [NSDate date];
    
    NSTimeInterval deltaMinOpen = [openDate timeIntervalSinceDate:nowDate] / 60;
    NSTimeInterval deltaMinClose = [closeDate timeIntervalSinceDate:nowDate] / 60;
    
    if (deltaMinOpen > 5.0f) {
        return 1;       // alert
    } else if (deltaMinOpen > 0.0f && deltaMinOpen <= 5.0f ) {
        return 2;       // adver
    } else if (deltaMinOpen < 0.0f && deltaMinClose > 0.0f) {
        return 3;       // play
    }
    return 0;
}

#pragma marker - get LiveState
- (BOOL)getLiveState:(NSInteger)itemID {
    //    return [[array[itemID] valueForKey:@"isOnAir"] boolValue];
    return true;
}

#pragma marker - yyyy-MM-dd hh:mm:ss to hh:mm
- (NSString *)getStartTime:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    
    NSString *strTime = [formatter stringFromDate:date];
    
    if (!strTime) {
        NSArray *stringArray = [[NSArray alloc] init];
        stringArray = [[NSString stringWithFormat:@"%@", date] componentsSeparatedByString:@" "];
        NSArray *arrayTime = [stringArray[1] componentsSeparatedByString:@":"];
        strTime = [NSString stringWithFormat:@"%@:%@", arrayTime[0], arrayTime[1]];
    }
    strTime = [NSString stringWithFormat:@"%@~", strTime];
    
    return strTime;
}

#pragma marker - yyyy-MM-dd hh:mm:ss to MM/dd
- (NSString *)getBroadcastOpenDate:(NSDate *)date {
    // get date string
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd"];
    [formatter setTimeZone:[NSTimeZone defaultTimeZone]];
    NSString *strDate = [formatter stringFromDate:date];
    if (!strDate) {
        NSArray *stringArray = [[NSArray alloc] init];
        stringArray = [[NSString stringWithFormat:@"%@", date] componentsSeparatedByString:@" "];
        NSArray *arrayDate = [stringArray[0] componentsSeparatedByString:@"-"];
        strDate = [NSString stringWithFormat:@"%@/%@", arrayDate[1], arrayDate[2]];
    }
    
    return strDate;
}


#pragma marker - DLScheduleCell Delegate
- (void)didTapSelectButtonItemID:(NSInteger)itemID IsNoti:(BOOL)isNoti {
    //    if ([self getLiveState:itemID]) {
    //        [DLTransitionManager showMoviePlayViewControllerWithLiveData:arrayOut[itemID] withTarget:nil];
    //    }
    //    else if (isNoti) {
    ////        [DLTransitionManager showWorkDetailViewControllerWithWorkData:arrayOut[itemID] withTarget:nil];
    //    }
}

/*
 通知ボタン（ベル）を押した際に呼ばれる
 一度押したら取り消しはできない
 */
- (void)didTapNotiButtonItemID:(NSIndexPath *)itemPosition {
    if (arrayOut != nil && arrayOut.count > itemPosition.section) {
        NSArray *tmpLists = arrayOut[itemPosition.section][@"list"];
        if (tmpLists != nil && tmpLists.count > itemPosition.row) {
            DLLive *selectedDic = arrayOut[itemPosition.section][@"list"][itemPosition.row];
            if (selectedDic) {
                NSString *tmpliveID = [selectedDic.liveId stringValue];
                NSDate *tmpBroadcast_open = selectedDic.broadcastOpen;
                if (tmpliveID != nil && tmpBroadcast_open != nil) {
                    //通知ボタンを押したので、既存のArrayのis_reservedフラグに「1」(YES)をセットする。
                    [selectedDic setValue:@"1" forKey:@"isReserved"];
                    
                    //また、サーバ側のis_reservedもセットするためのAPI呼び出し。
                    @weakify(self)
                    [DLAPIHelper postLiveReserve:[tmpliveID intValue] callback:^(NSObject *live, NSError *error) {
                        @strongify(self)
                        if (error) {
                            NSLog(@"%@", error);
                        } else {
                            if ([live isKindOfClass:[DLLive class]]) {
                                DLLive *tmpDLLive = (DLLive *)live;
                                //LocalPush設定
                                [self pushLocalNotification:tmpDLLive];
                                //通知完了のToastを表示。
                                [WINDOW makeToast:@"Your show has been successfully reserved! You will receive a notification before the show starts." duration:3.0f position:CSToastPositionCenter];
                            }
                        }
                    }];
                }
            }
        }
    }
}

- (void)didTapButtonItemID:(NSInteger)itemID {
    NSLog(@"itenID: >>>>>>>>>> %ld", itemID);
    
    //    NSSet *visibleSections = [NSSet setWithArray:[[table indexPathsForVisibleRows] valueForKey:@"section"]];
    //    NSLog(@"sectionNumber: %@", visibleSections);
}

- (IBAction)tapBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma marker - Set Local Notification
/*
 LocalNotificaiton登録
 */
- (void)pushLocalNotification:(DLLive *)notificationData {
    //同じLiveIDの通知が設定されている場合、削除してから再登録。
    if (notificationData != nil) {
        NSString *tmpLiveID = [notificationData.liveId stringValue];
        if (tmpLiveID != nil) {
            for (UILocalNotification *notify in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
                NSDictionary *tmpUserInfo = notify.userInfo;
                if (tmpUserInfo != nil) {
                    NSString *tmpLiveId = [tmpUserInfo objectForKey:@"liveID"];
                    if (tmpLiveId != nil) {
                        if([tmpLiveId isEqualToString:tmpLiveID]){
                            [[UIApplication sharedApplication] cancelLocalNotification:notify];
                        }
                    }
                }
            }
            
            //ローカル通知を設定。
            //ローカル通知のFireDate設定（５分前）
            NSDate *tmpDate = notificationData.broadcastOpen;
            if (tmpDate == nil) {
                return;
            }
            NSDate *tmpFireDate = [tmpDate initWithTimeInterval:-(5 * 60) sinceDate:tmpDate];
            
            //ローカル通知のタイトル設定
            NSString *tmpNotificationTitle = notificationData.title;
            if (tmpNotificationTitle == nil) {
                return;
            }
            
            UILocalNotification *notification = [[UILocalNotification alloc] init];
            notification.fireDate = tmpFireDate;
            notification.timeZone = [NSTimeZone defaultTimeZone];
            notification.alertBody = [NSString stringWithFormat:@"\"%@\" will start live streaming in 5 minutes.", tmpNotificationTitle];
            notification.soundName = UILocalNotificationDefaultSoundName;
            notification.userInfo =  [NSDictionary dictionaryWithObjectsAndKeys:
                                      tmpLiveID, @"liveID"
                                      ,nil];
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        }
    }
}

@end
