//
//  DLNotificationService.h
//  AniLive
//

#ifndef DLNotificationService_h
#define DLNotificationService_h

#import "DLNotice.h"

@interface DLNotificationService : NSObject

+ (void) sendNotification: (NSDictionary*)noticeDict;
+ (void) onLaunchFromNotification:(NSDictionary*)noticeDict;
+ (void) transitScreenByNotification: (DLNotice*)notice callback:(void (^)(void))callback;
+ (void)reflectPushSettingAsTagToServer;
@end


#endif /* DLNotificationService_h */
