//
//  DLForumRootViewController.m
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/10/17.
//  Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLForumRootViewController.h"
#import "DLAppFrameManager.h"
#import "DLSegmentView.h"
#import "DLCatalogPopularViewController.h"
#import "DLCatalogAlphabeticalViewController.h"
#import "DLForumListViewController.h"
#import "DLForumGroupListViewController.h"

const static NSInteger kSegmentStartRow = 0;
const static CGFloat kSegmentViewHeight = 44;

@interface DLForumRootViewController () <UIScrollViewDelegate, DLSegmentViewDelegate>

@property(nonatomic, strong) DLSegmentView *segmentView;
@property(nonatomic, strong) UIScrollView *baseScrollView;
@property(nonatomic, strong) NSArray *segmentTitleArray;

@end

@implementation DLForumRootViewController

#pragma mark - Override Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;

    self.segmentTitleArray = @[Localizer(@"Popular"), Localizer(@"Alpahabetical")];

    [self initView];
    [self updateFrame];

    // 初期表示タブに移動させておく
    [self.baseScrollView setContentOffset:CGPointMake(self.baseScrollView.width * kSegmentStartRow, 0)];
    [self setScrollsToTopWithCurrentRow:kSegmentStartRow];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    self.segmentView.delegate = nil;
    self.baseScrollView.delegate = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Private Methods

- (void)initView {

    self.view.backgroundColor = [UIColor colorWithHex:DLConstColorLightBlue];

#pragma mark Segment View

    self.segmentView = [[DLSegmentView alloc] initWithTitles:self.segmentTitleArray startRow:kSegmentStartRow];
    self.segmentView.delegate = self;
    [self.view addSubview:self.segmentView];

#pragma mark Base Scroll View

    self.baseScrollView = [[UIScrollView alloc] init];
    self.baseScrollView.backgroundColor = [UIColor clearColor];
    self.baseScrollView.scrollsToTop = NO;
    self.baseScrollView.pagingEnabled = YES;
    self.baseScrollView.alwaysBounceHorizontal = YES;
    self.baseScrollView.showsHorizontalScrollIndicator = NO;
    self.baseScrollView.delegate = self;
    [self.view addSubview:self.baseScrollView];

#pragma mark Hot View Controller

    DLForumGroupListViewController *hotViewController = [DLForumGroupListViewController new];
    hotViewController.sortType = DLForumGroupSortTypeLatest;
    [self addChildViewController:hotViewController];
    [self.baseScrollView addSubview:hotViewController.view];

#pragma mark Alphabetical View Controller

    DLForumGroupListViewController *alphabeticalViewController = [DLForumGroupListViewController new];
    alphabeticalViewController.sortType = DLForumGroupSortTypePopular;
    [self addChildViewController:alphabeticalViewController];
    [self.baseScrollView addSubview:alphabeticalViewController.view];
}

- (void)updateFrame {
    CGRect applicationFrame = [DLAppFrameManager sharedInstance].applicationFrame;
    float statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    self.segmentView.frame = CGRectMake(0, statusBarHeight, CGRectGetWidth(applicationFrame), kSegmentViewHeight);
    self.baseScrollView.frame = CGRectMake(0,
                                           self.segmentView.maxY,
                                           CGRectGetWidth(applicationFrame),
                                           CGRectGetHeight(applicationFrame) - self.segmentView.maxY - DLConstTabBarHeight);
    
    self.baseScrollView.contentSize = CGSizeMake(self.baseScrollView.width * self.segmentTitleArray.count, 0);
    self.baseScrollView.contentOffset = CGPointMake(self.baseScrollView.width * (self.segmentView.currentRow), self.baseScrollView.contentOffset.y);

    [self.childViewControllers enumerateObjectsUsingBlock:^(UIViewController *viewController, NSUInteger index, BOOL *stop) {
        viewController.view.frame = self.baseScrollView.bounds;
        viewController.view.x = self.baseScrollView.width * index;
    }];
}

- (void)setScrollsToTopWithCurrentRow:(NSInteger)currentRow {

    [self.childViewControllers enumerateObjectsUsingBlock:^(UIViewController *viewController, NSUInteger index, BOOL *stop) {

        if ([viewController conformsToProtocol:@protocol(DLSegmentViewControllerProtocol)]) {

            UIViewController <DLSegmentViewControllerProtocol> *segmentViewController = (UIViewController <DLSegmentViewControllerProtocol> *) viewController;

            [segmentViewController setScrollsToTop:index == currentRow];
        }
    }];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

    [self.segmentView setLineViewCenterWithOffset:scrollView.contentOffset];

    CGFloat offsetX = scrollView.contentOffset.x;

    NSInteger currentRow = (offsetX + scrollView.width / 2) / scrollView.width;
    if (currentRow < 0) {
        currentRow = 0;
    }
    if (currentRow > self.segmentView.numberOfButtons - 1) {
        currentRow = self.segmentView.numberOfButtons - 1;
    }

    [_segmentView setRow:currentRow isMoveLine:NO];

    [self setScrollsToTopWithCurrentRow:currentRow];
}

#pragma mark - DLSegmentViewDelegate

- (void)segmentView:(DLSegmentView *)segmentView didSelectSegmentAtRow:(NSInteger)row {

    [UIView animateWithDuration:0.3
                     animations:^{
                         [segmentView setRow:row isMoveLine:YES];

                         [self.baseScrollView setContentOffset:CGPointMake(self.baseScrollView.width * row, 0) animated:NO];

                     } completion:^(BOOL finished) {

                [self setScrollsToTopWithCurrentRow:row];
            }];
}

@end
