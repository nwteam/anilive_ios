//
// Created by akasetaichi on 2017/01/20.
// Copyright (c) 2017 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DLModelBase.h"


@interface DLAuthcode : DLModelBase
@property(readonly, nonatomic, strong) NSNumber *code;
@property(readonly, nonatomic, strong) NSNumber *userId;
@end