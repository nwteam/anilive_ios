//
//  DLPlayerContainerViewController.m
//  AniLive
//
//  Created by isaoeka on 2016/12/25.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPlayerContainerViewController.h"
#import "DLAppFrameManager.h"
#import "DLTransitionManager.h"
#import "DLAPIHelper.h"

@interface DLPlayerContainerViewController ()

@property (nonatomic, assign) BOOL isInTransitionAnimation;

@end

@implementation DLPlayerContainerViewController

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController {
    self = [super init];
    if (self) {
        self.isInTransitionAnimation = NO;
        
        [self initView];
        [self addRootViewController:rootViewController];
    }
    return self;
}

#pragma mark - Override Methods

- (void)viewDidLoad {
    [super viewDidLoad];

    [self updateFrame];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Private Methods

- (void)addRootViewController:(UIViewController *)rootViewController {
    [self addChildViewController:rootViewController];
    
    rootViewController.view.frame = self.view.bounds;
    [self.view addSubview:rootViewController.view];
    
    [rootViewController didMoveToParentViewController:self];
}

- (void)initView {
    self.view.backgroundColor = [UIColor yellowColor];
    
}

- (void)updateFrame {
    
}

#pragma mark - Public Methods

- (void)pushViewController:(UIViewController *)toViewController animated:(BOOL)animated {
    
    if (self.isInTransitionAnimation) {
        return;
    }
    
    NSInteger childVCCount = [self.childViewControllers count];
    if (childVCCount == 0) {
        // 最低でもrootとなるViewControllerは入る仕様とする
        return;
    }
    
    CGRect startRect = CGRectMake(self.view.width, 0, self.view.width, self.view.height);
    CGRect endRect   = CGRectMake(0              , 0, self.view.width, self.view.height);
    toViewController.view.frame = startRect;
    
    [self addChildViewController:toViewController];
    [self.view addSubview:toViewController.view];
    
    if (animated) {
        self.isInTransitionAnimation = YES;
        [UIView animateWithDuration:.2f
                         animations:^{
                             toViewController.view.frame = endRect;
                         }
                         completion:^(BOOL finished){
                             self.isInTransitionAnimation = NO;
                         }
         ];
    } else {
        toViewController.view.frame = endRect;
    }
    
    [toViewController didMoveToParentViewController:self];
}

- (void)popViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    if (self.isInTransitionAnimation) {
        return;
    }
    
    CGRect startRect = CGRectMake(0              , 0, self.view.width, self.view.height);
    CGRect endRect   = CGRectMake(self.view.width, 0, self.view.width, self.view.height);
    viewController.view.frame = startRect;
    [viewController willMoveToParentViewController:nil];
    
    if (animated) {
        self.isInTransitionAnimation = YES;
        [UIView animateWithDuration:.2f
                         animations:^{
                             viewController.view.frame = endRect;
                         }
                         completion:^(BOOL finished){
                             self.isInTransitionAnimation = NO;
                             
                             [viewController.view removeFromSuperview];
                             [viewController removeFromParentViewController];
                         }
        ];
    } else {
        viewController.view.frame = endRect;
        
        [viewController.view removeFromSuperview];
        [viewController removeFromParentViewController];
    }
    
    [viewController didMoveToParentViewController:self];
}

@end
