//
//  DLDeviceUtil.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/12/28.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DLDeviceUtil : NSObject

// モデル名を取得する
+ (NSString*) getDeviceName;

// iOSバージョンを取得する
+ (NSString*) getDeviceSystemVersion;

// bundle identifier を取得する
+ (NSString *)getBundleIdentifier;

// アプリのバージョンを取得する
+ (NSString *)getAppVersion;

// 言語設定を取得する
+ (NSString*)getCurrentLanguage;

// locale identifier を取得する
+ (NSString *)getCurrentLocaleIdentifier;

// UUID を取得する
+ (NSString *)getUUID;

// UIID を取得する
+ (NSString *)getUIID;

@end
