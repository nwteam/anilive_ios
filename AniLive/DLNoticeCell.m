//
//  DLNoticeCell.m
//  AniLive
//
//  Created by volcano on 12/22/16.
//  Copyright © 2016 AnimeConsortiumJapan. All rights reserved.
//

#import <UIImageView+AFNetworking.h>
#import "UIImageView+Category.h"
#import "Macro.h"

#import "DLNoticeCell.h"
#import "NSDate+Category.h"

const static CGFloat kNoticeCellHeight = 90.0f;

@interface DLNoticeCell()
@property (weak, nonatomic) IBOutlet UIImageView *asyncImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleTextView;
@property (weak, nonatomic) IBOutlet UILabel *timeTextView;
@property (weak, nonatomic) IBOutlet UIImageView *badgeImageView;


@end

@implementation DLNoticeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)setNotice:(DLNotice*)notice{
    _notice = notice;
    if (!notice){ return; }
    [_titleTextView setText:_notice.title];
    [_timeTextView setText: [notice.createdAt genPastTimeString]];
    if (NOT_NULL(notice.thumbImage)) {
        [UIImageView loadImageWithURLString:notice.thumbImage
                            targetImageView:self.asyncImageView];
    } else {
        [self.asyncImageView setImage:[UIImage imageNamed: @"thumb_placeholder.png"]];
    }
    
    //バッジ
    [self setRead:notice.isRead];
}

-(void)setRead:(BOOL)isRead{
    _isRead = isRead;
    
    if (isRead){
        [_badgeImageView setHidden:YES];
    }
    else{;
        [_badgeImageView setHidden:NO];
    }
}

+ (UINib *)getDefaultNib {
    return [UINib nibWithNibName:@"DLNoticeCell" bundle:nil];
}

+ (CGFloat)getCellHeight {
    return kNoticeCellHeight;
}
@end
