//
//  DLPreloadMovieManager.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/10/05.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DLPreloadMovieManager : NSObject

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView;

- (void)setPreloadCells;
- (void)removeAllPreloadCells;

@end
