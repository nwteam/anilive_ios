//
//  DLForumTitleView.m
//  AniLive
//
//  Created by isaoeka on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLForumTitleView.h"
#import "DLTimeLabelView.h"
#import "DLFireIImageView.h"
#import "DLSeeLaterButton.h"
#import "DLForum.h"

@interface DLForumTitleView () <DLSeeLaterButtonDelegate>

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) DLTimeLabelView *timeLabelView;
@property (nonatomic, strong) UILabel *commentsLabel;
@property (nonatomic, strong) DLFireIImageView *fireImageView;
@property (nonatomic, strong) DLSeeLaterButton *seeLaterButton;

@end

@implementation DLForumTitleView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
}

#pragma mark - Private Methods

- (void)initView {
    self.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkGray];
    
#pragma mark Title Label
    self.titleLabel = [self createTitleLabelWithFont:BASE_FONT_BOLD(14)];
    self.titleLabel.numberOfLines = 3;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    self.titleLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.titleLabel];
    
#pragma mark Time Label View
    self.timeLabelView = [DLTimeLabelView new];
    [self addSubview:self.timeLabelView];
    
#pragma mark Commetnts Label
    self.commentsLabel = [self createTitleLabelWithFont:BASE_FONT(10)];
    [self addSubview:self.commentsLabel];
    
#pragma mark Fire Image Label
    self.fireImageView = [[DLFireIImageView alloc] initWithFireCount:0];
    [self addSubview:self.fireImageView];
    
#pragma mark Star Btn
    self.seeLaterButton = [DLSeeLaterButton new];
    self.seeLaterButton.delegate = self;
    [self addSubview:self.seeLaterButton];
    
}

- (void)updateFrame {
    CGFloat thumbHeight = 10;
    CGFloat thumbMarginWidth = 2;
    CGFloat thumbMinY = self.height - thumbHeight - thumbMarginWidth - DLConstDefaultMargin;
    
#pragma mark Star Btn
    CGFloat starBtnSize = 20;
    self.seeLaterButton.frame = CGRectMake(self.width - DLConstDefaultMargin - starBtnSize,
                                           DLConstDefaultMargin,
                                           starBtnSize,
                                           starBtnSize);
    
#pragma mark Title Label
    // TODO: おそらくテキスト量によってこのView自体のHeightを決める実装になりそう
    [self.titleLabel sizeToFit];
    self.titleLabel.frame = CGRectMake(DLConstDefaultMargin,
                                       DLConstDefaultMargin,
                                       self.seeLaterButton.minX - DLConstDefaultMargin,
                                       self.titleLabel.height);
    
#pragma mark Time Label View
    [self.timeLabelView sizeToFit];
    CGSize timeLabelSize = [self.timeLabelView getTimeLabelSize];
    self.timeLabelView.frame = CGRectMake(DLConstDefaultMargin,
                                          thumbMinY,
                                          timeLabelSize.width + (2 * 2),
                                          timeLabelSize.height + (1 * 2));
    
#pragma mark Commetnts Label
    self.commentsLabel.frame = CGRectMake(self.timeLabelView.maxX + thumbMarginWidth,
                                          thumbMinY,
                                          100,
                                          self.timeLabelView.height);
    
#pragma mark Fire Image View
    self.fireImageView.frame = CGRectMake(self.commentsLabel.maxX + thumbMarginWidth,
                                          thumbMinY,
                                          50,
                                          self.timeLabelView.height);
    
}

- (UILabel *)createTitleLabelWithFont:(UIFont *)font {
    UILabel *label = [UILabel new];
    label.backgroundColor = [UIColor clearColor];
    label.font = font;
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.lineBreakMode = NSLineBreakByTruncatingTail;
    return label;
}

#pragma mark - Public Methods

- (void)setForum:(DLForum *)forum {
    
    self.titleLabel.text = forum.title;
    [self.timeLabelView setTimeStr:forum.forumPinnedTime];
    
    self.commentsLabel.text = [[NSString alloc] initWithFormat:@"%@ comments", forum.totalComments];
    [self.fireImageView setFireCount:[forum.popularity intValue]];
    
    [self updateFrame];
}

- (DLSeeLaterButton *)getSeeLaterButton {
    return self.seeLaterButton;
}

#pragma mark - See Later Btn Delegate

- (BOOL)seeLaterButtonDidTap:(DLSeeLaterButton *)button {
    
    if ([self.delegate respondsToSelector:@selector(forumTitleViewStarButtonDidTap)]) {
        BOOL highlighted = [self.delegate forumTitleViewStarButtonDidTap];
        return highlighted;
    }
    
    return NO;
}


@end
