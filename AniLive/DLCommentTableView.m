//
//  DLCommentTableView.m
//  AniLive
//
//  Created by isaoeka on 2016/12/30.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLCommentTableView.h"
#import "DLCommentCell.h"
#import "DLComment.h"

#import "DLAppFrameManager.h"

@interface DLCommentTableView () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray<DLComment *> *comments;

@end

@implementation DLCommentTableView

#pragma mark - Initializer

- (instancetype)init {
    self = [super init];
    if (self) {
        self.comments = [NSMutableArray<DLComment *> array];
        [self initView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

#pragma mark - Dealloc

- (void)dealloc {
}

#pragma mark - Private Methods

- (void)initView {
    self.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkGray];
    self.delegate   = self;
    self.dataSource = self;
    
    [self registerNib:[DLCommentCell getDefaultNib] forCellReuseIdentifier:@"comment-cell"];
}

- (void)updateFrame {
    
}

#pragma mark - Public Methods

- (void)loadComments:(NSMutableArray<DLComment *> *)comments {
    self.comments = comments;

    [self reloadData];
}

#pragma mark - Table View Delegate

#pragma mark Section

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

#pragma mark Cell

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [DLCommentCell getCellHeight];
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.comments count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DLCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"comment-cell"
                                                          forIndexPath:indexPath];
    [cell setComment:self.comments[indexPath.row]];
    
    return cell;
}

@end
