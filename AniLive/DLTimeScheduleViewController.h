//
//  DLTimeScheduleViewController.h
//  AniLive
//
//  Created by Loyal Lauzier on 12/21/16.
//  Copyright © 2016 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLScheduleCell.h"

@interface DLTimeScheduleViewController : UIViewController <
UITableViewDelegate,
UITableViewDataSource,
DLScheduleCellDelegate
>

@property (weak, nonatomic) IBOutlet UITableView *table;
- (IBAction)tapBtnBack:(id)sender;
@end
