/*
 *  XmlDic.h
 *  MinnaTomodachiLibrary
 *
 *  Created by ARAI Shigenari on 12/11/04.
 *  Copyright 2012 MinnaTomodachi Co., Ltd. All rights reserved.
 *
 *  The MIT License (MIT)
 */

#import <Foundation/Foundation.h>


@interface XmlDic : NSObject <NSXMLParserDelegate>
{
}


@property( nonatomic, retain ) NSString*		name;		//  名前 (name)
@property( nonatomic, retain ) NSMutableString*	value;		//  値 (value)
@property( nonatomic, assign ) XmlDic*			parent;		//  上位階層 (parent item)
@property( nonatomic, retain ) NSMutableArray*	items;		//  子要素 (child item)
@property( nonatomic, retain ) NSDictionary*	attributes;	//  属性 (attributes)


//  XML を取得してパースする
//  Get a XML from the uri, and parse it.
- (BOOL)parseWithURI:(NSString*)uri;

//  XML を指定してパースする
//  Parse the XML data.
- (void)parseWithData:(NSData*)data;

//  名前の要素を全て取得する
//  Get all items by the name.  
- (NSMutableArray*)find:(NSString*)name;

//  名前の要素を全て取得する
//  Get all items by the name.  
- (NSMutableArray*)find:(NSString*)name items:(NSMutableArray*)items;

//  名前の要素を最初に見付けた1つ取得する
//  Get first item only by the name.
- (NSString*)first:(NSString*)name;

//  名前の要素を最初に見付けた1つ取得する
//  Get first item only by the name.
- (XmlDic*)firstElement:(NSString*)name;

//  名前と値が同じ要素を最初に見付けた1つ取得する
//  Get first item only by the name, the tag and value.
- (XmlDic*)first:(NSString*)name tag:(NSString*)tag value:(NSString*)value;

//	属性の値が一致する？
//	Get first item only by the same attribute.
- (XmlDic*)first:(NSString*)name attribute:(NSString*)attribute value:(NSString*)value;


@end
