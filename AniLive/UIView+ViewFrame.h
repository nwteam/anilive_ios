//
//  UIView+ViewFrame.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ViewFrame)

- (CGPoint)origin;
- (CGFloat)x;
- (CGFloat)y;

- (CGFloat)minX;
- (CGFloat)midX;
- (CGFloat)maxX;
- (CGFloat)minY;
- (CGFloat)midY;
- (CGFloat)maxY;

- (CGSize)size;
- (CGFloat)width;
- (CGFloat)height;

- (void)setOrigin:(CGPoint)p;
- (void)setX:(CGFloat)x;
- (void)setY:(CGFloat)y;

- (void)setMidX:(CGFloat)x;
- (void)setMidY:(CGFloat)y;

- (void)setSize:(CGSize)s;
- (void)setWidth:(CGFloat)w;
- (void)setHeight:(CGFloat)h;


@end
