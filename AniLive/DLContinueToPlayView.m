//
//  DLContinueToPlayView.m
//  AniLive
//
//  Created by volcano on 1/4/17.
//  Copyright © 2017 AnimeConsortiumJapan. All rights reserved.
//

#import "DLContinueToPlayView.h"
#import "DLContinueToPlaySeeLaterCell.h"
#import "UIImageView+Category.h"
#import "DLUserDataManager.h"
#import "DLTransitionManager.h"
#import "DLSeelaterManager.h"
#import "DLOpenPlayMovieService.h"
#import "DLPoint.h"

static const CGFloat kDLContinueToPlayView_NextImageWidth = 225.0f;
static const CGFloat kDLContinueToPlayView_NextImageHeight = 128.0f;

@interface DLContinueToPlayView () <UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, strong) UIView *mainView;
@property(nonatomic, strong) UIButton *closeButton;
@property(nonatomic, strong) UIImageView *nextShowImage;
@property(nonatomic, strong) UIButton *nextShowButton;
@property(nonatomic, strong) UILabel *nextShowDescText;
@property(nonatomic, strong) UIButton *tenPointButton;
@property(nonatomic, strong) UIButton *watchAdButton;
@property(nonatomic, strong) UILabel *watchAdDescText;
@property(nonatomic, strong) UIView *lineView;
@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) UIButton *showNextViewButton;

@property(nonatomic, strong) NSMutableAttributedString *watchAdButtonAttrStr;
@property(nonatomic, strong) NSMutableAttributedString *watchAdDescAttrStr;
@property(nonatomic, strong) DLMovie *nextMovie;
@property(nonatomic, assign) BOOL shouldHideAdView;
@property(nonatomic, assign) BOOL shouldHideSeelaterList;
@property(nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation DLContinueToPlayView
- (instancetype)initWithParentViewController:(UIViewController *)parentViewController {
    self = [super init];
    if (self) {
        [self initView];

        self.parentVC = parentViewController;

        [self loadSeeLaterData];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
}

#pragma mark - Private Methods

- (void)initView {
    _shouldHideAdView = [[DLUserDataManager sharedInstance].user.billingType integerValue] == 2 ? YES : NO;
    self.dataArray = [[NSMutableArray alloc] init];
    self.delegate = nil;

    self.mainView = [[UIView alloc] initWithFrame:CGRectZero];

    [self addSubview:self.mainView];

#pragma mark Close Button

    self.closeButton = [[UIButton alloc] initWithFrame:CGRectZero];

    [self addSubview:self.closeButton];

    [self.closeButton addTarget:self action:@selector(onBtnClose) forControlEvents:UIControlEventTouchUpInside];

#pragma mark Next Show Image

    self.nextShowImage = [[UIImageView alloc] initWithFrame:CGRectZero];

    [self addSubview:self.nextShowImage];

#pragma mark Next Show Button

    self.nextShowButton = [[UIButton alloc] initWithFrame:CGRectZero];

    [self addSubview:self.nextShowButton];

    [self.nextShowButton addTarget:self action:@selector(onBtnNextShow) forControlEvents:UIControlEventTouchUpInside];

#pragma mark Next Show Description

    self.nextShowDescText = [[UILabel alloc] initWithFrame:CGRectZero];

    [self addSubview:self.nextShowDescText];

    self.showNextViewButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.showNextViewButton addTarget:self action:@selector(onBtnNextShow) forControlEvents:UIControlEventTouchUpInside];
    self.showNextViewButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.showNextViewButton];


    if (!_shouldHideAdView) {

#pragma mark Ten Point Button

        self.tenPointButton = [[UIButton alloc] initWithFrame:CGRectZero];

        [self addSubview:self.tenPointButton];

        [self.tenPointButton addTarget:self action:@selector(onBtnTenPoint) forControlEvents:UIControlEventTouchUpInside];

#pragma mark Watch Ads Button

        self.watchAdButton = [[UIButton alloc] initWithFrame:CGRectZero];

        self.watchAdButtonAttrStr = [[NSMutableAttributedString alloc] initWithString:@"Watch AD Now"];

        [self.watchAdButtonAttrStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHex:@"ffc900"] range:NSMakeRange(0, 12)];

        UIFont *font_hel = [UIFont fontWithName:@"HelveticaNeue-Italic" size:18.0f];

        [self.watchAdButtonAttrStr addAttribute:NSFontAttributeName value:font_hel range:NSMakeRange(0, 12)];

        [self.watchAdButton setAttributedTitle:self.watchAdButtonAttrStr forState:UIControlStateNormal];

        [self addSubview:self.watchAdButton];

        [self.watchAdButton addTarget:self action:@selector(onBtnWatchAds) forControlEvents:UIControlEventTouchUpInside];
        [UnityAds setDelegate:self];
#pragma mark Next Show Description Text

        self.watchAdDescText = [[UILabel alloc] initWithFrame:CGRectZero];

        self.watchAdDescText.numberOfLines = 3;

        [self addSubview:self.watchAdDescText];
    }

#pragma mark Line View

    self.lineView = [[UIView alloc] initWithFrame:CGRectZero];

    [self addSubview:self.lineView];

#pragma mark Table View

    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero];

    self.tableView.delegate = self;

    self.tableView.dataSource = self;

    UILabel *headerTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 50.0f)];

    headerTitle.text = @"See Later List";

    headerTitle.textAlignment = NSTextAlignmentCenter;

    headerTitle.textColor = [UIColor whiteColor];

    self.tableView.tableHeaderView = headerTitle;

    [self.tableView setShowsVerticalScrollIndicator:NO];

    [self.tableView registerNib:[DLContinueToPlaySeeLaterCell getDefaultNib] forCellReuseIdentifier:@"dlcontinuetoplayseelater-cell"];

    [self addSubview:self.tableView];
}

- (void)updateFrame {
#pragma mark Background View

    self.mainView.frame = self.frame;

    self.mainView.backgroundColor = [UIColor blackColor];

    self.mainView.alpha = 0.9;

#pragma mark Close Button

    self.closeButton.frame = CGRectMake(10, 15, 44, 42);

    [self.closeButton setImage:[UIImage imageNamed:@"ic-close"] forState:UIControlStateNormal];

#pragma mark Next Show Image

    self.nextShowImage.frame = CGRectMake((self.frame.size.width - kDLContinueToPlayView_NextImageWidth) / 2,
            84,
            kDLContinueToPlayView_NextImageWidth,
            kDLContinueToPlayView_NextImageHeight);


    self.nextShowImage.backgroundColor = [UIColor lightGrayColor];

#pragma mark Next Show Button

    self.nextShowButton.frame = CGRectMake(self.nextShowImage.frame.origin.x,
            48,
            self.nextShowImage.frame.size.width,
            22);

    [self.nextShowButton setTitleColor:[UIColor colorWithHex:@"ffc900"] forState:UIControlStateNormal];

    [self.nextShowButton setImage:[UIImage imageNamed:@"ic-tab-live-off"] forState:UIControlStateNormal];

    [self.nextShowButton setTitle:@"Next Show" forState:UIControlStateNormal];

    [self.nextShowButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];

    self.nextShowButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

#pragma mark Next Show Description Text

    self.nextShowDescText.frame = CGRectMake(self.nextShowImage.frame.origin.x,
            220,
            self.nextShowImage.size.width,
            45);

    [self.nextShowDescText setTextColor:[UIColor whiteColor]];

    [self setNextShowDesciption:@"Lovelive! Sunshine #02 May Our Dream Come True!"];

    self.nextShowDescText.numberOfLines = 2;

    self.showNextViewButton.frame = CGRectMake(CGRectGetMinX(_nextShowButton.frame), CGRectGetMinY(_nextShowButton.frame), _nextShowImage.frame.size.width, CGRectGetMaxY(_nextShowDescText.frame) - CGRectGetMinY(_nextShowButton.frame));

#pragma mark Ten Point Button

    self.tenPointButton.frame = CGRectMake(self.nextShowImage.frame.origin.x,
            290,
            self.nextShowImage.frame.size.width,
            22);

    [self.tenPointButton setTitleColor:[UIColor colorWithHex:@"ffc900"] forState:UIControlStateNormal];

    [self.tenPointButton setImage:[UIImage imageNamed:@"ic-tab-live-off"] forState:UIControlStateNormal];

    [self.tenPointButton setTitle:@"10 Points!" forState:UIControlStateNormal];

    [self.tenPointButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];

    self.tenPointButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

#pragma mark Watch Ads Button

    self.watchAdButton.frame = CGRectMake(self.nextShowImage.frame.origin.x,
            325,
            self.nextShowImage.frame.size.width,
            52);

    self.watchAdButton.layer.borderColor = [UIColor colorWithHex:@"ffc900"].CGColor;

    self.watchAdButton.layer.borderWidth = 1.0f;

    self.watchAdButton.layer.cornerRadius = 5.0f;

#pragma mark Watch Ads Description

    self.watchAdDescText.frame = CGRectMake(self.nextShowImage.frame.origin.x,
            385,
            self.nextShowImage.size.width,
            55);

    [self setAdsDescription:@"To watch Lovelive! Sunshine #02 May our Dream Come True! for FREE"];

#pragma mark Line View

    self.lineView.frame = CGRectMake(0,
            _shouldHideAdView ? 279 : 454,
            self.frame.size.width,
            1);
    self.lineView.backgroundColor = [UIColor darkGrayColor];

#pragma mark TableView

    self.tableView.backgroundColor = [UIColor clearColor];

    self.tableView.frame = CGRectMake(0,
            _shouldHideAdView ? 280 : 455,
            self.frame.size.width,
            self.frame.size.height - (_shouldHideAdView ? 280 : 455));

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

}

#pragma mark set Data

- (void)loadSeeLaterData {
    @weakify(self)
    [DLSeelaterManager loadSeelaterDatafromServerCompletion:^(BOOL success, NSArray *seelaterData) {
        @strongify(self)
        [_dataArray removeAllObjects];
        if (success) {
            self.lineView.hidden = NO;
            self.tableView.hidden = NO;
            [_dataArray addObjectsFromArray:seelaterData];
        } else {
            self.lineView.hidden = YES;
            self.tableView.hidden = YES;
        }
        [self.tableView reloadData];
        [self updateFrame];
    }];
}

- (void)disPlayWithCurrentMovie:(DLMovie *)currentMovie {
    @weakify(self, currentMovie)
    [DLAPIHelper fetchMovieByWorkID:[currentMovie.workId integerValue] pageNumber:1 perPage:10000 callback:^(NSMutableArray *movies, NSError *error) {
        @strongify(self, currentMovie)
        if (error) {
            [self displayNextMovie:nil];
        } else {
            if (movies && movies.count > 1) {
                [movies sortUsingComparator:^NSComparisonResult(id _Nonnull obj1, id _Nonnull obj2) {
                    DLMovie *movie1 = (DLMovie *) obj1;
                    DLMovie *movie2 = (DLMovie *) obj2;
                    if ([movie1.chapter integerValue] > [movie2.chapter integerValue]) {
                        return NSOrderedDescending;
                    }
                    return NSOrderedAscending;
                }];
                if (movies.count > [currentMovie.chapter integerValue]) {
                    [self displayNextMovie:movies[[currentMovie.chapter integerValue]]];
                } else {
                    [self displayNextMovie:nil];
                }
            } else {
                [self displayNextMovie:nil];
            }
        }
    }];
}

- (void)displayNextMovie:(DLMovie *)nextMovie {
    if (nextMovie) {
        _nextMovie = nextMovie;
        self.nextShowDescText.text = nextMovie.title;
        NSString *adString = [NSString stringWithFormat:@"To watch %@ for FREE", nextMovie.title];
        [self setAdsDescription:adString];
        [UIImageView loadImageWithURLString:self.nextMovie.image
                            targetImageView:self.nextShowImage];
    } else {
        self.nextShowDescText.text = @"This is the last chapter.";
        NSString *adString = @"To watch movies for FREE";
        [self setAdsDescription:adString];
        self.nextShowImage.image = nil;
    }

}

- (void)setAdsDescription:(NSString *)strDesc {

    self.watchAdDescAttrStr = [[NSMutableAttributedString alloc] initWithString:strDesc];

    NSInteger nWatchAdLen = self.watchAdDescAttrStr.length;

    [self.watchAdDescAttrStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHex:@"ffc900"] range:NSMakeRange(nWatchAdLen - 8, 8)];

    [self.watchAdDescAttrStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHex:@"ffffff"] range:NSMakeRange(0, nWatchAdLen - 9)];

    [self.watchAdDescAttrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:15.0f] range:NSMakeRange(0, nWatchAdLen)];

    self.watchAdDescText.attributedText = self.watchAdDescAttrStr;
}

- (void)setNextShowDesciption:(NSString *)strDesc {
    self.nextShowDescText.text = strDesc;
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataArray.count;
    //    return self.notices ? self.notices.count : 0;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DLContinueToPlaySeeLaterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"dlcontinuetoplayseelater-cell" forIndexPath:indexPath];
    [cell dispalyForum:_dataArray[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [DLContinueToPlaySeeLaterCell getCellHeight];
}


#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    [DLTransitionManager showForumDetailViewControllerWithForum:_dataArray[indexPath.row] withTarget:_parentVC];
}

#pragma mark UIButton Action

- (void)onBtnClose {
    NSLog(@"Action - Close");

    [self removeFromSuperview];

    if (self.delegate != nil) {
        [self.delegate onBtnClose];
    }
}

- (void)onBtnNextShow {
    NSLog(@"Action - Next Show");
    if (_nextMovie) {
        [DLOpenPlayMovieService tryPlayMovie:_nextMovie
                                        live:nil
                          fromViewController:self.parentVC
                                    callback:^{

                                    }];
    }
}

- (void)onBtnTenPoint {
    NSLog(@"Action - Ten Points Now");
}

- (void)onBtnWatchAds {
    NSLog(@"Action - Watch Ads Now");
    [self.watchAdButton setEnabled:NO];
    [UnityAds show:_parentVC];
}

#pragma mark - Unity Ads Delegate

- (void)unityAdsReady:(NSString *)placementId {
    [self.watchAdButton setEnabled:YES];
}

- (void)unityAdsDidError:(UnityAdsError)error withMessage:(NSString *)message {
    [self.watchAdButton setEnabled:NO];
}

- (void)unityAdsDidStart:(NSString *)placementId {
    [self.watchAdButton setEnabled:NO];
    [WINDOW makeToast:Localizer(@"point_ad_title")];
}

- (void)unityAdsDidFinish:(NSString *)placementId withFinishState:(UnityAdsFinishState)state {
    [self.watchAdButton setEnabled:YES];
    [DLAPIHelper addPoint:nil];
}

@end
