//
//  DLMyPageGroupListViewController.h
//  AniLive
//
//  Created by Isao Kono on 2016/11/04.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLSegmentViewControllerProtocol.h"

typedef NS_ENUM(NSInteger, DLMyPageSegmentType) {
    DLMyPageSegumentTypeVideoHistory = 0,
    DLMyPageSegumentTypeMyForum,
    DLMyPageSegumentTypeParticipateForum,
};

@interface DLMyPageGroupListViewController : UIViewController

- (instancetype)initWithType:(DLMyPageSegmentType)type;

@end

