//
// Created by Kotaro Itoyama on 2016/10/09.
// Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

@interface DLLocalMockServer : NSObject
+(void)installStubs;
@end
