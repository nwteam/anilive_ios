//
// Created by akasetaichi on 2017/01/20.
// Copyright (c) 2017 AnimeConsortiumJapan. All rights reserved.
//

#import "DLAuthcode.h"

@interface DLAuthcode()
@property(readwrite, nonatomic, strong) NSNumber *code;
@property(readwrite, nonatomic, strong) NSNumber *userId;
@end

@implementation DLAuthcode

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) { return nil; }
    self.code = CHECK_NULL_DATA_NUMBER(attributes[@"code"]);
    self.userId = CHECK_NULL_DATA_NUMBER(attributes[@"user_id"]);
    return self;
}

@end