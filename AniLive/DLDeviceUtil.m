//
//  DLDeviceUtil.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/12/28.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLDeviceUtil.h"
#import "UIApplication+UIID.h"
#import "DLKeyChainManager.h"
#import <sys/sysctl.h>

@implementation DLDeviceUtil

// モデル名を取得する
+ (NSString*) getDeviceName {
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *deviceName = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
    free(machine);
    
    // XXX: シミュレーターでデバックを行うため
    // TODO: 消す
    if ([[self class] isSimulator]) {
        deviceName = @"iPhone7,1";
    }
    
    return deviceName;
}

// iOSバージョンを取得する
+ (NSString*) getDeviceSystemVersion {
    
    return [NSString stringWithFormat:@"i%2.2f", IOS_VERSION];
}

// bundle identifier を取得する
+ (NSString *)getBundleIdentifier {
    
    return [[NSBundle mainBundle] bundleIdentifier];
}

// アプリのバージョンを取得する
+ (NSString *)getAppVersion {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

// 言語設定を取得する
+ (NSString*)getCurrentLanguage {
    NSArray *langs = [NSLocale preferredLanguages];
    if (!(langs.count > 0)) {
        return @"";
    }
    NSString *currentLanguage = [langs objectAtIndex:0];
    
    return currentLanguage;
}


// locale identifier を取得する
+ (NSString *)getCurrentLocaleIdentifier{
    return [[NSLocale currentLocale] localeIdentifier];
}

// UUID を取得する
+ (NSString *)getUUID {

    NSString *savedUuid = [[DLKeyChainManager sharedInstance] getUUID];
    if (NOT_EMPTY(savedUuid)) {
        NSLog(@"uuid: %@", savedUuid);
        return savedUuid;
    } else {
        NSString *uuid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [[DLKeyChainManager sharedInstance] saveUUID:uuid];
        NSLog(@"uuid: %@", uuid);
        return uuid;
    }
}

// UIID を取得する
+ (NSString *)getUIID {
    
    // XXX: シミュレーターでデバックを行うため
    // TODO: 消す
    if ([[self class] isSimulator]) {
        return @"aaaa";
    }
    
    NSString *savedUiid = [[DLKeyChainManager sharedInstance] getUIID];
    if (NOT_EMPTY(savedUiid)) {
        NSLog(@"backup-id: %@", savedUiid);
        return savedUiid;
    } else {
        NSString *uiid = [[UIApplication sharedApplication] uniqueInstallationIdentifier];
        [[DLKeyChainManager sharedInstance] saveUIID:uiid];
        NSLog(@"backup-id: %@", uiid);
        return uiid;
    }
}


#pragma mark - Private Methods

+ (BOOL)isSimulator {
    return TARGET_OS_SIMULATOR != 0;
}

@end
