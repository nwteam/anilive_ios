//
//  DLTabBarView.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DLTabBarViewDelegate;

@interface DLTabBarView : UIView

@property (nonatomic, weak) id<DLTabBarViewDelegate> delegate;

- (instancetype)initWithNumberOfTabs:(NSInteger)numberOfTabs withStartTab:(NSInteger)startTab;

@end

@protocol DLTabBarViewDelegate <NSObject>

@optional
- (void)tabBarView:(DLTabBarView *)tabBarItemView didSelectIndex:(NSInteger)index;

@end
