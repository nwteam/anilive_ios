//
//  DLPlayerViewPromotionAd.m
//  AniLive
//
//  Created by ykkc on 2017/01/17.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "DLPlayerViewPromotionAd.h"
#import "DLPromotion.h"
#import "UIView+Toast.h"
#import "DLLive.h"
#import "DLPlayer.h"
#import "DLAPIHelper.h"
#import "DLHighlightedButton.h"

@interface DLPlayerViewPromotionAd ()

@property (nonatomic, strong) NSMutableArray<DLPromotion *> *promovionVideos;
@property (nonatomic, strong) DLPromotion* currentPromotionVideo;
@property (nonatomic, strong) AVPlayerItem* playerItem;
@property (nonatomic, assign) NSInteger playAdTime;

@property (nonatomic, strong) UIImageView *reserveImageView;
@property (nonatomic, strong) DLHighlightedButton *reserveButton;

@end

@implementation DLPlayerViewPromotionAd

- (instancetype)initPromotionAds:(NSInteger)playAdTime {
    self = [super init];
    if (self) {
        self.playAdTime = playAdTime;
        [self initView];
        [self fetchPromotionMovieDatass];
    }
    return self;
}


#pragma mark Private Method

- (void)initView {
    
    self.backgroundColor = [UIColor redColor];
    
    self.delegate = nil;
    self.promovionVideos = [NSMutableArray<DLPromotion*> new];
}

- (void)updateFrame {
    self.reserveImageView = [UIImageView new];
    self.reserveImageView.backgroundColor = [UIColor clearColor];
    self.reserveImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.reserveImageView.image = [UIImage imageNamed:DLConstIconNotificationsOff];
    [self addSubview:self.reserveImageView];
    self.reserveImageView.hidden = NO;
    
    self.reserveButton = [DLHighlightedButton buttonWithType:UIButtonTypeSystem];
    self.reserveButton.backgroundColor = [UIColor clearColor];
    self.reserveButton.isNoNeedHighlight = YES;
    [self.reserveButton addTarget:self
                           action:@selector(reserveButtonDidTap)
                 forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.reserveButton];
    self.reserveButton.hidden = NO;
    
    self.reserveImageView.width = self.reserveImageView.height = 15;
    self.reserveButton.width = self.reserveButton.height = 40;
    self.reserveButton.x = 0;
    self.reserveButton.y = self.height - self.reserveButton.height;
    self.reserveImageView.center = self.reserveButton.center;
}

- (void)fetchPromotionMovieDatass {
    @weakify(self)
    [DLAPIHelper fetchPromotionMoviesWithQuantity:2
                                         callback:^(NSMutableArray<DLPromotion*> *promotions, NSError *error) {
                                             @strongify(self)
                                             if([promotions count] > 0){
                                                 self.promovionVideos = promotions;
                                                 [self initProcess];
                                             }else{
                                                 [self.delegate onAdMovieEnded:(self.playAdTime <= 0 ? 0 : self.playAdTime)];
                                             }
                                         }];
}

-(void) initProcess {
    NSInteger index = [self.promovionVideos count];
    self.currentPromotionVideo = [self.promovionVideos objectAtIndex:index - 1];
    self.playerItem = [[AVPlayerItem alloc] initWithURL:[NSURL URLWithString:self.currentPromotionVideo.moviePath]];
    self.player = nil;
    self.player = [[DLPlayer alloc] initWithPlayerItem:self.playerItem
                           withAllowsExternalPlayback:YES];
    self.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    [self setPlayer:self.player];
    
    [self.player addObserver:self
                  forKeyPath:@"status"
                     options:NSKeyValueObservingOptionNew
                     context:nil];
    
    [self updateFrame];
}

- (void)reserveButtonDidTap {
    NSLog(@"reserveButtonDidTap");
    [self.reserveImageView setImage:[UIImage imageNamed:DLConstIconNotificationsOn]];
    
    @weakify(self)
    [DLAPIHelper postLiveReserve:[self.currentPromotionVideo.liveId intValue] callback:^(NSObject *live, NSError *error) {
        @strongify(self)
        if (error) {
            NSLog(@"%@", error);
        } else {
            if ([live isKindOfClass:[DLLive class]]) {
                DLLive *tmpDLLive = (DLLive *)live;
                //LocalPush設定
                [self pushLocalNotification:tmpDLLive];
                //通知完了のToastを表示。
                [WINDOW makeToast:@"Your show has been successfully reserved! You will receive a notification before the show starts." duration:3.0f position:CSToastPositionCenter];
            }
        }
    }];

//    if ([self.delegate respondsToSelector:@selector(pauseButtonDidTap)]) {
//        [self.delegate pauseButtonDidTap];
//    }
}

#pragma mark - KVO

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if ([keyPath isEqualToString:@"status"]) {
        if ([self.player status] == AVPlayerStatusReadyToPlay) {
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(playerItemDidReachEnd:)
                                                         name:AVPlayerItemDidPlayToEndTimeNotification
                                                       object:[self.player currentItem]];
            [self.player play];
        }
        else if ([self.player status] == AVPlayerItemStatusFailed){
            [self.delegate onAdMovieError];
        }

        [self.player removeObserver:self forKeyPath:@"status" context:nil];
    }
}

#pragma mark - Notification

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    NSLog(@"playerItemDidReachEnd");
    // 動画広告再生の終了イベント
    self.playAdTime -= CMTimeGetSeconds(self.player.currentItem.duration);
    
    self.playAdTime -= CMTimeGetSeconds(self.player.currentTime);
    if(self.playAdTime < 15){
        [self.delegate onAdMovieEnded:(self.playAdTime <= 0 ? 0 : self.playAdTime)];
    }else{
        [self.player pause];
        self.player = nil;
        [self fetchPromotionMovieDatass];
    }
}

//TODO: Util作りたい
#pragma marker - Set Local Notification
/*
 LocalNotificaiton登録
 */
- (void)pushLocalNotification:(DLLive *)notificationData {
    //同じLiveIDの通知が設定されている場合、削除してから再登録。
    if (notificationData != nil) {
        NSString *tmpLiveID = [notificationData.liveId stringValue];
        if (tmpLiveID != nil) {
            for (UILocalNotification *notify in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
                NSDictionary *tmpUserInfo = notify.userInfo;
                if (tmpUserInfo != nil) {
                    NSString *tmpLiveId = [tmpUserInfo objectForKey:@"liveID"];
                    if (tmpLiveId != nil) {
                        if([tmpLiveId isEqualToString:tmpLiveID]){
                            [[UIApplication sharedApplication] cancelLocalNotification:notify];
                        }
                    }
                }
            }
            
            //ローカル通知を設定。
            //ローカル通知のFireDate設定（５分前）
            NSDate *tmpDate = notificationData.broadcastOpen;
            if (tmpDate == nil) {
                return;
            }
            NSDate *tmpFireDate = [tmpDate initWithTimeInterval:-(5 * 60) sinceDate:tmpDate];
            
            //ローカル通知のタイトル設定
            NSString *tmpNotificationTitle = notificationData.title;
            if (tmpNotificationTitle == nil) {
                return;
            }
            
            UILocalNotification *notification = [[UILocalNotification alloc] init];
            notification.fireDate = tmpFireDate;
            notification.timeZone = [NSTimeZone defaultTimeZone];
            notification.alertBody = [NSString stringWithFormat:@"\"%@\" will start live streaming in 5 minutes.", tmpNotificationTitle];
            notification.soundName = UILocalNotificationDefaultSoundName;
            notification.userInfo =  [NSDictionary dictionaryWithObjectsAndKeys:
                                      tmpLiveID, @"liveID"
                                      ,nil];
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        }
    }
}

@end
