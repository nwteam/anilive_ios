//
//  DLSettingsCell.m
//  AniLive
//
//  Created by Isao Kono on 2016/11/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLSettingsCell.h"
#import "DLNotificationService.h"

const static CGFloat kCellBaseHeight = 50;
const NSString* kSettingCell_userDefaultsKey = @"userDefaultsKey";

@interface DLSettingsCell ()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UISwitch *accessorySwitch;
@property (nonatomic, strong) UILabel *accessoryLabel;
@property (nonatomic, strong) NSString *switchAction;
@property (nonatomic, strong) NSString *switchUserDefaultsKey;
@end

@implementation DLSettingsCell

#pragma mark - ORVSuperCollectionViewCellProtocol

- (void)initView {
    [super initView];
    
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.accessoryView.backgroundColor = [UIColor clearColor];
    
#pragma mark Text Label
    
    self.titleLabel = [UILabel new];
    self.titleLabel.font = BASE_FONT_BOLD(15);
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.numberOfLines = 1;
    [self.contentView addSubview:self.titleLabel];
    
#pragma mark Description Text View
    
    self.descriptionLabel = [UILabel new];
    self.descriptionLabel.font = BASE_FONT_BOLD(12);
    self.descriptionLabel.textColor = [UIColor colorWithHex:DLConstColorCodeGray];
    self.descriptionLabel.numberOfLines = 0;
    self.descriptionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.contentView addSubview:self.descriptionLabel];
    
#pragma mark Toggle Siwtch
    
    self.accessorySwitch = [UISwitch new];
    self.accessorySwitch.onTintColor = [UIColor colorWithHex:DLConstColorCodeYellow];
    self.accessorySwitch.on = YES;
    [self.accessorySwitch addTarget:self
                          action:@selector(onChangeSwitch:)
                forControlEvents:UIControlEventValueChanged];
    
    self.accessoryLabel = [UILabel new];
    self.accessoryLabel.font = BASE_FONT_BOLD(14);
    self.accessoryLabel.textColor = [UIColor whiteColor];
    self.accessoryLabel.numberOfLines = 1;
    
}

- (void)initializeCell {
    [super initializeCell];
    
    self.descriptionLabel.hidden = YES;
    self.accessorySwitch.hidden = YES;
    self.accessoryLabel.hidden = YES;
}

+ (NSString *)getCellIdentifier {
    return NSStringFromClass([self class]);
}

#pragma mark - Class Methods

+ (CGFloat)getCellBaseHeight {
    return kCellBaseHeight;
}

- (CGFloat)getCellHeight {
    return self.descriptionLabel.text != nil ? kCellBaseHeight + self.descriptionLabel.height : kCellBaseHeight;
}

#pragma mark - Override Methods

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
}

#pragma mark - Public Methods

- (void)setCellData:(NSDictionary *)args {
    self.titleLabel.text = args[@"title"];

    if ([args[@"isSwitch"] boolValue]) {
        self.accessorySwitch.hidden = NO;
        self.accessoryView = self.accessorySwitch;
        if ([args objectForKey:kSettingCell_userDefaultsKey]) {
            self.switchUserDefaultsKey = [args objectForKey:kSettingCell_userDefaultsKey];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults registerDefaults:@{self.switchUserDefaultsKey: @YES}];
            BOOL switchValue = [userDefaults boolForKey:self.switchUserDefaultsKey];
            self.accessorySwitch.on = switchValue;
            self.switchAction = [args objectForKey:@"action"];
            self.switchUserDefaultsKey = [args objectForKey:kSettingCell_userDefaultsKey];
        }
    }
    
    if (args[@"label"] != nil) {
        self.accessoryLabel.hidden = NO;
        self.accessoryLabel.text = args[@"label"];
        self.accessoryView = self.accessoryLabel;
    }
    
    if (args[@"description"] != nil) {
        self.descriptionLabel.hidden = NO;
        self.descriptionLabel.text = args[@"description"];
    }
    
    [self updateFrame];
}

#pragma mark - Private Methods

- (void)updateFrame {
    self.titleLabel.frame = CGRectMake(DLConstDefaultMargin,
                                       0,
                                       self.contentView.width,
                                       kCellBaseHeight);
    
    self.descriptionLabel.frame = CGRectMake(self.titleLabel.x,
                                             self.titleLabel.maxY,
                                             self.titleLabel.width,
                                             self.titleLabel.height);
    [self.descriptionLabel sizeToFit];
    
    [self.accessoryLabel sizeToFit];
    
}

- (void)onChangeSwitch:(UISwitch*)sender {
    if ([@"reflectPushSettingAsTagToServer" isEqualToString: self.switchAction]) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setBool:self.accessorySwitch.on forKey:self.switchUserDefaultsKey];
        [userDefaults synchronize];

        [DLNotificationService reflectPushSettingAsTagToServer];
    }
}

@end
