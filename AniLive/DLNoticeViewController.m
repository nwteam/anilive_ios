//
//  DLNoticeViewController.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLNoticeViewController.h"
#import "DLAppFrameManager.h"
#import "DLNavigationBarView.h"
#import "DLMainTabBarController.h"
#import "DLNoticeCell.h"
#import "DLPreloadMovieManager.h"
#import "DLAPIHelper.h"
#import "DLNotificationService.h"
#import "DLProgressDialogView.h"

@interface DLNoticeViewController () <UITableViewDelegate, UITableViewDataSource, DLNavigationBarViewDelegate>

@property (nonatomic, strong) DLNavigationBarView *navigationBarView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic) NSMutableArray *notices;
@property (nonatomic, assign) NSUInteger pageNumber;
@property (nonatomic, assign) NSMutableArray *forums;
@property (readwrite, nonatomic, strong) RLMNotificationToken *realmToken;

@end

@implementation DLNoticeViewController

#pragma mark - Override Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.notices = [[NSMutableArray alloc] init];
    
    [self initView];
    [self updateFrame];
    [self loadData];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    /*
     * この画面を表示している時に、通知が来た場合の対応
     * ※タブのバッチはAppDelegate側で対応
     */
    RLMRealm* realm = [RLMRealm defaultRealm];
    self.realmToken = [realm addNotificationBlock:^(NSString *notification, RLMRealm* realm) {
        if ([DLNotice getAll].count != self.notices.count) {
            self.notices = [DLNotice getAll];
            [self.tableView reloadData];
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Private Methods

- (void)initView {
    
#pragma mark Navigation Bar
    
    self.navigationBarView = [[DLNavigationBarView alloc]initWithType:DLNavigationBarViewTypeTitle];
    [self.navigationBarView setTitle:Localizer(@"Notice")];
    [self.view addSubview:self.navigationBarView];
    
#pragma mark Self View background
    
    self.view.backgroundColor = [UIColor colorWithHex:DLConstColorDarkBlue];
    
#pragma mark Table View
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero];
    
    self.tableView.backgroundColor = [UIColor blackColor];
    
    self.tableView.separatorColor = [UIColor clearColor];
    
    [self.tableView registerNib:[DLNoticeCell getDefaultNib] forCellReuseIdentifier:@"notice-cell"];
    
    [self.tableView setShowsVerticalScrollIndicator:NO];
    
    [self.view addSubview:self.tableView];
    
}

- (void)updateFrame {
    
    self.navigationBarView.frame = CGRectMake(0,
                                              CGRectGetMinX([DLAppFrameManager sharedInstance].applicationFrame),
                                              CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame),
                                              DLConstNavigationBarHeight);
    
    self.tableView.frame = CGRectMake(0, self.navigationBarView.maxY, CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame), CGRectGetHeight([DLAppFrameManager sharedInstance].applicationFrame) - self.navigationBarView.maxY - DLConstTabBarHeight);
    
    
}
- (void)loadData {
    self.notices = [DLNotice getAll];
}

- (void)additionalLoad {
    @weakify(self)
    [DLAPIHelper fetchForumsWithPageNumber:self.pageNumber SortType:DLForumGroupSortTypeLatest callback:^(NSMutableArray<DLForum *> *forums) {
        @strongify(self)
        [self.forums addObjectsFromArray:forums];
        [self.tableView reloadData];
    }];
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.notices ? self.notices.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DLNoticeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"notice-cell" forIndexPath:indexPath];
    [cell setNotice:self.notices[(NSUInteger) indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [DLNoticeCell getCellHeight];
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Notice Cell Selected - %ld", (long)indexPath.row);
    DLProgressDialogView *progressDialog = [DLProgressDialogView initView];
    [self.view addSubview:progressDialog];
    [progressDialog bringSubviewToFront:self.view];
    [progressDialog start];

    DLNotice *notice = [self.notices objectAtIndex:indexPath.row];

    RLMRealm *realm = RLMRealm.defaultRealm;
    [DLNotice changeToReadAll];
    [self loadData];
    [self.tableView reloadData];

    [DLNotificationService transitScreenByNotification:notice callback:^{
        [progressDialog dismiss];
    }];

}

#pragma mark - Scroll view delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    float offset = (scrollView.contentOffset.y - (scrollView.contentSize.height - scrollView.frame.size.height));
    if (offset >= 0 && offset <= 5) {
        [self additionalLoad];
    }
}
@end
