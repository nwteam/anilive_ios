//
//  DLCatalogAlphabeticalCell.h
//  AniLive
//
//  Created by isaoeka on 2016/11/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLSuperTableViewCell.h"
#import "DLMovie.h"

@interface DLVideoHistoryCell : DLSuperTableViewCell

+ (CGFloat)getCellHeight;

- (void)setCellData:(DLMovie *)movieData;
- (void)setSeparatorLineIsHidden:(BOOL)isHidden;

@end
