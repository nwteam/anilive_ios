//
//  DLSuperTableViewCell.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/16.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLSuperTableViewCell.h"

@implementation DLSuperTableViewCell

- (void)prepareForReuse {
    [super prepareForReuse];
    [self initializeCell];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // セルのハイライトをさせないように制御
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self initView];
    }
    return self;
}

#pragma mark - DLSuperTableViewCellProtocol

- (void)initView {
    
}

- (void)initializeCell {
    
}

+ (NSString *)getCellIdentifier {
    
    return [self getCellIdentifier];
}

@end
