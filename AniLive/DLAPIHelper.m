//
// Created by Kotaro Itoyama on 2016/10/09.
// Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLAPIHelper.h"
#import "DLForum.h"
#import "DLReportType.h"
#import "DLLocalMockServer.h"
#import "DLLive.h"
#import "DLTimeSchedule.h"
#import "DLUser.h"
#import "DLMovie.h"
#import "DLForumGroup.h"
#import "DLComment.h"
#import "DLWork.h"
#import "DLWatch.h"
#import "DLPoint.h"
#import "DLPromotion.h"
#import "DLDeviceUtil.h"
#import "DLUserDataManager.h"
#import "DLAuthcode.h"
#import "DLUserDevice.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import <SDWebImage/SDWebImageDownloader.h>
#import <SDWebImage/SDWebImageManager.h>
#import <StoreKit/StoreKit.h>

typedef NS_ENUM(NSInteger, DLAPIMethod) {
    GET = 0,
    POST,
    PATCH,
    DELETE
};

@interface DLAPIHelper () <SKRequestDelegate>
@property NSDateFormatter *formatter;
@property NSDateFormatter *timeFormatter;
@property NSDate *timeReferenceDate;
@property AFHTTPSessionManager *manager;
@property(atomic) NSMutableDictionary *forumStore;
@property (nonatomic, strong) dispatch_semaphore_t semaphore;
@property(atomic) BOOL isFetchingUser;
@end

@implementation DLAPIHelper
@synthesize manager, formatter, timeFormatter, timeReferenceDate, forumStore;

+ (instancetype)sharedManager {
    static DLAPIHelper *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = self.new;
#ifdef LOCAL_MOCK
        [DLLocalMockServer installStubs];
#endif
    });
    
    return _sharedManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        formatter = NSDateFormatter.new;
        formatter.timeZone = [NSTimeZone timeZoneWithName:@"Asia/Tokyo"];
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        
        timeFormatter = NSDateFormatter.new;
        timeFormatter.dateFormat = @"HH:mm:ss";
        timeReferenceDate = [timeFormatter dateFromString:@"00:00:00"];
        
        manager = [AFHTTPSessionManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/json", @"text/plain"]];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.requestSerializer.timeoutInterval = 10.0;
        manager.requestSerializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithObjects:@"GET", @"HEAD", nil];
        self.semaphore = dispatch_semaphore_create(1);
        
        forumStore = NSMutableDictionary.dictionary;
    }
    return self;
}

+ (AFHTTPSessionManager *)sesmgr {
    return DLAPIHelper.sharedManager.manager;
}

+ (NSInteger)myUserId {
    return 99999;
}

+ (NSDate *)dateFromString:(NSString *)dateString {
    if (NOT_NULL(dateString)) {
        return [DLAPIHelper.sharedManager.formatter dateFromString:dateString];
    }
    return nil;
}

+ (NSTimeInterval)timeIntervalFromString:(NSString *)string {
    return [[DLAPIHelper.sharedManager.timeFormatter dateFromString:string] timeIntervalSinceDate:DLAPIHelper.sharedManager.timeReferenceDate];
}

#pragma mark /Work
/**
 *  GET /works
 */
+ (void)fetchWorksWithPageNumber:(NSInteger)pageNumber
                         perPage:(NSInteger)perPage
                        sortType:(NSString *)sortType
                        callback:(void (^)(NSMutableArray *works, NSError *error))callback {
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    
    // Required
    parameters[@"page_no"] = [NSNumber numberWithInteger:pageNumber];
    parameters[@"per_page"] = [NSNumber numberWithInteger:perPage];
    
    // Optional
    if (NOT_NULL(sortType)) {
        parameters[@"sort_type"] = sortType;
    }
    
    [DLAPIHelper requestWithMethod:GET
                         urlString:kAPIWork
                        parameters:parameters
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", kAPIWork, error.localizedDescription);
                         callback([NSMutableArray array], error);
                     } else {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSMutableArray *works = [DLWork instancesFromDictionaries:responseObject[@"works"]];
                             if (NOT_NULL(works)) {
                                 callback(works, nil);
                             }
                         }
                     }
                 }];
}
+ (void)validateReceiptURL:(NSURL *)receiptURL {
    NSData *receiptData = [NSData dataWithContentsOfURL:receiptURL];
    if (receiptData) {
        [DLAPIHelper.sesmgr POST:kAPIValidate
                      parameters:@{@"receipt": [receiptData base64EncodedStringWithOptions:0], @"user_id": [ud stringForKey:KEY_USER_ID], @"restore": [ud stringForKey:KEY_RESTORATION]}
                         success:^(NSURLSessionDataTask *task, id responseObject) {
                             if ([responseObject isKindOfClass:[NSDictionary class]]) {
                                 NSDictionary *dic = responseObject;
                                 NSLog(@"%@", dic);
                                 BOOL flag = NO;

                                 NSString *val = dic[@"is_subscription_valid"];
                                 if (val) {
                                     [ud setBool:[val boolValue] forKey:KEY_IS_SUBSCRIPTION_VALID];
                                 }

                                 if ([ud boolForKey:KEY_RESTORATION]) {
                                     [ud setBool:NO forKey:KEY_RESTORATION];
                                     flag = YES;
                                 }


                                 if (flag || val) {
                                     [ud synchronize];
                                     [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_KEY_RELOAD object:self];
                                 }

                             } else if ([responseObject isKindOfClass:[NSData class]]) {
                                 NSString *resp = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                                 NSLog(@"%@", resp);
                             } else {
                                 NSLog(@"%@", responseObject);
                             }
                         }
                         failure:^(NSURLSessionDataTask *task, NSError *error) {
                             NSData *data = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                             NSString *body = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                             NSLog(@"done ERROR:%@", error.description);
                             NSLog(@"body:%@", body);
                         }
        ];
    }
}

/**
 *  GET /works/{work_id}
 */
+ (void)fetchWorksByWorkID:(NSInteger)workId
                  callback:(void (^)(DLWork *work, NSError *error))callback {
    
    [DLAPIHelper requestWithMethod:GET
                         urlString:[NSString stringWithFormat:@"%@/%ld", kAPIWork, (long)workId]
                        parameters:nil
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", [NSString stringWithFormat:@"%@/%ld", kAPIWork, (long)workId], error.localizedDescription);
                         callback([NSObject new], error);
                     } else {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             callback((DLWork *) [DLWork instanceFromDictionary:responseObject[@"work"]], nil);
                         }else{
                             callback(nil, nil);
                         }

                     }
                 }];
}

/**
 *  GET /works/{work_id}/forums
 */
+ (void)fetchForumsByWorkID:(NSInteger)workId
                 pageNumber:(NSInteger)pageNumber
                    perPage:(NSInteger)perPage
                   callback:(void (^)(NSMutableArray *forums, NSError *error))callback {
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    
    // Required
    parameters[@"page_no"] = [NSNumber numberWithInteger:pageNumber];
    parameters[@"per_page"] = [NSNumber numberWithInteger:perPage];
    
    [DLAPIHelper requestWithMethod:GET
                         urlString:[NSString stringWithFormat:@"%@/%ld/forums", kAPIWork, (long)workId]
                        parameters:parameters
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", [NSString stringWithFormat:@"%@/%ld/forums", kAPIWork, (long)workId], error.localizedDescription);
                         callback([NSMutableArray array], error);
                     } else {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSMutableArray *forums = [DLForum instancesFromDictionaries:responseObject[@"forums"]];
                             if (NOT_NULL(forums)) {
                                 callback(forums, nil);
                             }
                         }
                     }
                 }];
}

/**
 *  GET /works/{work_id}/movies
 */
+ (void)fetchMovieByWorkID:(NSInteger)workId
                pageNumber:(NSInteger)pageNumber
                   perPage:(NSInteger)perPage
                  callback:(void (^)(NSMutableArray *movies, NSError *error))callback {
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    
    // Required
    parameters[@"page_no"] = [NSNumber numberWithInteger:pageNumber];
    parameters[@"per_page"] = [NSNumber numberWithInteger:perPage];
    
    [DLAPIHelper requestWithMethod:GET
                         urlString:[NSString stringWithFormat:@"%@/%ld/movies", kAPIWork, (long)workId]
                        parameters:parameters
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", [NSString stringWithFormat:@"%@/%ld/movies", kAPIWork, (long)workId], error.localizedDescription);
                         callback([NSMutableArray array], error);
                     } else {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSMutableArray *movies = [DLMovie instancesFromDictionaries:responseObject[@"movies"]];
                             if (NOT_NULL(movies)) {
                                 callback(movies, nil);
                             }
                         }
                     }
                 }];
}

#pragma mark /Movie
/**
 GET /movies/{movie_id}/forums
 */
+ (void)fetchMoviesForumsByMovieID:(NSInteger)movieId
                          callback:(void (^)(NSMutableArray<DLForum *> *forums, NSError *error))callback {
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    
#ifdef LOCAL_MOCK
    [DLAPIHelper requestWithMethod:GET
                         urlString:kAPIForum
                        parameters:parameters
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", error.localizedDescription);
                         callback([NSMutableArray array], error);
                         
                     } else {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSMutableArray *forums = [DLForum instancesFromDictionaries:responseObject[@"forums"]];
                             if (NOT_NULL(forums)) {
                                 callback(forums, nil);
                             }
                         }
                     }
                 }];
#else
    [parameters setObject:@"00:00:00" forKey:@"start_time"];
    [parameters setObject:@"23:59:59" forKey:@"end_time"];
    
    [DLAPIHelper requestWithMethod:GET
                         urlString:[NSString stringWithFormat:@"%@/%ld/forums", kAPIMovie, (long)movieId]
                        parameters:parameters
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", [NSString stringWithFormat:@"%@/%ld/forums", kAPIMovie, (long)movieId], error.localizedDescription);
                         callback([NSMutableArray array], error);
                         
                     } else {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSMutableArray *forums = [DLForum instancesFromDictionaries:responseObject[@"forums"]];
                             if (NOT_NULL(forums)) {
                                 callback(forums, nil);
                             }
                         }
                     }
                 }];
#endif
    
}

/**
 *  POST /movies/{movie_id}/forums
 */
+ (void)postForumsByMovieID:(NSInteger)movieId
                  forumTitle:(NSString *)forumTitle
             forumPinnedTime:(NSString *)forumPinnedTime
                    callback:(void (^)(DLForum *forum, NSError *error))callback {

    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"title"] = forumTitle;
    parameters[@"forum_pinned_time"] = forumPinnedTime;
    
    [DLAPIHelper requestWithMethod:POST
                         urlString:[NSString stringWithFormat:@"%@/%ld/forums", kAPIMovie, movieId]
                        parameters:parameters
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", [NSString stringWithFormat:@"%@/%ld/forums", kAPIMovie, movieId], error.localizedDescription);
                         callback([DLForum new], error);
                     } else {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             DLForum *forum = (DLForum *)[DLForum instanceFromDictionary:responseObject[@"forums"]];
                             if (NOT_NULL(forum)) {
                                 callback(forum, nil);
                             }
                         }
                     }
                 }];
}

/**
 *  GET /movies/{movie_id}/watch
 */
+ (void)fetchWatch:(NSInteger)movieId
            liveId:(NSInteger)liveId
          callback:(void (^)(DLWatch *watch, NSError *error))callback {
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    
    // Required
    if (liveId) {
        parameters[@"live_id"] = @(liveId);
    }
    [parameters setValue:[NSString stringWithFormat:@"%ld",movieId] forKey:@"movie_id"];
    
    [DLAPIHelper requestWithMethod:GET
                         urlString:[NSString stringWithFormat:@"%@/%ld/watch", kAPIMovie, (long)movieId]
                        parameters:parameters
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", [NSString stringWithFormat:@"%@/%ld/watch", kAPIMovie, (long)movieId], error.localizedDescription);
                         callback(nil, error);
                     } else {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             DLWatch *watch = (DLWatch *) [DLWatch instanceFromDictionary:responseObject[@"watch"]];
                             if (NOT_NULL(watch)) {
                                 callback(watch, nil);
                             }
                         }
                     }
                 }];
    
}

/**
 '/users/movies'
 14.MyPage（視聴履歴）で使用 視聴履歴情報を、指定順に返却する
 
 @param page_no: ページングする際の何ページ目かを表す
 @param per_page: ページングする際の1ページの要素数
 @param order: 表示順（ASC：昇順、DESC：降順、 デフォルト:ASC）
 */
+ (void)fetchVideoHistoryByPageNumber:(NSInteger)pageNumber
                              perPage:(NSInteger)perPage
                            orderType:(DLOrderType)orderType
                             callback:(void (^)(NSMutableArray *movies, NSError *error))callback {
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    
    // Required
    parameters[@"page_no"] = [NSNumber numberWithInteger:pageNumber];
    parameters[@"per_page"] = [NSNumber numberWithInteger:perPage];
    
    // Optional
    switch (orderType) {
        case DLOrderTypeAscending:
        {
            parameters[@"order"] = @"ASC";
            break;
        }
        case DLOrderTypeDescending:
        {
            parameters[@"order"] = @"DESC";
            break;
        }
        default:
        {
            // nilだった場合は指定しない
            break;
        }
    }
    
    [DLAPIHelper requestWithMethod:GET
                         urlString:kAPIUsersMovies
                        parameters:parameters
                 completionHandler:^(id responseObject, NSError *error) {
                     if (!error) {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSMutableArray<DLMovie *> *movies = [DLMovie instancesFromDictionaries:responseObject[@"movies"]];
                             if (movies && movies.count > 0) {
                                 callback(movies, error);
                             }
                         }
                     }
                     else {
                         NSLog(@"fetchVideoHistoryByPageNumber - Error - @%@", error);
                         callback(nil, error);
                     }
                 }];
}

/**
 *  GET /movies/{movie_id}/point/used
 */
+ (void)fetchPointUsedHistory:(NSInteger)movieId
                   callback:(void (^)(NSObject *point, NSError *error))callback {
    
    [DLAPIHelper requestWithMethod:GET
                         urlString:[NSString stringWithFormat:@"%@/%ld/point/used", kAPIMovie, movieId]
                        parameters:nil
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", [NSString stringWithFormat:@"%@/%ld/point/used", kAPIMovie, movieId], error.localizedDescription);
                         callback([NSObject new], error);
                     } else {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSObject *point = [DLPoint instanceFromDictionary:responseObject[@"point_used"]];
                             if (NOT_NULL(point)) {
                                 callback(point, nil);
                             }
                         }
                     }
                 }];
}

#pragma mark /Live
/**
 *  GET /lives
 */
+ (void)fetchLives:(void (^)(NSMutableArray *lives))callback {

    [DLAPIHelper requestWithMethod:GET
                         urlString:kAPILive
                        parameters:nil
                 completionHandler:^(id responseObject, NSError *error) {
                     if (!error) {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSMutableArray *lives = [NSMutableArray new];
                             NSMutableArray *currentLives = [DLLive instancesFromDictionaries:responseObject[@"lives"]];
                             NSMutableArray *endedLives = [DLLive instancesFromDictionaries:responseObject[@"lives_end"]];
                             [lives addObjectsFromArray:[currentLives mutableCopy]];
                             [lives addObjectsFromArray:[endedLives mutableCopy]];
                             if (lives && lives.count > 0) {
                                 callback(lives);
                             }
                         }
                     }
                     else {
                         NSLog(@"fetchLives - Error - @%@", error);
                     }
                 }];
}

+ (void)fetchLive: (NSInteger) liveId callback:(void (^)(DLLive *live))callback {
    [DLAPIHelper fetchLives:^(NSMutableArray *lives) {
        for (DLLive *live in lives) {
            if (live.liveId.longValue == liveId) {
                callback(live);
            }
            callback(nil);
        }
    }];

}

/**
 *  GET /lives/{live_id}/movies
 */
+ (void)fetchMoviesWithLiveId:(NSInteger)liveId
                     callback:(void (^)(NSMutableArray<DLMovie *> *movies, NSError *error))callback {
    
    [DLAPIHelper requestWithMethod:GET
                         urlString:[NSString stringWithFormat:@"%@/%ld/movies", kAPILive, (long)liveId]
                        parameters:nil
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", [NSString stringWithFormat:@"%@/%ld/movies", kAPILive, (long)liveId], error.localizedDescription);
                         callback([NSMutableArray<DLMovie *> array], error);
                     } else {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSMutableArray<DLMovie *> *movies = [DLMovie instancesFromDictionaries:responseObject[@"movies"]];
                             if (NOT_NULL(movies)) {
                                 callback(movies, nil);
                             }
                         }
                     }
                 }];
    
}

/**
 *  GET /lives/promotions
 */
+ (void)fetchPromotionMoviesWithQuantity:(NSInteger)quantity
                                callback:(void (^)(NSMutableArray<DLPromotion*> *promotions, NSError *error))callback {
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"quantity"] = [NSString stringWithFormat:@"%ld", (quantity < 1 ? 1 : quantity)];
    
    [DLAPIHelper requestWithMethod:GET
                         urlString:[NSString stringWithFormat:@"%@/promotions", kAPILive]
                        parameters:parameters
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", [NSString stringWithFormat:@"%@/promotions", kAPILive], error.localizedDescription);
                         callback([NSMutableArray new], error);
                     } else {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSMutableArray *promotions = [DLPromotion instancesFromDictionaries:responseObject[@"promotions"]];
                             if (NOT_NULL(promotions)) {
                                 callback(promotions, nil);
                             }
                         }
                     }
                 }];
}

/**
 *  POST /lives/reserve
 */
+ (void)postLiveReserve:(NSInteger)liveId
               callback:(void (^)(NSObject *live, NSError *error))callback {
    [DLAPIHelper requestWithMethod:POST
                         urlString:API_SUFFIX_ADD(kAPILive, @"reserve")
                        parameters:@{@"live_id": @(liveId)}
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", API_SUFFIX_ADD(kAPILive, @"reserve"), error.localizedDescription);
                         callback([NSObject new], error);
                     } else {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSObject *live = [DLLive instanceFromDictionary:responseObject[@"live"]];
                             if (NOT_NULL(live)) {
                                 callback(live, nil);
                             }
                         }
                     }
                 }];
}

/**
 *  GET /lives/reserves
 */
+ (void)fetchLivesReserve:(void (^)(NSMutableArray *lives, NSError *error))callback {
    [DLAPIHelper requestWithMethod:GET
                         urlString:API_SUFFIX_ADD(kAPILive, @"reserves")
                        parameters:nil
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", API_SUFFIX_ADD(kAPILive, @"reserves"), error.localizedDescription);
                         callback([NSMutableArray new], error);
                     } else {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSMutableArray *lives = [DLLive instancesFromDictionaries:responseObject[@"lives"]];
                             if (NOT_NULL(lives)) {
                                 callback(lives, nil);
                             }
                         }
                     }
                 }];
}

/**
 *  GET /lives/timeschedules
 */
+ (void)fetchTimeSchedules:(void (^)(NSMutableArray *lives))callback {
    
    [DLAPIHelper.sesmgr GET:[NSString stringWithFormat:@"%@/timeschedules", kAPILive]
                 parameters:nil
                    success:^(NSURLSessionDataTask *task, id responseObject) {
                        if ([responseObject isKindOfClass:[NSDictionary class]]) {
                            NSMutableArray *lives = [DLLive instancesFromDictionaries:responseObject[@"lives"]];
                            if (lives && lives.count > 0) {
                                callback(lives);
                            }
                        }
                    }
                    failure:nil
     ];
}

#pragma mark /Forum

/**
 *  DELETE /comments/{comment_id}
 */
+ (void)deleteComment:(DLComment *)comment {
    [DLAPIHelper.sesmgr DELETE:API_SUFFIX_ADD(kAPIComment, comment.commentId)
                    parameters:nil
                       success:^(NSURLSessionDataTask *task, id responseObject) {
                           // TODO 通知かコールバックか
                       }
                       failure:^(NSURLSessionDataTask *task, NSError *error) {
                           
                       }];
}

/**
 *  PATCH /comments/{comment_id}
 */
+ (void)editComment:(DLComment *)comment
           withBody:(NSString *)body
           callback:(void (^)(BOOL flag))callback {
    [DLAPIHelper.sesmgr PATCH:API_SUFFIX_ADD(kAPIComment, comment.commentId)
                   parameters:@{@"body": body}
                      success:^(NSURLSessionDataTask *task, id responseObject) {
                          callback(YES);
                      }
                      failure:^(NSURLSessionDataTask *task, NSError *error) {
                          
                          callback(NO);
                      }];
}

/**
 *  GET /forums
 */
+ (void)fetchForumsWithPageNumber:(NSInteger)pageNumber
                         SortType:(DLForumGroupSortType)sortType
                         callback:(void (^)(NSMutableArray<DLForum*> *forums))callback {
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"page_no"] = [NSString stringWithFormat:@"%ld", pageNumber];
    params[@"per_page"] = [NSString stringWithFormat:@"%ld", DLConstPerPageCatalogDetailForums];
    params[@"sort_type"] = sortType == DLForumGroupSortTypeLatest ? @"popular" : @"latest";
    
    [DLAPIHelper requestWithMethod:GET
                         urlString:kAPIForum
                        parameters:params
                 completionHandler:^(id responseObject, NSError *error) {
                     if (!error) {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSMutableArray *forums = [DLForum instancesFromDictionaries:responseObject[@"forums"]];
                             if (forums && forums.count > 0) {
                                 callback(forums);
                             }
                         }
                     }
                     else {
                         NSLog(@"fetchForumsWithPageNumber - Error - @%@", error);
                     }
                 }];
}

/**
 *  DELETE /forums/{forum_id}
 */
+ (void)deleteForumByForumID:(NSInteger)forumId
                    callback:(void (^)(BOOL flag))callback {
    
    [DLAPIHelper.sesmgr POST:[NSString stringWithFormat:@"%@/%ld", kAPIForum, (long)forumId]
                  parameters:nil
                     success:^(NSURLSessionDataTask *task, id responseObject) {
                         if (callback) {
                             callback(YES);
                         }
                     } failure:^(NSURLSessionDataTask *task, NSError *error) {
                         if (callback) {
                             callback(NO);
                         }
                     }];
}

/**
 *  GET /forums/{forum_id}
 */
+ (void)fetchForumByForumID:(NSInteger)forumId
                   callback:(void (^)(NSObject *forum, NSError *error))callback {
    
    [DLAPIHelper requestWithMethod:GET
                         urlString:[NSString stringWithFormat:@"%@/%ld", kAPIForum, (long)forumId]
                        parameters:nil
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", [NSString stringWithFormat:@"%@/%ld", kAPIForum, (long)forumId], error.localizedDescription);
                         callback([NSObject new], error);
                     } else {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSObject *forum = [DLForum instanceFromDictionary:responseObject[@"forum"]];
                             if (NOT_NULL(forum)) {
                                 callback(forum, nil);
                             }
                         }
                     }
                 }];
}

/**
 *  GET /forums/{forum_id}/comments
 */
+ (void)fetchCommentsByForumID:(NSInteger)forumId
                    pageNumber:(NSInteger)pageNumber
                      callback:(void (^)(NSMutableArray *comments, NSError *error))callback {
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    
    // Required
    parameters[@"page_no"] = [NSNumber numberWithInteger:pageNumber];
    parameters[@"per_page"] = [NSNumber numberWithInteger:DLConstPerPageComments];
    
    [DLAPIHelper requestWithMethod:GET
                         urlString:[NSString stringWithFormat:@"%@/%ld/comments", kAPIForum, (long)forumId]
                        parameters:parameters
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", [NSString stringWithFormat:@"%@/%ld/comments", kAPIForum, (long)forumId], error.localizedDescription);
                         callback([NSMutableArray array], error);
                     } else {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSMutableArray *comments = [DLComment instancesFromDictionaries:responseObject[@"comments"]];
                             if (NOT_NULL(comments)) {
                                 callback(comments, nil);
                             }
                         }
                     }
                 }];
}

/**
 *  POST /forums/{forum_id}/comments
 */
+ (void)postComment:(NSString *)comment
      targetForumId:(NSInteger)forumId
           callback:(void (^)(BOOL flag , id responseObject))callback {
    [DLAPIHelper requestWithMethod:POST
                         urlString:[NSString stringWithFormat:@"%@/%ld/comments", kAPIForum, forumId]
                        parameters:@{@"body": comment}
                 completionHandler:^(id responseObject, NSError *error) {
                     if (!error) {
                         callback(YES,responseObject);
                     }
                     else {
                         callback(NO,nil);
                     }
                 }];
}

#pragma mark /User
+ (void)fetchUserWithCallback:(void (^)(NSError *error))callback {
    [DLAPIHelper requestProcessWithMethod:GET
                                urlString:kAPIUser
                               parameters:nil
                        completionHandler:^(id responseObject, NSError *error) {

                            if (error) {
                                NSLog(@"Error Occured!!!\n[%@]\nReason: %@", kAPIUser, error.localizedDescription);
                            } else {
                                NSDictionary *responseDic = responseObject;
                                [[DLUserDataManager sharedInstance] saveUserSessionToken:[responseDic valueForKeyPath:@"user.user"]];
                                [[DLUserDataManager sharedInstance] saveUserPointData:[responseDic valueForKeyPath:@"user.user_point"]];
                            }
                            callback(error);
                        }];
}

+ (void)postUserWithCallback:(void (^)(NSError *error))callback {
    [DLAPIHelper requestProcessWithMethod:POST
                                urlString:kAPIUser
                               parameters:nil
                        completionHandler:^(id responseObject, NSError *error) {
                            
                            if (error) {
                                NSLog(@"Error Occured!!!\n[%@]\nReason: %@", kAPIUser, error.localizedDescription);
                            } else {
                                NSDictionary *responseDic = responseObject;
                                [[DLUserDataManager sharedInstance] saveUserData:responseDic[@"user"]];
                            }
                            callback(error);
                        }];
}

+ (void)patchUser:(NSString *)name callback:(void (^)(NSError *error))callback {
    [DLAPIHelper requestWithMethod:PATCH
                         urlString:kAPIUser
                        parameters:@{@"name": name}
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", kAPIUser, error.localizedDescription);
                     }
                     callback(error);
                 }];
}

/**
 '/users/creates'
 14.MyPage画面で使用 userが作成したフォーラムの一覧を返す
 
 @param page_no: ページングする際の何ページ目かを表す
 @param per_page: ページングする際の1ページの要素数
 */
+ (void)fetchMyForumByPageNumber:(NSInteger)pageNumber
                         perPage:(NSInteger)perPage
                        callback:(void (^)(NSMutableArray *forums, NSError *error))callback {
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    
    // Required
    parameters[@"page_no"] = [NSNumber numberWithInteger:pageNumber];
    parameters[@"per_page"] = [NSNumber numberWithInteger:perPage];
    
    [DLAPIHelper requestWithMethod:GET
                         urlString:kAPIUsersMovies
                        parameters:parameters
                 completionHandler:^(id responseObject, NSError *error) {
                     if (!error) {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSMutableArray *forums = [DLForum instancesFromDictionaries:responseObject[@"forums"]];
                             if (forums && forums.count > 0) {
                                 callback(forums, error);
                             }
                         }
                     }
                     else {
                         NSLog(@"fetchMyForumByPageNumber - Error - @%@", error);
                         callback(nil, error);
                     }
                 }];
}

/**
 '/users/participations'
 14.MyPage画面で使用  userが参加中のフォーラム一覧を返却する  ※参加中には「あとで読む」も含まれる
 
 @param page_no: ページングする際の何ページ目かを表す
 @param per_page: ページングする際の1ページの要素数
 */
+ (void)fetchParticipateForumByPageNumber:(NSInteger)pageNumber
                                  perPage:(NSInteger)perPage
                                 callback:(void (^)(NSMutableArray *forums, NSError *error))callback {
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    
    // Required
    parameters[@"page_no"] = [NSNumber numberWithInteger:pageNumber];
    parameters[@"per_page"] = [NSNumber numberWithInteger:perPage];
    
    [DLAPIHelper requestWithMethod:GET
                         urlString:kAPIUsersParticipations
                        parameters:parameters
                 completionHandler:^(id responseObject, NSError *error) {
                     if (!error) {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSMutableArray *forums = [DLForum instancesFromDictionaries:responseObject[@"forums"]];
                             if (forums && forums.count > 0) {
                                 callback(forums, error);
                             }
                         }
                     }
                     else {
                         NSLog(@"fetchParticipateForumByPageNumber - Error - @%@", error);
                         callback(nil, error);
                     }
                 }];
}

/*
 * PATCH /users/svod/ios
 */
+ (void)patchSubscriptionVideoOnDemand:(NSString *)receipt
                      trialEndDateTime:(NSString *)trialEndDateTime
                              callback:(void (^)(NSInteger statusCode, NSError *error))callback {

    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"receipt"] = receipt;
    parameters[@"trial_end_datetime"] = trialEndDateTime;
    
    [DLAPIHelper requestWithMethod:PATCH
                         urlString:kAPISVOD
                        parameters:parameters
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", kAPISVOD, error.localizedDescription);
                     }
                     callback([responseObject integerValue], error);
                 }];
}

#pragma mark /Device
+ (void)fetchUsersWithDeviceID:(NSInteger)deviceID
                          UUID:(NSString *)UUID
                      callback:(void (^)(NSMutableArray *users))callback {
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    [parameters setObject:[NSNumber numberWithInteger:deviceID] forKey:@"device-id"];
    [parameters setObject:UUID forKey:@"uuid"];
    
    [DLAPIHelper.sesmgr GET:kAPIUser
                 parameters:parameters
                    success:^(NSURLSessionDataTask *task, id responseObject) {
                        NSLog(@">>>>>>>> %@", responseObject);
                        
                        if ([responseObject isKindOfClass:[NSDictionary class]]) {
                            NSMutableArray *user = [DLUser instancesFromDictionaries:responseObject[@"user"]];
                            if (user) {
                                callback(user);
                            }
                        }
                    }
                    failure:nil
     ];
}

+ (void)issueAuthcode: (NSNumber*) code
             callback: (void (^)(DLAuthcode *authcode))callback {

    [DLAPIHelper requestWithMethod:POST
                         urlString:kAPIAuthcodeIssue
                        parameters:@{
                                     @"code" : code
                                   }
                 completionHandler:^(id responseObject, NSError *error) {
                     if (!error) {
                         DLAuthcode *authcode = (DLAuthcode *)[DLAuthcode instanceFromDictionary:responseObject[@"authcode"]];
                         callback(authcode);
                     }
                     else {
                         callback(nil);
                         NSLog(@"issueAuthCode@%@", error);
                     }
                 }];
}

/*
 * POST /authcode/check
 */
+ (void)checkAuthcode: (NSString*) code
               userId: (NSString*) userId
            callback:(void (^)(NSMutableArray<DLUserDevice*> *userDevices, NSError *error))callback {

    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"code"] = code;
    parameters[@"user_id"] = userId;

    [DLAPIHelper requestWithMethod:POST
                         urlString:kAPIAuthcodeCheck
                        parameters:parameters
                 completionHandler:^(id responseObject, NSError *error) {
                     if (!error) {
                         NSMutableArray<DLUserDevice*> *userDevices = [DLUserDevice instancesFromDictionaries:responseObject[@"users_devices"]];
                         callback(userDevices, nil);
                     }
                     else {
                         NSLog(@"issueAuthCode@%@", error);
                         callback(nil, error);
                     }
                 }];
}

/*
 * PATCH /devices/relation
 */
+ (void)revisionDevicesRelation: (NSString*) code
                         userId: (NSString*) userId
                releaseDeviceId: (NSString*) releaseDeviceId
                       callback: (void (^)(BOOL isSuccess)) callback {

    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"code"] = code;
    parameters[@"user_id"] = userId;
    parameters[@"release_device_id"] = releaseDeviceId;

    [DLAPIHelper requestWithMethod:PATCH
                         urlString:kAPIDevicesRelationRevision
                        parameters:parameters
                 completionHandler:^(id responseObject, NSError *error) {
                     callback(error == nil);
                 }];
}


/*
 * POST /restore/check/ios
 */
+ (void)checkRestoreSubscription:(NSString *)receipt
                        callback:(void (^)(NSInteger statusCode, NSError *error))callback {
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"receipt"] = receipt;
    
    [DLAPIHelper requestWithMethod:POST
                         urlString:kAPICheckRestore
                        parameters:parameters
                 completionHandler:^(id responseObject, NSError *error) {
                     if (error) {
                         NSLog(@"Error Occured!!!\n[%@]\nReason: %@", kAPICheckRestore, error.localizedDescription);
                     }
                     callback([responseObject integerValue], error);
                 }];
}

#pragma mark /SeeLater

+ (void)fetchSeeLaterDatacallback:(void (^)(NSMutableArray *forums))callback {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"page_no"] = @"1";
    params[@"per_page"] = @"10000";
    [DLAPIHelper requestWithMethod:GET
                         urlString:kAPISeeLaters
                        parameters:params
                 completionHandler:^(id responseObject, NSError *error) {
                     if (!error) {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSMutableArray *forums = [DLForum instancesFromDictionaries:responseObject[@"forums"]];
                             if (forums && forums.count > 0) {
                                 callback(forums);
                             }
                             else {
                                 callback(nil);
                             }
                         }
                         else {
                             callback(nil);
                         }
                     }
                     else {
                         NSLog(@"fetchSeeLaterData - Error - @%@", error);
                         callback(nil);
                     }
                 }];
}

+ (void)addSeelaterDataWithForum:(DLForum *)forum callback:(void (^)(BOOL success))callback {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"forum_id"] = forum.forumId;
    [DLAPIHelper requestWithMethod:POST
                         urlString:kAPISeeLater
                        parameters:params
                 completionHandler:^(id responseObject, NSError *error) {
                     if (!error) {
                         callback(YES);
                     }
                     else {
                         NSLog(@"fetchSeeLaterData - Error - @%@", error);
                         callback(NO);
                     }
                 }];
}

+ (void)removeSeelaterDataWithForum:(DLForum *)forum callback:(void (^)(BOOL success))callback {
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"forum_id"] = forum.forumId;
    [DLAPIHelper requestWithMethod:DELETE
                         urlString:kAPISeeLater
                        parameters:params
                 completionHandler:^(id responseObject, NSError *error) {
                     if (!error) {
                         callback(YES);
                     }
                     else {
                         NSLog(@"fetchSeeLaterData - Error - @%@", error);
                         callback(NO);
                     }
                 }];
}

#pragma mark /Point

/**
 '/points/used'
 ポイントの消費履歴一覧を取得する
 */

+ (void)fetchPointsUsed:(void (^)(NSMutableArray *points))callback {
    [DLAPIHelper requestWithMethod:GET
                         urlString:kAPIPoint
                        parameters:nil
                 completionHandler:^(id responseObject, NSError *error) {
                     if (!error) {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSMutableArray *points = [DLPoint instancesFromDictionaries:responseObject[@"points_used"]];
                             if (points && points.count > 0) {
                                 callback(points);
                             }
                         }
                     }
                     else {
                         NSLog(@"fetchLives - Error - @%@", error);
                     }
                 }];
}

+ (void)checkAdsShowingTimesCallback:(void (^)(BOOL flag))callback; {
    [DLAPIHelper requestWithMethod:GET urlString:kAPIPointCheck parameters:nil completionHandler:^(id responseObject, NSError *error) {
        callback(!error);
    }];
}

+ (void)usePointWithMovieId:(NSNumber *)movieID reason:(NSString *)reason callback:(void (^)(BOOL flag))callback {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:movieID forKey:@"movie_id"];
    [param setValue:@"1" forKey:@"reason_code"];
    [param setValue:reason forKey:@"reason_text"];
    [DLAPIHelper requestWithMethod:DELETE
                         urlString:kAPIPoint
                        parameters:param
                 completionHandler:^(id responseObject, NSError *error) {
                     if (!error) {
                         callback(YES);
                     }
                     else {
                         callback(NO);
                     }
                 }];
}

+ (void)addPoint:(void (^)(DLPoint *point))callback {
    [DLAPIHelper requestWithMethod:POST
                         urlString:kAPIPoint
                        parameters:@{
                                @"reason_code":@"1",
                                @"reason_text":@"UnityAds動画広告視聴で付与"
                        }
                 completionHandler:^(id responseObject, NSError *error) {
        if (error) {
            [WINDOW makeToast:Localizer(@"forum_17")];
            callback(nil);
        } else {
            DLPoint *point = (DLPoint *) [DLPoint instanceFromDictionary:responseObject[@"user_point"][0]];
            if (point) {
                [WINDOW makeToast:[NSString stringWithFormat:Localizer(@"forum_16"), point.point]];
            }else{
                [WINDOW makeToast:Localizer(@"forum_17")];
            }
            callback(point);
        }
    }];
}

#pragma mark /Participation
#pragma mark /Other
+ (void)loadReportTypescallback:(void (^)(NSMutableArray *types))callback {
    [DLAPIHelper requestWithMethod:GET
                         urlString:kAPIReportTypes
                        parameters:nil
                 completionHandler:^(id responseObject, NSError *error) {
                     if (!error) {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSMutableArray *types = [DLReportType instancesFromDictionaries:responseObject[@"report_categories"]];
                             if (types && types.count > 0) {
                                 callback(types);
                             }
                             else {
                                 callback(nil);
                             }
                         }
                     }
                     else {
                         callback(nil);
                     }
                 }];
}

+ (void)reportComment:(DLComment *)comment reportType:(DLReportType *)type fronm:(DLForum *)froum callback:(void (^)(BOOL flag))callback {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:froum.forumId forKey:@"forum_id"];
    [param setValue:comment.commentId forKey:@"comment_id"];
    [param setValue:type.categoryID forKey:@"report_category_id"];
    [DLAPIHelper requestWithMethod:POST urlString:kAPIReport parameters:param completionHandler:^(id responseObject, NSError *error) {
        if (!error) {
            callback(YES);
        }
        else {
            callback(NO);
        }
    }];
}

//TODO: 消す予定
+ (void)fetchMovies:(NSArray *)movieIds callback:(void (^)(NSMutableArray *movies))callback {
    [DLAPIHelper.sesmgr GET:kAPIMovie
                 parameters:@{@"ids": movieIds}
                    success:^(NSURLSessionDataTask *task, id responseObject) {
                        if ([responseObject isKindOfClass:[NSDictionary class]]) {
                            NSMutableArray *movies = [DLMovie instancesFromDictionaries:responseObject[@"movies"]];
                            if (movies && movies.count > 0) {
                                callback(movies);
                            }
                        }
                    }
                    failure:^(NSURLSessionDataTask *task, NSError *error) {
                        NSLog(@"%@, %@", task, error);
                    }
     ];
}


#pragma mark - Private Methods

+ (void)getUserWithCallback:(void (^)(NSError *error))callback {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        dispatch_semaphore_wait(DLAPIHelper.sharedManager.semaphore, DISPATCH_TIME_FOREVER);
        
        // メインキューに戻さないと、描画が止まってしまう
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [DLAPIHelper requestProcessWithMethod:GET
                                        urlString:kAPIUser
                                       parameters:nil
                                completionHandler:^(id responseObject, NSError *error) {
                                    
                                    if (error) {
                                        NSLog(@"Error Occured!!!\n[%@]\nReason: %@", kAPIUser, error.localizedDescription);
                                    } else {
                                        NSDictionary *responseDic = responseObject;
                                        [[DLUserDataManager sharedInstance] saveUserSessionToken:[responseDic valueForKeyPath:@"user.user"]];
                                        [[DLUserDataManager sharedInstance] saveUserPointData:[responseDic valueForKeyPath:@"user.user_point"]];
                                    }
                                    dispatch_semaphore_signal(DLAPIHelper.sharedManager.semaphore);
                                    callback(error);
                                }];
        });
    });
}

// 通常はこのメソッドを使
// 用してAPIをリクエストする
+ (void)requestWithMethod:(DLAPIMethod)method
                urlString:(NSString *)urlString
               parameters:(NSDictionary *)parameters
        completionHandler:(void (^)(id responseObject, NSError *error))completionHandler {
    if([DLAPIHelper sharedManager].isFetchingUser){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [DLAPIHelper requestWithMethod:method
                                 urlString:urlString
                                parameters:parameters
                         completionHandler:completionHandler];
        });
    }else{
        [DLAPIHelper requestProcessWithMethod:method
                                    urlString:urlString
                                   parameters:[parameters mutableCopy]
                            completionHandler:^(id responseObject, NSError *error) {
                                if([responseObject isKindOfClass:[NSNumber class]] && 401 == [responseObject intValue]){
                                    if(![DLAPIHelper sharedManager].isFetchingUser){
                                        [DLAPIHelper sharedManager].isFetchingUser = YES;
                                        [DLAPIHelper getUserWithCallback:^(NSError *error2) {
                                            [DLAPIHelper sharedManager].isFetchingUser = NO;
                                        }];
                                        [DLAPIHelper requestWithMethod:method
                                                             urlString:urlString
                                                            parameters:parameters
                                                     completionHandler:completionHandler];
                                    }
                                }else{
                                    completionHandler(responseObject, error);
                                }
                            }];
    }
}

// POST /users の場合のみこのメソッドを使用してAPIをリクエストする
+ (void)requestProcessWithMethod:(DLAPIMethod)method
                       urlString:(NSString *)urlString
                      parameters:(NSMutableDictionary *)parameters
               completionHandler:(void (^)(id responseObject, NSError *error))completionHandler {
    
    AFHTTPSessionManager *manager = DLAPIHelper.sesmgr;
    
    if (method == POST && [urlString isEqualToString:kAPIUser]) {
        [manager.requestSerializer setValue:[DLDeviceUtil getAppVersion] forHTTPHeaderField:@"client-version"];
        [manager.requestSerializer setValue:[DLDeviceUtil getDeviceName] forHTTPHeaderField:@"device"];
        [manager.requestSerializer setValue:[DLDeviceUtil getDeviceSystemVersion] forHTTPHeaderField:@"os-version"];
        // TODO: 一覧の値が決まり次第Enumで宣言し直す
        [manager.requestSerializer setValue:@"231" forHTTPHeaderField:@"country-id"];
        [manager.requestSerializer setValue:[DLDeviceUtil getCurrentLocaleIdentifier] forHTTPHeaderField:@"locale"];
        [manager.requestSerializer setValue:[DLDeviceUtil getUUID] forHTTPHeaderField:@"uuid"];
        [manager.requestSerializer setValue:[DLDeviceUtil getUIID] forHTTPHeaderField:@"backup-id"];

    } else {
        
        // TODO: 一覧の値が決まり次第Enumで宣言し直す
        [manager.requestSerializer setValue:@"231" forHTTPHeaderField:@"country-id"];
        [manager.requestSerializer setValue:[DLDeviceUtil getUUID] forHTTPHeaderField:@"uuid"];
        [manager.requestSerializer setValue:[[DLUserDataManager sharedInstance].user.deviceId stringValue] forHTTPHeaderField:@"device-id"];
        [manager.requestSerializer setValue:[DLUserDataManager sharedInstance].user.sessionToken forHTTPHeaderField:@"session-token"];
    }
    
    void (^successBlock)(NSURLSessionDataTask * task, id _Nonnull responseObject) = ^(NSURLSessionDataTask * _Nonnull task, id _Nonnull responseObject) {
        NSLog(@"\nResponseDic:\n%@", responseObject);
        completionHandler(responseObject, nil);
    };
    
    void (^failureBlock)(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) = ^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        NSHTTPURLResponse* response = (NSHTTPURLResponse*)task.response;
        if(response.statusCode == 401){
            completionHandler(@401, error);
        }else{
            completionHandler(nil, error);
        }
    };
    
    switch (method) {
        case POST:
            [manager POST:urlString parameters:parameters success:successBlock failure:failureBlock];
            break;
        case PATCH:
            [manager PATCH:urlString parameters:parameters success:successBlock failure:failureBlock];
            break;
        case DELETE:
            [manager DELETE:urlString parameters:parameters success:successBlock failure:failureBlock];
            break;
        default:
            [manager GET:urlString parameters:parameters success:successBlock failure:failureBlock];
            break;
    }
}

+ (void) printCloudFrontSigningQueryParams:(NSArray*) cookies{
    // デバッグ用関数
    NSMutableString *signQuery= NSMutableString.string;
    for (NSHTTPCookie *cookie in cookies) {
        [signQuery appendString: signQuery.length == 0 ? @"?" : @"&"];
        [signQuery appendString:[cookie.name substringFromIndex:11]];
        [signQuery appendString:@"="];
        [signQuery appendString:cookie.value];
    }
    
    [signQuery appendFormat:@"&Expires=%ld", (long)[[cookies[0] expiresDate] timeIntervalSince1970]];
    NSLog(@"CloudFront: %@", signQuery);
}

@end
