//
//  DLPlayerForumDetailViewController.m
//  AniLive
//
//  Created by isaoeka on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPlayerForumDetailViewController.h"
#import "DLPlayerOptionBarView.h"
#import "DLForumTitleView.h"
#import "DLSeeLaterButton.h"
#import "DLCommentTableView.h"
#import "DLAppFrameManager.h"
#import "DLForum.h"
#import "DLComment.h"

@interface DLPlayerForumDetailViewController () <DLForumTitleViewDelegate>

@property (nonatomic, strong) DLForumTitleView *forumTitleView;
@property (nonatomic, strong) DLCommentTableView *commentTableView;
@property (nonatomic, strong) NSMutableArray<DLComment *> *comments;

@end

@implementation DLPlayerForumDetailViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

#pragma mark - Override Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - API

- (void)loadCommentData:(DLForum *)forum {
    // TODO perPage=0だとstatus400になるので適切なローディング機構に修正する
    @weakify(self)
    [DLAPIHelper fetchCommentsByForumID:[forum.forumId integerValue]
                             pageNumber:1
                               callback:^(NSMutableArray<DLComment *> *comments, NSError *error) {
                                   @strongify(self)
                                   [comments enumerateObjectsUsingBlock:^(DLComment *comment, NSUInteger idx, BOOL *stop) {
                                       [self.comments addObject:comment];
                                   }];
                                   
                                   [self.commentTableView loadComments:comments];
                               }];
}

#pragma mark - Private Methods

- (void)initView {
    self.view.backgroundColor = [UIColor blueColor];
    
#pragma mark Player Option Bar View

    self.playerOptionBarView = [[DLPlayerOptionBarView alloc] initWithType:DLPlayerOptionViewForumDetail];
    [self.view addSubview:self.playerOptionBarView];
    
#pragma mark Player Forum Title View

    self.forumTitleView = [DLForumTitleView new];
    self.forumTitleView.delegate = self;
    [self.view addSubview:self.forumTitleView];
    
#pragma mark Comment Table View
    
    self.commentTableView = [DLCommentTableView new];
    [self.view addSubview:self.commentTableView];
}

- (void)updateFrame {
    
    self.playerOptionBarView.frame = CGRectMake(0,
                                                0,
                                                self.view.width,
                                                kPlayerOptionViewHeight);
    
    self.forumTitleView.frame = CGRectMake(0,
                                           self.playerOptionBarView.maxY,
                                           self.view.width,
                                           kPlayerForumTitleViewHeight);
    
    self.commentTableView.frame = CGRectMake(0,
                                             self.forumTitleView.maxY,
                                             self.view.width,
                                             self.view.height - self.forumTitleView.maxY);
}

#pragma mark - Public Methods

- (void)setForum:(DLForum *)forum {
    
    [self.forumTitleView setForum:forum];
    
    [self loadCommentData:forum];
}

#pragma mark - DLForumTitleViewDelegate

- (BOOL)forumTitleViewStarButtonDidTap {
    
    // XXX: 仮で入れ替わるようにしています
    return [[self.forumTitleView getSeeLaterButton] isSeeLater] ? NO : YES;
}

@end
