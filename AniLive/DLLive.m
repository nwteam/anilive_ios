//
// Created by Kotaro Itoyama on 2016/10/09.
// Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLLive.h"

@interface DLLive ()
@property(readwrite, nonatomic, strong) NSNumber *liveId;
@property(readwrite, nonatomic, strong) NSString *title;
@property(readwrite, nonatomic, strong) NSDate *broadcastOpen;
@property(readwrite, nonatomic, strong) NSDate *broadcastClose;
@property(readwrite, nonatomic, strong) NSString *image;
@property(readwrite, nonatomic, strong) NSString *highlightMovieUrl;
@property(readwrite, nonatomic, strong) NSNumber *totalViews;
@property(readwrite, nonatomic, strong) NSNumber *totalForumsCount;
@property(readwrite, nonatomic, assign) BOOL isReserved;

@end

@implementation DLLive

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }

    self.liveId = CHECK_NULL_DATA_NUMBER(attributes[@"live_id"]);
    self.title = CHECK_NULL_DATA_STRING(attributes[@"title"]);
    self.broadcastOpen = [DLAPIHelper dateFromString:attributes[@"broadcast_open"]];
    self.broadcastClose = [DLAPIHelper dateFromString:attributes[@"broadcast_close"]];
    self.image = CHECK_NULL_DATA_STRING(attributes[@"image"]);
    self.highlightMovieUrl = [CHECK_NULL_DATA_STRING(attributes[@"highlight_movie_url"]) stringByReplacingOccurrencesOfString:@"http://" withString:@"https://"];
    self.totalViews = CHECK_NULL_DATA_NUMBER(attributes[@"popularity"]);
    self.totalForumsCount = CHECK_NULL_DATA_NUMBER(attributes[@"total_forums"]);

    NSNumber *isBoolValue = 0;
    if (NOT_NULL(attributes[@"is_reserved"])) {
        isBoolValue = CHECK_NULL_DATA_NUMBER(attributes[@"is_reserved"]);
    }
        self.isReserved = isBoolValue.boolValue;
    
    return self;
}

- (BOOL)isOnAir {
    //TODO: 時間を見て判定を返すように修正
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate* currentDate = [NSDate date];
    
    if ([currentDate compare:self.broadcastOpen] == NSOrderedAscending)
        return NO;
    
    if ([currentDate compare:self.broadcastClose] == NSOrderedDescending)
        return NO;
    
    return YES;
}

@end
