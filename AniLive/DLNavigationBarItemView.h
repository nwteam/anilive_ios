//
//  DLNavigationBarItemView.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/23.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLHighlightedButton.h"

const static CGFloat kDefaultItemViewSide = 36;

@interface DLNavigationBarItem : NSObject

@property (readonly, nonatomic, strong) NSString *title;
@property (readonly, nonatomic, strong) UIColor *enabledTitleColor;
@property (readonly, nonatomic, strong) UIColor *disabledTitleColor;
@property (readonly, nonatomic, strong) UIImage *image;
@property (readonly, nonatomic, assign) CGFloat itemWidth;

- (instancetype)initWithImage:(UIImage *)image;

- (instancetype)initWithTitle:(NSString *)title
        withEnabledTitleColor:(UIColor *)enabledTitleColor;

- (instancetype)initWithTitle:(NSString *)title
        withEnabledTitleColor:(UIColor *)enabledTitleColor
       withDisabledTitleColor:(UIColor *)disabledTitleColor;

@end

@interface DLNavigationBarItemView : UIView

@property (readonly, nonatomic, strong) UILabel *label;
@property (readonly, nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) DLHighlightedButton *button;
@property (readonly, nonatomic, strong) DLNavigationBarItem *item;

- (instancetype)initWithItem:(DLNavigationBarItem *)item;

@end
