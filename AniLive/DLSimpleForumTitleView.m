//
//  DLSimpleForumTitleView.m
//  AniLive
//
//  Created by isaoeka on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLSimpleForumTitleView.h"
#import "DLSeeLaterButton.h"
#import "DLForum.h"

typedef NS_ENUM(NSInteger, SimpleForumTitleViewTag) {
    TitleLabelTag = 100,
    StarButtonTag,
};

@interface DLSimpleForumTitleView () <DLSeeLaterButtonDelegate>

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) DLSeeLaterButton *seeLaterButton;

@end

@implementation DLSimpleForumTitleView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
}

#pragma mark - Private Methods

- (void)initView {
    self.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkGray];
    
#pragma mark Title Label
    self.titleLabel = [self createTitleLabelWithFont:BASE_FONT_BOLD(15)];
    self.titleLabel.tag = TitleLabelTag;
    self.titleLabel.userInteractionEnabled = YES;
    self.titleLabel.numberOfLines = 1;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    self.titleLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.titleLabel];
    
#pragma mark Star Btn
    self.seeLaterButton = [DLSeeLaterButton new];
    self.seeLaterButton.delegate = self;
    self.seeLaterButton.tag = StarButtonTag;
    [self addSubview:self.seeLaterButton];
    
}

- (void)updateFrame {
    
#pragma mark Star Btn
    CGFloat starBtnSize = 20;
    self.seeLaterButton.frame = CGRectMake(self.width - DLConstDefaultMargin - starBtnSize,
                                           DLConstDefaultMargin,
                                           starBtnSize,
                                           starBtnSize);
    
#pragma mark Title Label
    [self.titleLabel sizeToFit];
    self.titleLabel.frame = CGRectMake(DLConstDefaultMargin,
                                       DLConstDefaultMargin,
                                       self.seeLaterButton.minX - (DLConstDefaultMargin * 2),
                                       self.titleLabel.height);
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    switch (touch.view.tag) {
        case TitleLabelTag: {
            if ([self.delegate respondsToSelector:@selector(simpleForumTitleViewTitleLabelDidTap)]) {
                [self.delegate simpleForumTitleViewTitleLabelDidTap];
            }
            break;
        }
        default:
            break;
    }
}

- (UILabel *)createTitleLabelWithFont:(UIFont *)font {
    UILabel *label = [UILabel new];
    label.backgroundColor = [UIColor clearColor];
    label.font = font;
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.lineBreakMode = NSLineBreakByTruncatingTail;
    return label;
}

#pragma mark - Public Methods

- (void)setForum:(DLForum *)forum {
    self.titleLabel.text = forum.title;
    
    [self updateFrame];
}

- (DLSeeLaterButton *)getSeeLaterButton {
    return self.seeLaterButton;
}

#pragma mark - See Later Btn Delegate

- (BOOL)seeLaterButtonDidTap:(DLSeeLaterButton *)button {
    
    if ([self.delegate respondsToSelector:@selector(simpleForumTitleViewStarButtonDidTap)]) {
        BOOL highlighted = [self.delegate simpleForumTitleViewStarButtonDidTap];
        return highlighted;
    }
    
    return NO;
}

@end
