//
//  DLContinueToPlaySeeLaterCell.h
//  AniLive
//
//  Created by volcano on 1/4/17.
//  Copyright © 2017 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLForum.h"

@interface DLContinueToPlaySeeLaterCell : UITableViewCell
+ (UINib *)getDefaultNib;
+ (CGFloat)getCellHeight;

- (void)dispalyForum:(DLForum *)forum;

@end
