//
//  DLSplashViewController.m
//  AniLive
//
//  Created by Loyal Lauzier on 12/27/16.
//  Copyright © 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLSplashViewController.h"
#import "DLMainTabBarController.h"
#import "DLNavigationController.h"
#import "DLWebViewController.h"
#import "DLAppDelegate.h"
#import "DLAPIhelper.h"
#import "DLUserDataManager.h"
#import "NSString+Category.h"
#import "UITapGestureRecognizer+Category.h"

#import "DLNotice.h"
#import "DLWork.h"
#import "DLTransitionManager.h"

#import "DLNotificationService.h"
#import <UIImageView+WebCache.h>

@interface DLSplashViewController ()

@property (weak, nonatomic) IBOutlet UIView *viewSplash;
@property (weak, nonatomic) IBOutlet UIView *viewGif;
@property (weak, nonatomic) IBOutlet UIButton *btnStart;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgGif;

- (IBAction)tapBtnStart:(id)sender;

@property (nonatomic, strong) NSUserDefaults *userDefaults;
@property (nonatomic, assign) BOOL isTerms;

@end

@implementation DLSplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.userDefaults = [NSUserDefaults standardUserDefaults];
    
    // initialize
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initialize {
    if ([self.userDefaults objectForKey:DLConstKeyIsFirstLaunch]) {
        @weakify(self)
        [self loadGifView:^(NSError *error) {
            @strongify(self)
            if (error) {
                NSLog(@"Load GIF Error Occured!!!\nReason: %@", error.localizedDescription);
            }
            [self postUser];
        }];
    } else {
        [self loadSplashView];
    }
}

- (void)dealloc {
    FUNC_LOG
}

#pragma marker - load Splash View
- (void)loadSplashView {
    self.viewSplash.hidden = NO;
    self.viewGif.hidden = YES;

    // btn start
    [self.btnStart setTitle:Localizer(@"Start") forState:UIControlStateNormal];
    [self.btnStart setTitleColor:[UIColor colorWithHex:DLConstColorCodeYellow] forState:UIControlStateNormal];
    self.btnStart.backgroundColor = [UIColor clearColor];
    self.btnStart.layer.masksToBounds = YES;
    self.btnStart.layer.cornerRadius = 3.0;
    self.btnStart.layer.borderWidth = 1.0;
    self.btnStart.layer.borderColor = [UIColor colorWithHex:DLConstColorCodeYellow].CGColor;
    
    // terms of service and privacy policy
    self.descriptionLabel.numberOfLines = 0;
    self.descriptionLabel.userInteractionEnabled = YES;
    self.descriptionLabel.attributedText = [self getDescriptionLabelAttributedString];
    
    UITapGestureRecognizer *labelSingleTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                                             action:@selector(labelDidTap:)];
    labelSingleTapGesture .numberOfTapsRequired = 1;
    labelSingleTapGesture .numberOfTouchesRequired = 1;
    [self.descriptionLabel addGestureRecognizer:labelSingleTapGesture ];
    
    // Rights Label
    self.rightsLabel.text = Localizer(@"DescriptionRights");
}

- (NSAttributedString *)getDescriptionLabelAttributedString {
    
    NSMutableAttributedString *labelString = [[NSMutableAttributedString alloc]init];
    
    [NSString attributeString:labelString withString:Localizer(@"SplashDescriptionPrefix") withFont:BASE_FONT(11) withColor:[UIColor whiteColor]];
    [NSString attributeString:labelString withString:[NSString stringWithFormat:@" %@", Localizer(@"TermsOfService")] withFont:BASE_FONT(11) withColor:[UIColor colorWithHex:DLConstColorCodeYellow]];
    [NSString attributeString:labelString withString:[NSString stringWithFormat:@" %@", Localizer(@"And")] withFont:BASE_FONT(11) withColor:[UIColor whiteColor]];
    [NSString attributeString:labelString withString:[NSString stringWithFormat:@" %@", Localizer(@"Privacy Policy")] withFont:BASE_FONT(11) withColor:[UIColor colorWithHex:DLConstColorCodeYellow]];
    [NSString attributeString:labelString withString:Localizer(@"Dot") withFont:BASE_FONT(11) withColor:[UIColor whiteColor]];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    [NSString attributeParagraphStyle:labelString withParagraphStyle:paragraphStyle];
    
    return labelString;
}

#pragma marker - load Gif View
- (void)loadGifView:(void (^)(NSError *error))callback {
    self.viewSplash.hidden = YES;
    self.viewGif.hidden = NO;

    // load gif
    NSString *path = [[NSBundle mainBundle] pathForResource:DLConstImageSplashGIF ofType:@"gif"];
    NSURL *url = [[NSURL alloc] initFileURLWithPath:path];
    [self.imgGif sd_setImageWithURL:url completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        callback(error);
    }];
}

- (void)labelDidTap:(UITapGestureRecognizer *)recognizer {
    
    NSRange restoreRange = [self.descriptionLabel.text rangeOfString:Localizer(@"TermsOfService")];
    NSRange privacyPolicyRange = [self.descriptionLabel.text rangeOfString:Localizer(@"Privacy Policy")];
    
    if ([recognizer didTapAttributedTextInLabel:self.descriptionLabel withTargetRange:restoreRange]) {
        
        NSLog(@"Terms Of Service");
        DLWebViewController *controller = [[DLWebViewController alloc]initWithURLString:DLConstURLStringTerms];
        controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:controller animated:YES completion:nil];
    } else if ([recognizer didTapAttributedTextInLabel:self.descriptionLabel withTargetRange:privacyPolicyRange]) {
        
        NSLog(@"Privacy Policy");
        DLWebViewController *controller = [[DLWebViewController alloc]initWithURLString:DLConstURLStringPrivacyPolicy];
        controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:controller animated:YES completion:nil];
    }
}

#pragma marker - present to main viewcontroller
- (IBAction)tapBtnStart:(id)sender {
    [self postUser];
}

#pragma marker - go to LiveViewController
- (void)goToLiveViewController {
    
    // 初回起動判定
    [self.userDefaults setBool:NO forKey:DLConstKeyIsFirstLaunch];
    [self.userDefaults synchronize];
    
    // RootViewControllerを切り替える
    DLAppDelegate *appDelegate = (DLAppDelegate *)[[UIApplication sharedApplication] delegate];
    DLMainTabBarController *rootTabBarController = [[DLMainTabBarController alloc]init];
    DLNavigationController *rootNavigationController = [[DLNavigationController alloc] initWithRootViewController:rootTabBarController];
    appDelegate.rootTabBarController = rootTabBarController;
    appDelegate.rootNavigationController = rootNavigationController;
    appDelegate.window.rootViewController = rootNavigationController;
}

#pragma marker - post user
- (void)postUser {
    
#ifdef LOCAL_MOCK
    // モックにはユーザーデータがないのでスルー
    [self goToLiveViewController];
#else
    @weakify(self)
    [DLAPIHelper postUserWithCallback:^(NSError *error) {
        @strongify(self)
        if (error) {
            // TODO: 画面にエラー表示
        } else {
            [self goToLiveViewController];
            if (self.notificationTransitionDict != nil) {
                DLNotice* notice = [DLNotice initWithAttributes:self.notificationTransitionDict];
                [DLNotificationService transitScreenByNotification:notice callback:^{}];
            }
        }
    }];
#endif
}

#pragma mark - Rotate Config

- (BOOL)shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if (IS_IPAD) {
        return UIInterfaceOrientationMaskAll;
    } else {
        return UIInterfaceOrientationMaskPortrait;
    }
}

@end
