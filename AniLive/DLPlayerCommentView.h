//
//  DLPlayerCommentView.h
//  AniLive
//
//  Created by isaoeka on 2016/12/25.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

static const CGFloat kGrowingCommentViewHeight = 45;

@protocol DLPlayerCommentViewDelegate, DLPlayerCommentViewSenderDelegate;

@interface DLPlayerCommentView : UIView

@property (nonatomic, weak) id<DLPlayerCommentViewDelegate> delegate;
@property (nonatomic, weak) id<DLPlayerCommentViewSenderDelegate> commentEditDelegate;

@property (nonatomic, strong) UITextView *growingTextView;

- (void)resignComment:(BOOL)clearComment;

@end

@protocol DLPlayerCommentViewDelegate <NSObject>

@optional

- (void)playerCommentView:(DLPlayerCommentView *)growingTextView
         willChangeHeight:(float)height;

@end

@protocol DLPlayerCommentViewSenderDelegate <NSObject>

- (void)didEndEditComment:(NSString *)comment;

@end
