//
//  DLPlayerCreateForumViewController.h
//  AniLive
//
//  Created by isaoeka on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DLPlayerOptionBarView;

@interface DLPlayerCreateForumViewController : UIViewController

@property (nonatomic, strong) DLPlayerOptionBarView *playerOptionBarView;

- (void)sendForumByMovieID:(NSInteger)movieID
           forumPinnedTime:(NSString *)forumPinnedTime;

@end
