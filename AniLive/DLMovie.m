//
//  DLMovie.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/17.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLMovie.h"
#import "DLLive.h"
#import "DLForum.h"

@interface DLMovie ()

@property (readwrite, nonatomic, strong) NSNumber *workId;
@property (readwrite, nonatomic, strong) NSNumber *movieId;
@property (readwrite, nonatomic, strong) NSString *title;
@property (readwrite, nonatomic, strong) NSNumber *priority;
@property (readwrite, nonatomic, strong) NSNumber *chapter;
@property (readwrite, nonatomic, strong) NSString *youtubeEmbed;
@property (readwrite, nonatomic, strong) NSDate *releaseOpen;
@property (readwrite, nonatomic, strong) NSDate *releaseClose;
@property (readwrite, nonatomic, strong) NSString *image;
@property (readwrite, nonatomic, strong) NSNumber *salesType;
@property (readwrite, nonatomic, assign) NSNumber *pointFlag;
@property (readwrite, nonatomic) DLMovieType movieType;
@property (readwrite, nonatomic, strong) NSNumber *totalForums;
@property (readwrite, nonatomic, assign) BOOL isWatched;
@property (readwrite, nonatomic, strong) NSDate *watched;
@property (readwrite, nonatomic, strong) NSNumber *playTime;

@end

@implementation DLMovie

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.workId = CHECK_NULL_DATA_NUMBER(attributes[@"work_id"]);
    self.movieId = CHECK_NULL_DATA_NUMBER(attributes[@"movie_id"]);
    self.title = CHECK_NULL_DATA_STRING(attributes[@"title"]);
    self.priority = CHECK_NULL_DATA_NUMBER(attributes[@"priority"]);
    self.chapter = CHECK_NULL_DATA_NUMBER(attributes[@"chapter"]);
    self.youtubeEmbed = CHECK_NULL_DATA_STRING(attributes[@"youtube_embed"]);
    self.releaseOpen = [DLAPIHelper dateFromString:attributes[@"release_open"]];
    self.releaseClose = [DLAPIHelper dateFromString:attributes[@"release_close"]];
    self.image = CHECK_NULL_DATA_STRING(attributes[@"image"]);
    self.salesType = CHECK_NULL_DATA_NUMBER(attributes[@"sales_type"]);
    self.pointFlag = CHECK_NULL_DATA_NUMBER(attributes[@"point_flag"]);
    NSNumber *rawMovieType = CHECK_NULL_DATA_NUMBER(attributes[@"movie_type"]);
    self.movieType = rawMovieType ? (DLMovieType) rawMovieType.integerValue : UNSPECIFIED;
    self.totalForums = CHECK_NULL_DATA_NUMBER(attributes[@"total_forums"]);
    self.isWatched = (CHECK_NULL_DATA_NUMBER(attributes[@"is_watched"])).boolValue;
    self.watched = [DLAPIHelper dateFromString:attributes[@"watched"]];
    if (NOT_NULL(attributes[@"play_time"])) {
        self.playTime = [NSNumber numberWithInteger:[DLAPIHelper timeIntervalFromString:attributes[@"play_time"]]];
    } else {
        self.playTime = @0;
    }

    return self;
}

- (NSUInteger)getPointNeedToView {
    return DLConstNecessaryPointAmountOfWatchMovie;
}

- (instancetype) initWithForum: (DLForum *) forum{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.movieId = forum.movieId;
    self.title = forum.movieTitle;
    self.chapter = forum.chapter;
    self.workId = forum.workID;
    self.salesType = forum.salesType;
    self.pointFlag = forum.pointFlag;
    self.isWatched = NO;
    self.playTime = nil;
    self.movieType = NORMAL;
    self.image = nil;

    NSDateComponents *comps = NSDateComponents.new;
    [comps setHour:12];
    [comps setDay:30];
    [comps setMonth:9];
    [comps setYear:1980];
    self.releaseOpen = [[NSCalendar currentCalendar] dateFromComponents:comps];
    [comps setYear:2030];
    self.releaseClose = [[NSCalendar currentCalendar] dateFromComponents:comps];
    self.totalForums = nil;
    self.priority = nil;
    self.watched = NSDate.date;
    self.youtubeEmbed = nil;
    return self;
}
@end
