//
//  DLPointHistoryViewController.h
//  AniLive
//
//  Created by 市川 俊介 on 2016/12/24.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DLAPIHelper.h"
#import "DLNavigationBarView.h"

@interface DLPointHistoryViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, DLNavigationBarViewDelegate>

@end
