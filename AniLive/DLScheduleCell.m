//
//  DLScheduleCell.m
//  AniLive
//
//  Created by Loyal Lauzier on 12/21/16.
//  Copyright © 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLScheduleCell.h"

@implementation DLScheduleCell
@synthesize itemIdx;
@synthesize isNoti;
@synthesize btnNoti;
@synthesize itemPosition;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    isNoti = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)tapBtnNoti:(id)sender {
    [btnNoti setBackgroundImage:[UIImage imageNamed:DLConstIconNotificationsOn] forState:UIControlStateNormal];
    [self.delegate didTapNotiButtonItemID:itemPosition];
}

- (IBAction)tapBtnBG:(id)sender {
//    [self.delegate didTapSelectButtonItemID:itemIdx IsNoti:isNoti];
//    [self.delegate didTapButtonItemID:itemIdx];
}
@end
