//
//  DLUser.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/12/28.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLUser.h"

@interface DLUser ()
@property(readwrite, nonatomic, strong) NSString *name;
@property(readwrite, nonatomic, strong) NSString *sessionToken;
@property(readwrite, nonatomic, strong) NSNumber *deviceId;
@property(readwrite, nonatomic, strong) NSNumber *userStatus;
@property(readwrite, nonatomic, strong) NSNumber *billingType;
@property(readwrite, nonatomic, strong) NSDate *trialEndDatetime;
@property(readwrite, nonatomic, strong) NSNumber *point;
@property(readwrite, nonatomic, strong) NSDate *pointLimit;
@end

@implementation DLUser

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.name = CHECK_NULL_DATA_STRING(attributes[@"name"]);
    self.sessionToken= CHECK_NULL_DATA_STRING(attributes[@"session_token"]);
    self.deviceId = CHECK_NULL_DATA_NUMBER(attributes[@"device_id"]);
    self.userStatus = CHECK_NULL_DATA_NUMBER(attributes[@"user_status"]);
    self.billingType = CHECK_NULL_DATA_NUMBER(attributes[@"billing_type"]);
    self.trialEndDatetime = [DLAPIHelper dateFromString:attributes[@"trial_end_datetime"]];
    
    return self;
}

- (void)updateUserSessionToken:(NSDictionary *)attributes {
    self.sessionToken= CHECK_NULL_DATA_STRING(attributes[@"session_token"]);
}

- (void)updateUserPointData:(NSDictionary *)attributes {
    
    self.point = CHECK_NULL_DATA_NUMBER(attributes[@"point"]);
    self.pointLimit = attributes[@"point_limit"];
}

- (void)updateName:(NSString *)name {
    self.name = CHECK_NULL_DATA_STRING(name);
}

- (DLUserBillingType)getBillingType {
    if([self.billingType integerValue] == 1 ||
        self.trialEndDatetime == nil){
        return DLUserBillingTypeFree;
    }else{
        NSDate *currentDate = [NSDate new];
        if([currentDate compare:self.trialEndDatetime] == NSOrderedDescending){
            // 試用期間を過ぎている場合
            return DLUserBillingTypePremium;
        }else{
            return DLUserBillingTypeTrial;
        }
    }
}

@end
