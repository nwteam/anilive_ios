//
//  DLKeyChainManager.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLKeyChainManager.h"
#import "DLDeviceUtil.h"
#import <UICKeyChainStore.h>

@interface DLKeyChainManager ()

@property (nonatomic, strong) UICKeyChainStore *keychain;

@end

@implementation DLKeyChainManager

+ (instancetype)sharedInstance {
    static DLKeyChainManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[DLKeyChainManager alloc] init];
        [manager initConfig];
    });
    
    return manager;
}

- (void)initConfig {
    NSString *sharedAccessGroup = [NSBundle.mainBundle objectForInfoDictionaryKey: @"SharedAccessGroup"];
    self.keychain = [UICKeyChainStore keyChainStoreWithService:[DLDeviceUtil getBundleIdentifier] accessGroup:sharedAccessGroup];
    self.keychain.synchronizable = YES;
}

- (NSString *)getUUID {
    return [self.keychain stringForKey:DLConstKeyUUIDForKeyChain];
}

- (NSString *)getUIID {
    return [self.keychain stringForKey:DLConstKeyUIIDForKeyChain];
}

- (void)saveUUID:(NSString *)uuid {
    NSError *error;
    [self.keychain setString:uuid forKey:DLConstKeyUUIDForKeyChain error:&error];
    if (error) {
        NSLog(@"Save UDID Keychain Error Occured!!!\nReason: %@", error.localizedDescription);
    }
}

- (void)saveUIID:(NSString *)uiid {
    NSError *error;
    [self.keychain setString:uiid forKey:DLConstKeyUIIDForKeyChain error:&error];
    if (error) {
        NSLog(@"Save UIID Keychain Error Occured!!!\nReason: %@", error.localizedDescription);
    }
}

@end
