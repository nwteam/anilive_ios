//
//  DLPayingPointManager.h
//  AniLive
//
//  Created by shavin on 2017/1/10.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DLUnityAdsManager;

@interface DLPayingPointManager : NSObject {
    void (^payingPointBlock)(BOOL success);
}


+ (instancetype)sharedInstance;

- (void)startPayingWithController:(UIViewController *)controller formovie:(NSNumber*) movieID Completion:(void(^)(BOOL success))completetion;

+ (void)watchAdToAddPointWithTargetController:(UIViewController *)targetController adManager:(DLUnityAdsManager *)manager tostView:(UIView *)tostView;

+ (void)addAdsPointCompletion:(void(^)(BOOL success, NSDictionary *newPointDictionary))completetion;

@end
