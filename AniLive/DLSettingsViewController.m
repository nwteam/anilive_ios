//
//  DLSettingsViewController.m
//  AniLive
//
//  Created by Isao Kono on 2016/11/09.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLSettingsViewController.h"
#import "DLNavigationBarView.h"
#import "DLAppFrameManager.h"
#import "DLSettingsCell.h"
#import "DLSettingsSectionHeader.h"
#import "DLWebViewController.h"
#import "DLDeviceUtil.h"
#import "DLUserDataManager.h"

#import "DLWatchingPlanViewController.h"
#import "DLTransitionManager.h"
#import "DLConnectDeviceViewController.h"
#import "DLWatchingPlanViewController.h"
#import "DLTransitionManager.h"



@interface DLSettingsViewController () <DLNavigationBarViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) DLNavigationBarView *navigationBarView;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *settingList;
@property (nonatomic, assign) BOOL isModal;

@end

@implementation DLSettingsViewController

- (instancetype)initWithIsModal:(BOOL)isModal {
    if (self = [super init]) {
        self.isModal = isModal;
    }
    return self;
}

#pragma mark - Override Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    // TODO: Entity化 & Localize化
    // - TransitionType : 遷移先の指定
    // - isSwitch : スイッチボタンを伴うcellか
    // - label : lebelを伴うか
    // - description : cellに説明が伴うか
    self.settingList = [@[
        @{
            @"headerTitle": Localizer(@"Status"),
            @"cellList"   : [@[
                [@{
                    @"title" : @"Watching Plan",
                    @"label" : @" ",
                } mutableCopy],
                @{
                    @"title": @"Connect Device",
                    @"description": @"You can take over acount infomation such as your forums and premium with device cooperation",
                },
            ] mutableCopy],
        },
        @{
            @"headerTitle": Localizer(@"Features"),
            @"cellList"   : @[
                @{
                    @"title": @"Automatic switching of forums",
                    @"isSwitch": @YES,
                },
            ],
        },
        @{
            @"headerTitle": Localizer(@"Notifications"),
            @"cellList"   : @[
                @{
                    @"title": @"New Show Alerts",
                    @"isSwitch": @YES,
                    kSettingCell_userDefaultsKey: @"SETTING_NEW_SHOW_ALERTS",
                    @"action": @"reflectPushSettingAsTagToServer"
                },
                @{
                    @"title": @"live Alerts",
                    @"isSwitch": @YES,
                    kSettingCell_userDefaultsKey: @"SETTING_NEW_LIVE_ALERTS",
                    @"action": @"reflectPushSettingAsTagToServer"
                },
                @{
                    @"title": @"Comments Alerts",
                    @"isSwitch": @YES,
                    kSettingCell_userDefaultsKey: @"SETTING_NEW_COMMENTS_ALERTS",
                    @"action": @"reflectPushSettingAsTagToServer"
                },
            ],
        },
        @{
            @"headerTitle": Localizer(@"Suport"),
            @"cellList"   : @[
                @{
                    @"title": @"FAQ",
                },
                @{
                    @"title": @"Forum Rules",
                },
                @{
                    @"title": @"Terms of Service",
                },
                @{
                    @"title": @"Privacy Policy",
                },
                @{
                    @"title": @"License",
                },
                @{
                    @"title": @"Regarding Copyrights",
                },
                @{
                    @"title": @"Contact Us",
                },
            ],
        },
    ] mutableCopy];
    
    [self initView];
    [self updateFrame];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated {

    @weakify(self)
    [DLAPIHelper fetchUserWithCallback:^(NSError *error) {
        @strongify(self)
        if(NOT_NULL(error)){
            NSLog(@"error: %@: ", [error description]);
        }
        DLUser *user = [DLUserDataManager sharedInstance].user;
        NSString* userStatus = (user.getBillingType == DLUserBillingTypeFree ? Localizer(@"common_user_billing_free") : Localizer(@"common_user_billing_premium"));
        self.settingList[0][@"cellList"][0][@"label"] = userStatus;
        [self.tableView reloadData];
    }];
}

#pragma mark - Private Methods


- (void)initView {
    
    self.view.backgroundColor = [UIColor colorWithHex:DLConstColorDarkBlue];
    
#pragma mark Navigation Bar
    self.navigationBarView = [[DLNavigationBarView alloc] initWithType:DLNavigationBarViewTypeSettings];
    self.navigationBarView.delegate = self;
    // TODO: Localize化
    [self.navigationBarView setTitle:Localizer(@"Settings")];
    [self.view addSubview:self.navigationBarView];
    
#pragma mark Table View
    self.tableView = [UITableView new];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.scrollsToTop = NO;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.estimatedRowHeight = [DLSettingsCell getCellBaseHeight];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView registerClass:[DLSettingsCell class]
           forCellReuseIdentifier:[DLSettingsCell getCellIdentifier]];
    [self.view addSubview:self.tableView];

}

- (void)updateFrame {
    CGRect applicationFrame = [DLAppFrameManager sharedInstance].applicationFrame;
    
    self.navigationBarView.frame = CGRectMake(0,
                                              CGRectGetMinX(applicationFrame),
                                              CGRectGetWidth(applicationFrame),
                                              DLConstNavigationBarHeight);
    
    if (self.isModal) {
        self.tableView.frame = CGRectMake(0,
                                          DLConstNavigationBarHeight,
                                          CGRectGetWidth(applicationFrame),
                                          CGRectGetHeight(applicationFrame) - DLConstNavigationBarHeight);
    } else {
        self.tableView.frame = CGRectMake(0,
                                          DLConstNavigationBarHeight,
                                          CGRectGetWidth(applicationFrame),
                                          CGRectGetHeight(applicationFrame) - DLConstNavigationBarHeight - DLConstTabBarHeight);
    }
}

#pragma mark - DLNavigationBarViewDelegate

- (void)navigationBarViewDidTapLeftButton:(UIButton *)button {
    if (self.isModal) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Table View Delegate

#pragma mark Section

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return [DLSettingSectionHeader getSectionHeaderHeight];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    DLSettingSectionHeader *sectionHeader = [DLSettingSectionHeader new];
    [sectionHeader setTitle:self.settingList[section][@"headerTitle"]];
    
    return sectionHeader;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    view.tintColor = [UIColor colorWithHex:DLConstColorCodeDarkGray];
}

#pragma mark Cell

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // XXX 一次対応、本来ならcellから高さを取得したい
    NSString *description = self.settingList[indexPath.section][@"cellList"][indexPath.row][@"description"];
    return description != nil ? [DLSettingsCell getCellBaseHeight] * 2 : [DLSettingsCell getCellBaseHeight];
}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.settingList count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.settingList[section][@"headerTitle"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.settingList[section][@"cellList"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DLSettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:[DLSettingsCell getCellIdentifier]
                                                           forIndexPath:indexPath];
    
    [cell setCellData:self.settingList[indexPath.section][@"cellList"][indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES]; // 選択状態の解除をします。
    // Statusのセクションのみ反応する
    if ([self.settingList[indexPath.section][@"headerTitle"] isEqualToString:Localizer(@"Status")]) {
        if ([self.settingList[indexPath.section][@"cellList"][indexPath.row][@"title"] isEqualToString:@"Connect Device"]) {
            NSLog(@"Connect Divice");
            [self presentDLConnectDeviceViewController];
        }
    }
    
    //Supportのセクションのみ反応する
    if ([self.settingList[indexPath.section][@"headerTitle"] isEqualToString:Localizer(@"Status")]) {
        if ([self.settingList[indexPath.section][@"cellList"][indexPath.row][@"title"] isEqualToString:@"Watching Plan"]) {            
            NSLog(@"%@", [DLUserDataManager sharedInstance].user.billingType);
            NSLog(@"%@", [DLUserDataManager sharedInstance].user.trialEndDatetime);
            
            DLWatchingPlanViewController* controller;
            controller  = [[DLWatchingPlanViewController alloc] init];
            
            [self presentViewController:controller animated:YES completion:nil];

        }
    }
    else if ([self.settingList[indexPath.section][@"headerTitle"] isEqualToString:Localizer(@"Suport")]) {

        DLWebViewController *controller;

        if ([self.settingList[indexPath.section][@"cellList"][indexPath.row][@"title"] isEqualToString:@"Terms of Service"]) {
            NSLog(@"Terms Of Service");
            controller  = [[DLWebViewController alloc]initWithURLString:DLConstURLStringTerms];
        } else if ([self.settingList[indexPath.section][@"cellList"][indexPath.row][@"title"] isEqualToString:@"FAQ"]) {
            NSLog(@"FAQ");
            controller  = [[DLWebViewController alloc]initWithURLString:DLConstURLStringFaq];
        } else if ([self.settingList[indexPath.section][@"cellList"][indexPath.row][@"title"] isEqualToString:@"License"]) {
            NSLog(@"License");
            controller  = [[DLWebViewController alloc]initWithURLString:DLConstURLStringLicense];
        } else if ([self.settingList[indexPath.section][@"cellList"][indexPath.row][@"title"] isEqualToString:@"Privacy Policy"]) {
            NSLog(@"Privacy Policy");
            controller  = [[DLWebViewController alloc]initWithURLString:DLConstURLStringPrivacyPolicy];
        } else if ([self.settingList[indexPath.section][@"cellList"][indexPath.row][@"title"] isEqualToString:@"Forum Rules"]) {
            NSLog(@"Forum Rules");
            controller  = [[DLWebViewController alloc]initWithURLString:DLConstURLStringForumRules];
        } else if ([self.settingList[indexPath.section][@"cellList"][indexPath.row][@"title"] isEqualToString:@"Regarding Copyrights"]) {
            NSLog(@"Regarding Copyrights");
            controller  = [[DLWebViewController alloc]initWithURLString:DLConstURLStringCopyrights];
        } else if ([self.settingList[indexPath.section][@"cellList"][indexPath.row][@"title"] isEqualToString:@"Contact Us"]) {
            NSLog(@"Contact Us");
            //Contact Usページは、一旦URLの末尾にdevice_idをキーにつける
            
            //nullの場合もあるが良いか？要確認
            NSString *strContactUsUrl = [NSString stringWithFormat:@"%@?device_id=%@", DLConstURLStringContactUs, [[DLUserDataManager sharedInstance].user.deviceId stringValue]];
            controller  = [[DLWebViewController alloc]initWithURLString:strContactUsUrl];
            NSLog(@"Contact Us URL = %@", strContactUsUrl);
        } else {
            //ここは通ってはだめ
            return;
        }
        
        controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:controller animated:YES completion:nil];
    }
}

#pragma amrker - present ConnectDeviceViewCOntroller
- (void)presentDLConnectDeviceViewController{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.35;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    DLConnectDeviceViewController *connectDeviceViewController = [[DLConnectDeviceViewController alloc] init];
    [self presentViewController:connectDeviceViewController animated:NO completion:nil];
}

@end
