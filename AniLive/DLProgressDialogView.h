#import <UIKit/UIKit.h>

@interface DLProgressDialogView : UIView

+ (instancetype)initView;
- (void) start;
- (void) dismiss;

@end