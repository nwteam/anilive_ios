//
// Created by Kotaro Itoyama on 2016/10/09.
// Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLTimeSchedule.h"

@interface DLTimeSchedule ()
@property(readwrite, nonatomic, strong) NSNumber *liveId;
@property(readwrite, nonatomic, strong) NSNumber *movieId;
@property(readwrite, nonatomic, strong) NSDate *createdAt;
@property(readwrite, nonatomic, strong) NSDate *updatedAt;
@property(readwrite, nonatomic, strong) NSString *title;
@property(readwrite, nonatomic, strong) NSNumber *totalViewUserCount;
@property(readwrite, nonatomic, assign) BOOL isOnAir;
@property(readwrite, nonatomic, strong) NSNumber *totalCommentCount;
@property(readwrite, nonatomic, strong) NSDate *broadcastStartAt;
@property(readwrite, nonatomic, strong) NSDate *broadcastEndAt;
@property(readwrite, nonatomic, assign) NSNumber *totalMovieLength;
@property(readwrite, nonatomic, strong) NSString *thumbMovieUrl;
@property (readwrite, nonatomic, strong) NSString *smallImage;
@property (readwrite, nonatomic, strong) NSString *mediumImage;
@property (readwrite, nonatomic, strong) NSString *largeImage;
@end

@implementation DLTimeSchedule

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    _liveId = CHECK_NULL_DATA_NUMBER(attributes[@"id"]);
    _movieId = CHECK_NULL_DATA_NUMBER(attributes[@"movie_id"]);
    _createdAt = [DLAPIHelper dateFromString:attributes[@"created_at"]];
    _updatedAt = [DLAPIHelper dateFromString:attributes[@"updated_at"]];
    _title = CHECK_NULL_DATA_STRING(attributes[@"title"]);
    _totalViewUserCount = CHECK_NULL_DATA_NUMBER(attributes[@"total_view_user_count"]);
    NSNumber *isBoolValue = CHECK_NULL_DATA_NUMBER(attributes[@"is_on_the_air"]);
    _isOnAir = isBoolValue.boolValue;
    _totalCommentCount = CHECK_NULL_DATA_NUMBER(attributes[@"total_comment_count"]);
    _broadcastStartAt = [DLAPIHelper dateFromString:attributes[@"broadcast_start_at"]];
    _broadcastEndAt = [DLAPIHelper dateFromString:attributes[@"broadcast_end_at"]];
    
    if (NOT_NULL(attributes[@"total_movie_length"])) {
        self.totalMovieLength = [NSNumber numberWithInteger:[DLAPIHelper timeIntervalFromString:attributes[@"total_movie_length"]]];
    } else {
        self.totalMovieLength = @0;
    }
    _thumbMovieUrl = CHECK_NULL_DATA_STRING(attributes[@"thumb_movie_url"]);
    self.smallImage = CHECK_NULL_DATA_STRING([attributes valueForKeyPath:@"images.small"]);
    self.mediumImage = CHECK_NULL_DATA_STRING([attributes valueForKeyPath:@"images.medium"]);
    self.largeImage = CHECK_NULL_DATA_STRING([attributes valueForKeyPath:@"images.large"]);
    return self;
}

@end
