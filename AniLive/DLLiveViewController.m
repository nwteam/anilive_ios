//
//  DLLiveViewController.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

@import AVFoundation;
#import "DLLiveViewController.h"
#import "DLAppFrameManager.h"
#import "DLNavigationBarView.h"
#import "DLLiveMovieCell.h"
#import "DLNetworkManager.h"
#import "DLTransitionManager.h"
#import "DLPreloadMovieManager.h"
#import "DLAPIHelper.h"
#import "DLTimeScheduleViewController.h"

@interface DLLiveViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate, DLLiveMovieCellDelegate> {
    NSMutableArray *sendArray;
}

@property (nonatomic, strong) DLNavigationBarView *navigationBarView;
@property (nonatomic, strong) UICollectionViewFlowLayout *collectionViewFlowLayout;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray<DLLive *> *items;
@property (nonatomic, strong) DLPreloadMovieManager *preloadManager;
@property (nonatomic, assign) BOOL isScrolling;

@end

@implementation DLLiveViewController

#pragma mark - Override Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    self.items = [[NSMutableArray alloc]init];
    
    self.isScrolling = NO;
    
    [self initView];
    [self addObserver];
    [self updateFrame];
    [self loadData];
    
    self.preloadManager = [[DLPreloadMovieManager alloc]initWithCollectionView:self.collectionView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.isScrolling = NO;
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self addPlayerToCell];
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self removePlayerFromCell];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    FUNC_LOG
    [self removeObserver];
    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
}

#pragma mark - Private Methods

- (void)initView {
    
    self.view.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkBlue];
    
#pragma mark Navigation Bar
    
    self.navigationBarView = [[DLNavigationBarView alloc]initWithType:DLNavigationBarViewTypeLogo];
    [self.view addSubview:self.navigationBarView];
  
    // btn Time Schedule
    UIButton *btnSchedule = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSchedule addTarget:self action:@selector(tapBtnSchedule:) forControlEvents:UIControlEventTouchUpInside];
    [btnSchedule setImage:[UIImage imageNamed:@"icon_top_schedule"] forState:UIControlStateNormal];
    btnSchedule.frame = CGRectMake(self.view.frame.size.width - 40.f, 25.f, 36.f, 40.f);
    [self.view addSubview:btnSchedule];
    
#pragma mark Collection View
    
    self.collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc]init];
    self.collectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero
                                            collectionViewLayout:self.collectionViewFlowLayout];
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.collectionView registerClass:[DLLiveMovieCell class] forCellWithReuseIdentifier:[DLLiveMovieCell getCellIdentifier]];
    [self.view addSubview:self.collectionView];
}

#pragma marker - tap btn scadule
- (IBAction)tapBtnSchedule:(id)sender {
    [self loadTimeScheduleViewController];
    
    // test
    NSDictionary *notification = [[NSDictionary alloc] init];
    NSDictionary *messageDic = [NSDictionary dictionaryWithObject:notification forKey:@"notification"];
    NSNotification *messageNoti = [NSNotification notificationWithName:@"notiMessage"
                                                                object:self
                                                              userInfo:messageDic];
    [[NSNotificationCenter defaultCenter] postNotification:messageNoti];}

- (void)loadTimeScheduleViewController {
    DLTimeScheduleViewController *timescheduleVC = [[DLTimeScheduleViewController alloc] initWithNibName:@"DLTimeScheduleViewController" bundle:nil];
    [self.navigationController pushViewController:timescheduleVC animated:YES];
}

- (void)updateFrame {
    
    self.navigationBarView.frame = CGRectMake(0,
                                              CGRectGetMinX([DLAppFrameManager sharedInstance].applicationFrame),
                                              CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame),
                                              DLConstNavigationBarHeight);
    
    self.collectionView.frame = CGRectMake(0, self.navigationBarView.maxY, CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame), CGRectGetHeight([DLAppFrameManager sharedInstance].applicationFrame) - self.navigationBarView.maxY - DLConstTabBarHeight);
}

- (void)addObserver {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removePlayerFromCell)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addPlayerToCell)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    [[DLNetworkManager sharedInstance] addObserver:self
                                        forKeyPath:kNetworkStatusIdentifier
                                           options:NSKeyValueObservingOptionNew
                                           context:nil];
}

- (void)removeObserver {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [[DLNetworkManager sharedInstance] removeObserver:self
                                           forKeyPath:kNetworkStatusIdentifier];
}

- (void)loadData {
    @weakify(self)
    [DLAPIHelper fetchLives:^(NSMutableArray *lives) {
        @strongify(self)
        self.items = lives;
        [self.collectionView reloadData];

        // set send Array
        sendArray = lives;
        
        // 時間差でセルの自動再生の読み込みを始める
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self addPlayerToCell];
        });
    }];
}

- (void)addPlayerToCell {
    [self.preloadManager setPreloadCells];
}

- (void)removePlayerFromCell {
    [self.preloadManager removeAllPreloadCells];
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    
    if ([keyPath isEqualToString:kNetworkStatusIdentifier]) {
        if (![[DLNetworkManager sharedInstance] hasInternetConnection]) {
            [self removePlayerFromCell];
        }
    }
}

#pragma mark - UICollectionView DataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        DLLiveMovieCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[DLLiveMovieCell getCellIdentifier]
                                                                                     forIndexPath:indexPath];
        
        if (indexPath.row < self.items.count) {
            [cell setCellWithLiveData:self.items[indexPath.row]];
        }
        cell.delegate = self;
        return cell;
    }
    return  nil;
}

#pragma mark - UICollectionViewDelegateFlowLayout Protocol

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section==0) {
        return [DLLiveMovieCell getCellSizeWithParantWidth:self.collectionView.width];
    }
    return CGSizeZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    if (section == 0) {
        return 0;
    }
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    return 0;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return UIEdgeInsetsZero;
    }
    return UIEdgeInsetsZero;
}

#pragma mark - DLLiveMovieCellDelegate

- (BOOL)scrollViewIsScrolling {
    return self.isScrolling;
}

- (void)didTapLiveMovieCell:(DLLiveMovieCell *)cell withMovieData:(DLMovie *)movieData {
    
    [DLTransitionManager showMoviePlayViewControllerWithMovieData:movieData withTarget:self withIsModal:YES];
}

- (void)didTapLiveMovieCell:(DLLiveMovieCell *)cell withLiveData:(DLLive *)liveData {
    
    [DLTransitionManager showMoviePlayViewControllerWithLiveData:liveData withTarget:self withIsModal:YES];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [self addPlayerToCell];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    FUNC_LOG;
    
    self.isScrolling = YES;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    FUNC_LOG;
    
    self.isScrolling = NO;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    FUNC_LOG;
    if (!decelerate) {
        
        self.isScrolling = NO;
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    self.isScrolling = NO;
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
    FUNC_LOG;
    
    self.isScrolling = NO;
}

@end
