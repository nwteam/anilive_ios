//
//  DLCatalogPopularViewController.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/10/01.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLSegmentViewControllerProtocol.h"

@interface DLCatalogPopularViewController : UIViewController <DLSegmentViewControllerProtocol>

@end
