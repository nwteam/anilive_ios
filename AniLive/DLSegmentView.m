//
//  DLSegmentView.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/10/01.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLSegmentView.h"
#import "DLHighlightedButton.h"

const static CGFloat kSegmentLineViewHeight = 2;

@interface DLSegmentView ()

//@property (nonatomic, strong) UIView *segmentButtonUnderLineView;
@property (nonatomic, strong) UIView *segmentLineView;
@property (nonatomic, strong) NSMutableArray *buttonArray;

@property (readwrite, nonatomic, assign) NSInteger numberOfButtons;
@property (readwrite, nonatomic, assign) NSInteger currentRow;

@end

@implementation DLSegmentView

#pragma mark - init

- (instancetype)initWithTitles:(NSArray *)titleArray
                      startRow:(NSInteger)startRow{
    
    self = [super init];
    if (self) {
        self.currentRow = startRow;
        
        [self initWithTitles:titleArray];
    }
    return self;
}

- (void)initWithTitles:(NSArray *)titleArray {
    
    self.backgroundColor = [UIColor colorWithHex:DLConstColorLightBlue];
    
    self.buttonArray = [[NSMutableArray alloc]init];
    
    self.numberOfButtons = titleArray.count;
    
    for (int i=0; i<self.numberOfButtons; i++) {
        
        DLHighlightedButton *segmentButton = [DLHighlightedButton buttonWithType:UIButtonTypeSystem];
        segmentButton.backgroundColor = [UIColor colorWithHex:DLConstColorLightBlue];
        if (i == self.currentRow) {
            segmentButton.tintColor = [UIColor colorWithHex:DLConstColorCodeYellow];
        }
        else {
            segmentButton.tintColor = [UIColor colorWithHex:DLConstColorCodeGray];
        }
        segmentButton.titleLabel.font = BASE_FONT(14);
        segmentButton.titleLabel.numberOfLines = 0;
        segmentButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        segmentButton.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
        
        if (i<titleArray.count) {
            [segmentButton setTitle:(NSString *)titleArray[i] forState:UIControlStateNormal];
        }
        
        [segmentButton addTarget:self
                          action:@selector(segmentButtonChanged:)
                forControlEvents:UIControlEventTouchUpInside];
        segmentButton.tag = i;
        [self addSubview:segmentButton];
        [self.buttonArray addObject:segmentButton];
    }
    
    //    self.segmentButtonUnderLineView = [[UIView alloc]initWithFrame:CGRectMake(0, self.height - 1, self.width, 1)];
    //    self.segmentButtonUnderLineView.backgroundColor = [UIColor colorWithHex:ORVConstColorCodePress];
    //    [self addSubview:self.segmentButtonUnderLineView];
    
    self.segmentLineView = [[UIView alloc]init];
    self.segmentLineView.backgroundColor = [UIColor colorWithHex:DLConstColorCodeYellow];
    [self addSubview:self.segmentLineView];
    
    [self updateFrame];
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    [self updateFrame];
}

#pragma mark - Public Methods

- (void)updateFrame {
    
    for (int i=0; i<self.numberOfButtons; i++) {
        DLHighlightedButton *segmentButton = self.buttonArray[i];
        segmentButton.frame = CGRectMake((self.width/self.numberOfButtons)*i, 0, self.width/self.numberOfButtons, self.height);
    }
    
    //    self.segmentButtonUnderLineView.frame = CGRectMake(0, self.height - 1, self.width, 1);
    self.segmentLineView.frame = CGRectMake(0, self.height - kSegmentLineViewHeight, self.width/self.numberOfButtons, kSegmentLineViewHeight);
    self.segmentLineView.center = CGPointMake(self.segmentLineView.width/2 + (self.segmentLineView.width * self.currentRow), self.segmentLineView.midY);
}

- (void)setLineViewCenterWithOffset:(CGPoint)offset {
    
    self.segmentLineView.center = CGPointMake(self.segmentLineView.width/2 + offset.x/self.numberOfButtons, self.segmentLineView.midY);
}

- (void)setRow:(NSInteger)row isMoveLine:(BOOL)isMoveLine {
    
    self.currentRow = row;
    [self setUI:isMoveLine];
}

#pragma mark - Private Methods

- (void)setUI:(BOOL)isMoveLine {
    
    for (int i=0; i<self.numberOfButtons; i++) {
        
        DLHighlightedButton *segmentButton = self.buttonArray[i];
        if (i == self.currentRow) {
            segmentButton.tintColor = [UIColor colorWithHex:DLConstColorCodeYellow];
        }
        else {
            segmentButton.tintColor = [UIColor colorWithHex:DLConstColorCodeGray];
        }
    }
    
    if (isMoveLine) {
        self.segmentLineView.center = CGPointMake(self.segmentLineView.width/2 + (self.segmentLineView.width * self.currentRow), self.segmentLineView.midY);
    }
}

- (void)segmentButtonChanged:(UIButton *)button {
    
    if ([self.delegate respondsToSelector:@selector(segmentView:didSelectSegmentAtRow:)]) {
        [self.delegate segmentView:self didSelectSegmentAtRow:button.tag];
    }
}

#pragma mark - Dealloc

- (void)dealloc {
    //    self.segmentButtonUnderLineView = nil;
    self.buttonArray = nil;
}

@end
