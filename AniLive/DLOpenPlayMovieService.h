//
//  DLOpenPlayMovieService.h
//  AniLive
//
//  Created by Kotaro Itoyama on 2017/01/14.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

@class DLLive;
@class DLMovie;

typedef void (^DLOpenPlayMovieServiceCallback)(void);

@interface DLOpenPlayMovieService : NSObject
+ (instancetype)sharedInstance;
+ (void)tryPlayMovie:(DLMovie *) movie live:(DLLive*)live fromViewController:(UIViewController *) controller callback:(DLOpenPlayMovieServiceCallback)callback;

@end
