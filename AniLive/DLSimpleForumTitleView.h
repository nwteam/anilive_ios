//
//  DLSimpleForumTitleView.h
//  AniLive
//
//  Created by isaoeka on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

static const CGFloat kPlayerForumTitleViewHeight = 90;

@class DLForum;
@class DLSeeLaterButton;

@protocol DLSimpleForumTitleViewDelegate <NSObject>

@optional

- (BOOL)simpleForumTitleViewStarButtonDidTap;
- (void)simpleForumTitleViewTitleLabelDidTap;

@end

@interface DLSimpleForumTitleView : UIView

@property (nonatomic, weak) id<DLSimpleForumTitleViewDelegate> delegate;

- (void)setForum:(DLForum *)forum;
- (DLSeeLaterButton *)getSeeLaterButton;

@end
