//
//  DLSeeLaterButton.m
//  AniLive
//
//  Created by isaoeka on 2017/01/01.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLSeeLaterButton.h"

@interface DLSeeLaterButton ()

@property (nonatomic, assign) BOOL seeLaterFlag;

@end

@implementation DLSeeLaterButton

- (instancetype)init {
    // Type固定
    self = [[super class] buttonWithType:UIButtonTypeCustom];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
}

#pragma mark - Private Methods

- (void)initView {
    self.backgroundColor = [UIColor clearColor];
    
    // image
    [self setStarImageOfHighlighted:NO];
    
    // action
    [self addTarget:self
             action:@selector(buttonDidTap:)
   forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)updateFrame {
    
}

#pragma mark Button Action

- (void)buttonDidTap:(UIButton *)button {
    
    if ([self.delegate respondsToSelector:@selector(seeLaterButtonDidTap:)]) {
        BOOL seeLaterd = [self.delegate seeLaterButtonDidTap:self];
        [self setStarImageOfHighlighted:seeLaterd];
    }
}

- (BOOL) isSeeLater {
    return self.seeLaterFlag;
}

#pragma mark - Public Methods

- (BOOL)isStard {
    return self.seeLaterFlag;
}

- (void)setStarImageOfHighlighted:(BOOL)highlighted {
    self.seeLaterFlag = highlighted;
    
    NSString *starImageName = self.seeLaterFlag ? DLConstIconStarOn : DLConstIconStarOff;
    [self setBackgroundImage:[UIImage imageNamed:starImageName]
                    forState:UIControlStateNormal];
}

@end
