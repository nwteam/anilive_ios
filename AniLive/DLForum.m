//
//  DLForum.m
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/10/08.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLForum.h"
#import "DLMovie.h"

@interface DLForum ()
@property(nonatomic, readwrite, strong) NSNumber *forumId;
@property(nonatomic, readwrite, strong) NSNumber *movieId;
@property(nonatomic, readwrite, strong) NSString *title;
@property(nonatomic, readwrite, strong) NSString *movieTitle;
@property(nonatomic, readwrite, strong) NSNumber *totalForums;
@property(nonatomic, readwrite, strong) NSString *forumPinnedTime;
@property(nonatomic, readwrite, strong) NSNumber *totalComments;
@property(nonatomic, readwrite, strong) NSNumber *popularity;
@property(nonatomic, readwrite, strong) NSNumber *forumOwnerUserId;
@property(nonatomic, readwrite, strong) NSString *forumOwnerUserName;
@property(nonatomic, readwrite, strong) NSDate *created;
@property(nonatomic, readwrite, strong) NSDate *modified;
@property(nonatomic, readwrite, strong) NSString *movieImage;
@property(nonatomic, readwrite, strong) NSNumber *chapter;
@property(nonatomic, readwrite, strong) NSNumber *pointFlag;
@property(nonatomic, readwrite, strong) NSNumber *salesType;
@property(nonatomic, readwrite, strong) NSNumber *workID;
@property(nonatomic, readwrite, strong) NSString *workTitle;
@end

@implementation DLForum

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    // TODO: ネスト下データを担当するクラスの実装
    self.forumId = CHECK_NULL_DATA_NUMBER(attributes[@"forum_id"]);
    self.title = CHECK_NULL_DATA_STRING(attributes[@"title"]);
    self.movieId = CHECK_NULL_DATA_NUMBER(attributes[@"movie_id"]);
    self.forumPinnedTime = CHECK_NULL_DATA_STRING(attributes[@"forum_pinned_time"]);
    self.totalComments = CHECK_NULL_DATA_NUMBER(attributes[@"total_comments"]);
    self.popularity = CHECK_NULL_DATA_NUMBER(attributes[@"popularity"]);
    self.forumOwnerUserId = CHECK_NULL_DATA_NUMBER(attributes[@"forum_owner_user_id"]);
    self.forumOwnerUserName = CHECK_NULL_DATA_STRING(attributes[@"forum_owner_user_name"]);
    self.created = [DLAPIHelper dateFromString:attributes[@"created"]];
    self.modified = [DLAPIHelper dateFromString:attributes[@"modified"]];
    
    if (NOT_NULL(attributes[@"movie_title"])) {
        self.movieTitle = CHECK_NULL_DATA_STRING(attributes[@"movie_title"]);
    }
    if (NOT_NULL(attributes[@"image"])) {
        self.movieImage = CHECK_NULL_DATA_STRING(attributes[@"image"]);
    }
    if (NOT_NULL(attributes[@"chapter"])) {
        self.chapter = CHECK_NULL_DATA_NUMBER(attributes[@"chapter"]);
    }
    if (NOT_NULL(attributes[@"total_forums"])) {
        self.totalForums = CHECK_NULL_DATA_NUMBER(attributes[@"total_forums"]);
    }
    if (NOT_NULL(attributes[@"point_flag"])) {
        self.pointFlag = CHECK_NULL_DATA_NUMBER(attributes[@"point_flag"]);
    }
    if (NOT_NULL(attributes[@"sales_type"])) {
        self.salesType = CHECK_NULL_DATA_NUMBER(attributes[@"sales_type"]);
    }
    if (NOT_NULL(attributes[@"work_id"])) {
        self.workID = CHECK_NULL_DATA_NUMBER(attributes[@"work_id"]);
    }
    if (NOT_NULL(attributes[@"work_title"])) {
        self.workTitle = CHECK_NULL_DATA_STRING(attributes[@"work_title"]);
    }


    return self;
}

@end
