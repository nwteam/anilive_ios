//
//  DLSuperCollectionViewCell.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/16.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLSuperCollectionViewCell.h"

@implementation DLSuperCollectionViewCell

- (void)prepareForReuse {
    
    [super prepareForReuse];
    [self initializeCell];
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

#pragma mark - DLSuperCollectionViewCellProtocol

- (void)initView {
    
}

- (void)initializeCell {
    
}

+ (NSString *)getCellIdentifier {
    
    return [self getCellIdentifier];
}

@end
