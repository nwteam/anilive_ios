//
//  DLMyPageViewController.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLMyPageViewController.h"
#import "DLMyPageGroupListViewController.h"
#import "DLNavigationBarView.h"
#import "DLPointHistoryViewController.h"

#import "DLAppFrameManager.h"
#import "DLAPIHelper.h"
#import "DLSegmentView.h"
#import "DLHighlightedButton.h"
#import "DLTransitionManager.h"
#import "DLUserDataManager.h"
#import "DLUnityAdsButton.h"
#import "NSString+Category.h"
#import "DLPoint.h"
#import <UIView+Toast.h>
#import <UnityAds/UnityAds.h>

const static NSInteger kSegmentStartRow = 0;
const static CGFloat kSegmentViewHeight = 50;
const static CGFloat kBaseInfomationViewHeight = 198;

@interface DLMyPageViewController () <DLNavigationBarViewDelegate, UITextFieldDelegate, UIScrollViewDelegate, DLSegmentViewDelegate>

@property (nonatomic, strong) DLNavigationBarView *navigationBarView;

@property (nonatomic, strong) UIView *baseInformationView;
@property (nonatomic, strong) UITextField *nicknameTextField;
@property (nonatomic, strong) NSString *beforeNickname;
@property (nonatomic, strong) UILabel *daisukiPointArea;
@property (nonatomic, strong) DLUnityAdsButton *unityAdsButton;
@property (nonatomic, strong) DLHighlightedButton *pointLogButton;

@property (nonatomic, strong) DLSegmentView *segmentView;
@property (nonatomic, strong) UIScrollView *baseScrollView;
@property (nonatomic, strong) NSArray *segmentTitleArray;
@property (nonatomic, strong) NSMutableArray *segmentControllerArray;

@property (nonatomic, strong) DLMyPageGroupListViewController *historyVC;
@property (nonatomic, strong) DLMyPageGroupListViewController *myForumVC;
@property (nonatomic, strong) DLMyPageGroupListViewController *participateForumVC;

@end

@implementation DLMyPageViewController

#pragma mark - Override Methods

- (void)viewDidLoad {
    [super viewDidLoad];

    self.automaticallyAdjustsScrollViewInsets = NO;

    self.segmentTitleArray = @[Localizer(@"mypage_tab_left"),
                               Localizer(@"mypage_tab_center"),
                               Localizer(@"mypage_tab_right")];
    self.segmentControllerArray = [NSMutableArray new];

    [self initView];
    [self updateFrame];

    // 初期表示タブに移動させておく
    [self.baseScrollView setContentOffset:CGPointMake(self.baseScrollView.width * kSegmentStartRow, 0)];
    [self setScrollsToTopWithCurrentRow:kSegmentStartRow];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [UnityAds setDelegate:self];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self syncAdButtonState];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    self.segmentView.delegate = nil;
    self.baseScrollView.delegate = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Private Methods

- (void)initView {

    self.view.backgroundColor = [UIColor colorWithHex:DLConstColorDarkBlue];

#pragma mark Navigation Bar

    // baritemにFontAwesome使用したい気持ちがある
    self.navigationBarView = [[DLNavigationBarView alloc] initWithType:DLNavigationBarViewTypeMyPage];
    self.navigationBarView.delegate = self;
    [self.navigationBarView setTitle:Localizer(@"tab_menu_mypage")];
    [self.view addSubview:self.navigationBarView];

#pragma mark Base Infomation View

    self.baseInformationView = [UIView new];
    [self.view addSubview:self.baseInformationView];

#pragma mark Nickname Text Field

    self.nicknameTextField = [UITextField new];
    self.nicknameTextField.backgroundColor = [UIColor clearColor];
    self.nicknameTextField.text = [[DLUserDataManager sharedInstance] user].name;
    self.nicknameTextField.attributedPlaceholder = [[NSAttributedString alloc]initWithString:Localizer(@"mypage_name_placeholder")
                                                                                  attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHex:DLConstColorCodeThinGray],
                                                                                               NSFontAttributeName: BASE_FONT_BOLD(22)}];
    self.nicknameTextField.font = BASE_FONT_BOLD(22);
    self.nicknameTextField.textColor = [UIColor whiteColor];
    self.nicknameTextField.tintColor = [UIColor colorWithHex:DLConstColorCodeYellow];
    self.nicknameTextField.borderStyle = UITextBorderStyleNone;
    self.nicknameTextField.textAlignment = NSTextAlignmentCenter;
    self.nicknameTextField.returnKeyType = UIReturnKeyDone;
    self.nicknameTextField.delegate = self;
    [self.baseInformationView addSubview:self.nicknameTextField];

#pragma mark Daisuki Point Area

    self.daisukiPointArea = [UILabel new];
    self.daisukiPointArea.backgroundColor = [UIColor clearColor];
    self.daisukiPointArea.text = [NSString stringWithFormat:Localizer(@"mypage_current_point"), [[DLUserDataManager sharedInstance] user].point];
    self.daisukiPointArea.font = BASE_FONT(13);
    self.daisukiPointArea.textColor = [UIColor whiteColor];
    self.daisukiPointArea.textAlignment = NSTextAlignmentCenter;
    [self.baseInformationView addSubview:self.daisukiPointArea];

#pragma mark Unity Ads Button
    
    self.unityAdsButton = [[DLUnityAdsButton alloc]init];
    @weakify(self)
    [self.unityAdsButton buttonDidTap:^(UIButton *button) {
        @strongify(self)
        [self.unityAdsButton setIsEnabled:NO];
        [UnityAds show:self];
    }];
    [self.baseInformationView addSubview:self.unityAdsButton];

#pragma mark Point Log Button

    self.pointLogButton = [DLHighlightedButton buttonWithType:UIButtonTypeSystem];
    self.pointLogButton.backgroundColor = [UIColor colorWithHex:DLConstColorLightBlue];
    self.pointLogButton.isNoNeedHighlight = YES;
    self.pointLogButton.layer.cornerRadius = 2;
    self.pointLogButton.layer.masksToBounds = true;
    [self.pointLogButton setTitle:Localizer(@"mypage_point_history") forState:UIControlStateNormal];
    self.pointLogButton.tintColor = [UIColor whiteColor];
    self.pointLogButton.titleLabel.font = BASE_FONT(12);
    self.pointLogButton.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
    [self.pointLogButton addTarget:self
                            action:@selector(pointLogButtonTapped:)
                  forControlEvents:UIControlEventTouchUpInside];
    [self.baseInformationView addSubview:self.pointLogButton];


#pragma mark Segment View

    self.segmentView = [[DLSegmentView alloc] initWithTitles:self.segmentTitleArray startRow:kSegmentStartRow];
    self.segmentView.delegate = self;
    [self.view addSubview:self.segmentView];

#pragma mark Base Scroll View

    self.baseScrollView = [UIScrollView new];
    self.baseScrollView.backgroundColor = [UIColor clearColor];
    self.baseScrollView.scrollsToTop = NO;
    self.baseScrollView.pagingEnabled = YES;
    self.baseScrollView.alwaysBounceHorizontal = YES;
    self.baseScrollView.showsHorizontalScrollIndicator = NO;
    // スワイプジェスチャーでもキーボードを閉じれるようにする
    self.baseScrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    self.baseScrollView.delegate = self;
    [self.view addSubview:self.baseScrollView];

#pragma mark History View Controller

    self.historyVC = [[DLMyPageGroupListViewController alloc] initWithType:DLMyPageSegumentTypeVideoHistory];
    [self.baseScrollView addSubview:self.historyVC.view];
    [self.segmentControllerArray addObject:self.historyVC];

#pragma mark MyForum View Controller

    self.myForumVC = [[DLMyPageGroupListViewController alloc] initWithType:DLMyPageSegumentTypeMyForum];
    [self.baseScrollView addSubview:self.myForumVC.view];
    [self.segmentControllerArray addObject:self.myForumVC];

#pragma mark ParticipateForum View Controller

    self.participateForumVC = [[DLMyPageGroupListViewController alloc] initWithType:DLMyPageSegumentTypeParticipateForum];
    [self.baseScrollView addSubview:self.participateForumVC.view];
    [self.segmentControllerArray addObject:self.participateForumVC];
}

- (void)updateFrame {
    
    CGFloat kNicknameTopMargin = 30;
    CGFloat kNicknameTextFieldHeight = 26;
    CGFloat kNicknamePointMidMargin = 8;
    CGFloat kDaisukiPointLabelHeight = 20;
    CGFloat kPointLabelLogButtonMidMargin = 8;
    CGFloat kPointLogButtonWidth = 134;
    CGFloat kPointLogButtonHeight = 24;
    CGFloat kAdsButtonBottomMargin = 24;
    
    CGRect applicationFrame = [DLAppFrameManager sharedInstance].applicationFrame;

    self.navigationBarView.frame = CGRectMake(0,
                                              CGRectGetMinX(applicationFrame),
                                              CGRectGetWidth(applicationFrame),
                                              DLConstNavigationBarHeight);

    self.baseInformationView.frame = CGRectMake(0,
                                                DLConstNavigationBarHeight,
                                                CGRectGetWidth(applicationFrame),
                                                kBaseInfomationViewHeight);

    self.nicknameTextField.frame = CGRectMake(0,
                                              kNicknameTopMargin,
                                              CGRectGetWidth(applicationFrame) - (DLConstDefaultMargin * 2),
                                              kNicknameTextFieldHeight);
    self.nicknameTextField.midX = CGRectGetWidth(applicationFrame) / 2;

    self.daisukiPointArea.frame = CGRectMake(0,
                                             self.nicknameTextField.maxY + kNicknamePointMidMargin,
                                             CGRectGetWidth(applicationFrame),
                                             kDaisukiPointLabelHeight);
    
    self.pointLogButton.frame = CGRectMake(0, self.daisukiPointArea.maxY + kPointLabelLogButtonMidMargin,
                                           kPointLogButtonWidth,
                                           kPointLogButtonHeight);
    self.pointLogButton.midX = CGRectGetMidX(applicationFrame);

    self.unityAdsButton.width = [DLUnityAdsButton buttonWidth];
    self.unityAdsButton.height = [DLUnityAdsButton buttonHeight];
    self.unityAdsButton.midX = CGRectGetMidX(applicationFrame);
    self.unityAdsButton.y = self.baseInformationView.height - [DLUnityAdsButton buttonHeight] - kAdsButtonBottomMargin;

    self.segmentView.frame = CGRectMake(0,
                                        self.baseInformationView.maxY,
                                        CGRectGetWidth(applicationFrame),
                                        kSegmentViewHeight);

    self.baseScrollView.frame = CGRectMake(0,
                                           self.segmentView.maxY,
                                           CGRectGetWidth(applicationFrame),
                                           CGRectGetHeight(applicationFrame) - self.segmentView.maxY - DLConstTabBarHeight);
    self.baseScrollView.contentSize = CGSizeMake(self.baseScrollView.width * self.segmentTitleArray.count, 0);
    self.baseScrollView.contentOffset = CGPointMake(self.baseScrollView.width * (self.segmentView.currentRow),
                                                    self.baseScrollView.contentOffset.y);

    @weakify(self)
    [self.segmentControllerArray enumerateObjectsUsingBlock:^(UIViewController *viewController, NSUInteger index, BOOL *stop) {
        @strongify(self)
        viewController.view.frame = self.baseScrollView.bounds;
        viewController.view.x = self.baseScrollView.width * index;
    }];
}

- (BOOL)validateNickname:(NSString *)nickname {
    
    // 前後変化チェック
    if ([nickname isEqualToString:self.beforeNickname]) {
        // トーストは表示しない
        return NO;
    }
    
    // 空文字チェック
    if ([nickname isEqualToString:@""] || nickname.length == 0) {
        [WINDOW makeToast:Localizer(@"mypage_error_name_empty")];
        return NO;
    }
    
    // 文字数チェック（最小）
    if (nickname.length < DLConstNicknameWordCountMin) {
        [WINDOW makeToast:[NSString stringWithFormat:Localizer(@"mypage_error_name_min"), DLConstNicknameWordCountMin]];
        return NO;
    }
    
    // 文字数チェック（最大）
    if (nickname.length > DLConstNicknameWordCountMax) {
        [WINDOW makeToast:[NSString stringWithFormat:Localizer(@"mypage_error_name_max"), DLConstNicknameWordCountMax]];
        return NO;
    }
    
    return YES;
}

- (void)resetNickname {
    // 変更前のニックネームに戻す
    self.nicknameTextField.text = self.beforeNickname;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - UIButton Action

- (void)pointLogButtonTapped:(UIButton*)button {
    // TODO: 履歴ページに遷移させる
    DLPointHistoryViewController *pointHistoryViewController = [[DLPointHistoryViewController alloc] init];
    [self.navigationController pushViewController:pointHistoryViewController animated:YES];
}

#pragma mark - DLSegmentViewDelegate

- (void)setScrollsToTopWithCurrentRow:(NSInteger)currentRow {

    [self.segmentControllerArray enumerateObjectsUsingBlock:^(UIViewController *viewController, NSUInteger index, BOOL *stop) {

        if ([viewController conformsToProtocol:@protocol(DLSegmentViewControllerProtocol)]) {

            UIViewController <DLSegmentViewControllerProtocol> *segmentViewController = (UIViewController <DLSegmentViewControllerProtocol> *)viewController;

            if (index == currentRow) {
                [segmentViewController setScrollsToTop:YES];
            } else {
                [segmentViewController setScrollsToTop:NO];
            }
        }
    }];
}

#pragma mark - DLNavigationBarViewDelegate

- (void)navigationBarViewDidTapRightButton:(UIButton *)button {
    [DLTransitionManager showSettingsViewComtroller:self withIsModal:YES];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    // 変更前のニックネームを保持
    self.beforeNickname = textField.text;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    if (textField == self.nicknameTextField && [self validateNickname:textField.text]) {
        
        @weakify(self)
        [DLAPIHelper patchUser:textField.text
                      callback:^(NSError *error) {
                          @strongify(self)
                          if (error) {
                              [WINDOW makeToast:Localizer(@"error_by_signal")];
                              // 変更前のニックネームに戻す
                              [self resetNickname];
                          } else {
                              [[[DLUserDataManager sharedInstance] user] updateName:textField.text];
                              self.nicknameTextField.text = [[DLUserDataManager sharedInstance] user].name;
                          }
                      }];
    } else {
        // 変更前のニックネームに戻す
        [self resetNickname];
    }
    return YES;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

    [self.segmentView setLineViewCenterWithOffset:scrollView.contentOffset];

    CGFloat offsetX = scrollView.contentOffset.x;

    NSInteger currentRow = (offsetX + scrollView.width / 2) / scrollView.width;
    if (currentRow < 0) {
        currentRow = 0;
    }
    if (currentRow > self.segmentView.numberOfButtons - 1) {
        currentRow = self.segmentView.numberOfButtons - 1;
    }

    [_segmentView setRow:currentRow isMoveLine:NO];

    [self setScrollsToTopWithCurrentRow:currentRow];
}

#pragma mark - DLSegmentViewDelegate

- (void)segmentView:(DLSegmentView *)segmentView didSelectSegmentAtRow:(NSInteger)row {

    [UIView animateWithDuration:0.3
                     animations:^{
                         [segmentView setRow:row isMoveLine:YES];

                         [self.baseScrollView setContentOffset:CGPointMake(self.baseScrollView.width * row, 0) animated:NO];

                     }completion:^(BOOL finished) {

                         [self setScrollsToTopWithCurrentRow:row];
                     }];
}

#pragma mark - UnityAds Button

- (void) syncAdButtonState{
    if([UnityAds isReady]){
        [self.unityAdsButton setIsEnabled:YES];
        [self.unityAdsButton setButtonTitle:Localizer(@"point_ad_title")];
    }else{
        [self.unityAdsButton setIsEnabled:NO];
        [self.unityAdsButton setButtonTitle:Localizer(@"point_ad_loading")];
    }
}

#pragma mark - UnityAds delegate

- (void)unityAdsReady:(NSString *)placementId {
    [self.unityAdsButton setIsEnabled:YES];
}

- (void)unityAdsDidError:(UnityAdsError)error withMessage:(NSString *)message {
    [self.unityAdsButton setIsEnabled:NO];
}

- (void)unityAdsDidStart:(NSString *)placementId {
    [self.unityAdsButton setIsEnabled:NO];
    [WINDOW makeToast:Localizer(@"point_ad_title")];
}

- (void)unityAdsDidFinish:(NSString *)placementId withFinishState:(UnityAdsFinishState)state {
    switch(state){
        case kUnityAdsFinishStateError:
        case kUnityAdsFinishStateSkipped:
            [self syncAdButtonState];
            break;
        case kUnityAdsFinishStateCompleted: {
            @weakify(self)
            [DLAPIHelper addPoint:^(DLPoint *point) {
                @strongify(self)
                [self syncAdButtonState];
            }];
            break;
        }
    }
}

@end
