//
//  DLAfterPlayViewController.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/10/22.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DLAfterPlayViewControllerDelegate;

@interface DLAfterPlayViewController : UIViewController

@property (nonatomic, weak) id<DLAfterPlayViewControllerDelegate> delegate;

@end

@protocol DLAfterPlayViewControllerDelegate <NSObject>

@optional
- (void)afterPlayViewControllerTappedCloseButton;

@end
