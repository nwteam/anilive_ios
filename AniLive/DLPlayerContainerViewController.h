//
//  DLPlayerContainerViewController.h
//  AniLive
//
//  Created by isaoeka on 2016/12/25.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DLForumGroupListViewController;

@protocol DLPlayContainerViewControllerDelegate <NSObject>
@optional
@end

@interface DLPlayerContainerViewController : UIViewController

@property (nonatomic, weak) id<DLPlayContainerViewControllerDelegate> delegate;

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController;

- (void)pushViewController:(UIViewController *)content
                  animated:(BOOL)animated;
- (void)popViewController:(UIViewController *)content
                  animated:(BOOL)animated;

@end
