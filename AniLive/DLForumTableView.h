//
//  DLForumTableView.h
//  AniLive
//
//  Created by isaoeka on 2016/12/30.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DLForum;

@interface DLForumTableView : UITableView

- (void)loadForums:(NSMutableArray<DLForum *> *)forums;

@end
