//
//  DLWorkDetailVideoViewController.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/11/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLWorkDetailVideoViewController.h"
#import "DLAppFrameManager.h"
#import "DLAPIHelper.h"
#import "DLVideoHistoryCell.h"
#import "DLTransitionManager.h"
#import "DLPayingPointManager.h"
#import "UIAlertController+Category.h"
#import "DLOpenPlayMovieService.h"

@interface DLWorkDetailVideoViewController () <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *items;
@property(nonatomic, strong) NSNumber *workId;
@property(nonatomic, assign) CGFloat clearCellHeight;
@property(nonatomic, assign) NSInteger currentPageNumber;
@property(nonatomic, assign) BOOL isLoading;
@property(nonatomic, assign) BOOL isExistNextPage;

@end

@implementation DLWorkDetailVideoViewController

- (instancetype)initWithHeaderClearCellHeight:(CGFloat)clearCellHeight withWorkId:(NSNumber *)workId {
    self = [super init];
    if (self) {
        self.clearCellHeight = clearCellHeight;
        self.workId = workId;
    }
    return self;
}

#pragma mark - Override Methods

- (void)viewDidLoad {
    [super viewDidLoad];

    self.automaticallyAdjustsScrollViewInsets = NO;

    self.items = [[NSMutableArray alloc] init];

    [self initView];
    [self updateFrame];
    [self loadData:DLLoadingTypeInitial];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)setOffsetToBaseScrollView:(CGPoint)offset {
    [super setOffsetToBaseScrollView:offset];
    self.tableView.contentOffset = offset;
}

- (void)dealloc {
    FUNC_LOG
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - DLSegmentViewControllerProtocol

- (void)setScrollsToTop:(BOOL)scrollsToTop {

    self.tableView.scrollsToTop = scrollsToTop;
}

#pragma mark - Private Methods

- (void)initView {

#pragma mark Table View

    self.tableView = [[UITableView alloc] init];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.scrollsToTop = NO;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[UITableViewCell class]
           forCellReuseIdentifier:DLConstClearCellIdentifier];
    [self.tableView registerClass:[DLVideoHistoryCell class] forCellReuseIdentifier:[DLVideoHistoryCell getCellIdentifier]];
    [self.view addSubview:self.tableView];
}

- (void)updateFrame {
    self.tableView.frame = self.view.bounds;
}

- (void)loadData:(DLLoadingType)loadingType {
    
    if (self.isLoading) {
        return;
    }
    
    if (loadingType == DLLoadingTypeInitial || loadingType == DLLoadingTypePullRefresh) {
        self.currentPageNumber = 1;
        self.isExistNextPage = YES;
    }
    
    if (!self.isExistNextPage) {
        return;
    }
    
    self.isLoading = YES;
    
    @weakify(self)
    [DLAPIHelper fetchMovieByWorkID:[self.workId integerValue]
                         pageNumber:self.currentPageNumber
                            perPage:DLConstPerPageCatalogDetailMovies
                           callback:^(NSMutableArray *movies, NSError *error) {
                               @strongify(self)
                               
                               if (NOT_NULL(error)) {
                                   // TODO: エラー時のポップアップなどを表示する
                               } else {
                                   if (loadingType == DLLoadingTypeInitial || loadingType == DLLoadingTypePullRefresh) {
                                       [self.items removeAllObjects];
                                   }
                                   
                                   if (movies.count > 0) {
                                       [self.items addObjectsFromArray:movies];
                                       self.currentPageNumber += 1;
                                   } else {
                                       self.isExistNextPage = NO;
                                   }
                               }
                               
                               [self.tableView reloadData];
                               
                               self.isLoading = NO;
                           }];
}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (section == 0) {
        return 1;
    } else {
        return self.items.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section == 0) {

        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DLConstClearCellIdentifier
                                                                forIndexPath:indexPath];
        cell.userInteractionEnabled = NO;
        cell.backgroundColor = [UIColor clearColor];

        return cell;
    } else {
        
        DLVideoHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:[DLVideoHistoryCell getCellIdentifier] forIndexPath:indexPath];
        
        if (indexPath.row < self.items.count) {
            DLMovie *movieData = self.items[indexPath.row];
            [cell setCellData:movieData];
        }
        return cell;
    }
    return nil;
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section == 0) {
        return self.clearCellHeight;
    } else {
        return [DLVideoHistoryCell getCellHeight];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // TODO DLOpenPlayMovieService.tryPlayMovie を使用する
    if (indexPath.row < self.items.count) {
        DLMovie *movieData = self.items[indexPath.row];

        [DLOpenPlayMovieService tryPlayMovie:movieData
                                        live:nil
                          fromViewController:self
                                    callback:^{

                                    }];
    }
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    self.currentContentOffset = scrollView.contentOffset;

    if ([self.delegate respondsToSelector:@selector(retractableScrollViewDidScroll:)]) {
        [self.delegate retractableScrollViewDidScroll:scrollView];
    }
    
    CGFloat offsetY = scrollView.contentOffset.y;
    
    if (offsetY > self.tableView.contentSize.height - self.tableView.height - DLConstPagingBottomMargin) {
        [self loadData:DLLoadingTypePaging];
    }
}

@end
