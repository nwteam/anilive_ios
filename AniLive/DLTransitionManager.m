//
//  DLTransitionManager.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLTransitionManager.h"
#import "DLNavigationController.h"
#import "DLMoviePlayViewController.h"
#import "DLForumListViewController.h"
#import "DLWorkDetailViewController.h"
#import "DLSettingsViewController.h"
#import "DLForumViewController.h"
#import "DLWorkInfoViewController.h"
#import "DLWatchingPlanViewController.h"

#import "DLMovie.h"
#import "DLLive.h"
#import "DLWork.h"
#import "DLForumGroup.h"
#import "DLAppDelegate.h"


@implementation DLTransitionManager

// 最前面のUIViewControllerを取得
+ (UIViewController *)getMostTopController {

    UIViewController *topController;
    topController = [UIApplication sharedApplication].keyWindow.rootViewController;

    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }

    while ([topController isKindOfClass:[UINavigationController class]] || [topController isKindOfClass:[UITabBarController class]]) {
        if ([topController isKindOfClass:[UINavigationController class]]) {
            topController = ((UINavigationController *) topController).topViewController;
        }

        if ([topController isKindOfClass:[UITabBarController class]]) {
            topController = ((UITabBarController *) topController).selectedViewController;
        }
    }

    return topController;
}

+ (DLAppDelegate *)getAppDelegate {
    return (DLAppDelegate *)[[UIApplication sharedApplication] delegate];
}

+ (void)showMoviePlayViewControllerWithMovieData:(DLMovie *)movieData withTarget:(UIViewController *)target {
    [self showMoviePlayViewControllerWithMovieData:movieData withTarget:target withIsModal:NO];
}

+ (void)showMoviePlayViewControllerWithMovieData:(DLMovie *)movieData withTarget:(UIViewController *)target withIsModal:(BOOL)isModal {
    
    UIViewController *targetController;
    
    if (NOT_NULL(target)) {
        targetController = target;
    } else {
        targetController = [self getMostTopController];
    }
    
    DLMoviePlayViewController *controller = [[DLMoviePlayViewController alloc] initWithMovieData:movieData withIsModal:isModal];
    
    if (isModal) {
        [targetController presentViewController:controller animated:YES completion:nil];
    } else {
        if (NOT_NULL(targetController.navigationController)) {
            [targetController.navigationController pushViewController:controller animated:YES];
        }
    }
}

+ (void)showMoviePlayViewControllerWithLiveData:(DLLive *)liveData withTarget:(UIViewController *)target {
    [self showMoviePlayViewControllerWithLiveData:liveData withTarget:target withIsModal:NO];
}

+ (void)showMoviePlayViewControllerWithLiveData:(DLLive *)liveData withTarget:(UIViewController *)target withIsModal:(BOOL)isModal {

    UIViewController *targetController;

    if (NOT_NULL(target)) {
        targetController = target;
    } else {
        targetController = [self getMostTopController];
    }

    DLMoviePlayViewController *controller = [[DLMoviePlayViewController alloc] initWithLiveData:liveData withIsModal:isModal];

    if (isModal) {
        [targetController presentViewController:controller animated:YES completion:nil];
    } else {
        if (NOT_NULL(targetController.navigationController)) {
            [targetController.navigationController pushViewController:controller animated:YES];
        }
    }
}

+ (void)showForumListViewControllerWithForumGroup:(DLForumGroup *)group withTarget:(UIViewController *)target {

    // RootにあるViewControllerにアクセスする場合のみAppDelegateから取得する
    DLAppDelegate *appDelegate = [DLTransitionManager getAppDelegate];

    DLForumListViewController *controller = DLForumListViewController.new;
    controller.group = group;
    [appDelegate.rootNavigationController pushViewController:controller animated:YES];
}

+ (void)showForumListViewControllerWithForum:(DLForum *)froum withTarget:(UIViewController *)target {
    
    // RootにあるViewControllerにアクセスする場合のみAppDelegateから取得する
    DLAppDelegate *appDelegate = [DLTransitionManager getAppDelegate];
    
    DLForumListViewController *controller = DLForumListViewController.new;
    controller.froum = froum;
    [appDelegate.rootNavigationController pushViewController:controller animated:YES];
}

+ (void)showForumDetailViewControllerWithForum:(DLForum *)forum withTarget:(UIViewController *)target {
    
    // RootにあるViewControllerにアクセスする場合のみAppDelegateから取得する
    DLAppDelegate *appDelegate = [DLTransitionManager getAppDelegate];
    
    DLForumViewController *controller = DLForumViewController.new;
    controller.forum = forum;
    [controller refreshForum];
    [appDelegate.rootNavigationController pushViewController:controller animated:YES];
}

+ (void)showWorkDetailViewControllerWithWorkData:(DLWork *)workData withTarget:(UIViewController *)target{

    DLWorkDetailViewController *controller = [[DLWorkDetailViewController alloc]initWithWorkData:workData];

    if (NOT_NULL(target) && NOT_NULL(target.navigationController)) {
        [target.navigationController pushViewController:controller animated:YES];
    } else {
        // RootにあるViewControllerにアクセスする場合のみAppDelegateから取得する
        DLAppDelegate *appDelegate = [DLTransitionManager getAppDelegate];
        [appDelegate.rootNavigationController pushViewController:controller animated:YES];
    }
}

+ (void)showWorkInfoViewControllerWithWorkData:(DLWork *)workData withTarget:(UIViewController *)target {
    
    DLWorkInfoViewController *controller = [[DLWorkInfoViewController alloc]initWithWorkData:workData];
    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    if (NOT_NULL(target)) {
        [target presentViewController:controller animated:YES completion:nil];
    } else {
        [[self getMostTopController] presentViewController:controller animated:YES completion:nil];
    }
}

+ (void)showSettingsViewComtroller:(UIViewController *)target withIsModal:(BOOL)isModal {

    UIViewController *targetController;
    if (NOT_NULL(target)) {
        targetController = target;
    } else {
        targetController = [self getMostTopController];
    }

    DLSettingsViewController *controller = [[DLSettingsViewController alloc] initWithIsModal:isModal];

    if (isModal) {
        [targetController presentViewController:controller animated:YES completion:nil];
    } else {
        if (NOT_NULL(targetController.navigationController)) {
            [targetController.navigationController pushViewController:controller animated:YES];
        }
    }
}

+ (void)showRegisterViewController:(UIViewController *)target withIsModal:(BOOL)isModal{
    UIViewController *targetController;
    if (NOT_NULL(target)) {
        targetController = target;
    } else {
        targetController = [self getMostTopController];
    }

    DLWatchingPlanViewController *controller = [DLWatchingPlanViewController new];

    if (isModal) {
        [targetController presentViewController:controller animated:YES completion:nil];
    } else {
        if (NOT_NULL(targetController.navigationController)) {
            [targetController.navigationController pushViewController:controller animated:YES];
        }
    }
}

@end
