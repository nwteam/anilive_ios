//
// Created by Kotaro Itoyama on 2016/10/23.
// Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLForumGroup.h"
#import "DLForum.h"

@interface DLForumGroup ()
@property(readwrite, nonatomic, strong) DLMovie *movie;
@property(readwrite, nonatomic, strong) NSArray *forums;
@end

@implementation DLForumGroup {

}

+ (instancetype)createFromMovie:(DLMovie *)movie andForums:(NSArray *)forums {
    DLForumGroup *group = self.new;
    group.movie = movie;
    group.forums = forums;
    return group;
}

+ (NSMutableArray *)groupsFromForums:(NSMutableArray *)forums andMovies:(NSArray *)movies {
    NSMutableArray *groups = NSMutableArray.array;
    for (DLMovie *movie in movies) {
        NSMutableIndexSet *discards = [NSMutableIndexSet indexSet];
        [forums enumerateObjectsUsingBlock:^(DLForum *forum, NSUInteger idx, BOOL *stop) {
            if ([movie.movieId isEqualToNumber:forum.movieId]) {
                [discards addIndex:idx];
            }
        }];
        [groups addObject:[DLForumGroup createFromMovie:movie andForums:[forums objectsAtIndexes:discards]]];
        [forums removeObjectsAtIndexes:discards];
    }
    return groups;
}


@end