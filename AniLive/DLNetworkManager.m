//
//  DLNetworkManager.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/19.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLNetworkManager.h"
#import "DLReachability.h"

@interface DLNetworkManager ()

@property (nonatomic, strong) DLReachability *reachability;

@end

@implementation DLNetworkManager

+ (instancetype)sharedInstance {
    static DLNetworkManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[DLNetworkManager alloc] init];
        [manager initConfig];
    });
    
    return manager;
}

#pragma mark - init

- (void)initConfig {
    
    self.reachability = [DLReachability reachabilityForInternetConnection];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiedNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    [self.reachability startNotifier];
    
    self.networkStatus = (NSInteger)[self.reachability currentReachabilityStatus];
}

#pragma mark - Reachability Notification

- (void)notifiedNetworkStatus:(NSNotification *)notification {
    
    self.networkStatus = (NSInteger)[self.reachability currentReachabilityStatus];
}

#pragma mark - Public Methods

- (BOOL)hasInternetConnection {
    
    if (self.networkStatus > ORVNetworkTypeNotReachable) {
        return YES;
    }
    
    return NO;
}

- (ORVNetworkType)getNetworkStatus {
    
    return self.networkStatus;
}

#pragma mark - Dealloc

- (void)dealloc {
    
    [self.reachability stopNotifier];
}

@end
