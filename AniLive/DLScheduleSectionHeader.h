//
//  DLScheduleSectionHeader.h
//  AniLive
//
//  Created by Loyal on 2016/12/21.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DLScheduleSectionHeader : UIView

+ (CGFloat)getSectionHeaderHeight;

- (void)setTitle:(NSString *)title;

@end

