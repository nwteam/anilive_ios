//
//  DLPlayerView.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/17.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPlayerView.h"
#import "DLPlayer.h"

@implementation DLPlayerView

+(Class)layerClass{
    
    return [AVPlayerLayer class];
}

-(AVPlayer *)player{
    
    return [(AVPlayerLayer *)[self layer] player];
}

-(void)setPlayer:(AVPlayer *)player{
    
    [(AVPlayerLayer *)[self layer] setPlayer:player];
}

@end
