//
//  UIImageView+Category.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/15.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "UIImageView+Category.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <objc/runtime.h>

@implementation UIImageView (Category)

+ (void)loadImageWithURLString:(NSString*)urlString targetImageView:(UIImageView*)targetImageView {
    [self loadImageWithURLString:urlString targetImageView:targetImageView placeholderImage:nil isEnableCache:YES isRealTimeUpdate:NO forceAnimationType:DLForceAnimationTypeNoSelected completionBlock:nil];
}

+ (void)loadImageWithURLString:(NSString*)urlString targetImageView:(UIImageView*)targetImageView completionBlock:(void(^)())completionBlock {
    [self loadImageWithURLString:urlString targetImageView:targetImageView placeholderImage:nil isEnableCache:YES isRealTimeUpdate:NO forceAnimationType:DLForceAnimationTypeNoSelected completionBlock:completionBlock];
}

+ (void)loadImageWithURLString:(NSString*)urlString targetImageView:(UIImageView*)targetImageView isEnableCache:(BOOL)isEnableCache {
    [self loadImageWithURLString:urlString targetImageView:targetImageView placeholderImage:nil isEnableCache:isEnableCache isRealTimeUpdate:NO forceAnimationType:DLForceAnimationTypeNoSelected completionBlock:nil];
}

+ (void)loadImageWithURLString:(NSString*)urlString targetImageView:(UIImageView*)targetImageView placeholderImage:(UIImage*)placeholderImage {
    [self loadImageWithURLString:urlString targetImageView:targetImageView placeholderImage:placeholderImage isEnableCache:YES isRealTimeUpdate:NO forceAnimationType:DLForceAnimationTypeNoSelected completionBlock:nil];
}

+ (void)loadImageWithURLString:(NSString*)urlString targetImageView:(UIImageView*)targetImageView placeholderImage:(UIImage*)placeholderImage isEnableCache:(BOOL)isEnableCache {
    [self loadImageWithURLString:urlString targetImageView:targetImageView placeholderImage:placeholderImage isEnableCache:isEnableCache isRealTimeUpdate:NO forceAnimationType:DLForceAnimationTypeNoSelected completionBlock:nil];
}

+ (void)loadImageWithURLString:(NSString*)urlString targetImageView:(UIImageView*)targetImageView placeholderImage:(UIImage*)placeholderImage isEnableCache:(BOOL)isEnableCache isRealTimeUpdate:(BOOL)isRealTimeUpdate forceAnimationType:(DLForceAnimationType)forceAnimationType {
    [self loadImageWithURLString:urlString targetImageView:targetImageView placeholderImage:placeholderImage isEnableCache:isEnableCache isRealTimeUpdate:NO forceAnimationType:DLForceAnimationTypeNoSelected completionBlock:nil];
}

+ (void)loadImageWithURLString:(NSString*)urlString targetImageView:(UIImageView*)targetImageView placeholderImage:(UIImage*)placeholderImage isEnableCache:(BOOL)isEnableCache isRealTimeUpdate:(BOOL)isRealTimeUpdate forceAnimationType:(DLForceAnimationType)forceAnimationType completionBlock:(void(^)())completionBlock {
    
    // 既に他の画像URLをロード中の場合に、その処理をキャンセルする制御。
    // この処理がないと一覧画面をスクロール中にセルに違う画像が表示されてしまう。
    id<SDWebImageOperation> lastOperation = [targetImageView operation];
    if (NOT_NULL(lastOperation)) {
        [lastOperation cancel];
    }
    
    if (!NOT_EMPTY(urlString)) {
        return;
    }
    
    SDWebImageOptions options = SDWebImageHandleCookies;
    if (isEnableCache) {
        options |= SDWebImageRetryFailed;
    } else {
        options |= SDWebImageCacheMemoryOnly;
    }
    if (isRealTimeUpdate) {
        [[SDImageCache sharedImageCache] removeImageForKey:urlString fromDisk:YES];
    }
    
    if (NOT_NULL(placeholderImage)) {
        targetImageView.image = placeholderImage;
        targetImageView.alpha = 1.0;
    }
    
    id<SDWebImageOperation> operation = [[SDWebImageManager sharedManager]
                                         downloadImageWithURL:[NSURL URLWithString:[urlString stringByReplacingOccurrencesOfString:@"http://" withString:@"https://"]]
                                         options:options
                                         progress:nil
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                             
                                             // 既にImageViewが同じ画像を読み込み済みの場合に、再表示処理をしないように制御
                                             // この制御がないと、一瞬画像がちらついてしまう。
                                             if ([targetImageView.image isEqual:image]) {
                                                 if (completionBlock) {
                                                     completionBlock();
                                                 }
                                                 
                                                 return;
                                             }
                                             
                                             BOOL isNeedAnimation = NO;
                                             
                                             switch (forceAnimationType) {
                                                 case DLForceAnimationTypeNoAnimation:
                                                     isNeedAnimation = NO;
                                                     break;
                                                 case DLForceAnimationTypeDoAnimation:
                                                     isNeedAnimation = YES;
                                                     break;
                                                 default:
                                                 {
                                                     switch (cacheType) {
                                                         case SDImageCacheTypeMemory:
                                                             isNeedAnimation = NO;
                                                             break;
                                                         case SDImageCacheTypeDisk:
                                                         default:
                                                             isNeedAnimation = YES;
                                                             break;
                                                     }
                                                 }
                                                     break;
                                             }
                                             
                                             if (isNeedAnimation) {
                                                 targetImageView.alpha = 0.0;
                                                 [UIView transitionWithView:targetImageView
                                                                   duration:0.2
                                                                    options:UIViewAnimationOptionTransitionCrossDissolve
                                                                 animations:^{
                                                                     targetImageView.image = image;
                                                                     targetImageView.alpha = 1.0;
                                                                 }
                                                                 completion:nil];
                                             } else {
                                                 targetImageView.image = image;
                                                 targetImageView.alpha = 1.0;
                                             }
                                             
                                             if (completionBlock) {
                                                 completionBlock();
                                             }
                                             
                                         }];
    
    [targetImageView setOperation:operation];
}

#pragma mark - Setter Getter Methods

- (void)setOperation:(id<SDWebImageOperation>)operation {
    objc_setAssociatedObject(self, _cmd, operation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id<SDWebImageOperation>)operation {
    return objc_getAssociatedObject(self, @selector(setOperation:));
}

@end
