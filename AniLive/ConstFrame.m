//
//  ConstFrame.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "ConstFrame.h"

@implementation ConstFrame

//ナビゲーションバー
const CGFloat DLConstNavigationBarHeight = 64;

//タブバー
const CGFloat DLConstTabBarHeight = 56;

// デフォルトマージン
const CGFloat DLConstDefaultMargin = 16;

@end
