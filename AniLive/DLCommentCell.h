//
//  DLCommentCell.h
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/10/24.
//  Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLSuperTableViewCell.h"
#import "DLComment.h"

@interface DLCommentCell : DLSuperTableViewCell

@property(nonatomic, strong) DLComment *comment;

+ (UINib *)getDefaultNib;
+ (CGFloat)getCellHeight;

@end
