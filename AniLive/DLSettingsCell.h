//
//  DLSettingsCell.h
//  AniLive
//
//  Created by Isao Kono on 2016/11/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLSuperTableViewCell.h"

typedef NS_ENUM(NSInteger, DLSettingsCellType) {
    DLSettingsCellTypeNone = 0,
    DLSettingsCellTypeHoge,
};

extern const NSString* kSettingCell_userDefaultsKey;

@interface DLSettingsCell : DLSuperTableViewCell

+ (NSString *)getCellIdentifier;

+ (CGFloat)getCellBaseHeight;
- (CGFloat)getCellHeight;
- (void)setCellData:(NSDictionary *)args;

@end
