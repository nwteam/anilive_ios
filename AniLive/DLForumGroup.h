//
// Created by Kotaro Itoyama on 2016/10/23.
// Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//


#import "DLModelBase.h"

#import "DLMovie.h"

@interface DLForumGroup : NSObject

@property(readonly, nonatomic, strong) DLMovie *movie;
@property(readonly, nonatomic, strong) NSArray *forums;

+ (instancetype)createFromMovie:(DLMovie *)movie andForums:(NSArray *)forums;

+ (NSMutableArray *)groupsFromForums:(NSMutableArray *)forums andMovies:(NSArray *)movies;
@end