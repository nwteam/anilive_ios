//
//  DLWatch.m
//  AniLive
//
//  Created by ykkc on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLWatch.h"

@interface DLWatch ()
@property (readwrite, nonatomic, strong) NSString *ssrcd;
@property (readwrite, nonatomic, strong) NSNumber *productId;
@property (readwrite, nonatomic, strong) NSString *movieInitId;
@property (readwrite, nonatomic, strong) NSString *playUrl;
@property (readwrite, nonatomic, strong) NSString *sttmvUrl;
@property (readwrite, nonatomic, strong) NSMutableArray *bwLabel;
@property (readwrite, nonatomic, strong) NSString *captionUrl;
@property (readwrite, nonatomic, strong) NSMutableArray *captionLang;
@property (readwrite, nonatomic, strong) NSString *copyrightStr;
@property (readwrite, nonatomic, strong) NSString *preimgUrl;
@property (readwrite, nonatomic, assign) BOOL autoplayFlag;
@property (readwrite, nonatomic, strong) NSString *worksTitle;
@property (readwrite, nonatomic, strong) NSString *adId;
@property (readwrite, nonatomic, strong) NSString *moviesTitle;
@property (readwrite, nonatomic, strong) NSString *fov;
@property (readwrite, nonatomic, strong) NSString *subscriptionStatus;
@property (readwrite, nonatomic, strong) NSString *productid;

@end

@implementation DLWatch

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.ssrcd = CHECK_NULL_DATA_STRING(attributes[@"ssrcd"]);
    self.productId = CHECK_NULL_DATA_NUMBER(attributes[@"product_id"]);
    self.movieInitId = CHECK_NULL_DATA_STRING(attributes[@"init_id"]);
    self.playUrl = CHECK_NULL_DATA_STRING(attributes[@"play_url"]);
    self.sttmvUrl = CHECK_NULL_DATA_STRING(attributes[@"sttmv_url"]);
    //    self.bwLabel @"bw_label"
    self.captionUrl = CHECK_NULL_DATA_STRING(attributes[@"caption_url"]);
    //    self.captionLang = @"caption_lang"
    self.copyrightStr = CHECK_NULL_DATA_STRING(attributes[@"copyright_str"]);
    self.preimgUrl = CHECK_NULL_DATA_STRING(attributes[@"preimg_url"]);
    NSNumber *isBoolValue = CHECK_NULL_DATA_NUMBER(attributes[@"autoplay_flag"]);
    self.autoplayFlag = isBoolValue.boolValue;
    self.worksTitle = CHECK_NULL_DATA_STRING(attributes[@"works_title"]);
    self.adId = CHECK_NULL_DATA_STRING(attributes[@"ad_id"]);
    self.moviesTitle = CHECK_NULL_DATA_STRING(attributes[@"movies_title"]);
    self.fov = CHECK_NULL_DATA_STRING(attributes[@"fov"]);
    self.subscriptionStatus = CHECK_NULL_DATA_STRING(attributes[@"subscription_status"]);
    self.productid = CHECK_NULL_DATA_STRING(attributes[@"product_id"]);
    
    return self;
}

@end
