//
//  NSString+Category.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/17.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "NSString+Category.h"

@implementation NSString (Category)

+ (NSString*)commasFormatedStringFromNumber:(NSNumber*)number {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setPositiveFormat:@",###"];
    NSString *formattedString = [formatter stringForObjectValue:number];
    return formattedString;
}

+ (NSString*)formatSecTimeToString:(float)time {
    int s = fmod(time, 60);
    int m = (time - s) / 60;
    if (m >= 60) {
        int h = m / 60;
        m = m % 60;
        return [NSString stringWithFormat:@"%01d:%02d:%02d", h, m, s];
    } else {
        return [NSString stringWithFormat:@"%01d:%02d", m, s];
    }
}

+ (NSString*)formatMillisecTimeToString:(float)time {
    
    return [self formatSecTimeToString:(time / 1000)];
}

- (NSMutableAttributedString*)getAdjustLineSpaceStringWithLineSpacing:(CGFloat)lineSpacing {
    return [self getAdjustLineSpaceStringWithLineSpacing:lineSpacing withLineBreakMode:NSLineBreakByWordWrapping];
}

- (NSMutableAttributedString*)getAdjustLineSpaceStringWithLineSpacing:(CGFloat)lineSpacing withLineBreakMode:(NSLineBreakMode)lineBreakMode {
    return [self getAdjustLineSpaceStringWithLineSpacing:lineSpacing withLineBreakMode:lineBreakMode withTextAlignment:NSTextAlignmentLeft];
}

- (NSMutableAttributedString*)getAdjustLineSpaceStringWithLineSpacing:(CGFloat)lineSpacing withLineBreakMode:(NSLineBreakMode)lineBreakMode withTextAlignment:(NSTextAlignment)textAlignment {
    NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
    paragrahStyle.lineSpacing = lineSpacing;
    paragrahStyle.alignment = textAlignment;
    paragrahStyle.lineBreakMode = lineBreakMode;
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:self];
    [attributedText addAttribute:NSParagraphStyleAttributeName
                           value:paragrahStyle
                           range:NSMakeRange(0, attributedText.length)];
    return attributedText;
}

+ (void)attributeString:(NSMutableAttributedString*)mutableAttributedString withString:(NSString*)string withFont:(UIFont*)font withColor:(UIColor*)color {
    NSDictionary *stringAttributes = @{ NSForegroundColorAttributeName : color,
                                        NSFontAttributeName : font };
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:string
                                                                           attributes:stringAttributes];
    [mutableAttributedString appendAttributedString:attributedString];
}

+ (void)attributeParagraphStyle:(NSMutableAttributedString*)mutableAttributedString withParagraphStyle:(NSMutableParagraphStyle*)paragrahStyle {
    [mutableAttributedString addAttribute:NSParagraphStyleAttributeName
                                     value:paragrahStyle
                                     range:NSMakeRange(0, mutableAttributedString.length)];
}

+ (NSString *) calcMultiple: (NSInteger) number unit:(NSString *) unit {
    return [NSString stringWithFormat:@"%d %@%@",
            number,
            unit,
            number < 2 ? @"" : @"s"
    ];
}

@end
