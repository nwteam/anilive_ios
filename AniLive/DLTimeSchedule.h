//
// Created by Loyal on 2016/12/22.
// Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLModelBase.h"

@interface DLTimeSchedule : DLModelBase
@property(readonly, nonatomic, strong) NSNumber *liveId;
@property(readonly, nonatomic, strong) NSNumber *movieId;
@property(readonly, nonatomic, strong) NSDate *createdAt;
@property(readonly, nonatomic, strong) NSDate *updatedAt;
@property(readonly, nonatomic, strong) NSString *title;
@property(readonly, nonatomic, strong) NSNumber *totalViewUserCount;
@property(readonly, nonatomic, assign) BOOL isOnAir;
@property(readonly, nonatomic, strong) NSNumber *totalCommentCount;
@property(readonly, nonatomic, strong) NSDate *broadcastStartAt;
@property(readonly, nonatomic, strong) NSDate *broadcastEndAt;
@property(readonly, nonatomic, assign) NSNumber *totalMovieLength;
@property(readonly, nonatomic, strong) NSString *thumbMovieUrl;
@property (readonly, nonatomic, strong) NSString *smallImage;
@property (readonly, nonatomic, strong) NSString *mediumImage;
@property (readonly, nonatomic, strong) NSString *largeImage;
@end
