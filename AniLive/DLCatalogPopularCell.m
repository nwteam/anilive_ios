//
//  DLCatalogPopularCell.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/10/01.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLCatalogPopularCell.h"
#import "DLAppFrameManager.h"
#import "UIImageView+Category.h"
#import "NSString+Category.h"

const static CGFloat kFooterViewHeight = 35;

@interface DLCatalogPopularCell ()

@property (nonatomic, strong) UIImageView *thumbnailImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *footerView;

@end

@implementation DLCatalogPopularCell

#pragma mark - ORVSuperCollectionViewCellProtocol

- (void)initView {
    [super initView];
    
    self.backgroundColor = [UIColor clearColor];
    
    self.thumbnailImageView = [[UIImageView alloc]init];
    self.thumbnailImageView.backgroundColor = [UIColor lightGrayColor];
    self.thumbnailImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.thumbnailImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.thumbnailImageView];
    
    self.footerView = [[UIView alloc]init];
    self.footerView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.footerView];
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = self.footerView.backgroundColor;
    self.titleLabel.font = BASE_FONT(12);
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.numberOfLines = 2;
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.footerView addSubview:self.titleLabel];
}

- (void)initializeCell {
    [super initializeCell];
    
    self.titleLabel.text = @"";
    self.thumbnailImageView.image = nil;
}

+ (NSString *)getCellIdentifier {
    
    return NSStringFromClass([self class]);
}

#pragma mark - Class Methods

+ (CGSize)getCellSizeWithParantWidth:(CGFloat)parantWidth {
    
    NSInteger numberOfCell = [[DLAppFrameManager sharedInstance] getNumberOfCellPerRowForCatalog];
    
    CGFloat cellWidth = (parantWidth - ((numberOfCell + 1) * kCatalogCellSpaceMargin)) / numberOfCell;
    CGFloat cellHeight = (cellWidth / 2 * 3) + kFooterViewHeight;
    
    return CGSizeMake(cellWidth, cellHeight);
}

#pragma mark - Override Methods

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
}

#pragma mark - Public Methods

- (void)setCellData:(DLWork *)workData {
    
    if (NOT_NULL(workData.image1)) {
        [UIImageView loadImageWithURLString:workData.image1
                            targetImageView:self.thumbnailImageView];
    }
    
    self.titleLabel.attributedText = [workData.title getAdjustLineSpaceStringWithLineSpacing:0 withLineBreakMode:NSLineBreakByTruncatingTail];
    
    [self updateFrame];
}

#pragma mark - Private Methods

- (void)updateFrame {
    
    CGFloat thumbnailHeight = self.width / 2 * 3;
    
    self.thumbnailImageView.frame = CGRectMake(0, 0, self.width, thumbnailHeight);
    
    self.footerView.frame = CGRectMake(0, self.thumbnailImageView.maxY, self.width, kFooterViewHeight);
    
    self.titleLabel.width = self.width;
    [self.titleLabel sizeToFit];
    self.titleLabel.x = 0;
    self.titleLabel.y = 4;
}

@end
