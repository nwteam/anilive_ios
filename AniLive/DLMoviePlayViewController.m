//
//  DLMoviePlayViewController.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/21.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

@import AVFoundation;
@import PSDKLibrary;
@import MediaPlayer;

// ViewControllers
#import "DLMoviePlayViewController.h"
#import "DLAfterPlayViewController.h"
#import "DLPlayerContainerViewController.h"
#import "DLPlayerForumDetailViewController.h"
#import "DLPlayerForumListViewController.h"

// Views
#import "DLNavigationBarView.h"
#import "DLAdPolicySelector.h"
#import "DLPlayerOptionBarView.h"
#import "DLSimpleForumTitleView.h"
#import "DLPlayerCommentView.h"
#import "DLSeeLaterButton.h"
#import "DLPlayerViewIsolated.h"
#import "UIView+Toast.h"
#import "DLYoutubePlayer.h"
#import "DLContinueToPlayView.h"
#import "DLPlayerViewPromotionAd.h"

// Managers
#import "DLAppFrameManager.h"
#import "DLPrimeTimeConfigManager.h"
#import "DLMoviePlayerDataManager.h"

// Models
#import "DLMovie.h"
#import "DLLive.h"
#import "DLForum.h"
#import "DLWatch.h"
#import "DLCaptionItem.h"

// Other
#import "DLAppDelegate.h"
#import "PTVideoAnalyticsTracker.h"
#import "M3U8Parser.h"
#import "NSString+Category.h"


static NSString *kPlayerIsPlayingKeyPath = @"isPlaying";
static NSString *kIsFullScreenKeyPath = @"isFullScreen";

// user defaults
static NSString *kUserDefaultsTopicAutoChangeKey = @"topicAutoChangeKey";

@interface DLMoviePlayViewController () <DLNavigationBarViewDelegate, DLPlayerControlViewDelegate, DLPlayerBaseViewDelegate, DLAfterPlayViewControllerDelegate, DLPlayerOptionBarViewDelegate, DLPlayerCommentViewDelegate, DLSimpleForumTitleViewDelegate, DLPlayerViewPrimeTimeDelegate, DLPlayerViewPromotionAdDelegate, DLMoviePlayerDataManagerDelegate, DLPlayerCommentViewSenderDelegate>

@property (nonatomic, weak) DLAppDelegate *appDelegate;

@property (nonatomic, strong) PTMediaPlayer *mediaPlayer;
@property (nonatomic, strong) PTVideoAnalyticsTracker *videoAnalyticsTracker;
@property (nonatomic, strong) PTMediaPlayerItem *mediaPlayerItem;
@property (nonatomic, strong) DLMoviePlayerDataManager *forumDataManager;

// Movie
@property (nonatomic, strong) UIView<DLPlayerViewIsolatedProtocol> *playerBaseView;
@property (nonatomic, strong) PTMediaPlayerView *playerView;
@property (nonatomic, strong) DLPlayerCommentView *growingCommentView;
@property (nonatomic, strong) DLContinueToPlayView *afterPlayView;
@property (nonatomic, strong) DLSimpleForumTitleView *simpleForumTitleView;
@property (nonatomic, strong) DLPlayerViewPromotionAd *promotionMoviePlayerView;
//TODO: これちょっとやめたいですね…
@property (nonatomic, strong) NSString *currentPlayTime;

// Container
@property (nonatomic, strong) DLPlayerContainerViewController *playerContanerViewController;
@property (nonatomic, strong) DLPlayerForumDetailViewController *playerForumDetailVC;
@property (nonatomic, strong) DLPlayerForumListViewController *playerForumListVC;

// Models
@property (nonatomic, strong) DLLive *mLive;
@property (nonatomic, strong) DLMovie *mMovie;
@property (nonatomic, strong) DLWatch *mWatch;
@property (nonatomic, strong) DLCaptionItem *mCaptionItem;
@property (nonatomic, strong) NSMutableArray<DLMovie *> *mMoviePlayList;
@property (nonatomic, strong) NSMutableArray<DLCaptionItem *> *captionItemList;

// flags
@property (nonatomic, assign) BOOL isLive;
@property (nonatomic, assign) BOOL isModal;
@property (nonatomic, assign) BOOL isEnableRotate;
@property (nonatomic, assign) BOOL isFullScreen;
@property (nonatomic, assign) BOOL isForumCommentModeInLandScape;
@property (nonatomic, assign) BOOL isLiveEscapeFirstAds;
@property (nonatomic, assign) BOOL isDisplayedKeyboard;

@property(nonatomic) BOOL isForceRequestAds;
@property(nonatomic) NSTimeInterval playAdsTime;
@property(nonatomic) BOOL isConfluence;
@property(nonatomic) NSTimeInterval livePassedMovieTime;
@property(nonatomic) NSTimeInterval livePassedAdTime;
@property(nonatomic) NSUInteger mMoviePlayIndex;
@property(nonatomic, strong) DLAdPolicySelector *adPolicySelector;
@property(nonatomic, strong) NSDate *playRequestedTime;
@property(nonatomic, strong) NSMutableString *tmpXmlCaption;
@property(nonatomic, strong) NSMutableDictionary *tmpCaptionItemData;
@property(nonatomic) int captionPosition;

@end

@implementation DLMoviePlayViewController

- (instancetype)initWithMovieData:(DLMovie *)movieData withIsModal:(BOOL)isModal {
    self = [super init];
    if (self) {
        self.mMovie = movieData;
        self.isLive = NO;
        self.isModal = isModal;
        self.isDisplayedKeyboard = NO;
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithLiveData:(DLLive *)liveData withIsModal:(BOOL)isModal {
    self = [super init];
    if (self) {
        self.mLive = liveData;
        self.isLive = YES;
        self.isModal = isModal;
        self.isDisplayedKeyboard = NO;
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
    self.mMoviePlayIndex = 0;
    self.mMoviePlayList = [NSMutableArray new];
    self.forumDataManager = [DLMoviePlayerDataManager new];
    self.forumDataManager.delegate = self;
}

- (void)playLive {
    self.playRequestedTime = NSDate.new;
    NSTimeInterval compareOpen = [self.mLive.broadcastOpen timeIntervalSinceDate:self.playRequestedTime];
    NSTimeInterval compareClose= [self.mLive.broadcastClose timeIntervalSinceDate:self.playRequestedTime];
    void (^callback)(NSMutableArray<DLMovie *> *, NSError *) = ^(NSMutableArray<DLMovie *> *movies, NSError *error) {
        [self.mMoviePlayList addObjectsFromArray:movies];
        if(self.mMoviePlayList.count > 0) {
            self.livePassedMovieTime = 0;
            self.mMoviePlayIndex = 0;
            self.captionPosition = 0;
            [self requestWatch];
        }
        [self loadInitialForum];
    };
    
    if (compareOpen > 300) {
        // Live開始5分以上前
        [WINDOW makeToast:@"This live stream is not available right now. Please wait a moment."];
        // TODO 以下を動くようにする
        [self dismiss];
        return;
    }else if (300 >= compareOpen && compareOpen > 0) {
        // Live開始前5分以内
        self.isLiveEscapeFirstAds = NO;
        self.isForceRequestAds = YES;
        self.playAdsTime = compareOpen;
    }else if (0 >= compareOpen && compareClose >= 0) {
        // Live中
        self.isConfluence = YES;
        // この時点で格納されるのはこのライブの開始時刻からの経過時間
        self.livePassedMovieTime = -compareOpen;
        callback = ^(NSMutableArray<DLMovie *> *movies, NSError *error) {
            [self.mMoviePlayList addObjectsFromArray:movies];
            if (self.mMoviePlayList.count > 0) {
                int sumLength = 0,
                        diff = 0;
                NSUInteger liveOffsetIndex = 0;
                for (NSUInteger i = 0; i < self.mMoviePlayList.count; i++) {
                    diff = sumLength;
                    sumLength += self.mMoviePlayList[i].playTime.intValue;
                    liveOffsetIndex = i;
                    if (self.livePassedMovieTime < sumLength) {
                        // 本編再生導線
                        self.livePassedAdTime = 0;
                        break;
                    } else {
                        sumLength += self.playAdsTime;
                        NSTimeInterval ad_remains = sumLength - self.livePassedMovieTime;
                        if (ad_remains > 0) {
                            // 広告再生導線
                            self.livePassedAdTime = self.playAdsTime - ad_remains;
                            self.livePassedMovieTime = 0;
                            break;
                        }
                    }
                }

                self.mMoviePlayIndex = liveOffsetIndex;
                // この時点で再生しようとしている動画の先頭からの経過時間に変換される
                if(self.livePassedMovieTime > 0) {
                    self.livePassedMovieTime -= diff;

                    // 恐らくこれが必要とされるケースには到達しないと思われるが、
                    // ロジックの不備がないことをまだ確認できていないためフェイルセーフのため用意しておく
                    self.livePassedMovieTime = MAX(self.livePassedMovieTime, 0);
                }
                [self requestWatch];
                [self loadInitialForum];
            }
        };
    }else if(0 > compareClose && compareClose >= -604800){
        // Live公開終了から168時間以内
    }else if(-604800 > compareClose){
        // Live公開終了から168時間以上
        [WINDOW makeToast:@"This live has already closed."];
        // TODO 以下を動くようにする
        [self dismiss];
        return;
    }

    [DLAPIHelper fetchMoviesWithLiveId:self.mLive.liveId.integerValue
                              callback:callback];
}

- (void)dismiss{
    if(self.navigationController){
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)playMovie {
    self.playRequestedTime = NSDate.new;
    NSTimeInterval compareOpen = [self.mMovie.releaseOpen timeIntervalSinceDate:self.playRequestedTime];
    NSTimeInterval compareClose= [self.mMovie.releaseClose timeIntervalSinceDate:self.playRequestedTime];

    if (compareOpen > 0 && 0 > compareClose) {
        // 動画が公開期間外だった場合
        //Toast.makeText(this, "This movie has already closed.", Toast.LENGTH_LONG).show();
        [self dismiss];
    } else if (self.mMovie && self.mMovie.youtubeEmbed && ![@"" isEqualToString:self.mMovie.youtubeEmbed]){
        [(DLYoutubePlayer *)self.playerBaseView embedYouTube:self.mMovie.youtubeEmbed];
    }else {
        [self requestWatch];
    }
    [self loadInitialForum];
}

- (void)requestWatch{
    [self.afterPlayView disPlayWithCurrentMovie:self.mMoviePlayList[self.mMoviePlayIndex]];
    [DLAPIHelper fetchWatch:self.mMoviePlayList[self.mMoviePlayIndex].movieId.integerValue
                     liveId:(self.mLive ? self.mLive.liveId.intValue : 0)
                   callback:^(DLWatch *watch, NSError *error) {
                       if(error || !watch) {
                           //Toastを表示しているViewControllerを直後にdismissするのでどちらかしか動かない
                           // TODO どちらも動くようにする
                           [WINDOW makeToast:@"This movie has already closed."];
                           [self dismiss];
                       }else{
                           [self onWatchCallBack:watch];
                       }
                   }];
}

- (void)onWatchCallBack:(DLWatch *)watch {
    self.mWatch = watch;
    [self refreshForumCommentView];
    [self updateMovieInformation];

    //M3U8を解析して、各種レンディションのm3u8を保存
    M3U8Parser* parser = [M3U8Parser new];
    
    NSString       *play_url      = self.mWatch.playUrl;
    NSMutableArray *m3u8URLs      = [NSMutableArray new];
    NSMutableArray *m3u8Rendition = [NSMutableArray new];
    
    if ([parser parseWithURI:play_url daisukiConvert:YES]) {
        if (parser.children.count > 0) {
            //パースされた子m3u8のURLをcontextでストック
            m3u8URLs = [NSMutableArray array];
            for (int i = 0; i < parser.children.count; i++) {
                [m3u8URLs addObject:parser.children[i]];
            }
            m3u8Rendition = [NSMutableArray array];
            for (int i = 0; i < parser.lengths.count; i++) {
                [m3u8Rendition addObject:parser.lengths[i]];
            }
        } else {
            NSLog(@"Error4");
        }
    } else {
        NSLog(@"Error5");
    }

    if (NOT_NULL(self.mWatch.captionUrl)) {
        [self getCaptionXml:self.mWatch.captionUrl];
    }
    
    NSString *url = self.mWatch.playUrl;
    
    // 一旦一番Rateが低いものを使用する。
    if([m3u8URLs count] > 0){
        url = [m3u8URLs objectAtIndex:0];
    }
    
    // [緩募]rendition設定周りが土管実装でイケてないのでいい案あったらください…mm
    [self readyMediaPlayer:url renditions:m3u8Rendition];
}

- (void)readyMediaPlayer:(NSString *)url renditions:(NSMutableArray<NSString *> *)renditions {

    PTMetadata *metadata = [DLPrimeTimeConfigManager getMetaDataWithWatchData:self.mWatch
                                                                   requestAds:!self.isLiveEscapeFirstAds];
    self.isLiveEscapeFirstAds = NO;
    
    url = [url stringByReplacingOccurrencesOfString:DLConstVideoProxyServerTargetURLString
                                         withString:[NSString stringWithFormat:DLConstVideoProxyServerLocalHost, DLConstVideoProxyServerHostingPort]];

    NSString *movieId = [NSString stringWithFormat:@"daisuki_%@_0", self.mWatch.productId];
    PTMediaPlayerItem *resource = [[PTMediaPlayerItem alloc] initWithUrl:[NSURL URLWithString:url]
                                                                 mediaId:movieId
                                                                metadata:metadata];
    resource.secure = YES;

    [self.playerBaseView setMediaPlayerItem:resource renditions:renditions];

    // Live途中参加機能
    [self.playerBaseView seekToTime:CMTimeMakeWithSeconds(self.livePassedMovieTime + [NSDate.new timeIntervalSinceDate:self.playRequestedTime], 1)];
}

- (void)updateMovieInformation {

}

- (void)refreshForumCommentView {

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;

    self.appDelegate = (DLAppDelegate *)[[UIApplication sharedApplication] delegate];
    [self initView];
    [self updateFrame];
    [self addPlayerObserver];
    [self addFullScreenObserver];

    [self loadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.appDelegate setTabBarViewIsHidden:YES];
    self.isEnableRotate = YES;

    [self setNotificationCenter];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.appDelegate setTabBarViewIsHidden:NO];
    self.isEnableRotate = NO;
    [[DLAppFrameManager sharedInstance] setCurrentOrientationForce:UIInterfaceOrientationPortrait];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [[DLAppFrameManager sharedInstance] updateApplicationFrame];
    [self updateFrame];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

    [self removePlayerObserver];
    [self removeFullScreenObserver];
    [self removePlayerBaseView];

    [self.playerBaseView getPlayerControlView].delegate = nil;
    self.playerBaseView.baseViewDelegate = nil;
    self.playerBaseView.primetimeDelegate = nil;
    self.promotionMoviePlayerView.delegate = nil;
    self.afterPlayView.delegate = nil;
    self.growingCommentView.delegate = nil;
    self.playerForumDetailVC.playerOptionBarView.delegate = nil;
    self.playerForumListVC.playerOptionBarView.delegate = nil;
    self.simpleForumTitleView.delegate = nil;

    self.appDelegate = nil;
    self.playerBaseView = nil;
    self.growingCommentView = nil;
    self.afterPlayView = nil;
    self.simpleForumTitleView = nil;
    self.playerContanerViewController = nil;
    self.playerForumDetailVC = nil;
    self.playerForumListVC = nil;

    self.mLive = nil;
    self.mMovie = nil;
    self.mWatch = nil;
    self.playRequestedTime = nil;
    self.mMoviePlayList = nil;

}

- (void)dealloc {
    FUNC_LOG

}

#pragma mark - API

- (void)loadData {
    
    // 初回の動画広告を表示しない
    self.isLiveEscapeFirstAds = YES;
    
    if (self.mLive) {
        [self playLive];
    } else {
        // Live情報がない場合(カタログから選択された場合等) GET /movies/{movie_id}/watch を叩く
        [self.mMoviePlayList addObject:self.mMovie];
        [self playMovie];
    }
}

- (void) loadInitialForum{
    // [緩募] promise
    [DLAPIHelper fetchMoviesForumsByMovieID:self.mMoviePlayList[0].movieId.intValue
                                   callback:^(NSMutableArray<DLForum *> *forums, NSError *error) {
                                       if(forums && forums.count > 0){

                                           [self.playerForumListVC setForums:forums];
                                           [self.forumDataManager updateForums:[forums mutableCopy]];
                                       }else{
                                           if(!error && forums && [forums count] == 0){
                                               [self.forumDataManager updateForums:[NSMutableArray<DLForum *> new]];
                                           }
                                       }
                                   }];
}

#pragma mark - Private Methods

- (void)initView {
    self.view.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkGray];

#pragma mark Player View

    if (self.mMovie && self.mMovie.youtubeEmbed && ![@"" isEqualToString:self.mMovie.youtubeEmbed]) {
        self.playerBaseView = [[DLYoutubePlayer alloc] initIsLive:NO];
        self.playerBaseView.baseViewDelegate = self;
        [self.view addSubview:self.playerBaseView];
    } else{
        self.playerBaseView = [[DLPlayerViewIsolated alloc] initIsLive:self.isLive];
        self.playerBaseView.backgroundColor = [UIColor blackColor];
        [self.playerBaseView startIndicator];
        self.playerBaseView.baseViewDelegate = self;
        self.playerBaseView.primetimeDelegate = self;
        [self.playerBaseView setPlayerViewHidden:NO withAnimated:YES];

    }
    [self.view addSubview:self.playerBaseView];

#pragma mark Player Control View

    [self.playerBaseView getPlayerControlView].delegate = self;

#pragma mark Container View Controller

    self.playerForumDetailVC = [DLPlayerForumDetailViewController new];
    self.playerForumListVC   = [DLPlayerForumListViewController new];

    self.playerForumDetailVC.playerOptionBarView.delegate = self;
    self.playerForumListVC.playerOptionBarView.delegate   = self;

    self.playerContanerViewController = [[DLPlayerContainerViewController alloc] initWithRootViewController:self.playerForumDetailVC];
    [self.view addSubview:self.playerContanerViewController.view];

#pragma mark Simple Forum Title View

    self.simpleForumTitleView = [DLSimpleForumTitleView new];
    self.simpleForumTitleView.delegate = self;
    [self.view addSubview:self.simpleForumTitleView];

#pragma mark Growing Text View

    self.growingCommentView = [DLPlayerCommentView new];
    self.growingCommentView.delegate = self;
    self.growingCommentView.commentEditDelegate = self;
    [self.view addSubview:self.growingCommentView];

#pragma mark After Play View

    self.afterPlayView = [[DLContinueToPlayView alloc] initWithParentViewController:self];
    self.afterPlayView.delegate = self;
    [self.view addSubview:self.afterPlayView];
    self.afterPlayView.hidden = YES;

}

- (void)updateFrame {
    CGRect applicationFrame = [DLAppFrameManager sharedInstance].applicationFrame;
    float  statusBarHeight  = [UIApplication sharedApplication].statusBarFrame.size.height;


    if (IS_PORTRAIT) {
        self.simpleForumTitleView.hidden = YES;

        CGFloat playerWidth  = CGRectGetWidth(applicationFrame);
        CGFloat playerHeight = playerWidth / 16 * 9;

        [self.playerBaseView updateFrame: CGRectMake(0, statusBarHeight, playerWidth, playerHeight)];

        self.playerContanerViewController.view.frame = CGRectMake(0,
                                                                  self.playerBaseView.maxY,
                                                                  self.playerBaseView.width,
                                                                  CGRectGetHeight(applicationFrame) - self.playerBaseView.maxY - kGrowingCommentViewHeight);
        if (!self.isDisplayedKeyboard) {
            self.growingCommentView.frame = CGRectMake(0,
                                                       CGRectGetHeight(applicationFrame) - kGrowingCommentViewHeight,
                                                       CGRectGetWidth(applicationFrame),
                                                       kGrowingCommentViewHeight);
        }

    } else if (IS_LANDSCAPE && self.isFullScreen) {
        self.simpleForumTitleView.hidden = YES;

        [self.playerBaseView updateFrame:applicationFrame];

        // 画面外に出す
        self.playerContanerViewController.view.y = applicationFrame.size.height;

    } else if (IS_LANDSCAPE && self.isForumCommentModeInLandScape) {
        self.simpleForumTitleView.hidden = YES;

        CGFloat playerWidth = CGRectGetWidth(applicationFrame) / 5 * 3;
        CGFloat playerHeight = playerWidth / 16 * 9;

        [self.playerBaseView updateFrame: CGRectMake(0, 0, playerWidth, playerHeight)];

        self.playerContanerViewController.view.frame = CGRectMake(self.playerBaseView.maxX,
                                                                  0,
                                                                  CGRectGetWidth(applicationFrame) - self.playerBaseView.width,
                                                                  CGRectGetHeight(applicationFrame));

        self.growingCommentView.frame = CGRectMake(0,
                                                   CGRectGetHeight(applicationFrame) - kGrowingCommentViewHeight,
                                                   self.playerBaseView.width,
                                                   kGrowingCommentViewHeight);

    } else {
        self.simpleForumTitleView.hidden = NO;

        CGFloat viewWidth = CGRectGetWidth(applicationFrame);
        CGFloat playerHeight = viewWidth / 16 * 9 - 70;
        CGFloat playerWidth = playerHeight / 9 * 16;

        self.playerBaseView.height = playerHeight;
        self.playerBaseView.width = playerWidth;
        self.playerBaseView.midX = self.view.midX;
        self.playerBaseView.y = 20;


        self.simpleForumTitleView.frame = CGRectMake(0,
                                                     self.playerBaseView.maxY,
                                                     viewWidth,
                                                     CGRectGetHeight(applicationFrame) - self.playerBaseView.maxY);

        // 画面外に出す
        self.playerContanerViewController.view.y = CGRectGetHeight(applicationFrame);
        self.growingCommentView.y = CGRectGetHeight(applicationFrame);
    }

    self.afterPlayView.frame = applicationFrame;
}

- (void)removePlayerBaseView {

    if (NOT_NULL(self.playerBaseView)) {
        [self.playerBaseView pause];
        [self.playerBaseView removeFromSuperview];
        self.playerBaseView = nil;
    }
}

#pragma mark Observer

- (void)addPlayerObserver {
    [self.playerBaseView addObserver:self
                          forKeyPath:kPlayerIsPlayingKeyPath
                             options:NSKeyValueObservingOptionNew
                             context:nil];
}

- (void)removePlayerObserver {

    @try{
        [self.playerBaseView removeObserver:self
                                 forKeyPath:kPlayerIsPlayingKeyPath];
    }@catch(id anException){
        // 設定していない場合でも落ちないための処理のため、Exceptionは無視する
    }
}

- (void)addFullScreenObserver {
    [self addObserver:self
           forKeyPath:kIsFullScreenKeyPath
              options:NSKeyValueObservingOptionNew
              context:nil];
}

- (void)removeFullScreenObserver {

    @try{
        [self removeObserver:self
                  forKeyPath:kIsFullScreenKeyPath];
    }@catch(id anException){
        // 設定していない場合でも落ちないための処理のため、Exceptionは無視する
    }
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context {

    if ([keyPath isEqualToString:kPlayerIsPlayingKeyPath]) {
        [[self.playerBaseView getPlayerControlView] updateIsPlaying:self.playerBaseView.isPlaying];

    } else if ([keyPath isEqualToString:kIsFullScreenKeyPath]) {

        [[self.playerBaseView getPlayerControlView] updateIsFullScreen:self.isFullScreen];
    }
}

#pragma mark - DLNavigationBarViewDelegate

- (void)navigationBarViewDidTapLeftButton:(UIButton *)button {
    if (self.isModal) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - DLPlayerControlViewDelegate

- (void)backButtonDidTap {
    if (self.isModal) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)forumButtonDidTap {}

- (void)pauseButtonDidTap {

    [self.playerBaseView pause];
}

- (void)playButtonDidTap {

    [self.playerBaseView play];
}

- (void)fullScreenButtonDidTap {

    self.isFullScreen = YES;
    self.isForumCommentModeInLandScape = NO;

    if (IS_PORTRAIT) {
        [[DLAppFrameManager sharedInstance] setCurrentOrientationForce:UIInterfaceOrientationLandscapeLeft];
    } else if (IS_LANDSCAPE_LEFT) {
        [[DLAppFrameManager sharedInstance] setCurrentOrientationForce:UIInterfaceOrientationLandscapeLeft];
    } else if (IS_LANDSCAPE_RIGHT) {
        [[DLAppFrameManager sharedInstance] setCurrentOrientationForce:UIInterfaceOrientationLandscapeRight];
    }

    [self updateFrame];
}

- (void)shrinkButtonDidTap {

    self.isFullScreen = NO;
    [self updateFrame];
}

- (void)sliderChangedWithSeekTime:(CMTime)seekTime {

    [self.playerBaseView seekToTime:seekTime];
}

#pragma mark - DLPlayerBaseViewDelegate

- (void)playerBaseViewDidLoadedMovie:(DLPlayerViewIsolated *)playerBaseView {

    [playerBaseView play];
    [playerBaseView setPlayerViewHidden:NO withAnimated:YES];
}

- (void)playerBaseViewPlayerSwipeGestureLeft:(UISwipeGestureRecognizer *)gesture {
    if (IS_LANDSCAPE) {
        self.isForumCommentModeInLandScape = YES;
        [self updateFrame];
    }
}

- (void)playerBaseViewPlayerSwipeGestureRight:(UISwipeGestureRecognizer *)gesture {
    if (IS_LANDSCAPE) {
        self.isForumCommentModeInLandScape = NO;
        [self updateFrame];
    }
}

#pragma mark - DLPlayerViewPrimeTimeDelegate

- (void)onPTPlayerAdBreakStart {
    FUNC_LOG
}

- (void)onPTPlayerAdBreakComplete:(NSInteger)passedTime {
    FUNC_LOG
    
    // PrimeTimeの広告再生時間が60秒に満たない場合、番宣動画を再生する。
    if(passedTime < (self.playAdsTime == 0 ? DLConstPlayAdTime : self.playAdsTime)){
        [self.playerBaseView pause];
        self.promotionMoviePlayerView = [[DLPlayerViewPromotionAd alloc] initPromotionAds:(self.playAdsTime == 0 ? (DLConstPlayAdTime - passedTime) : self.playAdsTime)];
        self.promotionMoviePlayerView.delegate = self;
        self.promotionMoviePlayerView.frame = self.playerBaseView.frame;
        self.promotionMoviePlayerView.backgroundColor = [UIColor blackColor];
        [self.view addSubview:self.promotionMoviePlayerView];
    }
}

- (void)onPTPlayerPlayBreakstart {
    FUNC_LOG
    
    //TODO: これで使い方合ってるのか辰己さんに相談
    [[self.playerBaseView getPlayerControlView] updateIsLoaded:YES];
    [[self.playerBaseView getPlayerControlView] updateIsPlaying:YES];
}

- (void)onPTPlayerPlayBreakComplete {
    FUNC_LOG
    
    if (self.mMovie && self.mMovie.youtubeEmbed && ![@"" isEqualToString:self.mMovie.youtubeEmbed]) {
        [self dismiss];
        return;
    }

    self.mMoviePlayIndex += 1;
    if (self.mLive) {
        if ([self.mMoviePlayList count] > self.mMoviePlayIndex) {
            // PlayListの次の動画を再生する。
            [self requestWatch];
            return;
        }
    }
    //TODO: 再生後画面でIndexを使うのであればつじつま合わせのためにとかで-1してください(筋ではなさそうですが…)
    self.mMoviePlayIndex -= 1;
    // 次に再生する動画がなかった場合は再生後画面に遷移する。
    self.afterPlayView.hidden = NO;
}

- (void)onPTPlayerError {
    FUNC_LOG
}

- (void)onPlayerTimeUpdate:(NSInteger)currentTime
                  duration:(NSInteger)duration {
    NSLog(@"onPlayerTimeUpdate current: %ld total: %ld", (long)currentTime, (long)duration);
    
    //TODO: 外に逃がします。
    int s = (int)currentTime % 60;
    int m = (int)(currentTime - s) / 60;
    int h = m / 60;
    m = m % 60;
    self.currentPlayTime = [NSString stringWithFormat:@"%02d:%02d:%02d", h, m, s];
    [[self.playerBaseView getPlayerControlView] updateTimeView:currentTime duration:duration];

    [self captionUpdate:currentTime duration:duration];
}

- (void)captionUpdate:(NSInteger)currentTime
             duration:(NSInteger)duration {
    
    long currentSecond  = currentTime % 60;
    long currentMinutes = currentTime / 60;
    long currentHour    = currentMinutes / 60;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.locale = locale;
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS";
    NSString *currentDateString = [NSString stringWithFormat:@"2001-01-01T%2ld:%2ld:%2ld.000", currentHour, currentMinutes, currentSecond];
    NSDate *currentTimeDate = [dateFormatter dateFromString:currentDateString];
    
    for (int i = self.captionPosition; i < [self.captionItemList count]; i++) {
        
        DLCaptionItem *tmpCaptionItem = self.captionItemList[i];
        NSString *captionBeginTime     = [NSString stringWithFormat:@"2001-01-01T%@", tmpCaptionItem.beginTimeMs];
        NSDate   *captionBeginTimeDate = [dateFormatter dateFromString: captionBeginTime];
        NSString *captionEndTime       = [NSString stringWithFormat:@"2001-01-01T%@", tmpCaptionItem.endTimeMs];
        NSDate *captionEndTimeDate = [dateFormatter dateFromString: captionEndTime];
        
        if ([currentTimeDate compare:captionBeginTimeDate] == NSOrderedDescending &&
            [currentTimeDate compare:captionEndTimeDate]   == NSOrderedAscending) {
            //字幕表示時間内
            self.captionPosition = i;
            NSString *captionString = tmpCaptionItem.captionMessage;
            [[self.playerBaseView getPlayerControlView] updateCaptionView:captionString];
            
            break;
        } else {
            //字幕表示時間外
            [[self.playerBaseView getPlayerControlView] updateCaptionView:@""];
        }
    }
}

#pragma mark - DLPlayerViewPromotionAdDelegate

- (void)onAdMovieStarted {
    FUNC_LOG
}

- (void)onAdMovieEnded:(NSInteger)remainingTime {
    FUNC_LOG
    
    self.promotionMoviePlayerView.delegate = nil;
    [self.promotionMoviePlayerView removeFromSuperview];
    [self.playerBaseView play];
}

- (void)onAdMovieError {
    FUNC_LOG
    
    self.promotionMoviePlayerView.delegate = nil;
    [self.promotionMoviePlayerView removeFromSuperview];
    [self.playerBaseView play];
}

#pragma mark - DLAfterPlayViewControllerDelegate

- (void)afterPlayViewControllerTappedCloseButton {

    [self backButtonDidTap];
}

#pragma mark - DLContinuePlayViewDelegate

-(void)onBtnClose {
    [self backButtonDidTap];
}

- (void)showNextMovie:(DLMovie *)movie {
    [self.mMoviePlayList removeAllObjects];
    [self.mMoviePlayList addObject:movie];
    self.mMoviePlayIndex = 0;
    self.captionPosition = 0;
    self.mLive = nil;
    self.isLive = NO;
    self.afterPlayView.hidden = YES;
    [self requestWatch];
}

#pragma mark - DLPlayerOptionBarViewDelegate

- (void)playerOptionBarViewForumListButtonDidTap:(DLPlayerOptionBarViewType)optionBarViewType {
    switch (optionBarViewType) {
        case DLPlayerOptionViewForumDetail:
        {
            [self.playerContanerViewController pushViewController:self.playerForumListVC animated:YES];
            break;
        }
        case DLPlayerOptionViewForumList:
            break;
        case DLPlayerOptionViewCreateForum:
            break;
    }
}

- (void)playerOptionBarViewBackButtonDidTap:(DLPlayerOptionBarViewType)optionBarViewType {
    switch (optionBarViewType) {
        case DLPlayerOptionViewForumDetail:
        {
            [self.forumDataManager movePreviousForum];
            break;
        }
        case DLPlayerOptionViewForumList:
        {
            [self.playerContanerViewController popViewController:self.playerForumListVC animated:YES];
            break;
        }
        case DLPlayerOptionViewCreateForum:
        {
            break;
        }
    }
}

- (void)playerOptionBarViewNextButtonDidTap:(DLPlayerOptionBarViewType)optionBarViewType {
    switch (optionBarViewType) {
        case DLPlayerOptionViewForumDetail:
        {
            [self.forumDataManager moveNextForum];
            break;
        }
        case DLPlayerOptionViewForumList:
            break;
        case DLPlayerOptionViewCreateForum:
            break;
    }
}

- (void)playerOptionBarViewPostCommentButtonDidTap:(DLPlayerOptionBarViewType)optionBarViewType  {
    switch (optionBarViewType) {
        case DLPlayerOptionViewForumDetail:
            break;
        case DLPlayerOptionViewForumList:
            break;
        case DLPlayerOptionViewCreateForum:
            break;
    }
}

- (void)playerOptionBarViewOtherButtonDidTap:(DLPlayerOptionBarViewType)optionBarViewType {

    // TODO: 管理方法策定 & Util化
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL isTopicAutoChange = [userDefaults boolForKey:kUserDefaultsTopicAutoChangeKey];

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];

    [alertController addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"Topic auto change %@", isTopicAutoChange ? @"on" : @"off"]
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                          [userDefaults setBool:!isTopicAutoChange
                                                                         forKey:kUserDefaultsTopicAutoChangeKey];
                                                          [userDefaults synchronize];
                                                      }]];

    [alertController addAction:[UIAlertAction actionWithTitle:@"Report a violation"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
                                                      }]];

    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                                        style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction *action) {
                                                      }]];

    [self presentViewController:alertController animated:YES completion:nil];

}

- (void)crereateForumButtonDidTap:(NSString *)title pinnedTime:(NSString *)pinnedTime {
//    [self.playerCreateForumVC sendForumByMovieID:[[self.mMoviePlayList objectAtIndex:self.mMoviePlayIndex].movieId integerValue]
//                                forumPinnedTime:self.currentPlayTime];
}

#pragma mark - DLPlayerCommentViewDelegate

- (void)playerCommentView:(DLPlayerCommentView *)playerCommentView
         willChangeHeight:(float)height {

    CGRect beforeFrame = self.growingCommentView.frame;
    self.growingCommentView.y      += beforeFrame.size.height - height;
    self.growingCommentView.height  = height;
}

#pragma mark - DLSimpleForumTitleViewDelegate

- (void)simpleForumTitleViewTitleLabelDidTap {

    if (IS_LANDSCAPE) {
        self.isForumCommentModeInLandScape = YES;

        [self updateFrame];
    }
}

- (BOOL)simpleForumTitleViewStarButtonDidTap {

    // XXX: 仮で入れ替わるようにしています
    return [[self.simpleForumTitleView getSeeLaterButton] isSeeLater] ? NO : YES;
}

#pragma mark DLMoviePlayerViewModelDelegate
- (void)onUpdateCurrentForum:(DLForum *)forum
               existPrevious:(BOOL)existPrevious
                   existNext:(BOOL)existNext {

    // 現在表示中のForumと前後にForumがあるかが返ってくる
    // 現在のForum情報の更新が必要な人の更新
    [self.simpleForumTitleView setForum:forum];
    [self.playerForumDetailVC setForum:forum];
}

- (void)onUpdateCurrentForumComments:(NSMutableArray<DLComment *> *)comments {
//    [self.playerForumDetailVC ]
}


#pragma mark DLPlayerCommentViewSenderDelegate

- (void)didEndEditComment:(NSString *)comment {
    @weakify(self)
    [DLAPIHelper postComment:comment
               targetForumId:[[self.forumDataManager getCurrentForum].forumId integerValue]
                    callback:^(BOOL flag, id responseObject) {
                        @strongify(self)
                        [self.growingCommentView resignComment:flag];

                        //TODO: CommentListを別管理するまでの応急処置的なsomething
                        if(flag){
                            [self.playerForumDetailVC setForum:[self.forumDataManager getCurrentForum]];
                        }
                    }];
}


#pragma mark - NSNotification

- (void)setNotificationCenter {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)keyboardWillShow:(NSNotification *)note {
    CGRect keyboardFrame = [[note.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSNumber *duration   = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve      = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    keyboardFrame        = [self.view convertRect:keyboardFrame toView:nil];

    CGRect containerFrame   = self.growingCommentView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - (keyboardFrame.size.height + containerFrame.size.height);

    self.isDisplayedKeyboard = YES;
    [UIView animateWithDuration:[duration doubleValue]
                          delay:0.0f
                        options:[curve intValue]
                     animations:^{
                         self.growingCommentView.frame = containerFrame;
                     }
                     completion:^(BOOL finished){}
    ];
}

-(void)keyboardWillHide:(NSNotification *)note {
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve    = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];

    CGRect containerFrame   = self.growingCommentView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;

    self.isDisplayedKeyboard = NO;
    [UIView animateWithDuration:[duration doubleValue]
                          delay:0.0f
                        options:[curve intValue]
                     animations:^{
                         self.growingCommentView.frame = containerFrame;
                     }
                     completion:^(BOOL finished){}
    ];
}

#pragma mark - Rotate Config

- (BOOL)shouldAutorotate {
    return YES;
}

/*
 * Captionを取得するためのxmlurlのパース開始
 */
- (void)getCaptionXml:(NSString *)captionUrl {
    NSURL * url = [NSURL URLWithString:captionUrl];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setHTTPShouldHandleCookies:NO];
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue new]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if (error) {
                                   //            failure(error);
                               } else if(response)
                               {
                                   NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:data];
                                   xmlParser.delegate = self;
                                   BOOL success = [xmlParser parse];
                                   if (!success) {
                                       //xmlパース失敗
                                   }
                               }
                           }];
}

/*
 * NSXMLParserDelegate
 */

//XMLパース開始
- (void)parser:(NSXMLParser *)parser didStartMappingPrefix:(NSString *)prefix toURI:(NSString *)namespaceURI {}

- (void)parser:(NSXMLParser *)parser
   didStartElement:(NSString *)elementName
      namespaceURI:(NSString *)namespaceURI
     qualifiedName:(NSString *)qName
        attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {
    
    //開始タグ
    if ([elementName isEqualToString:@"tt"]) {
        //xml先頭タグの場合、字幕リストを初期化
        self.captionItemList = [NSMutableArray array];
    } else if ([elementName isEqualToString:@"p"]) {
        //pタグの場合、字幕アイテム、字幕文字を初期化。
        self.tmpCaptionItemData = [NSMutableDictionary new];
        self.tmpXmlCaption = [NSMutableString new];
        //字幕データの時間などをセット
        [self.tmpCaptionItemData setValue:[attributeDict valueForKey:@"begin"] forKey:@"begintime_ms"];
        [self.tmpCaptionItemData setValue:[attributeDict valueForKey:@"end"] forKey:@"endtime_ms"];
        [self.tmpCaptionItemData setValue:[attributeDict valueForKey:@"style"] forKey:@"style"];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    [self.tmpXmlCaption appendString:string];
}

- (void)parser:(NSXMLParser *)parser
 didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"p"]) {
        //pタグの場合、最終的な字幕文字列をセット。
        //字幕Modelが完成したので字幕Arrayに登録。
        [self.tmpCaptionItemData setValue:self.tmpXmlCaption forKey:@"caption_message"];
        [self.captionItemList addObject:(DLCaptionItem *)[DLCaptionItem instanceFromDictionary:self.tmpCaptionItemData]];
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if (IS_IPAD) {
        return UIInterfaceOrientationMaskAll;
    } else {
        if (self.isFullScreen) {
            return UIInterfaceOrientationMaskLandscape;
        }

        // 初回立ち上げ時に横向きで無いように制御
        if (self.isEnableRotate) {
            return UIInterfaceOrientationMaskAllButUpsideDown;
        }
        return UIInterfaceOrientationMaskPortrait;
    }
}

@end
