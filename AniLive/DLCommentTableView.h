//
//  DLCommentTableView.h
//  AniLive
//
//  Created by isaoeka on 2016/12/30.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DLComment;

@interface DLCommentTableView : UITableView

- (void)loadComments:(NSMutableArray<DLComment *> *)comments;

@end
