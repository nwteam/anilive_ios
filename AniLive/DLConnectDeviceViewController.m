//
//  DLConnectdeviceViewController.m
//  AniLive
//
//  Created by suisun on 2017/01/14.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLConnectDeviceViewController.h"

#import "DLNavigationBarView.h"
#import "DLAppFrameManager.h"
#import "DLAlertViewSetPassword.h"
#import "DLAlertViewResult.h"
#import "DLAuthcode.h"
#import "DLUserDevice.h"

@interface DLConnectDeviceViewController () <DLAlertViewResultDelegate, DLAlertViewSetPasswordDelegate>

@property(nonatomic, strong) DLNavigationBarView *navigationBarView;
@property(nonatomic, strong) UILabel *lblSubTitle1;
@property(nonatomic, strong) UILabel *lblSubTitle2;
@property(nonatomic, strong) UILabel *lblNotice1;
@property(nonatomic, strong) UILabel *lblNotice2;
@property(nonatomic, strong) UILabel *lblAttention1;
@property(nonatomic, strong) UILabel *lblAttention2;
@property(nonatomic, strong) UILabel *lblUserID;
@property(nonatomic, strong) UILabel *lblPassword;
@property(nonatomic, strong) UITextField *lblStrUserID;
@property(nonatomic, strong) UITextField *txtStrPassword;
@property(nonatomic, strong) UIView *lineView;
@property(nonatomic, strong) UIButton *btnSetPassword;
@property(nonatomic, strong) UIButton *btnAuthevticate;
@property(nonatomic, strong) UIView *bgView1;
@property(nonatomic, strong) UIView *bgView2;
@property(nonatomic, strong) DLAlertViewSetPassword *alertViewSetPassword;

@end

@implementation DLConnectDeviceViewController

//共通事項
const static CGFloat commonMargin                       = 10.0f;
const static CGFloat vSubTitleFontSize                  = 17.0f;
const static CGFloat vNoticeFontSize                    = 15.0f;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    
    //ナビゲーションはupdateFrameをする必要がある
    [self updateFrame];
    [self addTapGesture];
}

- (void) addTapGesture {
    UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateFrame {
    self.navigationBarView.frame = CGRectMake(0,
                                              CGRectGetMinY([DLAppFrameManager sharedInstance].applicationFrame),
                                              CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame),
                                              DLConstNavigationBarHeight);
    [self.view needsUpdateConstraints];
}

#pragma mark - Private Methods

- (void)initView {

    //背景色の設定
    self.view.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkGray];

    //ナビゲーションの設定
    self.navigationBarView = [[DLNavigationBarView alloc] initWithType:DLNavigationBarViewTypeForumList];
    self.navigationBarView.delegate = self;
    [self.navigationBarView setTitle:@"Connect Device"];
    [self.view addSubview:self.navigationBarView];

    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0f,
            DLConstNavigationBarHeight,
            self.view.bounds.size.width,
            self.view.bounds.size.height)];
    [scrollView setShowsVerticalScrollIndicator:NO];
    [self.view addSubview: scrollView];

    UIView *vContent     =   [[UIView alloc] init];
    vContent.frame       =   CGRectMake(0.0f,
                                          DLConstNavigationBarHeight,
                                          self.view.bounds.size.width,
                                          750.0f);
    [scrollView addSubview:vContent];

    self.lblSubTitle1 = [[UILabel alloc] initWithFrame:CGRectMake(commonMargin, 20, self.view.bounds.size.width - 20, 21)];
    self.lblSubTitle1.text = @"Take over data to other devices";
    self.lblSubTitle1.font = BASE_FONT(vSubTitleFontSize);
    self.lblSubTitle1.textColor = [UIColor whiteColor];
    [vContent addSubview:self.lblSubTitle1];
    
    self.lblNotice1 = [[UILabel alloc] initWithFrame:CGRectMake(commonMargin, 47, self.view.bounds.size.width - 20, 44)];
    self.lblNotice1.text = @"Please enter the authentication code to be issued by squid in the application";
    self.lblNotice1.lineBreakMode = NSLineBreakByWordWrapping;
    self.lblNotice1.numberOfLines = 0;
    self.lblNotice1.font = BASE_FONT(vNoticeFontSize);
    self.lblNotice1.textColor = [UIColor colorWithHex:@"8A8D93"];
    [vContent addSubview:self.lblNotice1];
    
    self.lblAttention1 = [[UILabel alloc] initWithFrame:CGRectMake(commonMargin, 98, self.view.bounds.size.width - 20, 44)];
    self.lblAttention1.text = @"Please note that account information on other devices will be deleted";
    self.lblAttention1.lineBreakMode = NSLineBreakByWordWrapping;
    self.lblAttention1.numberOfLines = 0;
    self.lblAttention1.font = BASE_FONT(vNoticeFontSize);
    self.lblAttention1.textColor = [UIColor colorWithHex:@"FC5F5F"];
    [vContent addSubview:self.lblAttention1];
    
    self.btnSetPassword = [[UIButton alloc] initWithFrame:CGRectMake(self.view.bounds.size.width - 160, 162, 130, 30)];
    [self.btnSetPassword setTitle:@"Set Password" forState:UIControlStateNormal];
    [self.btnSetPassword.titleLabel setFont:BASE_FONT(vNoticeFontSize)];
    self.btnSetPassword.backgroundColor = [UIColor colorWithHex:@"FFE500"];
    [self.btnSetPassword setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.btnSetPassword.layer.masksToBounds = YES;
    self.btnSetPassword.layer.cornerRadius = 3.0;
    [self.btnSetPassword addTarget:self action:@selector(tapBtnSetPassword) forControlEvents: UIControlEventTouchUpInside];
    [vContent addSubview:self.btnSetPassword];
    
    self.lblSubTitle2 = [[UILabel alloc] initWithFrame:CGRectMake(commonMargin, 226, self.view.bounds.size.width - 20, 21)];
    self.lblSubTitle2.text = @"Take over the data of other devices";
    self.lblSubTitle2.font = BASE_FONT(vSubTitleFontSize);
    self.lblSubTitle2.textColor = [UIColor whiteColor];
    [vContent addSubview:self.lblSubTitle2];
    
    self.lblNotice2 = [[UILabel alloc] initWithFrame:CGRectMake(commonMargin, 255, self.view.bounds.size.width - 20, 44)];
    self.lblNotice2.text = @"Please enter the authentication code issued by the application of other device";
    self.lblNotice2.lineBreakMode = NSLineBreakByWordWrapping;
    self.lblNotice2.numberOfLines = 0;
    self.lblNotice2.font = BASE_FONT(vNoticeFontSize);
    self.lblNotice2.textColor = [UIColor colorWithHex:@"8A8D93"];
    [vContent addSubview:self.lblNotice2];
    
    self.lblAttention2 = [[UILabel alloc] initWithFrame:CGRectMake(commonMargin, 306, self.view.bounds.size.width - 20, 44)];
    self.lblAttention2.text = @"Please note that account information on this devices will be deleted";
    self.lblAttention2.lineBreakMode = NSLineBreakByWordWrapping;
    self.lblAttention2.numberOfLines = 0;
    self.lblAttention2.font = BASE_FONT(vNoticeFontSize);
    self.lblAttention2.textColor = [UIColor colorWithHex:@"FC5F5F"];
    [vContent addSubview:self.lblAttention2];
    
    self.lblUserID = [[UILabel alloc] initWithFrame:CGRectMake(commonMargin, 367, 104, 21)];
    self.lblUserID.text = @"User ID";
    self.lblUserID.lineBreakMode = NSLineBreakByWordWrapping;
    self.lblUserID.numberOfLines = 0;
    self.lblUserID.font = BASE_FONT(vNoticeFontSize);
    self.lblUserID.textColor = [UIColor colorWithHex:@"8A8D93"];
    [vContent addSubview:self.lblUserID];
    
    self.lblStrUserID = [[UITextField alloc] initWithFrame:CGRectMake(commonMargin, 396, self.view.bounds.size.width - 20, 21)];
    self.lblStrUserID.text = @"";
    self.lblStrUserID.keyboardType = UIKeyboardTypeNumberPad;
    self.lblStrUserID.returnKeyType = UIReturnKeyDone;
    self.lblStrUserID.font = BASE_FONT(vSubTitleFontSize);
    self.lblStrUserID.textColor = [UIColor whiteColor];
    self.lblStrUserID.backgroundColor = [UIColor clearColor];
    [vContent addSubview:self.lblStrUserID];

    UIView* userIdLineView = [[UIView alloc] initWithFrame:CGRectMake(commonMargin, 423, self.view.bounds.size.width - 20, 1)];
    userIdLineView.backgroundColor = [UIColor colorWithHex:@"FFE500"];
    [vContent addSubview:userIdLineView];

    self.lblPassword = [[UILabel alloc] initWithFrame:CGRectMake(commonMargin, 440, 104, 21)];
    self.lblPassword.text = @"Password";
    self.lblPassword.lineBreakMode = NSLineBreakByWordWrapping;
    self.lblPassword.numberOfLines = 0;
    self.lblPassword.font = BASE_FONT(vNoticeFontSize);
    self.lblPassword.textColor = [UIColor colorWithHex:@"8A8D93"];
    [vContent addSubview:self.lblPassword];
    
    self.txtStrPassword = [[UITextField alloc] initWithFrame:CGRectMake(commonMargin, 466, self.view.bounds.size.width - 20, 21)];
    self.txtStrPassword.text = @"";
    self.txtStrPassword.keyboardType = UIKeyboardTypeNumberPad;
    [self.txtStrPassword setSecureTextEntry:YES];
    self.txtStrPassword.font = BASE_FONT(vSubTitleFontSize);
    self.txtStrPassword.textColor = [UIColor whiteColor];
    self.txtStrPassword.backgroundColor = [UIColor clearColor];
    [vContent addSubview:self.txtStrPassword];
    
    self.lineView = [[UIView alloc] initWithFrame:CGRectMake(commonMargin, 493, self.view.bounds.size.width - 20, 1)];
    self.lineView.backgroundColor = [UIColor colorWithHex:@"FFE500"];
    [vContent addSubview:self.lineView];
    
    self.btnAuthevticate = [[UIButton alloc] initWithFrame:CGRectMake(self.view.bounds.size.width - 160, 515, 130, 30)];
    [self.btnAuthevticate setTitle:@"Authenticate" forState:UIControlStateNormal];
    [self.btnAuthevticate.titleLabel setFont:BASE_FONT(vNoticeFontSize)];
    self.btnAuthevticate.backgroundColor = [UIColor colorWithHex:@"FFE500"];
    [self.btnAuthevticate setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.btnAuthevticate.layer.masksToBounds = YES;
    self.btnAuthevticate.layer.cornerRadius = 3.0;
    [self.btnAuthevticate addTarget:self action:@selector(tapBtnAuthenticate) forControlEvents: UIControlEventTouchUpInside];
    [vContent addSubview:self.btnAuthevticate];

    scrollView.contentSize = vContent.frame.size;
}

#pragma mark - DLNavigation Bar View Delegate

- (void)navigationBarViewDidTapLeftButton:(UIButton *)button {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma marker - tap btn set password
- (void)tapBtnSetPassword {
    self.bgView1 = [[UIView alloc] initWithFrame:self.view.frame];
    self.bgView1.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.86];
    
    self.alertViewSetPassword = [[DLAlertViewSetPassword alloc] initDLAlertViewSetPassword];
    self.alertViewSetPassword.delegate = self;
    self.alertViewSetPassword.center = self.view.center;
    [self.bgView1 addSubview:self.alertViewSetPassword];
    [self.view addSubview:self.bgView1];
}

#pragma marker - tap btn Authenticate
- (void)tapBtnAuthenticate {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;

    @weakify(self)
    [DLAPIHelper checkAuthcode:self.lblStrUserID.text
                        userId:self.txtStrPassword.text
                      callback:^(NSMutableArray<DLUserDevice *> *userDevices, NSError *error) {
                          @strongify(self)
                          if (userDevices) {
                              if (userDevices.count >= 2) {
                                  [self showSelectorOfInvalidateDevice: userDevices];
                              } else {
                                  [DLAPIHelper revisionDevicesRelation:self.txtStrPassword.text
                                                                userId:self.lblStrUserID.text
                                                       releaseDeviceId:nil
                                                              callback:^(BOOL isSuccess) {
                                                                [self showAuthenticateSuccessDialog];
                                                              }];
                              }

                          }
                          else {
                              UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"error"
                                                                                                       message:@"error"
                                                                                                preferredStyle:UIAlertControllerStyleAlert];
                              [alertController addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                              }]];
                              [self presentViewController:alertController animated:YES completion:nil];

                          }
                      }];

}

- (void)showAuthenticateSuccessDialog {
    self.bgView2 = [[UIView alloc] initWithFrame:self.view.frame];
    self.bgView2.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.86];

    DLAlertViewResult *alertViewResult = [[DLAlertViewResult alloc] initDLAlertViewResult];
    alertViewResult.delegate = self;
    alertViewResult.center = self.view.center;
    [self.bgView2 addSubview:alertViewResult];
    [self.view addSubview:self.bgView2];
}

- (void)showSelectorOfInvalidateDevice: (NSMutableArray*) userDevices {
    // Action Sheet的なUIで、無効化するUIを選択する
    UIAlertController * alert =
            [UIAlertController alertControllerWithTitle:@"Please choose invalidate device."
                                                message:@""
                                         preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction * cancelAction =
            [UIAlertAction actionWithTitle:@"Cancel"
                                     style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                   }];

    [alert addAction:cancelAction];

    for (DLUserDevice *userDevice in userDevices) {

        UIAlertAction * deviceAction =
                [UIAlertAction actionWithTitle:userDevice.device
                                         style:UIAlertActionStyleDestructive
                                       handler:^(UIAlertAction * action) {
                                           // ボタンタップ時の処理
                                           [DLAPIHelper revisionDevicesRelation:self.txtStrPassword.text
                                                                         userId:self.lblStrUserID.text
                                                                releaseDeviceId:[userDevice.deviceId description]
                                                                       callback:^(BOOL isSuccess) {
                                                                           [self showAuthenticateSuccessDialog];
                                                                       }];
                                       }];
        [alert addAction:deviceAction];
    }
    [self presentViewController:alert
                       animated:YES completion:nil];
}

#pragma marker - DLAlertViewSetPassword Delegate
- (void)didTapButtonDLAlertViewSetPassword:(NSInteger)index {
    NSLog(@"aaaa %@", _alertViewSetPassword.txtPassword.text);
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;

    @weakify(self)
    [DLAPIHelper issueAuthcode:[formatter numberFromString: self.alertViewSetPassword.txtPassword.text]
                      callback:^(DLAuthcode *authcode) {
                          @strongify(self)
                          if (authcode) {
                              UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"user_id: %@", [authcode.userId description]]
                                                                                                       message:[NSString stringWithFormat:@"password: %@", [authcode.code description]]
                                                                                                preferredStyle:UIAlertControllerStyleAlert];
                              [alertController addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                              }]];
                              [self presentViewController:alertController animated:YES completion:nil];
                              [self.bgView1 removeFromSuperview];
                          }
                          else {
                              UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"error"
                                                                                                       message:@"error"
                                                                                                preferredStyle:UIAlertControllerStyleAlert];
                              [alertController addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                              }]];
                              [self presentViewController:alertController animated:YES completion:nil];

                          }
    }];

}

#pragma marker - DLAlertViewResult Delegate
- (void)didTapButtonDLAlertViewResult:(NSInteger)index {
    [self.bgView2 removeFromSuperview];
}

@end
