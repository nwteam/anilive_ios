//
//  DLWebViewController.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLWebViewController.h"
#import "DLNavigationBarView.h"
#import "DLAppFrameManager.h"
@import WebKit;

static NSString *kLoadingKeyPath = @"loading";
static NSString *kTitleKeyPath = @"title";

@interface DLWebViewController () <DLNavigationBarViewDelegate>

@property (nonatomic, strong) DLNavigationBarView *navigationBarView;
@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) NSString *initialTitle;
@property (nonatomic, strong) NSString *initialUrlString;

@end

@implementation DLWebViewController

- (instancetype)initWithURLString:(NSString *)urlString
{
    return [self initWithURLString:urlString withTitle:@""];
}

- (instancetype)initWithURLString:(NSString *)urlString withTitle:(NSString *)title
{
    self = [super init];
    if (self) {
        self.initialTitle = title;
        self.initialUrlString = urlString;
    }
    return self;
}

- (void)dealloc {
    FUNC_LOG
    self.navigationBarView.delegate = nil;
    [self removeObserver];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initView];
    [self updateFrame];
    [self addObserver];
    [self initialLoading];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)initView {
    
    self.view.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkGray];

#pragma mark Navigation Bar
    
    self.navigationBarView = [[DLNavigationBarView alloc]initWithType:DLNavigationBarViewTypeWeb];
    self.navigationBarView.delegate = self;
    
    if (NOT_EMPTY(self.initialTitle)) {
        [self.navigationBarView setTitle:self.initialTitle];
    }
    [self.view addSubview:self.navigationBarView];
    
#pragma mark Web View
    
    self.webView = [[WKWebView alloc]init];
    self.webView.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkGray];
    [self.view addSubview:self.webView];
}

- (void)updateFrame {
    
    self.navigationBarView.frame = CGRectMake(0,
                                              CGRectGetMinX([DLAppFrameManager sharedInstance].applicationFrame),
                                              CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame),
                                              DLConstNavigationBarHeight);
    
    self.webView.frame = CGRectMake(0, self.navigationBarView.maxY, CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame), CGRectGetHeight([DLAppFrameManager sharedInstance].applicationFrame) - self.navigationBarView.maxY);
}

- (void)initialLoading {
    NSURLRequest *urlRequest = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:self.initialUrlString]];
    [self.webView loadRequest:urlRequest];
}

- (void)addObserver {
    
    [self.webView addObserver:self
                   forKeyPath:kLoadingKeyPath
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    
    [self.webView addObserver:self
                  forKeyPath:kTitleKeyPath
                     options:NSKeyValueObservingOptionNew
                     context:nil];
}

- (void)removeObserver {
    
    @try{
        [self.webView removeObserver:self forKeyPath:kLoadingKeyPath];
    }@catch(id anException){
        // 設定していない場合でも落ちないための処理のため、Exceptionは無視する
    }
    
    @try{
        [self.webView removeObserver:self forKeyPath:kTitleKeyPath];
    }@catch(id anException){
        // 設定していない場合でも落ちないための処理のため、Exceptionは無視する
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:kLoadingKeyPath]) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = self.webView.loading;
    } else if ([keyPath isEqualToString:kTitleKeyPath]) {
        if (!NOT_EMPTY(self.initialTitle)) {
            [self.navigationBarView setTitle:self.webView.title];
        }
    }
}

#pragma mark - DLNavigationBarViewDelegate

- (void)navigationBarViewDidTapLeftButton:(UIButton *)button {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
