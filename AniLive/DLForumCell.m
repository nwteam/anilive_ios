//
//  DLForumCell.m
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/10/08.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLForumCell.h"
#import "DLForum.h"
#import "UIImageView+Category.h"

const static CGFloat kCellHeight = 80;

@interface DLForumCell()
@property (weak, nonatomic) IBOutlet UIImageView *asyncImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleTextView;
@property (weak, nonatomic) IBOutlet UILabel *tagTextView;
@property (weak, nonatomic) IBOutlet UILabel *commentNumTextView;
@property (weak, nonatomic) IBOutlet UILabel *authorTextView;

@end

@implementation DLForumCell

@synthesize asyncImageView, titleTextView, tagTextView, commentNumTextView, authorTextView;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setForum:(DLForum *)forum {
    _forum = forum;
    if(!forum){
        return;
    }
    [titleTextView setText:_forum.title];
    NSMutableString *tagString = [@"" mutableCopy];
//    for(DLTag *tag in _forum.hashTags){
//        [tagString appendString:tag.name];
//        [tagString appendString:@" "];
//    }
    
    if (NOT_NULL(forum.movieImage)) {
        [UIImageView loadImageWithURLString:forum.movieImage
                            targetImageView:self.asyncImageView];
    }
    
    
    [tagTextView setText:tagString];
    [commentNumTextView setText:[_forum.totalComments description]];
    NSTimeInterval interval = -[_forum.created timeIntervalSinceNow];
    [authorTextView setText:[NSString stringWithFormat:@"%d hours ago by %@", (int) (interval / 3600), _forum.forumOwnerUserId]];
}

+ (UINib *)getDefaultNib {
    return [UINib nibWithNibName:@"DLForumCell" bundle:nil];
}

+ (NSString *)getCellIdentifier {
    
    return @"forum-cell";
}

+ (CGFloat)getCellHeight {
    return kCellHeight;
}

@end
