//
//  UIImageView+Category.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/15.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, DLForceAnimationType) {
    DLForceAnimationTypeNoSelected = 0,
    DLForceAnimationTypeDoAnimation,
    DLForceAnimationTypeNoAnimation
};

@interface UIImageView (Category)

+ (void)loadImageWithURLString:(NSString*)urlString targetImageView:(UIImageView*)targetImageView;
+ (void)loadImageWithURLString:(NSString*)urlString targetImageView:(UIImageView*)targetImageView completionBlock:(void(^)())completionBlock;
+ (void)loadImageWithURLString:(NSString*)urlString targetImageView:(UIImageView*)targetImageView isEnableCache:(BOOL)isEnableCache;
+ (void)loadImageWithURLString:(NSString*)urlString targetImageView:(UIImageView*)targetImageView placeholderImage:(UIImage*)placeholderImage;
+ (void)loadImageWithURLString:(NSString*)urlString targetImageView:(UIImageView*)targetImageView placeholderImage:(UIImage*)placeholderImage isEnableCache:(BOOL)isEnableCache;
+ (void)loadImageWithURLString:(NSString*)urlString targetImageView:(UIImageView*)targetImageView placeholderImage:(UIImage*)placeholderImage isEnableCache:(BOOL)isEnableCache isRealTimeUpdate:(BOOL)isRealTimeUpdate forceAnimationType:(DLForceAnimationType)forceAnimationType;
+ (void)loadImageWithURLString:(NSString*)urlString targetImageView:(UIImageView*)targetImageView placeholderImage:(UIImage*)placeholderImage isEnableCache:(BOOL)isEnableCache isRealTimeUpdate:(BOOL)isRealTimeUpdate forceAnimationType:(DLForceAnimationType)forceAnimationType completionBlock:(void(^)())completionBlock;

@end
