//
// Created by Kotaro Itoyama on 2016/10/09.
// Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLAPIHelper.h"

@interface DLModelBase : NSObject
- (instancetype)initWithAttributes:(NSDictionary *)attributes;
+ (NSObject *)instanceFromDictionary:(NSDictionary *)dictionary;
+ (NSMutableArray *)instancesFromDictionaries:(NSArray *)dictionaries;
@end
