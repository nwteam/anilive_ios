//
//  DLAdPolicySelector.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/12/24.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <PSDKLibrary/PTDefaultAdPolicySelector.h>

@interface DLAdPolicySelector : PTDefaultAdPolicySelector

@property (nonatomic, assign) BOOL shouldPlayAdBreaks;

@end
