//
//  DLWork.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/10/01.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLModelBase.h"

@interface DLWork : DLModelBase

@property (readonly, nonatomic, strong) NSNumber *workId;
@property (readonly, nonatomic, strong) NSString *title;
@property (readonly, nonatomic, strong) NSNumber *studioId;
@property (readonly, nonatomic, strong) NSString *studioName;
@property (readonly, nonatomic, strong) NSString *licensor;
@property (readonly, nonatomic, strong) NSNumber *creationYear;
@property (readonly, nonatomic, strong) NSString *synopsis;
@property (readonly, nonatomic, strong) NSString *staff;
@property (readonly, nonatomic, strong) NSString *cast;
@property (readonly, nonatomic, strong) NSString *youtube;
@property (readonly, nonatomic, strong) NSDate *releaseOpen;
@property (readonly, nonatomic, strong) NSDate *releaseClose;
@property (readonly, nonatomic, strong) NSString *image1;
@property (readonly, nonatomic, strong) NSString *image2;
@property (readonly, nonatomic, strong) NSString *image3;
@property (readonly, nonatomic, strong) NSString *image4;
@property (readonly, nonatomic, strong) NSString *image5;
@property (readonly, nonatomic, strong) NSNumber *totalMovies;
@property (readonly, nonatomic, strong) NSNumber *popularity;

@end
