//
//  UIAlertController+Category.h
//  AniLive
//
//  Created by 辰己佳祐 on 2017/01/14.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (Category)

+ (void)showSingleAlertWithTitle:(NSString *)title
                      withTarget:(UIViewController *)controller;
+ (void)showSingleAlertWithMessage:(NSString *)message
                        withTarget:(UIViewController *)controller;
+ (void)showSingleAlertWithTitle:(NSString *)title
                     withMessage:(NSString *)message
                      withTarget:(UIViewController *)controller;
+ (void)showSingleAlertWithTitle:(NSString *)title
                     withMessage:(NSString *)message
                       withStyle:(UIAlertControllerStyle)style
                      withTarget:(UIViewController *)controller;

@end
