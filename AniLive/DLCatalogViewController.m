//
//  DLCatalogViewController.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLCatalogViewController.h"
#import "DLAppFrameManager.h"
#import "DLSegmentView.h"
#import "DLCatalogPopularViewController.h"
#import "DLCatalogAlphabeticalViewController.h"

const static NSInteger kSegmentStartRow = 0;
const static CGFloat kSegmentViewHeight = 44;

@interface DLCatalogViewController () <UIScrollViewDelegate, DLSegmentViewDelegate>

@property (nonatomic, strong) DLSegmentView *segmentView;
@property (nonatomic, strong) UIScrollView *baseScrollView;
@property (nonatomic, strong) DLCatalogPopularViewController *popularViewController;
@property (nonatomic, strong) DLCatalogAlphabeticalViewController *alphabeticalViewController;
@property (nonatomic, strong) NSArray *segmentTitleArray;
@property (nonatomic, strong) NSMutableArray *segmentControllerArray;

@end

@implementation DLCatalogViewController

#pragma mark - Override Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.segmentTitleArray = @[Localizer(@"segment_catalog_popular"), Localizer(@"segment_catalog_atoz")];
    self.segmentControllerArray = [[NSMutableArray alloc]init];
    
    [self initView];
    [self updateFrame];
    
    // 初期表示タブに移動させておく
    [self.baseScrollView setContentOffset:CGPointMake(self.baseScrollView.width * kSegmentStartRow, 0)];
    [self setScrollsToTopWithCurrentRow:kSegmentStartRow];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
    self.segmentView.delegate = nil;
    self.baseScrollView.delegate = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Private Methods

- (void)initView {
    
    self.view.backgroundColor = [UIColor colorWithHex:DLConstColorLightBlue];

#pragma mark Segment View

    self.segmentView = [[DLSegmentView alloc]initWithTitles:self.segmentTitleArray startRow:kSegmentStartRow];
    self.segmentView.delegate = self;
    [self.view addSubview:self.segmentView];
    
#pragma mark Base Scroll View
    
    self.baseScrollView = [[UIScrollView alloc]init];
    self.baseScrollView.backgroundColor = [UIColor colorWithHex:DLConstColorDarkBlue];
    self.baseScrollView.scrollsToTop = NO;
    self.baseScrollView.pagingEnabled = YES;
    self.baseScrollView.alwaysBounceHorizontal = YES;
    self.baseScrollView.showsHorizontalScrollIndicator = NO;
    self.baseScrollView.delegate = self;
    [self.view addSubview:self.baseScrollView];
    
#pragma mark Popular View Controller
    
    self.popularViewController = [[DLCatalogPopularViewController alloc]init];
    [self.baseScrollView addSubview:self.popularViewController.view];
    [self.segmentControllerArray addObject:self.popularViewController];
    
#pragma mark Alphabetical View Controller
    
    self.alphabeticalViewController = [[DLCatalogAlphabeticalViewController alloc]init];
    [self.baseScrollView addSubview:self.alphabeticalViewController.view];
    [self.segmentControllerArray addObject:self.alphabeticalViewController];
}

- (void)updateFrame {
    
    self.segmentView.frame = CGRectMake(0, CGRectGetMinX([DLAppFrameManager sharedInstance].applicationFrame) + 20, CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame), kSegmentViewHeight);
    
    self.baseScrollView.frame = CGRectMake(0, self.segmentView.maxY, CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame), CGRectGetHeight([DLAppFrameManager sharedInstance].applicationFrame) - self.segmentView.maxY - DLConstTabBarHeight);
    self.baseScrollView.contentSize = CGSizeMake(self.baseScrollView.width * self.segmentTitleArray.count, 0);
    
    self.baseScrollView.contentOffset = CGPointMake(self.baseScrollView.width * (self.segmentView.currentRow), self.baseScrollView.contentOffset.y);
    
    @weakify(self)
    [self.segmentControllerArray enumerateObjectsUsingBlock:^(UIViewController *viewController, NSUInteger index, BOOL *stop) {
        @strongify(self)
        
        viewController.view.frame = self.baseScrollView.bounds;
        viewController.view.x = self.baseScrollView.width * index;
    }];
}

- (void)setScrollsToTopWithCurrentRow:(NSInteger)currentRow {
    
    [self.segmentControllerArray enumerateObjectsUsingBlock:^(UIViewController *viewController, NSUInteger index, BOOL *stop) {
        
        if ([viewController conformsToProtocol:@protocol(DLSegmentViewControllerProtocol)]) {
            
            UIViewController <DLSegmentViewControllerProtocol> *segmentViewController = (UIViewController <DLSegmentViewControllerProtocol> *)viewController;
            
            if (index == currentRow) {
                [segmentViewController setScrollsToTop:YES];
            } else {
                [segmentViewController setScrollsToTop:NO];
            }
        }
    }];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [self.segmentView setLineViewCenterWithOffset:scrollView.contentOffset];
    
    CGFloat offsetX = scrollView.contentOffset.x;
    
    NSInteger currentRow = (offsetX + scrollView.width / 2) / scrollView.width;
    if (currentRow < 0) {
        currentRow = 0;
    }
    if (currentRow > self.segmentView.numberOfButtons - 1) {
        currentRow = self.segmentView.numberOfButtons - 1;
    }
    
    [_segmentView setRow:currentRow isMoveLine:NO];
    
    [self setScrollsToTopWithCurrentRow:currentRow];
}

#pragma mark - DLSegmentViewDelegate

- (void)segmentView:(DLSegmentView *)segmentView didSelectSegmentAtRow:(NSInteger)row {
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         [segmentView setRow:row isMoveLine:YES];
                         
                         [self.baseScrollView setContentOffset:CGPointMake(self.baseScrollView.width * row, 0) animated:NO];
                         
                     }completion:^(BOOL finished) {
                         
                         [self setScrollsToTopWithCurrentRow:row];
                     }];
}

@end
