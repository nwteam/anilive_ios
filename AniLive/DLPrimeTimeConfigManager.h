//
//  DLPrimeTimeConfigManager.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/12/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PSDKLibrary/PTMetadata.h>

@class DLWatch;
@class DLUser;

@interface DLPrimeTimeConfigManager : NSObject

+ (PTMetadata *)getMetaDataWithWatchData:(DLWatch *)watchData requestAds:(BOOL)requestAds;
+ (NSString *) getUserMemberStatus:(DLUser *)user;

@end
