//
//  DLContinueToPlayView.h
//  AniLive
//
//  Created by volcano on 1/4/17.
//  Copyright © 2017 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UnityAds/UnityAds.h>
#import "DLMovie.h"

@protocol DLContinueToPlayViewDelegate;
@interface DLContinueToPlayView : UIView<UITableViewDelegate, UITableViewDataSource, UnityAdsDelegate>

@property (nonatomic, weak) id<DLContinueToPlayViewDelegate> delegate;
@property (nonatomic, strong) UIViewController *parentVC;

- (instancetype)initWithParentViewController:(UIViewController *)parentViewController;
- (void)disPlayWithCurrentMovie:(DLMovie *)currentMovie;
- (void)setAdsDescription:(NSString*)strDesc;
- (void)setNextShowDesciption:(NSString*)strDesc;

@end

@protocol DLContinueToPlayViewDelegate <NSObject>

@optional
- (void)onBtnClose;
- (void)showNextMovie:(DLMovie *)movie;
@end
