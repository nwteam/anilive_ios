//
//  DLPlayerCommentView.m
//  AniLive
//
//  Created by isaoeka on 2016/12/25.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPlayerCommentView.h"
#import "DLAppFrameManager.h"
#import "DLHighlightedButton.h"
#import "UITextView+Placeholder.h"
#import "DLAPIHelper.h"

static NSInteger maxInputLength = 140;
//static NSInteger maxInputLines  = 3;

@interface DLPlayerCommentView () <UITextViewDelegate>

@end

@implementation DLPlayerCommentView

#pragma mark - init

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    self.backgroundColor = [UIColor colorWithHex:DLConstColorLightBlue];
    
#pragma mark Growing Text View
    
    self.growingTextView = [UITextView new];
    self.growingTextView.delegate = self;
    self.growingTextView.returnKeyType = UIReturnKeyDone;
    
    self.growingTextView.backgroundColor = [UIColor colorWithHex:DLConstColorDarkBlue];
    self.growingTextView.layer.masksToBounds = YES;
    self.growingTextView.layer.cornerRadius = 4.0f;
    
    self.growingTextView.font = BASE_FONT(14);
    self.growingTextView.textColor = [UIColor colorWithHex:DLConstColorLightWhite];
    
    self.growingTextView.placeholder = Localizer(@"comment_placeholder");
    self.growingTextView.placeholderColor = [UIColor colorWithHex:DLConstColorLightWhite];
   
    [self addSubview:self.growingTextView];
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

#pragma mark - Dealloc

- (void)dealloc {
    self.growingTextView.delegate = nil;
}

#pragma mark - Public Methods

- (void)resignComment:(BOOL)clearComment {
    if(clearComment){
        self.growingTextView.text = @"";
    }
    [self.growingTextView resignFirstResponder];
}

#pragma mark - Private Methods

- (void)updateFrame {
    CGFloat marginTop     = DLConstDefaultMargin / 2;
    CGFloat textViewWidth = self.width - (DLConstDefaultMargin * 2);
    
    self.growingTextView.frame = CGRectMake(DLConstDefaultMargin,
                                            marginTop,
                                            textViewWidth,
                                            self.height - DLConstDefaultMargin);
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    // 文字数制限チェック
    NSMutableString *str = [textView.text mutableCopy];
    [str replaceCharactersInRange:range withString:text];
    if ([str length] >= maxInputLength) return NO;
    
    // 改行を抑制
    if ([text isEqualToString:@"\n"]) {
        if(str > 0){
            [self.commentEditDelegate didEndEditComment:self.growingTextView.text];
        }else{
            [self.growingTextView resignFirstResponder];
        }
        return NO;
    };

    
    [textView sizeToFit];
    if ([self.delegate respondsToSelector:@selector(playerCommentView:willChangeHeight:)]) {
        [self.delegate playerCommentView:self willChangeHeight:textView.height + DLConstDefaultMargin];
    }
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    [textView sizeToFit];
    if ([self.delegate respondsToSelector:@selector(playerCommentView:willChangeHeight:)]) {
        [self.delegate playerCommentView:self willChangeHeight:textView.height + DLConstDefaultMargin];
    }
}

@end
