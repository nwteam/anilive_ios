//
//  DLPrimeTimeConfigManager.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/12/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPrimeTimeConfigManager.h"
#import "PTVideoAnalyticsTrackingMetadata.h"
#import <PSDKLibrary/PTAuditudeMetadata.h>
#import <AdSupport/ASIdentifierManager.h>
#import "DLUserDataManager.h"
#import "DLMovie.h"
#import "DLLive.h"
#import "DLWatch.h"
#import "DLUser.h"

const NSInteger BILLING_TYPE_KEY_FREE = 1;
const NSInteger BILLING_TYPE_KEY_PREMIUM = 2;
const NSInteger BILLING_TYPE_KEY_TRIAL = 4;

NSString* const BILLING_TYPE_VALUE_FREE = @"guest";
NSString* const BILLING_TYPE_VALUE_PREMIUM = @"enable";
NSString* const BILLING_TYPE_VALUE_TRIAL = @"trial";
NSString* const BILLING_TYPE_VALUE_OTHER = @"disable";

@implementation DLPrimeTimeConfigManager

#pragma mark - Class Methods

+ (PTMetadata *)getMetaDataWithWatchData:(DLWatch *)watchData requestAds:(BOOL)requestAds {
    
    PTMetadata *metaData = [[PTMetadata alloc]init];
    
    // Adobe Analytics Advertisement
    PTVideoAnalyticsTrackingMetadata *videoAnalyticsMetaData = [DLPrimeTimeConfigManager getVideoAnalyticsMetadata:watchData];
    [metaData setMetadata:videoAnalyticsMetaData forKey:PTVideoAnalyticsTrackingMetadataKey];
    
    // Adobe Video Advertisement
    if(requestAds){
        PTAuditudeMetadata *auditudeMetaData = [DLPrimeTimeConfigManager getAdvertisingMetadata:watchData];
        [metaData setMetadata:auditudeMetaData forKey:PTAdResolvingMetadataKey];
    }
    
    return metaData;
}

#pragma mark - Private Methods

+ (PTVideoAnalyticsTrackingMetadata *) getVideoAnalyticsMetadata:(DLWatch *)watchData {
    PTVideoAnalyticsTrackingMetadata *videoAnalyticsMetaData = [[PTVideoAnalyticsTrackingMetadata alloc]initWithTrackingServer:DLConstAdobeAnalyticsTrackingServer
                                                                                                                     publisher:DLConstAdobeAnalyticsPublisher];
    videoAnalyticsMetaData.channel = [self getUserMemberStatus:[DLUserDataManager sharedInstance].user];
    videoAnalyticsMetaData.videoName = [NSString stringWithFormat:@"%@:%@", watchData.worksTitle, watchData.moviesTitle];
    videoAnalyticsMetaData.videoId = [watchData.productId description];
    videoAnalyticsMetaData.playerName = DLConstAdobeAnalyticsPlayerName;
    videoAnalyticsMetaData.useSSL = NO;
#ifdef RELEASE
    videoAnalyticsMetaData.debugLogging = NO;
    videoAnalyticsMetaData.quietMode = NO;
#else
    videoAnalyticsMetaData.debugLogging = YES;
    videoAnalyticsMetaData.quietMode = YES;
#endif
    videoAnalyticsMetaData.enableChapterTracking = YES;
    videoAnalyticsMetaData.videoMetadataBlock = ^NSDictionary *() {
        return @{@"video_coutry_info": @"US"};
    };
    
    return videoAnalyticsMetaData;
}

+ (PTAuditudeMetadata *) getAdvertisingMetadata:(DLWatch *)watchData {
    PTAuditudeMetadata *auditudeMetaData = [[PTAuditudeMetadata alloc]init];
    auditudeMetaData.domain = DLConstAdobeAdvertisementDomain;
    auditudeMetaData.zoneId = DLConstAdobeAdvertisementZoneId;
    auditudeMetaData.signalingMode = PTAdSignalingModeDefault;
    auditudeMetaData.isCreativeRepackagingEnabled = YES;
    
    NSMutableDictionary *targetingParameters = [[NSMutableDictionary alloc] init];
    targetingParameters[@"getGeo_country"] = @"US";
    targetingParameters[@"video_name"] = watchData.adId;
    targetingParameters[@"ad_flag"] = [self getUserMemberStatus:[DLUserDataManager sharedInstance].user];
    targetingParameters[@"ref_url"] = @"DAISUKI_APP_iOS";
    targetingParameters[@"PRE"] = @"3";
    targetingParameters[@"POST"] = @"3";
    
    ASIdentifierManager *asIdentifier = [ASIdentifierManager sharedManager];
    NSLog(@"Advertising Id:%@, TrackingEnabled:%@", asIdentifier.advertisingIdentifier.UUIDString, @([asIdentifier isAdvertisingTrackingEnabled]));
    
    if ([asIdentifier isAdvertisingTrackingEnabled]) {
        [targetingParameters setValue:asIdentifier.advertisingIdentifier.UUIDString forKey:@"dvid"];
        [targetingParameters setValue:@"1" forKey:@"dnt"];
    } else {
        [targetingParameters setValue:@"" forKey:@"dvid"];
        [targetingParameters setValue:@"0" forKey:@"dnt"];
    }
    auditudeMetaData.targetingParameters = targetingParameters;
    
    return auditudeMetaData;
}

+ (NSString *) getUserMemberStatus:(DLUser *)user {
    switch ([user.billingType integerValue]) {
        case BILLING_TYPE_KEY_FREE:{
            return BILLING_TYPE_VALUE_FREE;
        }
        case BILLING_TYPE_KEY_PREMIUM: {
            return BILLING_TYPE_VALUE_PREMIUM;
        }
        case BILLING_TYPE_KEY_TRIAL: {
            return BILLING_TYPE_VALUE_TRIAL;
        }
        default:{
            return BILLING_TYPE_VALUE_OTHER;
        }
    }
}

@end
