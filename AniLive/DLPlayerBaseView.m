//
//  DLPlayerBaseView.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/17.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPlayerBaseView.h"
#import "DLMovie.h"
#import "DLLive.h"
#import "DLPlayer.h"
#import "DLPrimeTimeConfigManager.h"
#import "DLAdPolicySelector.h"
#import <PSDKLibrary/PSDKLibrary.h>
#import "PTVideoAnalyticsTracker.h"

static NSString *kPlayerStatusKeyPath = @"status";
static NSString *kPlayerIsLoadedKeyPath = @"isLoaded";

@interface DLPlayerBaseView ()

@property (nonatomic, strong) DLMovie *movieData;
@property (nonatomic, strong) DLLive *liveData;
@property (nonatomic, strong) DLPlayer *player;
@property (nonatomic, strong) PTMediaPlayer *mediaPlayer;
@property (nonatomic, strong) PTVideoAnalyticsTracker *videoAnalyticsTracker;
@property (nonatomic, strong) DLAdPolicySelector *adPolicySelector;
@property (nonatomic, strong) AVPlayerItem *playerItem;
@property (nonatomic, strong) PTMediaPlayerItem *mediaPlayerItem;
@property (nonatomic, assign) id playTimeObserver;
@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;
@property (nonatomic, assign) DLPlayerBaseViewType type;
@property (nonatomic, assign) BOOL isLive;

@property (readwrite, nonatomic, assign) BOOL isPlaying;
@property (readwrite, nonatomic, assign) BOOL isLoaded;
@property (nonatomic, assign) BOOL isRepeat;

@end

@implementation DLPlayerBaseView

- (instancetype)initWithMovieData:(DLMovie *)movieData withType:(DLPlayerBaseViewType)type {
    self = [super init];
    if (self) {
        
        self.type = type;
        
        self.movieData = movieData;
        self.isLive = NO;
        
        switch (type) {
            case DLPlayerBaseViewTypeDisplay:
            {
                // TODO : mediaIdの使用用途が不明なので要確認
                PTMetadata* metaData = [PTMetadata new];
                self.mediaPlayerItem = [[PTMediaPlayerItem alloc]initWithUrl:[NSURL URLWithString:self.movieData.image] mediaId:@"" metadata:metaData];
                // TODO : ATS問題があるため、YESに変えるか検討
                self.mediaPlayerItem.secure = NO;
                break;
            }
                
            default:
            {
                self.playerItem = [[AVPlayerItem alloc]initWithURL:[NSURL URLWithString:self.movieData.image]];
                break;
            }
        }
        
        [self initProcess];
    }
    return self;
}

- (instancetype)initWithLiveData:(DLLive *)liveData withType:(DLPlayerBaseViewType)type {
    
    self = [super init];
    if (self) {
        
        self.type = type;
        
        self.liveData = liveData;
        self.isLive = YES;
        
        switch (type) {
            case DLPlayerBaseViewTypeDisplay:
            {
                // TODO : mediaIdの使用用途が不明なので要確認
                PTMetadata* metaData = [PTMetadata new];
                self.mediaPlayerItem = [[PTMediaPlayerItem alloc]initWithUrl:[NSURL URLWithString:self.movieData.image] mediaId:@"" metadata:metaData];
                // TODO : ATS問題があるため、YESに変えるか検討
                self.mediaPlayerItem.secure = NO;
                break;
            }
                
            default:
            {
                self.playerItem = [[AVPlayerItem alloc]initWithURL:[NSURL URLWithString:self.liveData.highlightMovieUrl]];
                break;
            }
        }
        
        [self initProcess];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
    
    switch (self.type) {
        case DLPlayerBaseViewTypeDisplay:
        {
            break;
        }
            
        default:
        {
            [self.player removeTimeObserver:self.playTimeObserver];
            break;
        }
    }
    [self removePlayerObserver];
    [self removePlayerIsLoadedObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Public Methods

- (void)setPlayerMute:(BOOL)isMute {
    
    switch (self.type) {
        case DLPlayerBaseViewTypeDisplay:
        {
            self.mediaPlayer.muted = isMute;
            break;
        }
            
        default:
        {
            self.player.muted = isMute;
            break;
        }
    }
}

- (void)setPlayerRepeat:(BOOL)isRepeat {
    self.isRepeat = isRepeat;
}

- (void)pause {
    
    if (!self.isPlaying) {
        return;
    }
    
    switch (self.type) {
        case DLPlayerBaseViewTypeDisplay:
        {
            [self.mediaPlayer pause];
            break;
        }
            
        default:
        {
            [self.player pause];
            break;
        }
    }
    self.isPlaying = NO;
    [self.playerControlView updateIsPlaying:self.isPlaying];
}

- (void)play {
    
    if (self.isPlaying) {
        return;
    }
    
    switch (self.type) {
        case DLPlayerBaseViewTypeDisplay:
        {
            [self.mediaPlayer play];
            break;
        }
            
        default:
        {
            [self.player play];
            break;
        }
    }
    
    self.isPlaying = YES;
    [self.playerControlView updateIsPlaying:self.isPlaying];
}

- (void)startIndicator {
    [self.indicatorView startAnimating];
}

- (void)stopIndicator {
    [self.indicatorView stopAnimating];
}

- (void)setPlayerViewHidden:(BOOL)isHidden withAnimated:(BOOL)animated {
    
    if (animated) {
        
        [UIView animateWithDuration:0.2
                         animations:^{
                             self.playerView.alpha = (isHidden ? 0 : 1);
                         }];
        
    } else {
        self.playerView.alpha = (isHidden ? 0 : 1);
    }
    
    // インジケータの表示制御
    if (!self.isLoaded && !isHidden) {
        [self startIndicator];
    } else {
        [self stopIndicator];
    }
}

- (void)seekToTime:(CMTime)time {
    switch (self.type) {
        case DLPlayerBaseViewTypeDisplay:
        {
            [self.mediaPlayer seekToTime:time];
            break;
        }
            
        default:
        {
            [self.player seekToTime:time];
            break;
        }
    }
}

#pragma mark - Private Methods

- (void)initProcess {
    [self initView];
    [self configPlayer];
    [self addSwipeGestureRecognizer];
}

- (void)configPlayer {
    
    switch (self.type) {
        case DLPlayerBaseViewTypeDisplay:
        {
            self.mediaPlayer = [PTMediaPlayer playerWithMediaPlayerItem:self.mediaPlayerItem];
            self.mediaPlayer.allowsAirPlayVideo = YES;
            self.mediaPlayer.videoGravity = PTMediaPlayerVideoGravityResizeAspect;
            self.mediaPlayer.closedCaptionDisplayEnabled = YES;
            self.mediaPlayer.view.contentMode = UIViewContentModeScaleAspectFit;
            [self insertSubview:self.mediaPlayer.view belowSubview:self.playerControlView];
            
            self.videoAnalyticsTracker = [[PTVideoAnalyticsTracker alloc]initWithMediaPlayer:self.mediaPlayer];
            self.adPolicySelector = [[DLAdPolicySelector alloc]init];
            [[PTDefaultMediaPlayerClientFactory defaultFactory] registerAdPolicySelector:self.adPolicySelector];
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(mediaPlayerStatusDidChange:)
                                                         name:PTMediaPlayerStatusNotification
                                                       object:self.mediaPlayer];
            //AddOvserver
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(mediaPlayerTimeDidChange:)
                                                         name:PTMediaPlayerTimeChangeNotification
                                                       object:self.mediaPlayer];
            break;
        }
            
        default:
        {
            self.player = [[DLPlayer alloc]initWithPlayerItem:self.playerItem
                                   withAllowsExternalPlayback:YES];
            self.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
            
            [self.playerView setPlayer:self.player];
            
            [self.player addObserver:self
                          forKeyPath:kPlayerStatusKeyPath
                             options:NSKeyValueObservingOptionNew
                             context:nil];
            
            @weakify(self)
            self.playTimeObserver = [self.player addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(0.1f, NSEC_PER_SEC)
                                                                              queue:nil
                                                                         usingBlock:^(CMTime time) {
                                                                             @strongify(self)
                                                                             [self.playerControlView updateCurrentTime];
                                                                         }];
            break;
        }
    }
    
    [self addObserver:self
           forKeyPath:kPlayerIsLoadedKeyPath
              options:NSKeyValueObservingOptionNew
              context:nil];
}

- (void)initView {
    
    self.indicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.indicatorView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.indicatorView];
    
    switch (self.type) {
        case DLPlayerBaseViewTypeDisplay:
        {
            if (self.isLive) {
                self.playerControlView = [[DLPlayerControlView alloc]initWithLiveData:self.liveData withMediaPlayerItem:self.mediaPlayerItem];
            } else {
                self.playerControlView = [[DLPlayerControlView alloc]initWithMovieData:self.movieData withMediaPlayerItem:self.mediaPlayerItem];
            }
            
            [self addSubview:self.playerControlView];
            break;
        }
            
        default:
        {
            self.playerView = [[DLPlayerView alloc]init];
            self.playerView.backgroundColor = [UIColor blackColor];
            [self addSubview:self.playerView];
            break;
        }
    }
}

- (void)updateFrame {
    
    self.indicatorView.frame = self.bounds;
    
    switch (self.type) {
        case DLPlayerBaseViewTypeDisplay:
        {
            self.mediaPlayer.view.frame = self.bounds;
            self.playerControlView.frame = self.bounds;
            break;
        }
            
        default:
        {
            self.playerView.frame = self.bounds;
            break;
        }
    }
}

- (void)removePlayerObserver {
    
    switch (self.type) {
        case DLPlayerBaseViewTypeDisplay:
        {
            @try{
                [[NSNotificationCenter defaultCenter] removeObserver:self
                                                                name:PTMediaPlayerStatusNotification
                                                              object:self.mediaPlayer];
            }@catch(id anException){
                // 設定していない場合でも落ちないための処理のため、Exceptionは無視する
            }
            break;
        }
            
        default:
        {
            @try{
                [self.player removeObserver:self forKeyPath:kPlayerStatusKeyPath];
            }@catch(id anException){
                // 設定していない場合でも落ちないための処理のため、Exceptionは無視する
            }
            break;
        }
    }
}

- (void)removePlayerIsLoadedObserver {
    
    @try{
        [self removeObserver:self forKeyPath:kPlayerIsLoadedKeyPath];
    }@catch(id anException){
        // 設定していない場合でも落ちないための処理のため、Exceptionは無視する
    }
}

#pragma mark - KVO

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if ([keyPath isEqualToString:kPlayerStatusKeyPath]) {
        if ([self.player status] == AVPlayerStatusReadyToPlay) {
            [self removePlayerObserver];
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(playerItemDidReachEnd:)
                                                         name:AVPlayerItemDidPlayToEndTimeNotification
                                                       object:[self.player currentItem]];
            
            self.isLoaded = YES;
        }
        else if ([_player status] == AVPlayerItemStatusFailed){
            [self removePlayerObserver];
            [self stopIndicator];
        }
        else if ([_player status] == AVPlayerItemStatusUnknown){
            [self removePlayerObserver];
            [self stopIndicator];
        }
    } else if ([keyPath isEqualToString:kPlayerIsLoadedKeyPath]) {
        if (self.isLoaded) {
            [self stopIndicator];
            
            if (NOT_NULL(self.playerControlView)) {
                // 最初の動画ロードが終わってからは常に表示
                [self.playerControlView updateIsLoaded:self.isLoaded];
            }
            
            if ([self.delegate respondsToSelector:@selector(playerBaseViewDidLoadedMovie:)]) {
                [self.delegate playerBaseViewDidLoadedMovie:self];
            }
        }
        
        if (self.isPlaying) {
            [self setPlayerViewHidden:NO withAnimated:YES];
        }
    }
}

#pragma mark - Notification

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    
    if (self.isRepeat) {
        AVPlayerItem *playerItem = [notification object];
        [playerItem seekToTime:kCMTimeZero];
    } else if (self.type == DLPlayerBaseViewTypeDisplay) {
        
        [self pause];
        
        if ([self.delegate respondsToSelector:@selector(playerBaseViewPlayerItemDidReachEnd)]) {
            [self.delegate playerBaseViewPlayerItemDidReachEnd];
        }
    }
}

#pragma mark - Gesture Recognizer

// TODO: 明示的にremoveさせる必要はあるか確認
- (void)addSwipeGestureRecognizer {
    UISwipeGestureRecognizer *swipeLeftGesture  = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft:)];
    UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
    
    swipeLeftGesture.direction  = UISwipeGestureRecognizerDirectionLeft;
    swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
    
    [self addGestureRecognizer:swipeLeftGesture];
    [self addGestureRecognizer:swipeRightGesture];
}

- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
    if ([self.delegate respondsToSelector:@selector(playerBaseViewPlayerSwipeGestureLeft:)]) {
        [self.delegate playerBaseViewPlayerSwipeGestureLeft:sender];
    }
}

- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
    if ([self.delegate respondsToSelector:@selector(playerBaseViewPlayerSwipeGestureRight:)]) {
        [self.delegate playerBaseViewPlayerSwipeGestureRight:sender];
    }
}

- (void)mediaPlayerStatusDidChange:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    PTMediaPlayerStatus status = [(NSNumber*)[userInfo objectForKey:PTMediaPlayerStatusKey] intValue];
    
    switch (status) {
        case PTMediaPlayerStatusCreated:
        {
            NSLog(@"CurrentMediaPlayerStatus: Created");
            break;
        }
        case PTMediaPlayerStatusInitializing:
        {
            NSLog(@"CurrentMediaPlayerStatus: Initializing");
            break;
        }
        case PTMediaPlayerStatusInitialized:
        {
            NSLog(@"CurrentMediaPlayerStatus: Initialized");
            break;
        }
        case PTMediaPlayerStatusReady:
        {
            NSLog(@"CurrentMediaPlayerStatus: Ready");
            
            [self removePlayerObserver];
            self.isLoaded = YES;
            break;
        }
        case PTMediaPlayerStatusPlaying:
        {
            NSLog(@"CurrentMediaPlayerStatus: Playing");
            break;
        }
        case PTMediaPlayerStatusPaused:
        {
            NSLog(@"CurrentMediaPlayerStatus: Paused");
            break;
        }
        case PTMediaPlayerStatusStopped:
        {
            NSLog(@"CurrentMediaPlayerStatus: Stopped");
            break;
        }
        case PTMediaPlayerStatusCompleted:
        {
            NSLog(@"CurrentMediaPlayerStatus: Completed");
            
            [self playerItemDidReachEnd:notification];
            break;
        }
        case PTMediaPlayerStatusError:
        {
            NSLog(@"CurrentMediaPlayerStatus: Error");
            NSLog(@"Reason: %@", self.player.error.localizedDescription);
            
            [self removePlayerObserver];
            [self stopIndicator];
            break;
        }
        default:
            break;
    }
}

- (void)mediaPlayerTimeDidChange:(NSNotification *)notification {
    
}

@end
