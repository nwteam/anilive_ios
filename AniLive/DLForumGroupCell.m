//
//  DLForumGroupCell.m
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/10/23.
//  Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLForumGroupCell.h"
#import "DLForumGroup.h"
#import "DLForum.h"
#import "DLForumGroupListViewController.h"
#import "DLForumListViewController.h"
#import "DLTransitionManager.h"
#import <UIImageView+AFNetworking.h>
#import "UIImageView+Category.h"

@interface DLForumGroupCell ()
@property(weak, nonatomic) IBOutlet UIImageView *ivThumbnail;
@property(weak, nonatomic) IBOutlet UILabel *lbMovieTitle;
@property(weak, nonatomic) IBOutlet UILabel *lbForumCount;
@property(weak, nonatomic) IBOutlet UILabel *lbForumTitle;
@property(weak, nonatomic) IBOutlet UILabel *lbCommentCountAndSoOn;
@property (weak, nonatomic) IBOutlet UIButton *seeMoreButton;
@property (weak, nonatomic) IBOutlet UILabel *lbMovieEpisode;
@property (weak, nonatomic) IBOutlet UIImageView *popularMarkA;
@property (weak, nonatomic) IBOutlet UIImageView *polularMarkB;
@property (weak, nonatomic) IBOutlet UIImageView *popularMarkC;

@end

@implementation DLForumGroupCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.seeMoreButton.layer.cornerRadius = 4.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setGroup:(DLForumGroup *)group {
    _group = group;
    if (!_group) {
        return;
    }
    self.lbMovieTitle.text = group.movie.title;
    self.lbForumCount.text = @(group.forums.count).stringValue;
    DLForum *forum = group.forums[0];
    self.lbForumTitle.text = forum.title;
//    NSTimeInterval interval = -[forum.created timeIntervalSinceNow];
//    self.lbCommentCountAndSoOn.text = [NSString stringWithFormat:@"%@ %d hours ago by %@",_froum.totalComments,
//                                                                 (int) (interval / 3600),
//                                                                 forum.forumOwnerUserName];

}

- (void)setFroum:(DLForum *)froum {
    _froum = froum;
    if (!_froum) {
        return;
    }
    self.lbMovieTitle.text = froum.movieTitle;
    self.lbMovieEpisode.text = [NSString stringWithFormat:@"Episode %@",froum.chapter];
    self.lbForumCount.text = [NSString stringWithFormat:@"%@ forums",froum.totalForums];
    self.lbForumTitle.text = froum.title;
    if ([froum.totalComments integerValue] > 0) {
        self.lbCommentCountAndSoOn.text = [NSString stringWithFormat:@"%@  comments",froum.totalComments];
        self.lbCommentCountAndSoOn.textColor = [UIColor colorWithHex:DLConstColorLightWhite];
    }
    else {
        self.lbCommentCountAndSoOn.text = @"NEW";
        self.lbCommentCountAndSoOn.textColor = [UIColor colorWithHex:DLConstColorCodeYellow];
    }
    
    if (NOT_NULL(_froum.movieImage)) {
        [UIImageView loadImageWithURLString:self.froum.movieImage
                            targetImageView:self.ivThumbnail];
    }
    _popularMarkC.hidden = [froum.popularity intValue] >= 10 ? NO : YES;
    _polularMarkB.hidden = [froum.popularity intValue] >= 3 ? NO : YES;
    _popularMarkA.hidden = [froum.popularity intValue] >= 1 ? NO : YES;
}

- (IBAction)onTapSeeMore:(id)sender {
    [DLTransitionManager showForumListViewControllerWithForum:_froum withTarget:nil];
}

- (void)loadImage {
//    [self.ivThumbnail setImageWithURL:[NSURL URLWithString:self.group.movie.mediumImage]];
    [UIImageView loadImageWithURLString:self.froum.movieImage
                        targetImageView:self.ivThumbnail];
}
@end
