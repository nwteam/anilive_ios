//
//  DLPointHistoryCell.m
//  AniLive
//
//  Created by 市川 俊介 on 2016/12/24.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPointHistoryCell.h"

const static CGFloat kCellHeight = 80.0f;   //ポイント消費履歴のリストの高さ

@interface DLPointHistoryCell()
@property (weak, nonatomic) IBOutlet UILabel *labelUpdateDate;
@property (weak, nonatomic) IBOutlet UILabel *labelExpireDate;
@property (weak, nonatomic) IBOutlet UILabel *labelUsedPoint;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

@end

@implementation DLPointHistoryCell

- (void)setPoint:(DLPoint *)point {
    _point = point;
    if (!point) {
        return;
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
    dateFormatter.dateFormat = @"MMM. yy, dd HH:mm";
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];

    self.labelTitle.text            =   point.reasonText;
    self.labelExpireDate.text       =   [NSString stringWithFormat:@"Watching period %@", [dateFormatter stringFromDate:point.viewLimit]];
    self.labelUsedPoint.text        =   [NSString stringWithFormat:@"-%dpt", [point.point intValue]];
    self.labelUpdateDate.text       =   [dateFormatter stringFromDate:point.pointDate];
    
}


@synthesize labelUpdateDate, labelExpireDate, labelUsedPoint, labelTitle;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (UINib *)getDefaultNib {
    return [UINib nibWithNibName:@"DLPointHistoryCell" bundle:nil];
}

+ (CGFloat)getCellHeight {
    return kCellHeight;
}

@end
