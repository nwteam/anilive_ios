//
//  DLAdPolicySelector.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/12/24.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLAdPolicySelector.h"

@interface DLAdPolicySelector ()

@property (nonatomic, strong) NSMutableDictionary *skipTimes;

@end

@implementation DLAdPolicySelector

- (id)init
{
    if (self = [super init])
    {
        self.shouldPlayAdBreaks = YES;
        self.skipTimes = [NSMutableDictionary dictionary];
    }
    
    return self;
}


- (NSArray *)selectAdBreaksToPlay:(PTAdPolicyInfo *)info
{
    NSString *key = [NSString stringWithFormat:@"%.0f", floorf(CMTimeGetSeconds(info.currentTime))];
    if (self.shouldPlayAdBreaks && [self.skipTimes valueForKey:key] == nil) {
        return [super selectAdBreaksToPlay:info];
    } else {
        return nil;
    }
}

- (PTAdBreakPolicyType)selectPolicyForAdBreak:(PTAdPolicyInfo *)info
{
    NSString *key = [NSString stringWithFormat:@"%.0f", floorf(CMTimeGetSeconds(info.currentTime))];
    PTAdBreakPolicyType result;
    if (self.shouldPlayAdBreaks && [self.skipTimes valueForKey:key] == nil) {
        result = [super selectPolicyForAdBreak:info];
    } else {
        result = PTAdBreakPolicyTypeSkip;
    }
    [self.skipTimes setValue:@"" forKey:key];
    
    return result;
}

@end
