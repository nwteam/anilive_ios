//
//  DLForumView.m
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/11/06.
//  Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLForumView.h"
#import "DLAppFrameManager.h"

@interface DLForumView ()
@property(nonatomic, readwrite) UITextView *tfComment;
@property(nonatomic, readwrite) SendTouchTableView *tableView;
@property(nonatomic, readwrite) UIView *textFieldBackVeiw;
@property(nonatomic, readwrite) UILabel *placeHoderLable;
@end

@implementation DLForumView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initView];
    }
    return self;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self updateFrame];
}

- (void)initView {

    self.tableView = SendTouchTableView.new;
    [self addSubview:self.tableView];
    [self.tableView registerNib:[UINib nibWithNibName:@"DLCommentCell" bundle:nil] forCellReuseIdentifier:@"comment-cell"];
    self.tableView.estimatedRowHeight = 20;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.textFieldBackVeiw = UIView.new;
    self.textFieldBackVeiw.backgroundColor = [UIColor colorWithHex:DLConstColorLightBlue];
    [self addSubview:self.textFieldBackVeiw];
    
    self.tfComment = UITextView.new;
    self.tfComment.backgroundColor = [UIColor colorWithHex:DLConstColorDarkBlue];
    self.tfComment.returnKeyType = UIReturnKeySend;
    self.tfComment.enablesReturnKeyAutomatically = YES;
    self.tfComment.textColor = [UIColor whiteColor];
    self.tfComment.font = [UIFont systemFontOfSize:14];;
    self.tfComment.layer.cornerRadius = 5.0;
//    self.tfComment.placeholder = @"Add Comment";
    [self.textFieldBackVeiw addSubview:self.tfComment];
    
    self.placeHoderLable = UILabel.new;
    self.placeHoderLable.text = @"Add Comment";
    self.placeHoderLable.backgroundColor = [UIColor clearColor];
    self.placeHoderLable.userInteractionEnabled = NO;
    self.placeHoderLable.textColor = [UIColor lightGrayColor];
    self.placeHoderLable.font = [UIFont systemFontOfSize:14];
    [self.textFieldBackVeiw addSubview:self.placeHoderLable];
}


- (void)updateFrame {
    CGFloat commentFieldHeight = 45.0;
    if (self.tfComment.text && self.tfComment.text.length) {
        CGSize textSize = [_tfComment.text sizeWithFont:_tfComment.font constrainedToSize:CGSizeMake(CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame) - 41, 80) lineBreakMode:NSLineBreakByWordWrapping];
        commentFieldHeight = textSize.height + 14 > 45 ? textSize.height + 14 : 45;
    }
    CGRect f = self.frame;
    CGFloat w = CGRectGetWidth(f);
    CGFloat h = CGRectGetHeight(f);

    self.tableView.frame = CGRectMake(0,
            0,
            w,
            h - commentFieldHeight);
    self.textFieldBackVeiw.frame = CGRectMake(0,
                                      CGRectGetMaxY(self.tableView.frame),
                                      w,
                                      commentFieldHeight);
    self.tfComment.frame = CGRectMake(16,
            7,
            w - 32,
            commentFieldHeight - 14);
    self.placeHoderLable.frame = CGRectMake(20,
                                            7,
                                            w - 32,
                                            commentFieldHeight - 14);
}

- (void)displayNewInputText:(NSString *)newStr {
    if (newStr && newStr.length > 0) {
        self.placeHoderLable.hidden = YES;
    }
    else {
        self.placeHoderLable.hidden = NO;
    }
    [self updateFrame];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
}

@end
