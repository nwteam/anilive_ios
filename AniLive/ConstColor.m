//
//  ConstColor.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/17.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "ConstColor.h"

@implementation ConstColor

NSString * const DLConstColorCodeGray = @"#878787";
NSString * const DLConstColorCodeThinGray = @"767A84";
NSString * const DLConstColorCodeDarkGray = @"#262626";
NSString * const DLConstColorCodeDarkBlue = @"0F111B";
NSString * const DLConstColorCodeYellow = @"FFE500";
NSString * const DLConstColorLightBlue = @"2A303F";
NSString * const DLConstColorLightWhite = @"BEBDBE";
NSString * const DLConstColorDarkBlue = @"13151D";

@end
