//
//  DLReportType.h
//  AniLive
//
//  Created by mac on 2017/1/9.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLModelBase.h"

@interface DLReportType : DLModelBase

@property(readonly, nonatomic, strong) NSNumber *categoryID;
@property(readonly, nonatomic, strong) NSString *name;

@end
