//
//  DLConnectDeviceViewController.h
//  AniLive
//
//  Created by suisun on 2017/01/14.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DLAPIHelper.h"
#import "DLNavigationBarView.h"

@interface DLConnectDeviceViewController : UIViewController<DLNavigationBarViewDelegate>

@end
