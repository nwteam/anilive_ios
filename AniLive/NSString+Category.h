//
//  NSString+Category.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/17.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Category)

+ (NSString*)commasFormatedStringFromNumber:(NSNumber*)number;
+ (NSString*)formatSecTimeToString:(float)time;
+ (NSString*)formatMillisecTimeToString:(float)time;

- (NSMutableAttributedString*)getAdjustLineSpaceStringWithLineSpacing:(CGFloat)lineSpacing;
- (NSMutableAttributedString*)getAdjustLineSpaceStringWithLineSpacing:(CGFloat)lineSpacing withLineBreakMode:(NSLineBreakMode)lineBreakMode;
- (NSMutableAttributedString*)getAdjustLineSpaceStringWithLineSpacing:(CGFloat)lineSpacing withLineBreakMode:(NSLineBreakMode)lineBreakMode withTextAlignment:(NSTextAlignment)textAlignment;

+ (void)attributeString:(NSMutableAttributedString*)mutableAttributedString withString:(NSString*)string withFont:(UIFont*)font withColor:(UIColor*)color;
+ (void)attributeParagraphStyle:(NSMutableAttributedString*)mutableAttributedString withParagraphStyle:(NSMutableParagraphStyle*)paragrahStyle;

+ (NSString *) calcMultiple: (NSInteger) number unit:(NSString *) unit;
@end
