//
//  DLAlertViewResult.m
//  AniLive
//
//  Created by suisun on 1/13/17.
//  Copyright © 2017 AnimeConsortiumJapan. All rights reserved.
//

#import "DLAlertViewResult.h"

@implementation DLAlertViewResult {
    BOOL isChecked;
}

@synthesize messageView;
@synthesize titleLabel;
@synthesize msgLabel;
@synthesize lblUserID;
@synthesize lblPassword;
@synthesize lblStrUserID;
@synthesize lblStrPassword;
@synthesize btnOK;

- (id)initDLAlertViewResult {
    NSArray* bundle = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    self = [bundle objectAtIndex:0];
    if (self) {
        // init
        [self initMainView];
    }
    return self;
}

- (void)initMainView {
    messageView.layer.cornerRadius = 5;
    messageView.clipsToBounds = true;
    
    titleLabel.text     = @"Password set successfully";
    msgLabel.text       = @"Your password has set successfully. Please enter the UserID and password below to take over data to other device.";
    lblUserID.text      = @"User ID :";
    lblPassword.text    = @"Password :";
    lblStrUserID.text   = @"123465789";
    lblStrPassword.text = @"******";
    
    [btnOK setTitle:@"OK" forState:UIControlStateNormal];
    btnOK.layer.masksToBounds   = YES;
    btnOK.layer.cornerRadius    = 3.0;

}

#pragma marker - dismiss keyboard when press return key
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField isFirstResponder]) {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    textField.delegate = self;
    textField.keyboardType = UIKeyboardTypeDefault;
    textField.returnKeyType = UIReturnKeyDone;
    return YES;
}

- (IBAction)tapBtnDLAlertViewResult:(UIButton *)sender {
    [self.delegate didTapButtonDLAlertViewResult:[sender tag]];
}

@end
