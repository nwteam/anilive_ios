//
//  DLPayingPointManager.m
//  AniLive
//
//  Created by shavin on 2017/1/10.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPayingPointManager.h"
#import "DLAPIHelper.h"
#import "DLUserDataManager.h"
#import "DLUnityAdsManager.h"
#import "DLPoint.h"

@interface DLPayingPointManager () {
    UIViewController *_targetVC;
}

@property (nonatomic, strong) NSNumber *movieID;
@property (nonatomic, strong) DLUnityAdsManager *adsManager;

@end

@implementation DLPayingPointManager

+ (instancetype)sharedInstance {
    static DLPayingPointManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[DLPayingPointManager alloc] init];
        [manager initConfig];
    });
    
    return manager;
}

- (void)initConfig {
    self.adsManager = [[DLUnityAdsManager alloc]init];
}

- (void)startPayingWithController:(UIViewController *)controller formovie:(NSNumber *)movieID Completion:(void (^)(BOOL))completetion {
    // TODO 消費ポイント定数化
    // TODO reason供給
    if ([[DLUserDataManager sharedInstance].user.point integerValue] > 25) {
        [DLAPIHelper usePointWithMovieId:movieID reason:nil callback:^(BOOL flag) {
            if (flag) {
                completetion(YES);
            } else {
                completetion(NO);
            }
        }];
    }
    else {
        [DLAPIHelper checkAdsShowingTimesCallback:^(BOOL flag) {
            if (flag) {
                payingPointBlock = completetion;
                _movieID = movieID;
                // 広告を表示するViewControllerの更新
                [self.adsManager adDidStart:^(NSString *placementId) {
                    [WINDOW makeToast:Localizer(@"point_ad_title")];
                }];
                [self.adsManager adDidError:^(NSString *errorMessage) {
                    [WINDOW makeToast:Localizer(@"show_ad_fail")];
                    if (payingPointBlock) {
                        payingPointBlock(NO);
                        payingPointBlock = nil;
                    }
                }];

                [self.adsManager adDidFinish:^(NSString *placementId, UnityAdsFinishState state) {
                    if (state == kUnityAdsFinishStateCompleted) {
                        _targetVC = controller;
                        [self addAdsPoint];
                    }
                    else {
                        [WINDOW makeToast:Localizer(@"show_ad_fail")];
                        if (payingPointBlock) {
                            payingPointBlock(NO);
                            payingPointBlock = nil;
                        }
                    }
                }];
                [self.adsManager updateTargetViewController:controller];
                [self.adsManager showAd];
            }
            else {
                [controller.view makeToast:@"You have already watched ads 20 times today. Try again tomorrow!"];
                [controller.view makeToast:Localizer(@"forum_8")];
                completetion(NO);
            }
        }];
    }
}

- (void)addAdsPoint {
    [DLAPIHelper addPoint:^(DLPoint *point) {
        if (point) {
            if ([point.point integerValue] < 25) {
                if (payingPointBlock) {
                    payingPointBlock = nil;
                }
            } else {
                [DLAPIHelper usePointWithMovieId:_movieID reason:nil callback:^(BOOL flag) {
                    if (flag) {
                        payingPointBlock(YES);
                    } else {
                        payingPointBlock(NO);
                    }
                }];
            }
        } else {
            if (payingPointBlock) {
                payingPointBlock(NO);
                payingPointBlock = nil;
            }
        }
    }];
}

+ (void)watchAdToAddPointWithTargetController:(UIViewController *)targetController adManager:(DLUnityAdsManager *)manager tostView:(UIView *)tostView {
    [DLAPIHelper checkAdsShowingTimesCallback:^(BOOL flag) {
        if (flag) {
            // 広告を表示するViewControllerの更新
            [manager adDidStart:^(NSString *placementId) {
                [WINDOW makeToast:Localizer(@"point_ad_title")];
            }];
            [manager adDidError:^(NSString *errorMessage) {
                [WINDOW makeToast:Localizer(@"show_ad_fail")];
            }];
            [manager adDidFinish:^(NSString *placementId, UnityAdsFinishState state) {
                if (state == kUnityAdsFinishStateCompleted) {
                    [DLAPIHelper addPoint:nil];
                }
                else {
                    [WINDOW makeToast:Localizer(@"show_ad_fail")];
                }
            }];

            [manager showAd];
        }
        else {
            [WINDOW makeToast:Localizer(@"forum_8")];
        }
    }];
}

@end
