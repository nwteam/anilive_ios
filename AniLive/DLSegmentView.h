//
//  DLSegmentView.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/10/01.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DLSegmentViewDelegate;

@interface DLSegmentView : UIView

@property (readonly, nonatomic, assign) NSInteger numberOfButtons;
@property (readonly, nonatomic, assign) NSInteger currentRow;

@property (nonatomic, weak) id<DLSegmentViewDelegate> delegate;

- (instancetype)initWithTitles:(NSArray *)titleArray
                      startRow:(NSInteger)startRow;

- (void)setLineViewCenterWithOffset:(CGPoint)offset;

- (void)setRow:(NSInteger)row isMoveLine:(BOOL) isMoveLine;

@end

@protocol DLSegmentViewDelegate <NSObject>

@optional
- (void)segmentView:(DLSegmentView *)segmentView didSelectSegmentAtRow:(NSInteger)row;

@end
