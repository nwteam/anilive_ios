//
//  DLUnityAdsManager.m
//  AniLive
//
//  Created by 辰己佳祐 on 2017/01/14.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLUnityAdsManager.h"

@interface DLUnityAdsManager () <UnityAdsDelegate>

@property (nonatomic, assign, readwrite) BOOL isAdReadyToPlay;
@property (nonatomic, assign, readwrite) BOOL isPlaying;

@property (nonatomic, weak) UIViewController *targetViewController;
@property (nonatomic, strong) id targetObserver;
@property (nonatomic, copy) DLUnityAdsManagerDidReadyBlock readyBlock;
@property (nonatomic, copy) DLUnityAdsManagerDidErrorBlock errorBlock;
@property (nonatomic, copy) DLUnityAdsManagerDidStartBlock startBlock;
@property (nonatomic, copy) DLUnityAdsManagerDidFinishBlock finishBlock;
@property (nonatomic, assign) BOOL isAddedObserver;

@end

@implementation DLUnityAdsManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self config];
    }
    return self;
}

- (instancetype)initTargetViewController:(UIViewController *)viewController
{
    self = [super init];
    if (self) {
        self.targetViewController = viewController;
        [self config];
    }
    return self;
}

- (void)dealloc {
    FUNC_LOG
    self.readyBlock = nil;
    self.errorBlock = nil;
    self.startBlock = nil;
    self.finishBlock = nil;
    [self removeObserver];
}

#pragma mark - Public Methods

- (void)addObserverWithTarget:(id)target {
    
    if (!self.isAddedObserver) {
        
        self.isAddedObserver = YES;
        self.targetObserver = target;
        
        [self addObserver:target
               forKeyPath:kAdsManagerReadyToPlayKeyPath
                  options:NSKeyValueObservingOptionNew
                  context:nil];
        
        [self addObserver:target
               forKeyPath:kAdsManagerIsPlayingKeyPath
                  options:NSKeyValueObservingOptionNew
                  context:nil];

        if([UnityAds isReady]){
            self.isAdReadyToPlay = YES;
        }
    }
}

- (void)removeObserver {
    @try{
        [self removeObserver:self.targetObserver
                  forKeyPath:kAdsManagerReadyToPlayKeyPath];
    }@catch(id anException){
        // 設定していない場合でも落ちないための処理のため、Exceptionは無視する
    }
    
    @try{
        [self removeObserver:self.targetObserver
                  forKeyPath:kAdsManagerIsPlayingKeyPath];
    }@catch(id anException){
        // 設定していない場合でも落ちないための処理のため、Exceptionは無視する
    }
}

- (void)updateTargetViewController:(UIViewController *)viewController {
    self.targetViewController = viewController;
}

- (void)showAd {
    if (self.isAdReadyToPlay && !self.isPlaying) {
        [UnityAds show:self.targetViewController];
    }
    else {
        if (_errorBlock) {
            _errorBlock(nil);
        }
    }
}

#pragma mark - Wrapped UnityAdsDelegate Methods

- (void)adDidReadyToPlay:(DLUnityAdsManagerDidReadyBlock)completion {
    self.readyBlock = [completion copy];
}

- (void)adDidError:(DLUnityAdsManagerDidErrorBlock)completion {
    self.errorBlock = [completion copy];
}

- (void)adDidStart:(DLUnityAdsManagerDidStartBlock)completion {
    self.startBlock = [completion copy];
}

- (void)adDidFinish:(DLUnityAdsManagerDidFinishBlock)completion {
    self.finishBlock = [completion copy];
}

#pragma mark - Private Methods

- (void)config {
    
    self.isAdReadyToPlay = NO;
    self.isPlaying = NO;
    
    // Load Unity Ad.
    [UnityAds setDelegate:self];
}

#pragma mark - Unity Ads Delegate

- (void)unityAdsReady:(NSString *)placementId {
    self.isAdReadyToPlay = YES;
    self.isPlaying = NO;
    if (self.readyBlock) {
        self.readyBlock(placementId);
    }
}

- (void)unityAdsDidError:(UnityAdsError)error withMessage:(NSString *)message {
    self.isAdReadyToPlay = NO;
    self.isPlaying = NO;
    if (self.errorBlock) {
        self.errorBlock(message);
    }
    NSLog(@"UnityAds Error!!!\nreason:%@", message);
}

- (void)unityAdsDidStart:(NSString *)placementId {
    self.isPlaying = YES;
    if (self.startBlock) {
        self.startBlock(placementId);
    }
}

- (void)unityAdsDidFinish:(NSString *)placementId withFinishState:(UnityAdsFinishState)state {
    self.isPlaying = NO;
    if (self.finishBlock) {
        self.finishBlock(placementId, state);
    }
}

@end
