//
//  DLTabBarController.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLMainTabBarController.h"
#import "DLNavigationController.h"

// Tab Controllers
#import "DLCatalogViewController.h"
#import "DLForumRootViewController.h"
#import "DLLiveViewController.h"
#import "DLNoticeViewController.h"
#import "DLMyPageViewController.h"
#import "DLAppFrameManager.h"
#import "DLTabBarView.h"
#import "DLAPIHelper.h"

const NSInteger kStartTabIndex = 2;

@interface DLMainTabBarController () <DLTabBarViewDelegate>

@property(nonatomic, strong) DLNavigationController *catalogViewController;
@property(nonatomic, strong) DLNavigationController *forumRootViewController;
@property(nonatomic, strong) DLNavigationController *liveViewController;
@property(nonatomic, strong) DLNavigationController *noticeViewController;
@property(nonatomic, strong) DLNavigationController *myPageViewController;
@property(nonatomic, strong) DLTabBarView *tabBarView;

@end

@implementation DLMainTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tabBar.hidden = YES;

    [self configViewControllers];
    [self configTabBarView];
    [self updateFrame];

    // 初期起動タブを指定
    self.selectedIndex = kStartTabIndex;
    
    // test
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recieveNoti:)
                                                 name:@"notiMessage"
                                               object:nil];
    
    NSLog(@"start tab: %ld", self.selectedIndex);
}

- (void)recieveNoti:(NSNotification *)noti {
    [self configTabBarView];
}

//- (void)refreshUI {
//    [self configTabBarView];
//}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    [self refreshUI];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [[DLAppFrameManager sharedInstance] updateApplicationFrame];
    [self updateFrame];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    FUNC_LOG
    self.tabBarView.delegate = nil;
}

#pragma mark - Public Methods

- (void)setTabBarViewIsHidden:(BOOL)isHidden {

    self.tabBarView.hidden = isHidden;
}

#pragma mark - Private Methods

- (void)configViewControllers {

    self.catalogViewController = [[DLNavigationController alloc] initWithRootViewController:[[DLCatalogViewController alloc] init]];
    self.forumRootViewController = [[DLNavigationController alloc] initWithRootViewController:[[DLForumRootViewController alloc] init]];
    self.liveViewController = [[DLNavigationController alloc] initWithRootViewController:[[DLLiveViewController alloc] init]];
    self.noticeViewController = [[DLNavigationController alloc] initWithRootViewController:[[DLNoticeViewController alloc] init]];
    self.myPageViewController = [[DLNavigationController alloc] initWithRootViewController:[[DLMyPageViewController alloc] init]];

    self.viewControllers = @[self.catalogViewController,
            self.forumRootViewController,
            self.liveViewController,
            self.noticeViewController,
            self.myPageViewController];
}

- (void)configTabBarView {

    self.tabBarView = [[DLTabBarView alloc] initWithNumberOfTabs:self.viewControllers.count
                                                    withStartTab:kStartTabIndex];
    self.tabBarView.delegate = self;
    [self.view addSubview:self.tabBarView];
}

- (void)updateFrame {
    
    self.tabBarView.frame = CGRectMake(0,
                                       CGRectGetHeight([DLAppFrameManager sharedInstance].applicationFrame) - DLConstTabBarHeight,
                                       CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame),
                                       DLConstTabBarHeight);
}

#pragma mark - DLTabBarViewDelegate

- (void)tabBarView:(DLTabBarView *)tabBarItemView didSelectIndex:(NSInteger)index {

    self.selectedIndex = index;
}

#pragma mark - Rotate Config

- (BOOL)shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if (IS_IPAD) {
        return UIInterfaceOrientationMaskAll;
    } else {
        return UIInterfaceOrientationMaskPortrait;
    }
}

@end
