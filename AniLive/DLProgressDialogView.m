#import "DLProgressDialogView.h"

@interface DLProgressDialogView ()
@property (nonatomic, strong) UIActivityIndicatorView *indicator;
@end

@implementation DLProgressDialogView
+ (instancetype) initView {
    DLProgressDialogView *view = [[DLProgressDialogView alloc] init];
    UIColor *color =  [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.3];;
    view.backgroundColor = color;
    view.frame = [[UIScreen mainScreen] bounds];
    view.indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    view.indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    view.indicator.center = view.center;
    [view addSubview:view.indicator];
    [view.indicator bringSubviewToFront:view];
    return view;
}

- (void) start {
    [self.indicator startAnimating];
}

- (void)dismiss {
    [self.indicator stopAnimating];
    [self removeFromSuperview];
}

@end