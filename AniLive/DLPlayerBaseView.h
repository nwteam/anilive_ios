//
//  DLPlayerBaseView.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/17.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLPlayerView.h"
#import "DLPlayerControlView.h"

typedef NS_ENUM(NSInteger, DLPlayerBaseViewType) {
    DLPlayerBaseViewTypeDisplay = 0,  // 通常再生
    DLPlayerBaseViewTypeAutoPlay      // 自動再生
};

@protocol DLPlayerBaseViewDelegate;

@class DLMovie, DLLive;
@interface DLPlayerBaseView : UIView

@property (nonatomic, strong) DLPlayerView *playerView;
@property (nonatomic, strong) DLPlayerControlView *playerControlView;
@property (readonly, nonatomic, assign) BOOL isPlaying;
@property (readonly, nonatomic, assign) BOOL isLoaded;
@property (nonatomic, weak) id<DLPlayerBaseViewDelegate> delegate;

- (instancetype)initWithMovieData:(DLMovie *)movieData withType:(DLPlayerBaseViewType)type;
- (instancetype)initWithLiveData:(DLLive *)liveData withType:(DLPlayerBaseViewType)type;
- (void)setPlayerMute:(BOOL)isMute;
- (void)setPlayerRepeat:(BOOL)isRepeat;
- (void)pause;
- (void)play;
- (void)startIndicator;
- (void)stopIndicator;
- (void)setPlayerViewHidden:(BOOL)isHidden withAnimated:(BOOL)animated;
- (void)seekToTime:(CMTime)time;

@end

@protocol DLPlayerBaseViewDelegate <NSObject>

@optional

- (void)playerBaseViewDidLoadedMovie:(DLPlayerBaseView *)playerBaseView;
- (void)playerBaseViewPlayerItemDidReachEnd;
- (void)playerBaseViewPlayerSwipeGestureLeft:(UISwipeGestureRecognizer *)gesture;
- (void)playerBaseViewPlayerSwipeGestureRight:(UISwipeGestureRecognizer *)gesture;

@end
