//
//  DLHighlightedButton.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/13.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DLHighlightedButtonDelegate;

@interface DLHighlightedButton : UIButton

@property (nonatomic, strong) UIColor *highlightedColor;
@property (nonatomic, strong) UIColor *nonHighlightedColor;
@property (nonatomic, weak) id<DLHighlightedButtonDelegate> delegate;
@property (nonatomic, assign) BOOL isNoNeedHighlight;

@end

@protocol DLHighlightedButtonDelegate <NSObject>

@optional
- (void)highlightedButtonHighlightChanged:(BOOL)highlighted;

@end
