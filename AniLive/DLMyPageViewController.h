//
//  DLMyPageViewController.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UnityAds/UnityAds.h>

@interface DLMyPageViewController : UIViewController <UnityAdsDelegate>

@end
