//
//  DLWorkDetailForumViewController.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/11/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLRetractableHeaderBaseViewController.h"
#import "DLSegmentViewControllerProtocol.h"

@interface DLWorkDetailForumViewController : DLRetractableHeaderBaseViewController <DLSegmentViewControllerProtocol>

- (instancetype)initWithHeaderClearCellHeight:(CGFloat)clearCellHeight  withWorkId:(NSNumber *)workId;

@end
