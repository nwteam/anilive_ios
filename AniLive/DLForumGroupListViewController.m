//
//  DLForumGroupListViewController.m
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/10/23.
//  Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLForumGroupListViewController.h"
#import "DLAPIHelper.h"
#import "DLForumGroup.h"
#import "DLForumCell.h"
#import "DLForumGroupCell.h"
#import "DLForumViewController.h"
#import "DLTransitionManager.h"

@interface DLForumGroupListViewController ()

@property(nonatomic, readwrite) NSMutableArray *forumGroups;
@property NSInteger pageNumber;
@end

@implementation DLForumGroupListViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        self.pageNumber = 1;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DLForumGroupCell" bundle:nil]
         forCellReuseIdentifier:@"forum-group-cell"];

    @weakify(self)
    [DLAPIHelper fetchForumsWithPageNumber:self.pageNumber SortType:self.sortType callback:^(NSMutableArray<DLForum *> *forums) {
       @strongify(self)
        self.forumGroups = forums;
        [self.tableView reloadData];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.forumGroups ? self.forumGroups.count : 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DLForumGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:@"forum-group-cell" forIndexPath:indexPath];
//    cell.group = self.forumGroups[(NSUInteger) indexPath.row];
    cell.froum = self.forumGroups[indexPath.row];
    cell.delegate = self;
//    if (!(tableView.dragging || tableView.decelerating)) {
//        [cell loadImage];
//    }
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 200;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    [DLTransitionManager showForumDetailViewControllerWithForum:self.forumGroups[indexPath.row] withTarget:nil];
}


#pragma mark - Scroll view delegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self loadImagesForOnscreenRows];
    float offset = (scrollView.contentOffset.y - (scrollView.contentSize.height - scrollView.frame.size.height));
    if (offset >= 0 && offset <= 5) {
        [self additionalLoad];
    }
}

#pragma mark - Private method

- (void)loadImagesForOnscreenRows {
    for (DLForumGroupCell *cell in self.tableView.visibleCells) {
        [cell loadImage];
    }
}

- (void)additionalLoad {
    @weakify(self)
    [DLAPIHelper fetchForumsWithPageNumber:++self.pageNumber SortType:self.sortType callback:^(NSMutableArray<DLForum *> *forumGroups) {
        @strongify(self)
        [self.forumGroups addObjectsFromArray:forumGroups];
        [self.tableView reloadData];
    }];
}

@end
