//
//  DLAlertViewResult.h
//  AniLive
//
//  Created by suisun on 1/13/17.
//  Copyright © 2017 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DLAlertViewResultDelegate <NSObject>
@optional
- (void)didTapButtonDLAlertViewResult:(NSInteger)index;
@end

@interface DLAlertViewResult : UIView <UITextFieldDelegate>

/** delegate */
@property (nonatomic, weak) id<DLAlertViewResultDelegate> delegate;
/** 確認メッセージラベル */

@property (weak, nonatomic) IBOutlet UIView *messageView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *msgLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblUserID;
@property (weak, nonatomic) IBOutlet UILabel *lblPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblStrUserID;
@property (weak, nonatomic) IBOutlet UILabel *lblStrPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnOK;

/** 初期化 */
- (id)initDLAlertViewResult;
- (IBAction)tapBtnDLAlertViewResult:(UIButton *)sender;

@end
