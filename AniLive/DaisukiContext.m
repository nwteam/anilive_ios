//
//  DaisukiContext.m
//  drmtest
//
//  Created by MacBookAir on 13/07/18.
//  Copyright (c) 2013年 agyou. All rights reserved.
//

#import "DLAppDelegate.h"
#import "DaisukiContext.h"
#import "XmlDic.h"
#import "M3U8Parser.h"
#undef SSLEAY_MACROS
#include <openssl/pem.h>
#include <openssl/x509v3.h>
#include <openssl/rsa.h>
#import <CommonCrypto/CommonDigest.h>
#include <ifaddrs.h>
#include <arpa/inet.h>


@interface DaisukiContext ()
{
	RSA*	_pubkey;	//	公開鍵
}

@end


@implementation DaisukiContext

//初期化と設定読み込み
- (void)loadSetting {
    self.hlsCache = nil;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *defaultsetting = @{
                           @"MOVIE_CLOSED_CAPTION" : @"en",
                           @"MOVIE_RENDITION": @"360P",
                           @"MOVIE_SERIES_PLAY": @(YES),
                           @"USER_ID": @"",
                           @"USER_PASSWORD": @"",
                           @"IS_FIRST_EXEC": @(YES),
                           @"IS_FIRST_HELP_TOP": @(YES),
                           @"IS_FIRST_HELP_MOVIE": @(YES),
                           @"IS_FIRST_HELP_MENU": @(YES),
                           @"SERIES_HASH": @""
                           };
    [defaults registerDefaults:defaultsetting];
    
    self.movieClosedCaptionLang = [defaults stringForKey:@"MOVIE_CLOSED_CAPTION"];
    self.movieRendition         = [defaults stringForKey:@"MOVIE_RENDITION"];
    self.autoSeriesPlayEnable   = [defaults boolForKey:@"MOVIE_SERIES_PLAY"];
    self.userID                 = [defaults stringForKey:@"USER_ID"];
    self.userPassword           = [defaults stringForKey:@"USER_PASSWORD"];
    self.isFirstExec            = [defaults boolForKey:@"IS_FIRST_EXEC"];
    self.isFirstHelpTop         = [defaults boolForKey:@"IS_FIRST_HELP_TOP"  ];
    self.isFirstHelpMovie       = [defaults boolForKey:@"IS_FIRST_HELP_MOVIE"];
    self.isFirstHelpMenu        = [defaults boolForKey:@"IS_FIRST_HELP_MENU" ];
    self.seriesHash             = [defaults stringForKey:@"SERIES_HASH" ];
    self.bannerCount = 0;
    self.lastError = DAISUKI_SUCCESS;
    
    _bgnCrypt = [[BGNCrypt alloc] init];
    [_bgnCrypt loadPublicKey];
    
    [self updateIPAddressCache];
}

- (void)saveSetting {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:_movieClosedCaptionLang forKey:@"MOVIE_CLOSED_CAPTION"];
    [defaults setObject:_movieRendition forKey:@"MOVIE_RENDITION"];
    [defaults setBool:_autoSeriesPlayEnable forKey:@"MOVIE_SERIES_PLAY"];
    [defaults setObject:_userID forKey:@"USER_ID"];
    [defaults setObject:_userPassword forKey:@"USER_PASSWORD"];
    [defaults setBool:_isFirstExec forKey:@"IS_FIRST_EXEC"];
    [defaults setBool:_isFirstHelpTop   forKey:@"IS_FIRST_HELP_TOP"  ];
    [defaults setBool:_isFirstHelpMovie forKey:@"IS_FIRST_HELP_MOVIE"];
    [defaults setBool:_isFirstHelpMenu  forKey:@"IS_FIRST_HELP_MENU" ];
    [defaults setObject:_seriesHash  forKey:@"SERIES_HASH" ];

    [defaults synchronize];
}



//ログイン
- (int)apiLogin:(NSString*)user password:(NSString*)password {
    self.isNeedToUpdateSeriesList = NO;
    self.lastError = -1;
    NSString* uri = [NSString
                     stringWithFormat:@"%@/api2/login/", BASE_URL];
    NSURL* url = [NSURL URLWithString:uri];

    NSMutableURLRequest* request = [[NSMutableURLRequest alloc]
                                    initWithURL:url
                                    cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    NSURLResponse*  response = nil;
    NSError*        error    = nil;
    NSData*         data     = nil;
    NSString*       myver    = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];

    NSString* params = [NSString
                        stringWithFormat:@"data[Customer][email]=%@&data[Customer][password]=%@&data[Customer][ClientVer]=%@", user, password, myver];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [request setValue:USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
    } else {
        [request setValue:USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
    }
    
    [request setHTTPMethod:@"POST"];	//メソッドをPOSTに指定します
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    data = [NSURLConnection sendSynchronousRequest:request
                                 returningResponse:&response error:&error];
    //Debug
    NSString *str= [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"%@", str);
    
    if (!data) {
        self.lastError = DAISUKI_NETWORK_ERROR;
        return DAISUKI_NETWORK_ERROR;
    }
    
    XmlDic* xml = [[XmlDic alloc]init];
    [xml parseWithData:data];
    
	NSString* result = [xml first:@"result"];
    if ([result isEqualToString:@"1"]) {
        self.userID = user;
        self.userPassword = password;
        self.memberID   = [xml first:@"uid" ];
        self.memberName = [xml first:@"name"];
        self.appVersion = [xml first:@"ipadver"];
        self.country    = [xml first:@"country"];
        self.bannerCount  = [xml first:@"banner"].intValue;
        NSString* seriesHash = [xml first:@"serieshash"];

        //	バージョンチェック
        NSString *myver = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        NSString* newestver = self.appVersion;

        if( [myver compare:newestver] != NSOrderedAscending)
        {
            _isNeedUpdate = NO;
        }
        else
        {
            _isNeedUpdate = YES;
            self.lastError = DAISUKI_READY_NEW_VER;

            return DAISUKI_READY_NEW_VER;
        }
        
        //トップのシリーズリストの更新が必要か？（バックグラウンドからの復帰時に仕様）
        if (![seriesHash isEqualToString:self.seriesHash]) {
            self.isNeedToUpdateSeriesList = YES;
        }

        self.lastError = DAISUKI_SUCCESS;
        
        return DAISUKI_SUCCESS;
    } else {
        NSString* err = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSRange isMaintenance = [err rangeOfString:@"Maintenance"];
    
        //	メンテナンス？
        if( isMaintenance.location!=NSNotFound ) {
            self.lastError = DAISUKI_MAINTENANCE;

            return DAISUKI_MAINTENANCE;
        } else {
            self.lastError = DAISUKI_IDPW_ERROR;
            return DAISUKI_IDPW_ERROR;
        }
    }
 
    self.lastError = DAISUKI_UNKNOWN_ERROR;
    return DAISUKI_UNKNOWN_ERROR;
}

//ログアウト
- (BOOL)apiLogout {
    NSString* uri = [NSString
                     stringWithFormat:@"%@/api2/logout", BASE_URL];
    NSURL* url = [NSURL URLWithString:uri];
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc]
                                    initWithURL:url
                                    cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [request setValue:USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
    } else {
        [request setValue:USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
    }

    NSURLResponse*  response = nil;
    NSError*        error    = nil;
    NSData*         data = nil;
    data = [NSURLConnection sendSynchronousRequest:request
                                 returningResponse:&response error:&error];

    if (!data) return NO;
    return YES;
}


//トップ
- (BOOL)apiTop {
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return [self apiTop_iPhone];
    } else {
        return [self apiTop_iPad];
    }
}


//トップ
- (BOOL)apiTop_iPhone {
    //	サーバの XML データの取得
    NSString* uri = [NSString stringWithFormat:@"%@/api2/top", BASE_URL];
    NSURL*    url = [NSURL URLWithString:uri];
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc]
    	initWithURL:url
        cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [request setValue:USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
    } else {
        [request setValue:USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
    }
    
    NSURLResponse* response = nil;
    NSError*       error    = nil;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:request
        returningResponse:&response error:&error];
    
    if( !data )
    {
        return NO;
    }
    
    //	画像 DL リストの取得
    XmlDic* dic = [[XmlDic alloc] init];
    [dic parseWithData:data];
        
    _seriesImages = nil;
    _seriesOriginImages = nil;
    _seriesOrigin = nil;
    _seriesTitles = nil;
    
    NSString* seriesHash = [dic first:@"serieshash"];
    if (!seriesHash || [seriesHash isEqualToString:@""]) {
        return NO;
    }
    self.seriesHash = seriesHash;
    self.isNeedToUpdateSeriesList = NO;
    
    _seriesOrigin = _seriesTitles = [dic find:@"series"];
    _seriesOriginImages = _seriesImages = [NSMutableArray array];
    
    //	画像の取得
    NSMutableArray* dstlist = [NSMutableArray array];
    
    static int i = 0;
    for( XmlDic* series in _seriesOrigin )
    {
        //	ぐるぐるの％ (表示系はメインスレッド)
        dispatch_async(dispatch_get_main_queue(), ^{
//            [APP_LOGIN() updateGuruGuru:i++ max:(int)_seriesOrigin.count];
        });
        
        //	画像ある？
        NSString* name  = [series first:@"series_id"];
        NSArray*  paths = NSSearchPathForDirectoriesInDomains( NSCachesDirectory, NSUserDomainMask, YES );
        NSString* dir   = [paths objectAtIndex:0];
        NSString* fname = [NSString stringWithFormat:@"%@/%@.jpg", dir, name];
        NSData*   data  = [NSData dataWithContentsOfFile:fname];
        
        //	画像のダウンロード？
        if( nil == data )
        {
            uri  = [NSString stringWithFormat:@"%@/img/series/%@/640_1136_iphone.jpg", AKAMAI_URL, name];
            url  = [NSURL URLWithString:uri];
            data = [NSData dataWithContentsOfURL:url];
            if( nil != data )
            {
	            [data writeToFile:fname atomically:YES];
            }
        }
        
        //	画像の準備
        UIImage* imageSRC = [UIImage imageWithData:data];
        if( nil == imageSRC ) {
            imageSRC = [UIImage imageNamed:@"background.png"];
        }
        
        if([[UIScreen mainScreen] scale] == 2.0f) {
            [_seriesImages addObject:imageSRC];
        } else {
            // 取得した画像の縦サイズ、横サイズを取得する
            int imageW = imageSRC.size.width;
            int imageH = imageSRC.size.height;
            
            float scale = 0.5;
            CGSize resizedSize = CGSizeMake(imageW * scale, imageH * scale);
            UIGraphicsBeginImageContext(resizedSize);
            [imageSRC drawInRect:CGRectMake(0, 0, resizedSize.width, resizedSize.height)];
            UIImage* resizedImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            [_seriesImages addObject:resizedImage];
        }
        
        //	画像ある？
        fname = [NSString stringWithFormat:@"%@/%@_m.jpg", dir, name];
        data  = [NSData dataWithContentsOfFile:fname];
        
        //	画像のダウンロード？
        if( nil == data )
        {
            uri  = [NSString stringWithFormat:@"%@/img/series/%@/640_1136_iphone.jpg", AKAMAI_URL, name];
            url  = [NSURL URLWithString:uri];
            data = [NSData dataWithContentsOfURL:url];
            
            if( nil != data )
            {
                [data writeToFile:fname atomically:YES];
            }
        }
        
        //	DL リストの更新
        if( nil != data )
        {
	        [dstlist addObject:name];
        }
    }
    
    //	ぐるぐるの％ (表示系はメインスレッド)
    dispatch_async(dispatch_get_main_queue(), ^{
//        [APP_LOGIN() updateGuruGuru:99 max:100];
    });
    
    //	画像の削除判定
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray* srclist  = [[defaults objectForKey:@"SeriesImageList"] mutableCopy];
    
    if( nil == srclist )
    {
        srclist = [NSMutableArray array];
    }
    
    for( NSString* srcname in srclist )
    {
        BOOL isexist = NO;
        
        for( NSString* dstname in dstlist )
        {
            if( [srcname isEqualToString:dstname] )
            {
                isexist = YES;
                break;
            }
        }
        
        //	画像の削除
        if( !isexist )
        {
            NSArray*  paths = NSSearchPathForDirectoriesInDomains( NSCachesDirectory, NSUserDomainMask, YES );
            NSString* dir   = [paths objectAtIndex:0];
            NSString* fname = [NSString stringWithFormat:@"%@/%@.jpg", dir, srcname];
            NSFileManager* files = [[NSFileManager alloc] init];
            [files removeItemAtPath:fname error:&error];
            paths = NSSearchPathForDirectoriesInDomains( NSCachesDirectory, NSUserDomainMask, YES );
            dir   = [paths objectAtIndex:0];
            fname = [NSString stringWithFormat:@"%@/movie/%@.jpg", dir, srcname];
            [files removeItemAtPath:fname error:&error];
        }
    }
    
    [defaults setObject:[NSArray arrayWithArray:dstlist] forKey:@"SeriesImageList"];
    [defaults synchronize];
    [self switchSimulcast:NO];
    
    return YES;
}


//トップ
- (BOOL)apiTop_iPad {
    //	サーバの XML データの取得
    NSString* uri = [NSString stringWithFormat:@"%@/api2/top", BASE_URL];
    NSURL*    url = [NSURL URLWithString:uri];
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc]
    	initWithURL:url
    	cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [request setValue:USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
    } else {
        [request setValue:USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
    }
    
    NSURLResponse* response = nil;
    NSError*       error    = nil;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:request
    	returningResponse:&response error:&error];
    
    //Debug
    //NSString *str= [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //NSLog(@"%@", str);

    if( !data )
    {
        return NO;
    }
    
    //	画像 DL リストの取得
    XmlDic* dic = [[XmlDic alloc] init];
    [dic parseWithData:data];

    _seriesImages = nil;
    _seriesOriginImages = nil;
    _seriesOrigin = nil;
    _seriesTitles = nil;
    
    NSString* seriesHash = [dic first:@"serieshash"];
    if (!seriesHash || [seriesHash isEqualToString:@""]) {
        return NO;
    }
    self.seriesHash = seriesHash;
    self.isNeedToUpdateSeriesList = NO;


    _seriesOrigin = _seriesTitles = [dic find:@"series"];
    _seriesOriginImages = _seriesImages = [NSMutableArray array];

    //	画像の取得
    NSMutableArray* dstlist = [NSMutableArray array];

    static int i = 0;
    for( XmlDic* series in _seriesOrigin )
    {
        //	ぐるぐるの％ (表示系はメインスレッド)
        dispatch_async(dispatch_get_main_queue(), ^{
//            [APP_LOGIN() updateGuruGuru:i++ max:(int)_seriesOrigin.count];
        });

        //	画像ある？
        NSString* name  = [series first:@"series_id"];
        NSArray*  paths = NSSearchPathForDirectoriesInDomains( NSCachesDirectory, NSUserDomainMask, YES );
        NSString* dir   = [paths objectAtIndex:0];
        NSString* fname = [NSString stringWithFormat:@"%@/%@.jpg", dir, name];
        NSData*   data  = [NSData dataWithContentsOfFile:fname];
        
        //	画像のダウンロード？
        if( nil == data )
        {
            uri  = [NSString stringWithFormat:@"%@/img/series/%@/2048_1536_top.jpg", AKAMAI_URL, name];
            url  = [NSURL URLWithString:uri];
            data = [NSData dataWithContentsOfURL:url];
            [data writeToFile:fname atomically:YES];
        }
        
        //	画像の準備
        UIImage* imageSRC = [UIImage imageWithData:data];
        if( nil == imageSRC ) {
            imageSRC = [UIImage imageNamed:@"background.png"];
        }
        
        if([[UIScreen mainScreen] scale] == 2.0f) {
            [_seriesImages addObject:imageSRC];
        } else {
            // 取得した画像の縦サイズ、横サイズを取得する
            int imageW = imageSRC.size.width;
            int imageH = imageSRC.size.height;
            
            float scale = 0.5;
            CGSize resizedSize = CGSizeMake(imageW * scale, imageH * scale);
            UIGraphicsBeginImageContext(resizedSize);
            [imageSRC drawInRect:CGRectMake(0, 0, resizedSize.width, resizedSize.height)];
            UIImage* resizedImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            [_seriesImages addObject:resizedImage];
        }
        
        //	画像ある？
        fname = [NSString stringWithFormat:@"%@/%@_m.jpg", dir, name];
        data  = [NSData dataWithContentsOfFile:fname];
        
        //	画像のダウンロード？
        if( nil == data )
        {
            uri  = [NSString stringWithFormat:@"%@/img/series/%@/2048_1536_movie.jpg", AKAMAI_URL, name];
            url  = [NSURL URLWithString:uri];
            data = [NSData dataWithContentsOfURL:url];
            [data writeToFile:fname atomically:YES];
        }
        
        
        //	DL リストの更新
        [dstlist addObject:name];
    }

    //	ぐるぐるの％ (表示系はメインスレッド)
    dispatch_async(dispatch_get_main_queue(), ^{
//        [APP_LOGIN() updateGuruGuru:99 max:100];
    });

    //	画像の削除判定
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray* srclist  = [[defaults objectForKey:@"SeriesImageList"] mutableCopy];

    if( nil == srclist )
    {
        srclist = [NSMutableArray array];
    }
    
    for( NSString* srcname in srclist )
    {
        BOOL isexist = NO;
        
        for( NSString* dstname in dstlist )
        {
            if( [srcname isEqualToString:dstname] )
            {
                isexist = YES;
                break;
            }
        }
        
        //	画像の削除
        if( !isexist )
        {
            NSArray*  paths = NSSearchPathForDirectoriesInDomains( NSCachesDirectory, NSUserDomainMask, YES );
            NSString* dir   = [paths objectAtIndex:0];
            NSString* fname = [NSString stringWithFormat:@"%@/%@.jpg", dir, srcname];
            NSFileManager* files = [[NSFileManager alloc] init];
            [files removeItemAtPath:fname error:&error];
            paths = NSSearchPathForDirectoriesInDomains( NSCachesDirectory, NSUserDomainMask, YES );
            dir   = [paths objectAtIndex:0];
            fname = [NSString stringWithFormat:@"%@/movie/%@.jpg", dir, srcname];
            [files removeItemAtPath:fname error:&error];
        }
    }
    
    [defaults setObject:[NSArray arrayWithArray:dstlist] forKey:@"SeriesImageList"];
    [defaults synchronize];
    [self switchSimulcast:NO];
    
    return YES;
}

// トークンリクエスト
- (NSString*)apiRequestToken {
    NSString* ret = @"";
    NSString* uri = [NSString
                     stringWithFormat:@"%@/api2/requestToken", BASE_URL];
    
    NSURL* url    = [NSURL URLWithString:uri];
    NSMutableURLRequest* rrequest = [[NSMutableURLRequest alloc]
                                     initWithURL:url
                                     cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [rrequest setValue:USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
    } else {
        [rrequest setValue:USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
    }
    
    NSURLResponse* response = nil;
    NSError*       error    = nil;
    
    NSData* data = [NSURLConnection
                    sendSynchronousRequest:rrequest
                    returningResponse:&response error:&error];
    if (!data) return NO;
    
	
	ret = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSRange isError = [ret rangeOfString:@"ERROR"];
    if (isError.location != NSNotFound) {
        NSLog(@"apiRequestToken: %@",ret);
        ret = @"";
    }
    
    return ret;
}

//シリーズ詳細情報取得
- (XmlDic*)apiSeriesDetail:(NSString*)series_id {
    NSString* uri  = [NSString
                      stringWithFormat:@"%@/api2/seriesdetail/%@", BASE_URL, series_id];
    
    XmlDic* xml = [[XmlDic alloc]init];
    BOOL r = [xml parseWithURI:uri];
    if (!r) {
        self.lastError = DAISUKI_NETWORK_ERROR;
        return nil;
    }
    
    return xml;
}

//シリーズ詳細情報取得
- (XmlDic*)apiProductDetail:(NSString*)product_id {
    NSString* uri  = [NSString
                      stringWithFormat:@"%@/api2/productdetail/%@", BASE_URL, product_id];
    
    XmlDic* xml = [[XmlDic alloc]init];
    BOOL r = [xml parseWithURI:uri];
    if (!r) {
        self.lastError = DAISUKI_NETWORK_ERROR;
        return nil;
    }
    
    return xml;
}


//	全シリーズを検索する
- (XmlDic*)apiAllTitles {
    //	サーバの XML データの取得
    NSString* uri = [NSString stringWithFormat:@"%@/api2/search/mode:1/limit:0", BASE_URL];
    NSURL*    url = [NSURL URLWithString:uri];
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc]
    	initWithURL:url
    	cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [request setValue:USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
    } else {
        [request setValue:USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
    }
    
    NSURLResponse* response = nil;
    NSError*       error    = nil;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:request
    	returningResponse:&response error:&error];
    
    if( !data )
    {
        return nil;
    }
	
    XmlDic* dic = [[XmlDic alloc] init];
    [dic parseWithData:data];
    
    return dic;
}


//	検索ワードを取得する
- (BOOL)apiGetSearchWords {
    //	サーバの XML データの取得
    NSString* uri = [NSString stringWithFormat:@"%@/api2/studios", BASE_URL];
    NSURL*    url = [NSURL URLWithString:uri];
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc]
    	initWithURL:url
        cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [request setValue:USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
    } else {
        [request setValue:USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
    }
    
    NSURLResponse* response = nil;
    NSError*       error    = nil;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:request
    	returningResponse:&response error:&error];
    
    if( !data )
    {
        return NO;
    }
	
    XmlDic* dic = [[XmlDic alloc] init];
    [dic parseWithData:data];
    
    //	スタジオの検索ワードの設定
    _searchWordStdios = [dic find:@"item"];
    
    //	サーバの XML データの取得
    uri = [NSString stringWithFormat:@"%@/api2/genre", BASE_URL];
    url = [NSURL URLWithString:uri];
    
    request = [[NSMutableURLRequest alloc]
    	initWithURL:url
    	cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [request setValue:USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
    } else {
        [request setValue:USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
    }
    
    response = nil;
    error    = nil;
    
    data = [NSURLConnection sendSynchronousRequest:request
    	returningResponse:&response error:&error];
    
    if( !data )
    {
        return NO;
    }
	
    dic = [[XmlDic alloc] init];
    [dic parseWithData:data];
    
    //	ジャンルの検索ワードの設定
    _searchWordCategories = [dic find:@"item"];
    
    return YES;
}


//	製品を検索する
- (BOOL)apiSearch:(NSString*)mode
            stdio:(NSString*)stdio
            genre:(NSString*)genre
          keyword:(NSString*)keyword
             page:(int)page {
    //	検索 API の作成
    NSMutableString* uri = [NSMutableString stringWithFormat:@"%@/api2/search/mode:%@/limit:0", BASE_URL, mode];
    
    if( nil != stdio )
    {
        [uri appendString:@"/studio_id:"];
        [uri appendString:stdio];
    }
    
    if( nil != genre )
    {
        [uri appendString:@"/genre:"];
        [uri appendString:genre];
    }
    
    if( nil != keyword )
    {
        [uri appendString:@"/keyword:"];
        [uri appendString:[self urlEncode:keyword]];
    }
    
    if( 0 <= page )
    {
        [uri appendFormat:@"/page:%d", page];
    }
    
    //	サーバの XML データの取得
    NSURL* url = [NSURL URLWithString:uri];
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc]
    	initWithURL:url
        cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [request setValue:USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
    } else {
        [request setValue:USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
    }
    
    NSURLResponse* response = nil;
    NSError*       error    = nil;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:request
    	returningResponse:&response error:&error];
    
    if( !data )
    {
        return NO;
    }
	
    XmlDic* dic = [[XmlDic alloc] init];
    [dic parseWithData:data];
    
    _searchSeries =  [dic find:@"item"];
    
    return YES;
}

//動画視聴履歴
- (XmlDic*)apiVideoHistory {
    NSString* uri  = [NSString
                      stringWithFormat:@"%@/api2/video_history", BASE_URL];
    
    XmlDic* xml = [[XmlDic alloc]init];
    BOOL r = [xml parseWithURI:uri];
    if (!r) {
        self.lastError = DAISUKI_NETWORK_ERROR;
        return nil;
    }
    
    return xml;
}

//動画購入履歴
- (XmlDic*)apiVideoPurchase {
    NSString* uri  = [NSString
                      stringWithFormat:@"%@/api2/video_history_purchased", BASE_URL];
    
    XmlDic* xml = [[XmlDic alloc]init];
    BOOL r = [xml parseWithURI:uri];
    if (!r) {
        self.lastError = DAISUKI_NETWORK_ERROR;
        return nil;
    }
    
    return xml;
}


//ムービー取得の初期化(プロパティ取得後に行うこと)
/*
- (int)initializeMovieLicense : (NSString*)product_id movie_id:(NSString*)movie_id level:(NSString*)level;
{
    self.productID = product_id;
    self.movieID = movie_id;
    
    self.hlsCache = nil;
    self.hlsCache = [[HLSCache alloc] init];

    //証明書の取得とデコード
    int r = [self apiGetLicense:product_id movie_id:movie_id];
    if (r != DAISUKI_SUCCESS) {
        NSLog(@"failed to get license");
        self.lastError = r;
        return r;
    }
    
    //キーの数の取得
    _moviekeys = [NSMutableArray array];
    XmlDic* keycountselem =[self.movieProperty firstElement:@"keycounts"];
    if (!keycountselem) {
        NSLog(@"failed to get keycounts from propery");
        self.lastError = DAISUKI_MOVIE_FORMAT_ERROR;
        return DAISUKI_MOVIE_FORMAT_ERROR;
    }
    XmlDic* keycountelem = [keycountselem firstElement:[NSString stringWithFormat:@"rendition%@",level]];
    if (!keycountelem) {
        NSLog(@"failed to get keycount for the rendition:%@", level);
        self.lastError = DAISUKI_MOVIE_FORMAT_ERROR;
        return DAISUKI_MOVIE_FORMAT_ERROR;
    }
    int keyCount = [keycountelem.value intValue];
    if (keyCount < 0) {
        NSLog(@"failed to get keycount");
        self.lastError = DAISUKI_MOVIE_FORMAT_ERROR;
        return DAISUKI_MOVIE_FORMAT_ERROR;
    }
    
    
    //キーの取得
    NSData* keys = [self apiGetAllKeys:product_id movie_id:movie_id level:level];
    if (!keys) {
        NSLog(@"Error in getting key1");
        self.lastError = DAISUKI_MOVIE_FORMAT_ERROR;
        return DAISUKI_MOVIE_FORMAT_ERROR;
    }
    if (keys.length < keyCount * 128) {
        NSLog(@"Error in getting key2");
        NSLog(@"KeyLen: %d  NeedLen:%d", (int)keys.length, keyCount * 128);
        NSString *str= [[NSString alloc] initWithData:keys encoding:NSUTF8StringEncoding];
        NSLog(@"%@", str);
        self.lastError = DAISUKI_MOVIE_FORMAT_ERROR;
        return DAISUKI_MOVIE_FORMAT_ERROR;
    }
    
    //キーの分解
    for( int i = 0; i < keyCount; i++ )
    {
        NSRange range = NSMakeRange(i * 128, 128);
        NSData* key = [keys subdataWithRange:range];
        int outlen;
        unsigned char outbuf[20];
                
        if((outlen = RSA_public_decrypt((int)key.length, [key bytes], outbuf,
                                        _pubkey, RSA_PKCS1_PADDING)) == -1)
        {
            self.lastError = DAISUKI_MOVIE_FORMAT_ERROR;
            return DAISUKI_MOVIE_FORMAT_ERROR;
        }
        
        NSData* keydata = [NSData dataWithBytes:outbuf length:outlen];
        [_moviekeys addObject:keydata];
    }
        
    self.lastError = DAISUKI_SUCCESS;
    return DAISUKI_SUCCESS;
}
 */
/*
- (int)getDecodedKeyCount
{
    return (int)_moviekeys.count;
}
*/

/*
- (NSData*)getDecodedKey :(int) keyNo
{
    return [_moviekeys objectAtIndex:keyNo];
}
*/

//	字幕を読み込んで初期設定する
- (void)loadCaption {
	//	字幕のクリア
	self.captions = nil;
	self.captions= [[XmlDic alloc] init];

    //	字幕ある？
	NSString* uri = self.captionURL;
	if( nil == uri || [uri isEqualToString:@""] )
	{
		return;
	}

    //字幕DXFP取得
    NSURL* url = [NSURL URLWithString:uri];

    NSMutableURLRequest* request = [[NSMutableURLRequest alloc]
               initWithURL:url
               cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [request setValue:USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
    } else {
        [request setValue:USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
    }
    
    NSURLResponse* response = nil;
    NSError* error    = nil;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:request
                                 returningResponse:&response error:&error];
    NSString *tmp= [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
	//  パースの開始 (<br /> は改行に変換)
    tmp  = [tmp stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    tmp  = [tmp stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    tmp  = [tmp stringByReplacingOccurrencesOfString:@"<br />" withString:@"--BRTAG--\n"];
    tmp  = [tmp stringByReplacingOccurrencesOfString:@"<BR />" withString:@"--BRTAG--\n"];
    tmp  = [tmp stringByReplacingOccurrencesOfString:@"<br>" withString:@"--BRTAG--\n"];
    tmp  = [tmp stringByReplacingOccurrencesOfString:@"<BR>" withString:@"--BRTAG--\n"];
    tmp  = [tmp stringByReplacingOccurrencesOfString:@">" withString:@">\n"];
    
    //行単位に分解
    NSArray *lines = [tmp componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    tmp = [NSString stringWithFormat:@""];
    NSMutableString* mtmp = [NSMutableString stringWithFormat:@""];
    for (NSUInteger i = 0; i < lines.count; i++) {
        [mtmp appendString:[lines[i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    }
    
    NSData* data2 = [mtmp dataUsingEncoding:NSUTF8StringEncoding];
	[self.captions parseWithData:data2];
}

//	表示シリーズタイトルを切り替える
- (void)switchSimulcast:(BOOL)onoff {
    _seriesTitles = [NSMutableArray array];
    _seriesImages = [NSMutableArray array];
    
    //	16 以上まで補填
    while( 16 >= _seriesTitles.count )
    {
        for( int i = 0; i < _seriesOrigin.count; i++ )
        {
            XmlDic* series = _seriesOrigin[i];
            
            if( [[series first:@"series_id"] isEqualToString:@"15"] )
            {
                continue;
            }
            
            if( !onoff || [[series first:@"simulcast"] isEqualToString:@"1"] )
            {
                [_seriesTitles addObject:series];
                [_seriesImages addObject:_seriesOriginImages[i]];
            }
        }
    }
}

//MD5変換
- (NSString *)md5:(NSString*)str {
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5( cStr, (unsigned int)strlen(cStr), result ); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

//現在時間をミリ秒で取得
-(double)getCurrentTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddHHmmssSSS"];
    NSDate* now_date = [NSDate date];
    NSString *now_dateStr = [formatter stringFromDate:now_date];
    double now  = [now_dateStr doubleValue];
    return now;
}

- (NSString *)stringByDecodingXMLEntities:(NSString*)src {
    NSUInteger myLength = [src length];
    NSUInteger ampIndex = [src rangeOfString:@"&" options:NSLiteralSearch].location;
    
    // Short-circuit if there are no ampersands.
    if (ampIndex == NSNotFound) {
        return src;
    }
    // Make result string with some extra capacity.
    NSMutableString *result = [NSMutableString stringWithCapacity:(myLength * 1.25)];
    
    // First iteration doesn't need to scan to & since we did that already, but for code simplicity's sake we'll do it again with the scanner.
    NSScanner *scanner = [NSScanner scannerWithString:src];
    do {
        // Scan up to the next entity or the end of the string.
        NSString *nonEntityString;
        if ([scanner scanUpToString:@"&" intoString:&nonEntityString]) {
            [result appendString:nonEntityString];
        }
        if ([scanner isAtEnd]) {
            goto finish;
        }
        // Scan either a HTML or numeric character entity reference.
        if ([scanner scanString:@"&amp;" intoString:NULL])
            [result appendString:@"&"];
        else if ([scanner scanString:@"&apos;" intoString:NULL])
            [result appendString:@"'"];
        else if ([scanner scanString:@"&quot;" intoString:NULL])
            [result appendString:@"\""];
        else if ([scanner scanString:@"&lt;" intoString:NULL])
            [result appendString:@"<"];
        else if ([scanner scanString:@"&gt;" intoString:NULL])
            [result appendString:@">"];
        else if ([scanner scanString:@"&#" intoString:NULL]) {
            BOOL gotNumber;
            unsigned charCode;
            NSString *xForHex = @"";
            
            // Is it hex or decimal?
            if ([scanner scanString:@"x" intoString:&xForHex]) {
                gotNumber = [scanner scanHexInt:&charCode];
            }
            else {
                gotNumber = [scanner scanInt:(int*)&charCode];
            }
            if (gotNumber) {
                [result appendFormat:@"%c", charCode];
            }
            else {
                NSString *unknownEntity = @"";
                [scanner scanUpToString:@";" intoString:&unknownEntity];
                [result appendFormat:@"&#%@%@;", xForHex, unknownEntity];
                NSLog(@"Expected numeric character entity but got &#%@%@;", xForHex, unknownEntity);
            }
            [scanner scanString:@";" intoString:NULL];
        }
        else {
            NSString *unknownEntity = @"";
            [scanner scanUpToString:@";" intoString:&unknownEntity];
            NSString *semicolon = @"";
            [scanner scanString:@";" intoString:&semicolon];
            [result appendFormat:@"%@%@", unknownEntity, semicolon];
            NSLog(@"Unsupported XML character entity %@%@", unknownEntity, semicolon);
        }
    }
    while (![scanner isAtEnd]);
    
finish:
    return result;
}

//	URLDecode
- (NSString *)urlDecode:(NSString*)uri {
    NSString *result = [(NSString *)uri stringByReplacingOccurrencesOfString:@"+" withString:@" "];
    result = [result stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return result;
}

//	URLEncode
- (NSString *)urlEncode:(NSString*)uri {
    NSString *result = (NSString*)CFBridgingRelease( CFURLCreateStringByAddingPercentEscapes(
    	kCFAllocatorDefault,
        (CFStringRef)uri,
        NULL,
        (CFStringRef)@"!*'();:@&=+$,/?%#[]",
        kCFStringEncodingUTF8 ) );
    return result;
}

// BGN仕様対応
- (int)apiBGNInit:(NSString*)movie_product_id {
    NSLog(@"BGNINIT");

    NSString* uri = [NSString
                     stringWithFormat:@"%@/fastAPI/bgn/init_ios/", BASE_URL];
    NSURL* url = [NSURL URLWithString:uri];
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc]
                                    initWithURL:url
                                    cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    NSURLResponse*  response = nil;
    NSError*        error    = nil;
    NSData*         data = nil;
    
    
    //送信用JSONパラメーター
    NSString* json = [NSString
                      stringWithFormat:
                      @"{\"ss_id\":\"%@\", \"mv_id\":\"0\",\"device_cd\":\"2\", \"ss1_prm\":\"%@\", \"ss2_prm\":\"%@\", \"ss3_prm\":\"\"}",
                      BGN_SS_ID,
                      self.memberID,
                      movie_product_id
                    ];
    NSString* encryptedJSON = [_bgnCrypt encryptByAES:json];
    
    //パラメーター設定
    NSString* params = [NSString
                        stringWithFormat:@"s=%@&a=%@&d=%@&e=iOS&c=UN&o=0",
                        BGN_SS_ID_HASH,
                        [self urlEncode:_bgnCrypt.encryptedAesKey],
                        [self urlEncode:encryptedJSON]];

    
    //HTTPでAPI問い合わせ
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [request setValue:USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
    } else {
        [request setValue:USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
    }
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    data = [NSURLConnection sendSynchronousRequest:request
                                 returningResponse:&response error:&error];
    
    NSString* responseStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"%@", responseStr);

    //返却内容をJSONでコード
    NSDictionary *resultJson = [NSJSONSerialization JSONObjectWithData: data options:NSJSONReadingAllowFragments error:&error];

    if (resultJson.count <= 0) {
        //JSONエラー
        NSLog(@"Error0");
        //NSLog(@"%@", responseStr);
        return DAISUKI_NETWORK_ERROR;
    }
    if (![[resultJson objectForKey:@"rcd"] isEqualToString:@"00"]) {
        NSLog(@"Error01");
        //NSLog(@"%@", responseStr);
        return DAISUKI_NETWORK_ERROR;
    }
    
    //暗号解読
    NSString* decrypted = [_bgnCrypt decryptByAES:[resultJson objectForKey:@"rtn"]];
    //NSLog(@"%@", decrypted);
    
    //文字列をトリミングしてからJSONをパース
    decrypted = [decrypted stringByTrimmingCharactersInSet:[NSCharacterSet controlCharacterSet]];
    NSData *decryptedData = [decrypted dataUsingEncoding:NSUTF8StringEncoding];
    if (!decryptedData) {
        NSLog(@"Error1");
        return DAISUKI_NETWORK_ERROR;
    }
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData: decryptedData options:NSJSONReadingAllowFragments error:&error];

    if (jsonDictionary.count <= 0) {
        //JSONエラー
        NSLog(@"Error2");
        return DAISUKI_NETWORK_ERROR;
    }
    
    
    {//動画のプロパティを保存
        NSDictionary* property = [jsonDictionary objectForKey:@"ss_ext"];
        
        _seriesTitle  = [property objectForKey:@"series"];
        _productTitle = [property objectForKey:@"product"];
        _adMovieName = [jsonDictionary objectForKey:@"series_id"];
        _subscriptionStatus = [jsonDictionary objectForKey:@"subscription_status"];
        _movieProductId = [jsonDictionary objectForKey:@"movie_product_id"];
        _captionURL   = [jsonDictionary objectForKey:@"caption_url"];
        _fov          = [property objectForKey:@"fov"];
        _vastURL      = [property objectForKey:@"vast"];
        
        NSDictionary* prevInfo = [property objectForKey:@"prev"];
        _prevProductID = [prevInfo objectForKey:@"movie_product_id"];

        NSDictionary* nextInfo = [property objectForKey:@"next"];
        _nextProductID = [nextInfo objectForKey:@"movie_product_id"];

        //広告挿入ポイントの取得
        if (self.movieAdCueuePoint) {
            [self.movieAdCueuePoint removeAllObjects];
            self.movieAdCueuePoint = nil;
        }
        self.movieAdCueuePoint = [NSMutableArray array];
        NSArray* ad = [jsonDictionary objectForKey:@"adque_msec" ];
        if (ad) {
            for (int i = 0; i < ad.count; i++) {
                [self.movieAdCueuePoint addObject: ad[i]];
            }
        }
        
        //字幕の言語の取得
        NSArray* captions = [jsonDictionary objectForKey:@"caption_lang"];
        _captionLangs = [NSMutableArray array];
        for (int i = 0; i < captions.count; i++) {
            NSDictionary* dic = captions[i];
            for (id key in [dic allKeys]) {
                [_captionLangs addObject:dic[key]];
            }
        }

        //レンディション用のラベル取得
        NSArray* rwlabels = [jsonDictionary objectForKey:@"bw_label"];
        _renditionLabels = [NSMutableArray array];
        for (int i = 0; i < rwlabels.count ; i++) {
            [_renditionLabels addObject:rwlabels[i]];
        }
    }
    
    {//M3U8を解析して、各種レンディションのm3u8を保存
        M3U8Parser* parser = [[M3U8Parser alloc] init];

        NSString* play_url = [jsonDictionary objectForKey:@"play_url"];
        self.playURL = play_url;
        
        if ([parser parseWithURI:play_url daisukiConvert:YES]) {
            if (parser.children.count > 0) {
                //パースされた子m3u8のURLをcontextでストック
                _m3u8URLs = [NSMutableArray array];
                for (int i = 0; i < parser.children.count; i++) {
                    [_m3u8URLs addObject:parser.children[i]];
                }
                _m3u8Rendition = [NSMutableArray array];
                for (int i = 0; i < parser.lengths.count; i++) {
                    [_m3u8Rendition addObject:parser.lengths[i]];
                }
            } else {
                NSLog(@"Error4");
                return DAISUKI_AKAMAI_ERROR;
            }
        } else {
            NSLog(@"Error5");
            return DAISUKI_AKAMAI_ERROR;
        }
    }
        
    //字幕の読みこみ
    [self loadCaption];
    
    //HLSキャッシュの初期化
    self.hlsCache = nil;
    self.hlsCache = [[HLSCache alloc] init];

    //視聴ログ
    _bgnSessionID = [NSString stringWithString:[jsonDictionary objectForKey:@"init_id"]];
    _bgnLogURL    = [NSString stringWithString:[jsonDictionary objectForKey:@"vl_url"]];
    _bgnLogCount  = 1;
    _bgnPlayStartTime = [NSDate date];
    [self apiBGNLog:0];
    
    return DAISUKI_SUCCESS;
}

// BGN仕様対応 視聴ログ
- (void)apiBGNLog : (int)videoPlayTimeMSec {
    
#if defined(_OFFLINE_MODE_)
    return;
#endif
    
    //_bgnSessionID
    NSString* uri = _bgnLogURL;
    NSURL* url = [NSURL URLWithString:uri];
    
    NSDate* now = [NSDate date];
    NSTimeInterval interval = [now timeIntervalSinceDate:_bgnPlayStartTime];

    int spendMSec = (int)(interval * 1000.0);
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc]
                                    initWithURL:url
                                    cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    //NSURLResponse*  response = nil;
    
    
    //パラメーター設定
    NSString* params = [NSString
                        stringWithFormat:@"s=%@&c=%d&p=%d&k=%d",
                        _bgnSessionID,
                        _bgnLogCount,
                        videoPlayTimeMSec,
                        spendMSec];
    
    //HTTPでAPI問い合わせ
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [request setValue:USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
    } else {
        [request setValue:USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
    }
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];

    //非同期でログ送信
    NSURLConnection* connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (connection == nil) {
        NSLog(@"connection error");
    }
    //data = [NSURLConnection sendSynchronousRequest:request
    //                             returningResponse:&response error:&error];
    //NSString* responseStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    _bgnLogCount++;
    
}

// BGN仕様対応 AkamaiのマルチレンディションM3U8から個別のm3u8を取得
- (NSString*)getM3U8URL:(NSString*)rendition {
    for (int i = 0; i < _renditionLabels.count; i++) {
        if ([_renditionLabels[i] isEqualToString:rendition]) {
            return _m3u8URLs[i];
        }
    }
    
    return _m3u8URLs[0];
}

//IPアドレスの取得
- (NSString *)getIPAddress {
    NSString *address = @"";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}

//BGN非同期ログ用
- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
}
- (void) connection:(NSURLConnection*)connection didReceiveData:(NSData *)data {
}
- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
}
- (void) connectionDidFinishLoading:(NSURLConnection*)connection {
}


//IPアドレスキャッシュの内部更新
- (void)updateIPAddressCache {
    
    _ipAddressCache = [self getIPAddress];
    //NSLog(@"updateIPAddressCache A:%@", _ipAddressCache);
}

- (NSString*)getLocalURL {
    NSString* localurl;
    
    if ([_ipAddressCache isEqualToString:@""]) {
        localurl = [NSString stringWithFormat:@"http://127.0.0.1:11059"];
    } else {
        localurl = [NSString stringWithFormat:@"http://127.0.0.1:11059"];
//        localurl = [NSString stringWithFormat:@"http://%@:11059", _ipAddressCache];
    }
    return localurl;
}


- (NSString *)subscriptionStatusFreeCaseString:(NSString *)freeCase
                                    chargeCase:(NSString *)chargeCase {
    if ([@"disabled" isEqualToString:self.subscriptionStatus]
        || [@"guest" isEqualToString:self.subscriptionStatus]) {
        return freeCase;
    } else {
        return chargeCase;
    }
}

@end
