//
//  DLPlayer.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/17.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPlayer.h"

@implementation DLPlayer

- (instancetype)initWithPlayerItem:(AVPlayerItem *)item withAllowsExternalPlayback:(BOOL)allowsExternalPlayback {
    self = [super initWithPlayerItem:item];
    if (self) {
        self.allowsExternalPlayback = allowsExternalPlayback;
    }
    return self;
}

@end
