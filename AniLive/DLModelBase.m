//
// Created by Kotaro Itoyama on 2016/10/09.
// Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLModelBase.h"


@implementation DLModelBase {

}

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

// 1件だけ返却される場合
+ (NSObject *)instanceFromDictionary:(NSDictionary *)dictionary {
    if (NOT_NULL(dictionary)) {
        return [[self alloc] initWithAttributes:dictionary];
    }
    return nil;
}

+ (NSMutableArray *)instancesFromDictionaries:(NSArray *)dictionaries {
    if (dictionaries && dictionaries.count > 0) {
        NSMutableArray *items = [@[] mutableCopy];
        for (NSDictionary *dict in dictionaries) {
            [items addObject:[[self alloc] initWithAttributes:dict]];
        }
        return items;
    }
    return nil;
}

@end
