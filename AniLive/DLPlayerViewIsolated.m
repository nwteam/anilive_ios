//
//  DLPlayerViewIsolated.m
//  AniLive
//
//  Created by Kotaro Itoyama on 2017/01/02.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPlayerViewIsolated.h"
#import "DLPrimeTimeConfigManager.h"
#import "DLAdPolicySelector.h"
#import <PSDKLibrary/PSDKLibrary.h>
#import "PTVideoAnalyticsTracker.h"
#import "DLMoviePlayViewController.h"
#import "DLCaptionItem.h"

static NSString *kPlayerStatusKeyPath = @"status";
static NSString *kPlayerIsLoadedKeyPath = @"isLoaded";

@interface DLPlayerViewIsolated ()

@property(nonatomic, strong) PTMediaPlayer *mediaPlayer;
@property(nonatomic, strong) PTVideoAnalyticsTracker *videoAnalyticsTracker;
@property(nonatomic, strong) DLAdPolicySelector *adPolicySelector;
@property(nonatomic, assign) id playTimeObserver;
@property(nonatomic, strong) UIActivityIndicatorView *indicatorView;
@property(nonatomic, assign) BOOL isLive;

@property(readwrite, nonatomic, assign) BOOL isPlaying;
@property(readwrite, nonatomic, assign) BOOL isLoaded;
@property(nonatomic, assign) BOOL isRepeat;

@property(nonatomic) BOOL pendingSeek;
@property(nonatomic) CMTime seekTargetTime;
@property(nonatomic, strong) NSDate *seekRequestedTime;

@property(nonatomic, assign) NSInteger passedAdTime;

@end

@implementation DLPlayerViewIsolated

@synthesize baseViewDelegate;
@synthesize primetimeDelegate;

DLPlayerControlView *playerControlView_;

- (instancetype)initIsLive:(BOOL)flag{
    self = [super init];
    if (self) {
        self.isLive = flag;
        [self initProcess];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)removeFromSuperview {
    self.baseViewDelegate = nil;
    self.primetimeDelegate = nil;
    self.indicatorView = nil;
    if(self.mediaPlayer.status != PTMediaPlayerStatusPaused){
        [self.mediaPlayer pause];
    }
    self.mediaPlayer = nil;
}

- (void)dealloc {
    FUNC_LOG
    [self removePlayerObserver];
    [self removePlayerIsLoadedObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Public Methods

- (void)setPlayerMute:(BOOL)isMute {
    self.mediaPlayer.muted = isMute;
}

- (void)setPlayerRepeat:(BOOL)isRepeat {
    self.isRepeat = isRepeat;
}

- (void)setMediaPlayerItem:(PTMediaPlayerItem *)mediaPlayerItem_ renditions:(NSMutableArray<NSString*>*)renditions {
    self.mediaPlayerItem = mediaPlayerItem_;
    if([self getPlayerControlView]){
        [self getPlayerControlView].mediaPlayerItem = mediaPlayerItem_;
    }
    [self configPlayer];
    
    if([renditions count] > 0){
        self.mediaPlayer.abrControlParameters = [[PTABRControlParameters alloc] initWithABRControlParameters:[[renditions objectAtIndex:0] intValue] minBitRate:[[renditions objectAtIndex:0] intValue] maxBitRate:[[renditions objectAtIndex:[renditions count] - 1] intValue]];
    }
    [self.mediaPlayer prepareToPlay];
}

- (void)pause {
    if (!self.isPlaying) {
        return;
    }
    [self.mediaPlayer pause];
    self.isPlaying = NO;
    [[self getPlayerControlView] updateIsPlaying:self.isPlaying];
}

- (void)play {
    if (self.isPlaying) {
        return;
    }
    [self.mediaPlayer play];

    self.isPlaying = YES;
    [[self getPlayerControlView] updateIsPlaying:self.isPlaying];
}

- (void)startIndicator {
    [self.indicatorView startAnimating];
}

- (void)stopIndicator {
    [self.indicatorView stopAnimating];
}

- (void)seekToTime:(CMTime)time {
    CGFloat progress = CMTimeGetSeconds(time);
    CGFloat duration = CMTimeGetSeconds(self.mediaPlayer.duration);
    int timeScale = 600;
    NSInteger value = duration * progress * timeScale;
    CMTime targetTime = CMTimeMake(value, timeScale);
    if(self.mediaPlayer &&
       (self.mediaPlayer.status == PTMediaPlayerStatusPlaying ||
        self.mediaPlayer.status == PTMediaPlayerStatusPaused ||
         self.mediaPlayer.status == PTMediaPlayerStatusReady)){
        [self.mediaPlayer seekToLocalTime:targetTime];
    }else {
        self.pendingSeek = YES;
        self.seekTargetTime = targetTime;
        self.seekRequestedTime = NSDate.new;
    }
}

- (void)setPlayerViewHidden:(BOOL)isHidden withAnimated:(BOOL)animated {
    if (animated) {
        [UIView animateWithDuration:0.2
                         animations:^{
                             self.mediaPlayer.view.alpha = (isHidden ? 0 : 1);
                         }];

    } else {
        self.mediaPlayer.view.alpha = (isHidden ? 0 : 1);
    }

    // インジケータの表示制御
    if (!self.isLoaded && !isHidden) {
        [self startIndicator];
    } else {
        [self stopIndicator];
    }
}

#pragma mark - Private Methods

- (void)initProcess {
    [self initView];
    [self addSwipeGestureRecognizer];
}

- (void)configPlayer {
    self.mediaPlayer = nil;
    if(!self.mediaPlayer && self.mediaPlayerItem) {
        self.mediaPlayer = [PTMediaPlayer playerWithMediaPlayerItem:self.mediaPlayerItem];
        self.mediaPlayer.currentTimeUpdateInterval = 1000;
        self.mediaPlayer.autoPlay = YES;
        self.mediaPlayer.allowsAirPlayVideo = YES;
        self.mediaPlayer.videoGravity = PTMediaPlayerVideoGravityResizeAspect;
        self.mediaPlayer.closedCaptionDisplayEnabled = YES;
        self.mediaPlayer.view.contentMode = UIViewContentModeScaleAspectFit;
        [self updateFrame];
        [self insertSubview:self.mediaPlayer.view belowSubview:[self getPlayerControlView]];
        self.videoAnalyticsTracker = [[PTVideoAnalyticsTracker alloc] initWithMediaPlayer:self.mediaPlayer];
        self.adPolicySelector = DLAdPolicySelector.new;
        [[PTDefaultMediaPlayerClientFactory defaultFactory] registerAdPolicySelector:self.adPolicySelector];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(mediaPlayerStatusDidChange:)
                                                     name:PTMediaPlayerStatusNotification
                                                   object:self.mediaPlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onMediaPlayerAdBreakStarted:)
                                                     name:PTMediaPlayerAdBreakStartedNotification
                                                   object:self.mediaPlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onMediaPlayerAdBreakCompleted:)
                                                     name:PTMediaPlayerAdBreakCompletedNotification
                                                   object:self.mediaPlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onMediaPlayerAdStarted:)
                                                     name:PTMediaPlayerAdStartedNotification
                                                   object:self.mediaPlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onMediaPlayerAdProgress:)
                                                     name:PTMediaPlayerAdProgressNotification
                                                   object:self.mediaPlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onMediaPlayerAdCompleted:)
                                                     name:PTMediaPlayerAdCompletedNotification
                                                   object:self.mediaPlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onAdClick:)
                                                     name:PTMediaPlayerAdClickNotification
                                                   object:self.mediaPlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onMediaPlayerTimeChange:)
                                                     name:PTMediaPlayerTimeChangeNotification
                                                   object:self.mediaPlayer];
        
    }
}

- (void)initView {
    self.indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.indicatorView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.indicatorView];
    self.playerControlView = [[DLPlayerControlView alloc] initIsLive:self.isLive];
    if(self.mediaPlayerItem){
        [self getPlayerControlView].mediaPlayerItem = self.mediaPlayerItem;
    }
    [self addSubview:[self getPlayerControlView]];
}

- (void)updateFrame {
    self.indicatorView.frame = [self getPlayerControlView].frame = self.bounds;
    if(self.mediaPlayer){
        self.mediaPlayer.view.frame = self.bounds;
    }
}

- (void)removePlayerObserver {
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:PTMediaPlayerStatusNotification
                                                      object:self.mediaPlayer];
    } @catch (id anException) {
        // 設定していない場合でも落ちないための処理のため、Exceptionは無視する
    }
}

- (void)removePlayerIsLoadedObserver {
    @try {
        [self removeObserver:self forKeyPath:kPlayerIsLoadedKeyPath];
    } @catch (id anException) {
        // 設定していない場合でも落ちないための処理のため、Exceptionは無視する
    }
}

#pragma mark - Notification

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    [self pause];

    if ([self.baseViewDelegate respondsToSelector:@selector(playerBaseViewPlayerItemDidReachEnd)]) {
        [self.baseViewDelegate playerBaseViewPlayerItemDidReachEnd];
    }
}

#pragma mark - Gesture Recognizer

// TODO: 明示的にremoveさせる必要はあるか確認
- (void)addSwipeGestureRecognizer {
    UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
    UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];

    swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;

    [self addGestureRecognizer:swipeLeftGesture];
    [self addGestureRecognizer:swipeRightGesture];
}

- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
    if ([self.baseViewDelegate respondsToSelector:@selector(playerBaseViewPlayerSwipeGestureLeft:)]) {
        [self.baseViewDelegate playerBaseViewPlayerSwipeGestureLeft:sender];
    }
}

- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
    if ([self.baseViewDelegate respondsToSelector:@selector(playerBaseViewPlayerSwipeGestureRight:)]) {
        [self.baseViewDelegate playerBaseViewPlayerSwipeGestureRight:sender];
    }
}

#pragma mark - Notification callback

- (void)mediaPlayerStatusDidChange:(NSNotification *)notification {
    PTMediaPlayerStatus status = (PTMediaPlayerStatus) [notification.userInfo[PTMediaPlayerStatusKey] integerValue];
    switch (status) {
        case PTMediaPlayerStatusCreated: {
            NSLog(@"CurrentMediaPlayerStatus: Created");
            break;
        }
        case PTMediaPlayerStatusInitializing: {
            NSLog(@"CurrentMediaPlayerStatus: Initializing");
            break;
        }
        case PTMediaPlayerStatusInitialized: {
            NSLog(@"CurrentMediaPlayerStatus: Initialized");
//            [self.mediaPlayer prepareToPlay];
            break;
        }
        case PTMediaPlayerStatusReady: {
            NSLog(@"CurrentMediaPlayerStatus: Ready");
//            [self removePlayerObserver];
            self.isLoaded = YES;
            if(self.pendingSeek){
                CMTime time;
                if(self.seekRequestedTime){
                    time = CMTimeAdd(self.seekTargetTime, CMTimeMakeWithSeconds([NSDate.new timeIntervalSinceDate:self.seekRequestedTime], 1));
                }else{
                    time = self.seekTargetTime;
                }
                [self.mediaPlayer seekToTime:time];
                self.pendingSeek = NO;
                self.seekRequestedTime = nil;
                self.seekTargetTime = CMTimeMakeWithSeconds(0, 1);
            }
            break;
        }
        case PTMediaPlayerStatusPlaying: {
            NSLog(@"CurrentMediaPlayerStatus: Playing");
            self.isPlaying = YES;
            break;
        }
        case PTMediaPlayerStatusPaused: {
            NSLog(@"CurrentMediaPlayerStatus: Paused");
            break;
        }
        case PTMediaPlayerStatusStopped: {
            NSLog(@"CurrentMediaPlayerStatus: Stopped");
            [self.mediaPlayer prepareToPlay];
            break;
        }
        case PTMediaPlayerStatusCompleted: {
            NSLog(@"CurrentMediaPlayerStatus: Completed");
            [self.primetimeDelegate onPTPlayerPlayBreakComplete];
            [self pause];
            break;
        }
        case PTMediaPlayerStatusError: {
            NSLog(@"CurrentMediaPlayerStatus: Error");
            NSLog(@"Reason: %@", self.mediaPlayer.error.debugDescription);
            [self.mediaPlayer prepareToPlay];
            //            [self removePlayerObserver];
            [self stopIndicator];
            break;
        }
        default:
            break;
    }
}

- (void)setPlayerControlView: (DLPlayerControlView *)playerControlView {
    playerControlView_ = playerControlView;
}

- (DLPlayerControlView *)getPlayerControlView {
    return playerControlView_;
}

- (void)updateFrame:(CGRect)frame {
    self.frame = frame;
}

- (void)onMediaPlayerTimeChange:(NSNotification *)notification {
    NSLog(@"onMediaPlayerTimeChange");
    NSInteger currentTime = (NSInteger)CMTimeGetSeconds(self.mediaPlayer.currentTime);
    NSInteger duration = (NSInteger)CMTimeGetSeconds(self.mediaPlayer.duration);
    [self.primetimeDelegate onPlayerTimeUpdate:currentTime duration:duration];    
}

- (void)onMediaPlayerAdBreakStarted:(NSNotification *)notification {
    NSLog(@"onMediaPlayerAdBreakStarted");
    self.passedAdTime = 0;
    [self.primetimeDelegate onPTPlayerAdBreakStart];
}

- (void)onMediaPlayerAdBreakCompleted:(NSNotification *)notification {
    NSLog(@"onMediaPlayerAdBreakCompleted");
    self.passedAdTime = CMTimeGetSeconds(self.mediaPlayer.currentTime);
    [self.primetimeDelegate onPTPlayerAdBreakComplete:self.passedAdTime];
}

- (void)onMediaPlayerAdStarted:(NSNotification *)notification {
    NSLog(@"onMediaPlayerAdStarted");
}

- (void)onMediaPlayerAdProgress:(NSNotification *)notification {
    NSLog(@"onMediaPlayerAdProgress");
}

- (void)onMediaPlayerAdCompleted:(NSNotification *)notification {
    NSLog(@"onMediaPlayerAdCompleted");
}

- (void)onAdClick:(NSNotification *)notification {
    NSLog(@"onAdClick");
}

@end
