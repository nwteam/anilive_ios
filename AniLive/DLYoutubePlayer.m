//
// Created by akasetaichi on 2017/01/17.
// Copyright (c) 2017 AnimeConsortiumJapan. All rights reserved.
//

#import <CoreMedia/CoreMedia.h>
#import "DLYoutubePlayer.h"

@interface DLYoutubePlayer ()

@property (nonatomic, strong) WKWebView* _webView;

@end

@implementation DLYoutubePlayer

@synthesize baseViewDelegate;
@synthesize primetimeDelegate;

DLPlayerControlView *youtubePlayerPlayerControlView_;

- (instancetype)initIsLive:(BOOL)flag{
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}


- (void)setPlayerMute:(BOOL)isMute {}

- (void)setPlayerRepeat:(BOOL)isRepeat{}
- (void)pause{}
- (void)play{}
- (void)startIndicator{}
- (void)seekToTime:(CMTime)time{}
- (void)setPlayerViewHidden:(BOOL)isHidden withAnimated:(BOOL)animated{

}

- (void)embedYouTube:(NSString*)code {
    static dispatch_once_t onceToken;

    // TODO
    /*dispatch_once( &onceToken, ^{
        [NSURLProtocol registerClass:[CustomURLProtocol class]];
    });*/

    //iOS7以下用youtube終了時判定
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerWillExitFullscreen:)
                                                 name:@"UIMoviePlayerControllerWillExitFullscreenNotification"
                                               object:nil];
    //iOS8用youtube終了時判定
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(youTubeFinished:) name:UIWindowDidBecomeHiddenNotification object:nil];


    //[self._webView setAllowsInlineMediaPlayback:NO];
    //[self._webView setMediaPlaybackRequiresUserAction:NO];

    Class class = NSClassFromString(@"UIAlertController");


    NSString* embedHTML = [NSString stringWithFormat:@"\
                           <html>\
                           <body style='margin:0px;padding:0px;background-color:yellow'>\
                           <script type='text/javascript' src='http://www.youtube.com/iframe_api'></script>\
                           <script type='text/javascript'>\
                           var g_player = null;\
                           function onYouTubeIframeAPIReady()\
                           {\
                                ytplayer=new YT.Player('playerId',{\
                                  events:{onReady:onPlayerReady, onStateChange:onPlayerStateChange},\
height: '100%%',\
width: '100%%',\
playerVars: {'playsinline': 1}}\
                                );\
                           }\
                           function onPlayerReady(a)\
                           { \
                                g_player=a;\
                                setTimeout(function(){a.target.playVideo();}, 10); \
                           }\
                           function onPlayerStateChange(d)\
                           { \
                           		if( YT.PlayerState.ENDED == d.data ) \
                           		{ \
                               		window.webkit.messageHandlers.didGetUserAgent.postMessage('end');\
                           		} \
                           }\
\
                           </script>\
                           <iframe id='playerId' type='text/html' style='width:100%%; height:100%%' src='http://www.youtube.com/embed/%@?enablejsapi=1&rel=0&playsinline=1&autoplay=1' frameborder='0' >\
                           \
                           </body>\
                           </html>",
                                                     //w,//(int)[UIScreen mainScreen].bounds.size.height,
                                                     //h,//(int)[UIScreen mainScreen].bounds.size.width,
                                                     code];
    //[self._webView setAllowsInlineMediaPlayback:YES];
    //[self._webView setMediaPlaybackRequiresUserAction:NO];
    [self._webView loadHTMLString:embedHTML baseURL:[[NSBundle mainBundle] resourceURL]];

}

- (void)initView {
    self.backgroundColor = [UIColor redColor];
    WKWebViewConfiguration *wkWebViewConfiguration = [[WKWebViewConfiguration alloc] init];
    wkWebViewConfiguration.allowsAirPlayForMediaPlayback = NO;
    wkWebViewConfiguration.allowsInlineMediaPlayback = YES;
    wkWebViewConfiguration.mediaPlaybackRequiresUserAction = NO;

    WKUserContentController *userContentController = [[WKUserContentController alloc] init];
    [userContentController addScriptMessageHandler:self name:@"didGetUserAgent"];
    wkWebViewConfiguration.userContentController = userContentController;
    self._webView = [[WKWebView alloc] initWithFrame:self.bounds configuration:wkWebViewConfiguration];
    self._webView.UIDelegate = self;
    self._webView.navigationDelegate = self;
    self._webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    //self._webView.scalesPageToFit = YES;
    [self addSubview:self._webView];
}

- (void)setPlayerControlView: (DLPlayerControlView *)playerControlView {
    youtubePlayerPlayerControlView_ = playerControlView;
}

- (DLPlayerControlView *)getPlayerControlView {
    return youtubePlayerPlayerControlView_;
}

- (void)updateFrame:(CGRect)frame {
    self.frame = frame;
    self._webView.frame = self.bounds;
    [self getPlayerControlView].frame = self.bounds;

}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)
        request navigationType:(UIWebViewNavigationType)navigationType {
    // schemeがnative だった場合
    NSLog(@"web request");
    if ([ request.URL.scheme isEqualToString:@"daisuki" ]) {

        // WebViewの読み込みは中断する。
        return NO;
    }
        // 通常のschemeの場合は、フックせずそのまま処理を続ける
    else {
        return YES;
    }
}

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
{
    [self.baseViewDelegate playerBaseViewPlayerItemDidReachEnd];
}

@end
