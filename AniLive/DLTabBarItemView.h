//
//  DLTabBarItemView.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DLTabBarItem : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIColor *normalTitleColor;
@property (nonatomic, strong) UIColor *highlightedTitleColor;
@property (nonatomic, strong) UIImage *normalIconImage;
@property (nonatomic, strong) UIImage *highlightedIconImage;
@property (nonatomic, strong) UIImage *imgBadge;
@property (nonatomic, assign) NSInteger tag;

@end

@protocol DLTabBarItemViewDelegate;

@interface DLTabBarItemView : UIView

@property (nonatomic, weak) id<DLTabBarItemViewDelegate> delegate;
@property (readonly, nonatomic, strong) DLTabBarItem *tabBarItem;
@property (strong, nonatomic) UIImageView *badgeImageView;

- (instancetype)initWithTabBarItem:(DLTabBarItem *)tabBarItem;
- (void)setButtonSelected:(BOOL)isSelected;

@end

@protocol DLTabBarItemViewDelegate <NSObject>

@optional
- (void)tabBarItemViewDidSelect:(DLTabBarItemView *)tabBarItemView;

@end
