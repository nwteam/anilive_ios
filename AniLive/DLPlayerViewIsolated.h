//
//  DLPlayerViewIsolated.h
//  AniLive
//
//  Created by Kotaro Itoyama on 2017/01/02.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLPlayerView.h"
#import "DLPlayerControlView.h"

@protocol DLPlayerBaseViewDelegate;
@protocol DLPlayerViewIsolatedProtocol;
@protocol DLPlayerViewPrimeTimeDelegate;

@class DLMovie, DLLive;
@interface DLPlayerViewIsolated : UIView <DLPlayerViewIsolatedProtocol>

@property (nonatomic, strong) PTMediaPlayerItem *mediaPlayerItem;

- (void)stopIndicator;

@end

@protocol DLPlayerBaseViewDelegate <NSObject>

@optional

- (void)playerBaseViewDidLoadedMovie:(DLPlayerViewIsolated *)playerBaseView;
- (void)playerBaseViewPlayerItemDidReachEnd;
- (void)playerBaseViewPlayerSwipeGestureLeft:(UISwipeGestureRecognizer *)gesture;
- (void)playerBaseViewPlayerSwipeGestureRight:(UISwipeGestureRecognizer *)gesture;

@end

@protocol DLPlayerViewIsolatedProtocol

//@property (nonatomic, strong) DLPlayerControlView *playerControlView;
- (void)setPlayerControlView: (DLPlayerControlView *)playerControlView;
- (DLPlayerControlView *)getPlayerControlView;

@property (readonly, nonatomic, assign) BOOL isPlaying;
@property (readonly, nonatomic, assign) BOOL isLoaded;
@property (nonatomic, weak) id<DLPlayerBaseViewDelegate> baseViewDelegate;
@property (nonatomic, weak) id<DLPlayerViewPrimeTimeDelegate> primetimeDelegate;
- (instancetype)initIsLive:(BOOL)flag;
- (void)setPlayerMute:(BOOL)isMute;
- (void)setPlayerRepeat:(BOOL)isRepeat;
- (void)pause;
- (void)play;
- (void)startIndicator;
- (void)seekToTime:(CMTime)time;
- (void)setPlayerViewHidden:(BOOL)isHidden withAnimated:(BOOL)animated;
- (void)updateFrame:(CGRect)frame;
- (void)setMediaPlayerItem:(PTMediaPlayerItem *)mediaPlayerItem_ renditions:(NSMutableArray<NSString*>*)renditions;

@end

@protocol DLPlayerViewPrimeTimeDelegate <NSObject>

- (void)onPTPlayerAdBreakStart;
- (void)onPTPlayerAdBreakComplete:(NSInteger)passedTime;
- (void)onPTPlayerPlayBreakstart;
- (void)onPTPlayerPlayBreakComplete;
- (void)onPTPlayerError;
- (void)onPlayerTimeUpdate:(NSInteger)currentTime duration:(NSInteger)duration;

@end
