//
//  DLForumGroupListViewController.h
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/10/23.
//  Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

typedef NS_ENUM(NSInteger, DLForumGroupSortType) {
    DLForumGroupSortTypeLatest = 0,
    DLForumGroupSortTypePopular
};


@interface DLForumGroupListViewController : UITableViewController
@property (nonatomic, assign) DLForumGroupSortType sortType;
@property(nonatomic, readonly) NSMutableArray *forumGroups;

@end
