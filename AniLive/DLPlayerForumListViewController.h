//
//  DLPlayerForumListViewController.h
//  AniLive
//
//  Created by isaoeka on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DLForum;
@class DLPlayerOptionBarView;

@interface DLPlayerForumListViewController : UIViewController

@property (nonatomic, strong) DLPlayerOptionBarView *playerOptionBarView;

- (void)setForums:(NSMutableArray<DLForum *> *)forums;

@end
