//
//  DLPoint.m
//  AniLive
//
//  Created by ykkc on 2016/12/30.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPoint.h"

@interface DLPoint ()
@property (readwrite, nonatomic, strong) NSDate *pointDate;
@property (readwrite, nonatomic, strong) NSNumber *point;
@property (readwrite, nonatomic, strong) NSString *reasonText;
@property (readwrite, nonatomic, strong) NSDate *viewLimit;
@end

@implementation DLPoint

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.pointDate = [DLAPIHelper dateFromString:attributes[@"point_date"]];
    self.point = CHECK_NULL_DATA_NUMBER(attributes[@"point"]);
    self.reasonText = CHECK_NULL_DATA_STRING(attributes[@"reason_text"]);
    self.viewLimit = [DLAPIHelper dateFromString:attributes[@"view_limit"]];

    return self;
}
@end
