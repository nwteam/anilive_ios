#import <Foundation/Foundation.h>

@interface NSDate (Category)

- (NSString*) genPastTimeString;

@end