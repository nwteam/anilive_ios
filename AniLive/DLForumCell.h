//
//  DLForumCell.h
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/10/08.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLSuperTableViewCell.h"

@class DLForum;

@interface DLForumCell : DLSuperTableViewCell

@property (nonatomic, weak) DLForum *forum;

+ (UINib *)getDefaultNib;
+ (CGFloat)getCellHeight;


@end
