//
//  DLForumView.h
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/11/06.
//  Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "SendTouchTableView.h"

@class DLForum;

@interface DLForumView : UIView
@property(nonatomic) DLForum *forum;
@property(nonatomic, readonly) SendTouchTableView *tableView;
@property(nonatomic, readonly) UITextView *tfComment;
@property(nonatomic, readonly) UIView *textFieldBackVeiw;
@property(nonatomic, readonly) UILabel *placeHoderLable;

- (void)displayNewInputText:(NSString *)newStr;

@end
