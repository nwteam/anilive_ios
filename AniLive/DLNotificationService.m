//
//  DLNotificationService.m
//  AniLive
//
//  PushNotificationを受けた時の動作を規定するクラス
//  Android版のGCMIntentServiceに対応
//

#import <Growthbeat/Growthbeat.h>
#import <Realm/Realm.h>

#import "DLAPIHelper.h"
#import "DLAppDelegate.h"
#import "DLNotificationService.h"
#import "DLTransitionManager.h"

@implementation DLNotificationService


+ (void) sendNotification:(NSDictionary*)noticeDict {
    DLNotice *notice = [DLNotice initWithAttributes: noticeDict];
    long noticeId = [[NSDate date] timeIntervalSince1970];
    notice.id = noticeId;
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm addObject:notice];
    [realm commitWriteTransaction];
}

+ (void) onLaunchFromNotification:(NSDictionary*)noticeDict {
    /*
     * Notificationから起動した時
     */
    DLAppDelegate *appDelegate = (DLAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate openSplashViewController: noticeDict];

}

+ (void) transitScreenByNotification: (DLNotice*)notice callback:(void (^)(void))callback{
    if (notice.transitionView == kDLNoticeTransactionView_PlayMovie) {
        [DLAPIHelper fetchLive: notice.transitionItemId
                      callback:^(DLLive *live) {
                          if (live != nil) {
                              [DLTransitionManager showMoviePlayViewControllerWithLiveData:live
                                                                                withTarget:nil
                                                                               withIsModal:YES];
                          }
                          callback();
                      }];
    }
    else if (notice.transitionView == kDLNoticeTransactionView_Forum) {

        [DLAPIHelper fetchForumByForumID: notice.transitionItemId
                                callback: ^(NSObject *forum, NSError *error){
                                    if (NOT_NULL(error)) {

                                    } else {
                                        [DLTransitionManager showForumDetailViewControllerWithForum:forum
                                                                                         withTarget:nil];
                                    }
                                    callback();
                                }];
    }
    else if (notice.transitionView == kDLNoticeTransactionView_Work) {
        [DLAPIHelper fetchWorksByWorkID: notice.transitionItemId
                               callback: ^(DLWork *work, NSError *error){
                                   if (NOT_NULL(error)) {

                                   } else {
                                       [DLTransitionManager showWorkDetailViewControllerWithWorkData:work
                                                                                          withTarget:nil];
                                   }
                                   callback();
                               }];
    }
}

+ (void)reflectPushSettingAsTagToServer {
    /*
     * 現在のアプリの設定値をTagとしてPushサーバー（GrowthPush）に送信する
     */
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    [userDefaults registerDefaults:@{
            kUserDefaultsKey_SettingNewShowAlert: @YES,
            kUserDefaultsKey_SettingNewLiveAlert: @YES,
            kUserDefaultsKey_SettingNewCommentsAlert: @YES
    }];

    NSString *value;
    value = [userDefaults boolForKey:kUserDefaultsKey_SettingNewShowAlert]? @"true" : @"false";
    [[GrowthPush sharedInstance] setTag:@"SettingNewShowAlert" value:value];
    value = [userDefaults boolForKey:kUserDefaultsKey_SettingNewLiveAlert]? @"true" : @"false";
    [[GrowthPush sharedInstance] setTag:@"SettingNewLiveAlert" value:value];
    value = [userDefaults boolForKey:kUserDefaultsKey_SettingNewCommentsAlert]? @"true" : @"false";
    [[GrowthPush sharedInstance] setTag:@"SettingNewCommentsAlert" value:value];

}


@end
