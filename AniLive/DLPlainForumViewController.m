//
//  DLPlainForumViewController.m
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/11/06.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//
//  動画再生画面での再利用を考慮して再生画面の構造に近い状態のコードを一時的に別ファイルとして保存する。
//  動作確認の場合はDLForumListViewController内でDLForumViewControllerが生成されている部分をこのクラスに書き換える。
//

#import "DLPlainForumViewController.h"
#import "DLForumHeaderView.h"
#import "DLForum.h"
#import "DLCommentCell.h"
#import "DLAppFrameManager.h"
#import "DLForumView.h"
#import "DLAPIHelper.h"

@interface DLPlainForumViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, DLNavigationBarViewDelegate>

@property(nonatomic, strong) DLNavigationBarView *navigationBarView;
@property(nonatomic, strong) DLForumHeaderView *headerView;
@property(nonatomic, strong) NSMutableArray *comments;
@property(nonatomic, strong) DLForumView *forumView;

@end

@implementation DLPlainForumViewController

#pragma mark - View Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    [self loadData];
    [self updateFrame];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self showDebug:@"viewWillAppear"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self showDebug:@"viewWillLayoutSubviews"];
}

- (void)updateViewConstraints {
    [self showDebug:@"updateViewConstraints before"];

    [super updateViewConstraints];
    [self showDebug:@"updateViewConstraints after"];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self showDebug:@"viewDidLayoutSubviews"];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self showDebug:@"viewDidAppear"];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

#pragma mark - Private Methods

- (void)initView {
    self.view.backgroundColor = UIColor.magentaColor;

    self.forumView = DLForumView.new;
    self.forumView.tableView.dataSource = self;
    self.forumView.tableView.delegate = self;
    self.forumView.tfComment.delegate = self;
    [self.view addSubview:self.forumView];

    self.headerView = DLForumHeaderView.new;
    [self.view addSubview:self.headerView];
    self.headerView.forum = self.forum;

#pragma mark Navigation Bar
    self.navigationBarView = [[DLNavigationBarView alloc] initWithType:DLNavigationBarViewTypeForumList];
    self.navigationBarView.delegate = self;
    [self.navigationBarView setTitle:Localizer(@"Forum detail")];
    [self.view addSubview:self.navigationBarView];
}

- (void)loadData {
    //TODO: IDだけ渡してAPIを叩く作りにしたいですmm
//    [DLAPIHelper fetchCommentsByForumID:nil pageNumber:1 callback:callback:^(NSMutableArray *comments) {
//        self.comments = comments;
//        [self.forumView.tableView reloadData];
//    }];
}

- (void)updateFrame {

    CGRect appFrame = DLAppFrameManager.sharedInstance.applicationFrame;
    CGFloat w = CGRectGetWidth(appFrame);
    CGFloat headerHeight = w * 0.75f;
    self.navigationBarView.frame = CGRectMake(0,
            CGRectGetMinY(appFrame),
            w,
            DLConstNavigationBarHeight);
    CGFloat tableTopY = CGRectGetMaxY(self.navigationBarView.frame);
    self.headerView.frame = CGRectMake(0,
            tableTopY,
            w,
            headerHeight);

    self.forumView.frame = CGRectMake(0,
            tableTopY,
            w,
            CGRectGetHeight(appFrame) - tableTopY);

    self.forumView.tableView.contentInset
            = self.forumView.tableView.scrollIndicatorInsets
            = (UIEdgeInsets) {.top=CGRectGetMaxY([self.view convertRect:self.headerView.frame toView:self.forumView])};
}

#pragma mark Debug Function

- (void)showDebug:(NSString *)tag {
    NSLog(@"%@", tag);
    NSLog(@" root %@", NSStringFromCGRect(self.view.frame));
    NSLog(@" header %@", NSStringFromCGRect(self.headerView.frame));
    NSLog(@" forum %@", NSStringFromCGRect(self.forumView.frame));
}

#pragma mark Keyboard Event Handler

- (void)keyboardWillShown:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
    [UIView setAnimationBeginsFromCurrentState:YES];

    CGRect f = self.view.frame;
    CGRect keyboardRect = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    f.origin.y = -CGRectGetHeight(keyboardRect);
    self.view.frame = f;

    [UIView commitAnimations];
}

- (void)keyboardWillHidden:(NSNotification *)notification {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
    [UIView setAnimationBeginsFromCurrentState:YES];

    CGRect f = self.view.frame;
    f.origin.y = 0;
    self.view.frame = f;

    [UIView commitAnimations];
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.comments ? self.comments.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DLCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"comment-cell" forIndexPath:indexPath];
    cell.comment = self.comments[(NSUInteger) indexPath.row];
    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}


#pragma mark - Scroll View Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(MAX(-scrollView.contentOffset.y, self.headerView.residualHeight), 0, 0, 0);
    CGRect f = self.headerView.frame;
    f.origin.y = scrollView.scrollIndicatorInsets.top + CGRectGetMinY(self.forumView.frame) - CGRectGetHeight(self.headerView.frame);
    self.headerView.frame = f;
}

#pragma mark - DLNavigation Bar View Delegate

- (void)navigationBarViewDidTapLeftButton:(UIButton *)button {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Text Field Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason {
    [DLAPIHelper postComment:textField.text
               targetForumId:[self.forum.forumId integerValue]
                    callback:^(BOOL flag, id responseObject) {
                        if(flag){
                            [self loadData];
                        }
                    }];
    textField.text = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end

