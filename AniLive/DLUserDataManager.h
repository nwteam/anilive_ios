//
//  DLUserDataManager.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DLUser.h"

@interface DLUserDataManager : NSObject

@property (readonly, nonatomic, strong) DLUser *user;

+ (instancetype)sharedInstance;

- (void)saveUserData:(NSDictionary *)dictionary;
- (void)saveUserSessionToken:(NSDictionary *)dictionary;
- (void)saveUserPointData:(NSDictionary *)dictionary;

@end
