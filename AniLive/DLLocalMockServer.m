//
// Created by Kotaro Itoyama on 2016/10/09.
// Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLLocalMockServer.h"
#import "OHPathHelpers.h"
#import "OHHTTPStubsResponse.h"
#import "OHHTTPStubs.h"


@implementation DLLocalMockServer

+ (void)installStubs {
    [self bulkInstallStubsFromDictionary:@{
            kAPIForum: kAPIForumSuffix,
            kAPIComment: kAPICommentSuffix,
            [NSString stringWithFormat:@"%@.json", kAPILive]: [NSString stringWithFormat:@"%@.json", kAPILiveSuffix],
            kAPITimeSchedule: kAPITimeScheduleSuffix,
            kAPIUser: kAPIUserSuffix,
            kAPIWork: kAPIWorkSuffix,
            [NSString stringWithFormat:@"%@.json", kAPIMovie]: [NSString stringWithFormat:@"%@.json", kAPIMovieSuffix],
            kAPIMovie: kAPIMovieSuffix,
            kAPIValidate: kAPIValidateSuffix,
    }];
}

+ (void)bulkInstallStubsFromDictionary:(NSDictionary *)dict {
    for (NSString *urlString in dict) {
        [self installStubWithURL:[NSURL URLWithString:urlString] AndFileName:dict[urlString]];
    }
}

+ (void)installStubWithURL:(NSURL *)url AndFileName:(NSString *)fileName {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
        NSURL *req = request.URL;
        return [req.host isEqualToString:url.host] && [req.path isEqualToString:url.path];
    }                   withStubResponse:^OHHTTPStubsResponse *(NSURLRequest *request) {
        return [OHHTTPStubsResponse responseWithFileAtPath:OHPathForFile(fileName, self.class)
                                                statusCode:200
                                                   headers:@{@"Content-Type": @"application/json"}];
    }];
}
@end
