//
//  DLPlayerControlView.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/24.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

// Frameworks
@import PSDKLibrary.PTMediaPlayerItem;

// Categories
#import "NSString+Category.h"

// Models
#import "DLMovie.h"
#import "DLLive.h"

// Views
#import "DLPlayerControlView.h"
#import "DLHighlightedButton.h"

static NSString *kPlayerItemDurationKeyPath = @"duration";
static NSString *kMediaPlayerItemLocalTimeKeyPath = @"localTime";
static NSString *kMediaPlayerItemLocalDurationKeyPath = @"localDuration";

@interface DLPlayerControlView ()

// Views
@property (nonatomic, strong) UIView *topShadowView;
@property (nonatomic, strong) CAGradientLayer *topGradientLayer;
@property (nonatomic, strong) UIImageView *backImageView;
@property (nonatomic, strong) DLHighlightedButton *backButton;
@property (nonatomic, strong) UIImageView *forumImageView;
@property (nonatomic, strong) DLHighlightedButton *forumButton;

@property (nonatomic, strong) UIView *bottomShadowView;
@property (nonatomic, strong) CAGradientLayer *bottomGradientLayer;
@property (nonatomic, strong) UIImageView *pauseImageView;
@property (nonatomic, strong) DLHighlightedButton *pauseButton;
@property (nonatomic, strong) UIImageView *playImageView;
@property (nonatomic, strong) DLHighlightedButton *playButton;
@property (nonatomic, strong) UIImageView *fullScreenImageView;
@property (nonatomic, strong) DLHighlightedButton *fullScreenButton;
@property (nonatomic, strong) UIImageView *shrinkImageView;
@property (nonatomic, strong) DLHighlightedButton *shrinkButton;
@property (nonatomic, strong) UISlider *playerSlider;
@property (nonatomic, strong) UILabel *currentTimeLabel;
@property (nonatomic, strong) UILabel *playTimeLabel;
@property (nonatomic, strong) AVPlayerItem *playerItem;
@property (nonatomic, strong) UILabel *captionLabel;

// Models
@property (nonatomic, strong) DLMovie *movieData;
@property (nonatomic, strong) DLLive *liveData;

// Flags
@property (nonatomic, assign) BOOL isLive;
@property (nonatomic, assign) BOOL isMediaPlayer;
@property (nonatomic, assign) BOOL isHiddenControl;
@property (nonatomic, assign, readwrite) BOOL isLoaded;
@property (nonatomic, assign, readwrite) BOOL isPlaying;
@property (nonatomic, assign, readwrite) BOOL isFullScreen;
@property (nonatomic, assign) BOOL isAnimating;

@end

@implementation DLPlayerControlView

- (instancetype)initWithMovieData:(DLMovie *)movieData
                   withPlayerItem:(AVPlayerItem *)playerItem {
    self = [super init];
    if (self) {
        self.movieData = movieData;
        self.playerItem = playerItem;
        self.isLive = NO;
        self.isMediaPlayer = NO;
        
        [self initProcess];
    }
    return self;
}

- (instancetype)initWithMovieData:(DLMovie *)movieData
              withMediaPlayerItem:(PTMediaPlayerItem *)mediaPlayerItem {
    self = [super init];
    if (self) {
        self.movieData = movieData;
        self.mediaPlayerItem = mediaPlayerItem;
        self.isLive = NO;
        self.isMediaPlayer = YES;
        
        [self initProcess];
    }
    return self;
}

- (instancetype)initWithLiveData:(DLLive *)liveData
                  withPlayerItem:(AVPlayerItem *)playerItem {
    self = [super init];
    if (self) {
        self.liveData = liveData;
        self.playerItem = playerItem;
        self.isLive = YES;
        self.isMediaPlayer = NO;
        
        [self initProcess];
    }
    return self;
}

- (instancetype)initWithLiveData:(DLLive *)liveData
             withMediaPlayerItem:(PTMediaPlayerItem *)mediaPlayerItem {
    self = [super init];
    if (self) {
        self.liveData = liveData;
        self.mediaPlayerItem = mediaPlayerItem;
        self.isLive = YES;
        self.isMediaPlayer = YES;
        
        [self initProcess];
    }
    return self;
}

- (instancetype)initIsLive:(BOOL)flag {
    self = [super init];
    if (self) {
        self.isLive = flag;
        self.isMediaPlayer = YES;
        [self initProcess];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
    [self removePlayerItemObserver];
}

#pragma mark - Public Methods

- (void)updateIsLoaded:(BOOL)isLoaded {
    self.isLoaded = isLoaded;
    [self updateHiddenViewsWithCompletion:nil];
}

- (void)updateIsPlaying:(BOOL)isPlaying {
    self.isPlaying = isPlaying;
    [self updateHiddenViewsWithCompletion:nil];
}

- (void)updateIsFullScreen:(BOOL)isFullScreen {
    self.isFullScreen = isFullScreen;
    [self updateHiddenViewsWithCompletion:nil];
}

- (void)updateCurrentTime {
    float currentTime = [self movieCurrentTime];
    self.currentTimeLabel.text = [NSString formatSecTimeToString:currentTime];
}

- (void)updateTimeView:(NSInteger)currentTime duration:(NSInteger)duration {
     self.currentTimeLabel.text = [NSString formatSecTimeToString:currentTime];
     self.playTimeLabel.text = [NSString formatSecTimeToString:duration];
}

- (void)updateCaptionView:(NSString *)captionMessage {
    self.captionLabel.text = captionMessage;
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                       context:(void *)context {
    
    if ([keyPath isEqualToString:kPlayerItemDurationKeyPath]) {
        [self updateDuration];
    } else if ([keyPath isEqualToString:kMediaPlayerItemLocalTimeKeyPath]) {
        [self updateCurrentTime];
    } else if ([keyPath isEqualToString:kMediaPlayerItemLocalDurationKeyPath]) {
        [self updateDuration];
    }
}

#pragma mark - Private Methods

- (void)initProcess {
    
    [self initView];
    [self addGradientToView];
    [self addPlayerItemObserver];
    [self pauseButtonHidden:YES];
    [self playButtonHidden:YES];
    [self fullScreenButtonHidden:YES];
    [self shrinkButtonHidden:YES];
    [self backButtonHidden:YES];
    [self forumButtonHidden:YES];
    [self currenTimeLabelHidden:YES];
    [self playTimeLabelHidden:YES];
    [self captionViewHidden:YES];
    [self sliderHidden:YES];
}

- (void)initView {
    
    self.backgroundColor = [UIColor clearColor];
    self.userInteractionEnabled = YES;
    
    // 上部影 View
    self.topShadowView = [UIView new];
    self.topShadowView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.topShadowView];
    
    self.backImageView = [UIImageView new];
    self.backImageView.backgroundColor = [UIColor clearColor];
    self.backImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.backImageView.image = [UIImage imageNamed:DLConstIconClose];
    [self addSubview:self.backImageView];
    
    self.backButton = [DLHighlightedButton buttonWithType:UIButtonTypeSystem];
    self.backButton.backgroundColor = [UIColor clearColor];
    self.backButton.isNoNeedHighlight = YES;
    [self.backButton addTarget:self
                        action:@selector(backButtonDidTap)
              forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.backButton];
    
    self.forumImageView = [UIImageView new];
    self.forumImageView.backgroundColor = [UIColor clearColor];
    self.forumImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.forumImageView.image = [UIImage imageNamed:DLConstIconAddForum];
    [self addSubview:self.forumImageView];
    
    self.forumButton = [DLHighlightedButton buttonWithType:UIButtonTypeSystem];
    self.forumButton.backgroundColor = [UIColor clearColor];
    self.forumButton.isNoNeedHighlight = YES;
    [self.forumButton addTarget:self
                         action:@selector(forumButtonDidTap)
               forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.forumButton];
    
    self.bottomShadowView = [UIView new];
    self.bottomShadowView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bottomShadowView];
    
    self.pauseImageView = [UIImageView new];
    self.pauseImageView.backgroundColor = [UIColor clearColor];
    self.pauseImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.pauseImageView.image = [UIImage imageNamed:DLConstIconPause];
    [self addSubview:self.pauseImageView];
    
    self.pauseButton = [DLHighlightedButton buttonWithType:UIButtonTypeSystem];
    self.pauseButton.backgroundColor = [UIColor clearColor];
    self.pauseButton.isNoNeedHighlight = YES;
    [self.pauseButton addTarget:self
                         action:@selector(pauseButtonDidTap)
               forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.pauseButton];
    
    self.playImageView = [UIImageView new];
    self.playImageView.backgroundColor = [UIColor clearColor];
    self.playImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.playImageView.image = [UIImage imageNamed:DLConstIconPlay];
    [self addSubview:self.playImageView];
    
    self.playButton = [DLHighlightedButton buttonWithType:UIButtonTypeSystem];
    self.playButton.backgroundColor = [UIColor clearColor];
    self.playButton.isNoNeedHighlight = YES;
    [self.playButton addTarget:self
                        action:@selector(playButtonDidTap)
              forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.playButton];
    
    self.fullScreenImageView = [UIImageView new];
    self.fullScreenImageView.backgroundColor = [UIColor clearColor];
    self.fullScreenImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.fullScreenImageView.image = [UIImage imageNamed:DLConstIconFullScreen];
    [self addSubview:self.fullScreenImageView];
    
    self.fullScreenButton = [DLHighlightedButton buttonWithType:UIButtonTypeSystem];
    self.fullScreenButton.backgroundColor = [UIColor clearColor];
    self.fullScreenButton.isNoNeedHighlight = YES;
    [self.fullScreenButton addTarget:self
                              action:@selector(fullScreenButtonDidTap)
                    forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.fullScreenButton];
    
    self.shrinkImageView = [UIImageView new];
    self.shrinkImageView.backgroundColor = [UIColor clearColor];
    self.shrinkImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.shrinkImageView.image = [UIImage imageNamed:DLConstIconShrink];
    [self addSubview:self.shrinkImageView];
    
    self.shrinkButton = [DLHighlightedButton buttonWithType:UIButtonTypeSystem];
    self.shrinkButton.backgroundColor = [UIColor clearColor];
    self.shrinkButton.isNoNeedHighlight = YES;
    [self.shrinkButton addTarget:self
                          action:@selector(shrinkButtonDidTap)
                forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.shrinkButton];
    
    self.playerSlider = [UISlider new];
    self.playerSlider.value = 0;
    self.playerSlider.tintColor = [UIColor cyanColor];
    [self.playerSlider addTarget:self
                          action:@selector(sliderValueChanged:)
                forControlEvents:UIControlEventValueChanged];
    [self addSubview:self.playerSlider];
    
    self.currentTimeLabel = [UILabel new];
    self.currentTimeLabel.backgroundColor = [UIColor clearColor];
    self.currentTimeLabel.font = BASE_FONT(11);
    self.currentTimeLabel.textColor = [UIColor whiteColor];
    self.currentTimeLabel.textAlignment = NSTextAlignmentCenter;
    self.currentTimeLabel.text = @"00:00";
    [self addSubview:self.currentTimeLabel];
    
    self.playTimeLabel = [UILabel new];
    self.playTimeLabel.backgroundColor = [UIColor clearColor];
    self.playTimeLabel.font = BASE_FONT(11);
    self.playTimeLabel.textColor = [UIColor whiteColor];
    self.playTimeLabel.textAlignment = NSTextAlignmentCenter;
    self.playTimeLabel.text = @"99:99";
    [self addSubview:self.playTimeLabel];
    
    self.captionLabel = [UILabel new];
    self.captionLabel.backgroundColor = [UIColor clearColor];
    self.captionLabel.font = BASE_FONT(11);
    self.captionLabel.textColor = [UIColor whiteColor];
    self.captionLabel.textAlignment = NSTextAlignmentCenter;
    self.captionLabel.numberOfLines = 5;
    self.captionLabel.shadowColor = [UIColor blackColor];
    self.captionLabel.shadowOffset = CGSizeMake(2, 2);
    self.captionLabel.text = @ "";
    [self addSubview:self.captionLabel];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                                action:@selector(hideOrShowControl)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tapGesture];
}

- (void)addGradientToView {
    
    self.topGradientLayer = [CAGradientLayer layer];
    self.topGradientLayer.colors = @[(id)[[UIColor blackColor] colorWithAlphaComponent:0.9].CGColor, (id)[UIColor clearColor].CGColor];
    self.topGradientLayer.locations = @[@0.0, @1.0];
    [self.topShadowView.layer addSublayer:self.topGradientLayer];
    
    self.bottomGradientLayer = [CAGradientLayer layer];
    self.bottomGradientLayer.colors = @[(id)[UIColor clearColor].CGColor, (id)[[UIColor blackColor] colorWithAlphaComponent:0.9].CGColor];
    self.bottomGradientLayer.locations = @[@0.0, @1.0];
    [self.bottomShadowView.layer addSublayer:self.bottomGradientLayer];
}

- (void)setMediaPlayerItem:(PTMediaPlayerItem *)mediaPlayerItem {
    [self removePlayerItemObserver];
    _mediaPlayerItem = mediaPlayerItem;
    [self addPlayerItemObserver];
}

- (void)addPlayerItemObserver {
    if (self.isMediaPlayer) {
        [self.mediaPlayerItem addObserver:self
                               forKeyPath:kMediaPlayerItemLocalTimeKeyPath
                                  options:NSKeyValueObservingOptionNew
                                  context:nil];
        [self.mediaPlayerItem addObserver:self
                               forKeyPath:kMediaPlayerItemLocalDurationKeyPath
                                  options:NSKeyValueObservingOptionNew
                                  context:nil];
//        [[self movieCurrentTime] addObserver:self
//                               forKeyPath:kMediaPlayerItemLocalDurationKeyPath
//                                  options:NSKeyValueObservingOptionNew
//                                  context:nil];
        
    } else {
        [self.playerItem addObserver:self
                          forKeyPath:kPlayerItemDurationKeyPath
                             options:NSKeyValueObservingOptionNew
                             context:nil];
    }
}

- (void)removePlayerItemObserver {
    
    if (self.isMediaPlayer) {
        @try{
            [self.mediaPlayerItem removeObserver:self
                                      forKeyPath:kMediaPlayerItemLocalTimeKeyPath];
        }@catch(id anException){
            // 設定していない場合でも落ちないための処理のため、Exceptionは無視する
        }
        @try{
            [self.mediaPlayerItem removeObserver:self
                                      forKeyPath:kMediaPlayerItemLocalDurationKeyPath];
        }@catch(id anException){
            // 設定していない場合でも落ちないための処理のため、Exceptionは無視する
        }
    } else {
        @try{
            [self.playerItem removeObserver:self
                                 forKeyPath:kPlayerItemDurationKeyPath];
        }@catch(id anException){
            // 設定していない場合でも落ちないための処理のため、Exceptionは無視する
        }
    }
}

- (void)updateFrame {
    
    self.topShadowView.frame = CGRectMake(0, 0, self.width, self.height / 3);
    self.topGradientLayer.frame = self.topShadowView.bounds;
    
    self.bottomShadowView.frame = CGRectMake(0, self.height / 3 * 2, self.width, self.height / 3);
    self.bottomGradientLayer.frame = self.bottomShadowView.bounds;
    
    self.backImageView.width = self.backImageView.height = 20;
    self.backButton.width = self.backButton.height = 40;
    self.forumImageView.width = self.forumImageView.height = 20;
    self.forumButton.width = self.forumButton.height = 40;
    self.pauseImageView.width = self.pauseImageView.height = 15;
    self.pauseButton.width = self.pauseButton.height = 40;
    self.playImageView.width = self.playImageView.height = 15;
    self.playButton.width = self.playButton.height = 40;
    self.fullScreenImageView.width = self.fullScreenImageView.height = 15;
    self.fullScreenButton.width = self.fullScreenButton.height = 40;
    self.shrinkImageView.width = self.shrinkImageView.height = 15;
    self.shrinkButton.width = self.shrinkButton.height = 40;
    
    self.backButton.x = 10;
    self.backButton.y = 10;
    self.backImageView.center = self.backButton.center;
    self.forumButton.x = self.width - self.forumButton.width - 10;
    self.forumButton.y = 10;
    self.forumImageView.center = self.forumButton.center;
    self.pauseButton.x = 10;
    self.pauseButton.y = self.height - self.pauseButton.height - 10;
    self.pauseImageView.center = self.pauseButton.center;
    self.playButton.x = 10;
    self.playButton.y = self.height - self.playButton.height - 10;
    self.playImageView.center = self.playButton.center;
    self.fullScreenButton.x = self.width - self.fullScreenButton.width - 10;
    self.fullScreenButton.y = self.height - self.fullScreenButton.height - 10;
    self.fullScreenImageView.center = self.fullScreenButton.center;
    self.shrinkButton.x = self.width - self.shrinkButton.width - 10;
    self.shrinkButton.y = self.height - self.shrinkButton.height - 10;
    self.shrinkImageView.center = self.shrinkButton.center;
    
    self.currentTimeLabel.frame = CGRectMake(self.playButton.maxX, 0, 50, 30);
    self.currentTimeLabel.midY = self.playButton.midY;
    self.playTimeLabel.frame = CGRectMake(self.fullScreenButton.minX - 50, 0, 50, 30);
    self.playTimeLabel.midY = self.fullScreenButton.midY;
    
    if (!self.isFullScreen && self.isHiddenControl) {
        self.playerSlider.frame = CGRectMake(-2, self.height - 16, self.width + 4, 30);
    } else {
        self.playerSlider.frame = CGRectMake(self.currentTimeLabel.maxX, 0, self.playTimeLabel.minX - self.currentTimeLabel.maxX, 30);
        self.playerSlider.midY = self.currentTimeLabel.midY;

    }
    self.captionLabel.frame = CGRectMake(0, self.playerSlider.frame.origin.y - 50, self.width , 50);
}

- (void)pauseButtonDidTap {
    
    if ([self.delegate respondsToSelector:@selector(pauseButtonDidTap)]) {
        [self.delegate pauseButtonDidTap];
    }
}

- (void)playButtonDidTap {
    
    if ([self.delegate respondsToSelector:@selector(playButtonDidTap)]) {
        [self.delegate playButtonDidTap];
    }
}

- (void)fullScreenButtonDidTap {
    
    if ([self.delegate respondsToSelector:@selector(fullScreenButtonDidTap)]) {
        [self.delegate fullScreenButtonDidTap];
    }
}

- (void)shrinkButtonDidTap {
    
    if ([self.delegate respondsToSelector:@selector(shrinkButtonDidTap)]) {
        [self.delegate shrinkButtonDidTap];
    }
}

- (void)backButtonDidTap {
    
    if ([self.delegate respondsToSelector:@selector(backButtonDidTap)]) {
        [self.delegate backButtonDidTap];
    }
}

- (void)forumButtonDidTap {
    
    if ([self.delegate respondsToSelector:@selector(forumButtonDidTap)]) {
        [self.delegate forumButtonDidTap];
    }
}

- (void)pauseButtonHidden:(BOOL)isHidden {
    self.pauseImageView.hidden = isHidden;
    self.pauseButton.hidden = isHidden;
}

- (void)playButtonHidden:(BOOL)isHidden {
    self.playImageView.hidden = isHidden;
    self.playButton.hidden = isHidden;
}

- (void)fullScreenButtonHidden:(BOOL)isHidden {
    self.fullScreenImageView.hidden = isHidden;
    self.fullScreenButton.hidden = isHidden;
}

- (void)shrinkButtonHidden:(BOOL)isHidden {
    self.shrinkImageView.hidden = isHidden;
    self.shrinkButton.hidden = isHidden;
}

- (void)topShadowViewHidden:(BOOL)isHidden {
    self.topShadowView.hidden = isHidden;
}

- (void)backButtonHidden:(BOOL)isHidden {
    self.backImageView.hidden = isHidden;
    self.backButton.hidden = isHidden;
}

- (void)forumButtonHidden:(BOOL)isHidden {
    self.forumImageView.hidden = isHidden;
    self.forumButton.hidden = isHidden;
}

- (void)currenTimeLabelHidden:(BOOL)isHidden {
    self.currentTimeLabel.hidden = isHidden;
}

- (void)playTimeLabelHidden:(BOOL)isHidden {
    self.playTimeLabel.hidden = isHidden;
}

- (void)captionViewHidden:(BOOL)isHidden {
    self.captionLabel.hidden = isHidden;
}

- (void)bottomShadowViewHidden:(BOOL)isHidden {
    self.bottomShadowView.hidden = isHidden;
}

- (void)sliderHidden:(BOOL)isHidden {
    self.playerSlider.hidden = isHidden;
}

- (void)hideOrShowControl {
    
    if (self.isAnimating) {
        return;
    }
    
    self.isAnimating = YES;
    
    self.isHiddenControl = !self.isHiddenControl;
    [self updateHiddenViewsWithCompletion:^{
        self.isAnimating = NO;
    }];
}

- (void)updateHiddenViewsWithCompletion:(void (^)(void))completion {
    
    if (self.isHiddenControl) {
        
        self.playerSlider.userInteractionEnabled = self.isFullScreen;
        [UIView animateWithDuration:0.2
                         animations:^{
                             
                             if (self.isFullScreen) {
                                 [self.playerSlider setThumbImage:nil forState:UIControlStateNormal];
                             } else {
                                 [self.playerSlider setThumbImage:[[UIImage alloc]init] forState:UIControlStateNormal];
                             }
                             
                             [self hideControlForUpdate];
                             [self updateFrame];
                         }completion:^(BOOL finished) {
                             if (NOT_NULL(completion)) {
                                 completion();
                             }
                         }];
        
    } else {
        
        self.playerSlider.userInteractionEnabled = YES;
        [UIView animateWithDuration:0.2
                         animations:^{
                             [self showControlForUpdate];
                             [self updateFrame];
                         }completion:^(BOOL finished) {
                             
                             if (self.isLoaded) {
                                 [self.playerSlider setThumbImage:nil forState:UIControlStateNormal];
                             } else {
                                 [self.playerSlider setThumbImage:[[UIImage alloc]init] forState:UIControlStateNormal];
                             }
                             
                             if (NOT_NULL(completion)) {
                                 completion();
                             }
                         }];
    }
}

- (void)hideControlForUpdate {
    [self pauseButtonHidden:YES];
    [self playButtonHidden:YES];
    [self bottomShadowViewHidden:YES];
    [self topShadowViewHidden:YES];
    [self backButtonHidden:YES];
    [self forumButtonHidden:YES];
    [self fullScreenButtonHidden:YES];
    [self shrinkButtonHidden:YES];
    [self currenTimeLabelHidden:YES];
    [self playTimeLabelHidden:YES];
    [self captionViewHidden:YES];
    [self sliderHidden:(!self.isLoaded || self.isFullScreen)];
}

- (void)showControlForUpdate {
    [self topShadowViewHidden:NO];
    [self bottomShadowViewHidden:NO];
    [self pauseButtonHidden:(!self.isPlaying && !self.isLoaded)];
    [self playButtonHidden:(self.isPlaying && !self.isLoaded)];
    [self currenTimeLabelHidden:NO];
    [self playTimeLabelHidden:NO];
    [self captionViewHidden:NO];
    [self backButtonHidden:NO];
    [self forumButtonHidden:NO];
    [self fullScreenButtonHidden:NO];
    [self shrinkButtonHidden:NO];
    [self sliderHidden:NO];

    //    [self currenTimeLabelHidden:!self.isLoaded];
    //    [self playTimeLabelHidden:!self.isLoaded];
    //    [self backButtonHidden:!self.isFullScreen];
    //    [self forumButtonHidden:!self.isFullScreen];
    //    [self fullScreenButtonHidden:(self.isFullScreen || !self.isLoaded)];
    //    [self shrinkButtonHidden:!self.isFullScreen];
    //    [self sliderHidden:!self.isLoaded];
}

- (void)sliderValueChanged:(UISlider *)slider{
    
    CMTime seekTime = CMTimeMakeWithSeconds(slider.value, NSEC_PER_SEC);
    
    if ([self.delegate respondsToSelector:@selector(sliderChangedWithSeekTime:)]) {
        [self.delegate sliderChangedWithSeekTime:seekTime];
    }
}

- (void)updateDuration {
    float duration = [self movieDuration];
    self.playTimeLabel.text = [NSString formatSecTimeToString:duration];
    self.playerSlider.maximumValue = duration;
}

-(Float64)movieDuration {
    if (NOT_NULL(self.playerItem) || NOT_NULL(self.mediaPlayerItem))
    {
        Float64 duration = self.isMediaPlayer ? CMTimeGetSeconds(self.mediaPlayerItem.duration) : CMTimeGetSeconds(self.playerItem.duration);
        // durationがNaNで取得されることがあるので、ここで判定
        if (isnan(duration) || duration < 0) {
            duration = 0;
        }
        return duration;
    } else {
        return 0;
    }
}

-(Float64)movieCurrentTime {
    if (NOT_NULL(self.playerItem) || NOT_NULL(self.mediaPlayerItem))
    {
        Float64 currentTime = self.isMediaPlayer ? CMTimeGetSeconds(self.mediaPlayerItem.currentTime) : CMTimeGetSeconds(self.playerItem.currentTime);
        // currentTimeがNaNで取得されることがあるので、ここで判定
        if (isnan(currentTime) || currentTime < 0) {
            currentTime = 0;
        }
        return currentTime;
    } else {
        return 0;
    }
}

@end
