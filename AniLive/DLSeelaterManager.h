//
//  DLSeelaterManager.h
//  AniLive
//
//  Created by mac on 2017/1/18.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DLForum.h"

@interface DLSeelaterManager : NSObject

+ (instancetype)sharedInstance;

+ (void)loadSeelaterDatafromServerCompletion:(void(^)(BOOL success, NSArray *seelaterData))completetion;

+ (void)addSeelaterWithForum:(DLForum *)forum Completion:(void(^)(BOOL success))completetion;

+ (void)removeSeelaterWithForum:(DLForum *)forum Completion:(void(^)(BOOL success))completetion;

+ (void)refreshLocalSeelaterDataWithSeelaterData:(NSArray *)seelaterData;

+ (BOOL)checkFroumIfInSeelater:(DLForum *)forum;

@end
