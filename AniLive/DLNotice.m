//
//  DLNotice.m
//  AniLive
//
//  Created by volcano on 12/22/16.
//  Copyright © 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLNotice.h"
#import "RLMRealm_Dynamic.h"

static NSString const *kDLNoticeKey_thumbImage  = @"thumb_image";
static NSString const *kDLNoticeKey_transition  = @"transition";
static NSString const *kDLNoticeKey_itemId  = @"item_id";
static NSString const *kDLNoticeKey_view  = @"view";
const int kDLNoticeTransactionView_PlayMovie = 0;
const int kDLNoticeTransactionView_Forum = 1;
const int kDLNoticeTransactionView_Work = 2;


@implementation DLNotice

+ (NSString *)primaryKey {
    return @"id";
}

+ (DLNotice *)initWithAttributes:(NSDictionary *)attributes {
    DLNotice* notice = [[DLNotice alloc] init];
    
    notice.title = [[attributes objectForKey:@"aps"] objectForKey:@"alert"];
    notice.createdAt = [NSDate date];
    notice.isRead = NO;
    notice.thumbImage = [attributes objectForKey:kDLNoticeKey_thumbImage];
    notice.transitionItemId = [[attributes objectForKey:kDLNoticeKey_transition][kDLNoticeKey_itemId] longValue];
    notice.transitionView = [[attributes objectForKey:kDLNoticeKey_transition][kDLNoticeKey_view] longValue];
    
    return notice;
}

+ (NSMutableArray *) getAll {
    RLMRealm * realm = RLMRealm.defaultRealm;
    RLMResults<DLNotice *> *results = [[DLNotice allObjects] sortedResultsUsingKeyPath:@"id" ascending:NO];
    NSMutableArray *notices = [NSMutableArray array];
    for (DLNotice *notice in results) {
        [notices addObject: notice];
    }
    return notices;
}

+ (void)changeToReadAll {
    NSMutableArray *notices = [DLNotice getAll];
    RLMRealm *realm = RLMRealm.defaultRealm;
    [realm beginWriteTransaction];
    for (DLNotice *notice in notices) {
        notice.isRead = YES;
    }
    [realm commitWriteTransaction];
}

+ (BOOL)isExistYetRead {
    RLMRealm *realm = RLMRealm.defaultRealm;
    RLMResults<DLNotice *> *results = [DLNotice objectsWhere:@"isRead == false"];
    return results.count > 0;
}



@end
