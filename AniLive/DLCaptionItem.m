//
//  DLCaptionItem.m
//  AniLive
//
//  Created by HisashiOkura on 2017/01/17.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLCaptionItem.h"

@interface DLCaptionItem()

@property(readwrite, nonatomic, strong) NSString *beginTimeMs;
@property(readwrite, nonatomic, strong) NSString *endTimeMs;
@property(readwrite, nonatomic, strong) NSString *style;
@property(readwrite, nonatomic, strong) NSString *captionMessage;

@end

@implementation DLCaptionItem

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.beginTimeMs = CHECK_NULL_DATA_STRING(attributes[@"begintime_ms"]);
    self.endTimeMs = CHECK_NULL_DATA_STRING(attributes[@"endtime_ms"]);
    self.style = CHECK_NULL_DATA_STRING(attributes[@"style"]);
    self.captionMessage = CHECK_NULL_DATA_STRING(attributes[@"caption_message"]);
    return self;
    
}

@end

