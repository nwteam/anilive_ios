//
//  DLPlayerOptionBarView.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/11/20.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPlayerOptionBarView.h"
#import "DLHighlightedButton.h"

static const NSInteger kForumListButtonTag = 178;
static const NSInteger kBackButtonTag = 278;
static const NSInteger kNextButtonTag = 378;
static const NSInteger kPostCommentButtonTag = 478;
static const NSInteger kOtherButtonTag = 578;

@interface DLPlayerOptionBarView ()

@property (nonatomic, assign) DLPlayerOptionBarViewType type;

// labels
@property (nonatomic, strong) UILabel *titleLabel;

// Buttons
@property (nonatomic, strong) DLHighlightedButton *forumListButton;
@property (nonatomic, strong) DLHighlightedButton *nextButton;
@property (nonatomic, strong) DLHighlightedButton *backButton;
@property (nonatomic, strong) DLHighlightedButton *postCommentButton;
@property (nonatomic, strong) DLHighlightedButton *otherButton;

@end

@implementation DLPlayerOptionBarView

- (instancetype)initWithType:(DLPlayerOptionBarViewType)type {
    self = [super init];
    if (self) {
        self.type = type;
        [self initView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
}

#pragma mark - Private Methods

- (void)initView {
    self.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkBlue];
    
    switch (self.type) {
        case DLPlayerOptionViewForumDetail:
        {
            // TODO: メソッドを分け可読性を高めたい
            self.forumListButton = [self getOptionButton];
            self.forumListButton.tag = kForumListButtonTag;
            [self.forumListButton setImage:[UIImage imageNamed:DLConstIconForumList]
                                  forState:UIControlStateNormal];
            [self addSubview:self.forumListButton];
            
            self.backButton = [self getOptionButton];
            self.backButton.tag = kBackButtonTag;
            [self.backButton setImage:[UIImage imageNamed:DLConstIconForumPrev]
                             forState:UIControlStateNormal];
            [self addSubview:self.backButton];
            
            self.nextButton = [self getOptionButton];
            self.nextButton.tag = kNextButtonTag;
            [self.nextButton setImage:[UIImage imageNamed:DLConstIconForumNext]
                             forState:UIControlStateNormal];
            [self addSubview:self.nextButton];
            
            self.postCommentButton = [self getOptionButton];
            self.postCommentButton.tag = kPostCommentButtonTag;
            [self.postCommentButton setImage:[UIImage imageNamed:DLConstIconForumPostComment]
                                    forState:UIControlStateNormal];
            [self addSubview:self.postCommentButton];
            
            self.otherButton = [self getOptionButton];
            self.otherButton.tag = kOtherButtonTag;
            [self.otherButton setImage:[UIImage imageNamed:DLConstIconForumOther]
                              forState:UIControlStateNormal];
            [self addSubview:self.otherButton];
            
            break;
        }
        case DLPlayerOptionViewForumList:
        {
            self.backButton = [self getOptionButton];
            self.backButton.tag = kBackButtonTag;
            [self.backButton setImage:[UIImage imageNamed:DLConstIconForumPrev]
                             forState:UIControlStateNormal];
            [self addSubview:self.backButton];
            
            [self createTitleLabelWithFont:BASE_FONT_BOLD(16)];
            
            break;
        }
        case DLPlayerOptionViewCreateForum:
        {
            self.backButton = [self getOptionButton];
            self.backButton.tag = kBackButtonTag;
            [self.backButton setImage:[UIImage imageNamed:DLConstIconForumPrev]
                             forState:UIControlStateNormal];
            [self addSubview:self.backButton];
            
            [self createTitleLabelWithFont:BASE_FONT_BOLD(16)];
            
            break;
        }
    }
    
}

- (void)updateFrame {
    
    switch (self.type) {
        case DLPlayerOptionViewForumDetail:
        {
            self.forumListButton.x = 0;
            self.backButton.x = kPlayerOptionViewHeight;
            self.nextButton.x = kPlayerOptionViewHeight * 2;
            self.postCommentButton.x = self.width - (kPlayerOptionViewHeight * 2);
            self.otherButton.x = self.width - kPlayerOptionViewHeight;
            
            break;
        }
        case DLPlayerOptionViewForumList:
        {
            self.backButton.x = 0;
            self.titleLabel.frame = CGRectMake(0, self.height - 40, self.width, 40);
            
            break;
        }
        case DLPlayerOptionViewCreateForum:
        {
            self.backButton.x = 0;
            self.titleLabel.frame = CGRectMake(0, self.height - 40, self.width, 40);
            
            break;
        }
    }
    
}

- (DLHighlightedButton *)getOptionButton {
    DLHighlightedButton *button = [DLHighlightedButton buttonWithType:UIButtonTypeCustom];
    button.isNoNeedHighlight = YES;
    button.backgroundColor = [UIColor clearColor];
    [button addTarget:self
               action:@selector(buttonDidTap:)
     forControlEvents:UIControlEventTouchUpInside];
    button.width = kPlayerOptionViewHeight;
    button.height = kPlayerOptionViewHeight;
    button.y = 0;
    return button;
}

- (void)buttonDidTap:(UIButton *)button {
    
    switch (button.tag) {
        case kForumListButtonTag:
        {
            NSLog(@"DLPlayerOptionBarView: forum list");
            if ([self.delegate respondsToSelector:@selector(playerOptionBarViewForumListButtonDidTap:)]) {
                [self.delegate playerOptionBarViewForumListButtonDidTap:self.type];
            }
            break;
        }
            
        case kBackButtonTag:
        {
            NSLog(@"DLPlayerOptionBarView: back");
            if ([self.delegate respondsToSelector:@selector(playerOptionBarViewBackButtonDidTap:)]) {
                [self.delegate playerOptionBarViewBackButtonDidTap:self.type];
            }
            break;
        }
            
        case kNextButtonTag:
        {
            NSLog(@"DLPlayerOptionBarView: next");
            if ([self.delegate respondsToSelector:@selector(playerOptionBarViewNextButtonDidTap:)]) {
                [self.delegate playerOptionBarViewNextButtonDidTap:self.type];
            }
            break;
        }
            
        case kPostCommentButtonTag:
        {
            NSLog(@"DLPlayerOptionBarView: post comment");
            if ([self.delegate respondsToSelector:@selector(playerOptionBarViewPostCommentButtonDidTap:)]) {
                [self.delegate playerOptionBarViewPostCommentButtonDidTap:self.type];
            }
            break;
        }
            
        case kOtherButtonTag:
        {
            NSLog(@"DLPlayerOptionBarView: other");
            if ([self.delegate respondsToSelector:@selector(playerOptionBarViewOtherButtonDidTap:)]) {
                [self.delegate playerOptionBarViewOtherButtonDidTap:self.type];
            }
            break;
        }
        default:
            break;
    }
}

- (void)createTitleLabelWithFont:(UIFont *)font {
    self.titleLabel = [UILabel new];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = font;
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self addSubview:self.titleLabel];
    
    // XXX: PublicなsetTitleを生やすか悩む
    switch (self.type) {
        case DLPlayerOptionViewForumDetail:
            break;
        case DLPlayerOptionViewForumList:
        {
            self.titleLabel.text = @"Forum List";
            break;
        }
        case DLPlayerOptionViewCreateForum:
        {
            self.titleLabel.text = Localizer(@"movie_post_topick");
            break;
        }
    }
}

@end
