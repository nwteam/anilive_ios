//
//  DLAppFrameManager.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLAppFrameManager.h"

typedef NS_ENUM(NSUInteger, PhoneInch) {
    PhoneInch320 = 320,
    PhoneInch375 = 375,
    PhoneInch414 = 414
};

@interface DLAppFrameManager ()

@property (readwrite, nonatomic, assign) DLAppFrameType appFrameType;

@property (readwrite, nonatomic, assign) CGRect applicationFrame;

@property (nonatomic, assign) UIInterfaceOrientation currentOrientation;

@end

@implementation DLAppFrameManager

+ (instancetype)sharedInstance {
    static DLAppFrameManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[DLAppFrameManager alloc] init];
        [manager initConfig];
    });
    
    return manager;
}

- (void)initConfig {
    
    self.updateEnabled = YES;
    
    [self updateApplicationFrame:nil];
    
    //デバイスの回転検知
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateApplicationFrame:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    
    //StatusBarFrame変更検知
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateApplicationFrame:)
                                                 name:UIApplicationDidChangeStatusBarFrameNotification
                                               object:nil];
    
    //スプリット検知
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateApplicationFrame:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}

#pragma mark - Public Methods

- (void)updateApplicationFrame {
    [self updateApplicationFrame:nil];
}

- (void)setCurrentOrientationForce:(UIInterfaceOrientation)orientation {
    
    if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
        SEL selector = NSSelectorFromString(@"setOrientation:");
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:[UIDevice currentDevice]];
        int val = orientation;
        [invocation setArgument:&val atIndex:2];
        [invocation invoke];
    }
}

- (NSInteger)getNumberOfCellPerRowForCatalog {
    
    return 3;
}

#pragma mark - Private Methods

- (void)updateApplicationFrame:(NSNotification*)notification {
    
    if (!self.updateEnabled) {
        return;
    }
    
    //変更されていない時に値を変更しないように制御
    CGRect currentAppFrame = DL_APPFRAME_RECT;
    
    if (notification.name == UIDeviceOrientationDidChangeNotification) {
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        
        if (orientation != self.currentOrientation) {
            [self checkAppFrameType:currentAppFrame];
            self.applicationFrame = currentAppFrame;
        }
        
        self.currentOrientation = orientation;
        
    } else if (!CGRectEqualToRect(self.applicationFrame, currentAppFrame)) {
        [self checkAppFrameType:currentAppFrame];
        self.applicationFrame = currentAppFrame;
    }
}

- (void)checkAppFrameType:(CGRect)rect {
    
    if (!IS_IPAD) {
        self.appFrameType = DLAppFrameTypeOther;
        return;
    }
    
    CGRect fullScreenRect = CGRectMake(0, CGRectGetMinY(DL_APPFRAME_RECT), CGRectGetWidth(SCREEN_RECT), CGRectGetHeight(DL_APPFRAME_RECT));
    
    //デバイス向き判定
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        
        //フルスクリーン判定
        if (CGRectEqualToRect(rect, fullScreenRect)) {
            self.appFrameType = DLAppFrameTypePortraitFull;
        }
        else {
            
            //５０％以下判定
            if (rect.size.width < fullScreenRect.size.width/2) {
                
                self.appFrameType = DLAppFrameTypePortraitSplitRight;
            }
            else {
                
                self.appFrameType = DLAppFrameTypePortraitSplitLeft;
            }
        }
    }
    else {
        
        //フルスクリーン判定
        if (CGRectEqualToRect(rect, fullScreenRect)) {
            self.appFrameType = DLAppFrameTypeLandscapeFull;
        }
        else {
            
            //５０％以下判定
            if (rect.size.width < fullScreenRect.size.width/2) {
                
                //iPhone判定
                if ([self isPhoneWidth:rect.size.width]) {
                    self.appFrameType = DLAppFrameTypeLandscapeSplitRight;
                }
                else {
                    self.appFrameType = DLAppFrameTypeLandscapeSplitCenter;
                }
            }
            else {
                
                self.appFrameType = DLAppFrameTypeLandscapeSplitLeft;
            }
        }
    }
    
    NSLog(@"appFrameType: %lu", (unsigned long)self.appFrameType);
}

- (BOOL)isPhoneWidth:(CGFloat)width {
    
    if (width == PhoneInch320) {
        return YES;
    }
    else if (width == PhoneInch375){
        return YES;
    }
    else if (width == PhoneInch414){
        return YES;
    }
    else {
        return NO;
    }
}

#pragma mark - Dealloc

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIDeviceOrientationDidChangeNotification
                                                  object:nil];
    
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidChangeStatusBarFrameNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
}

@end
