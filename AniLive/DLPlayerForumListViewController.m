//
//  DLPlayerForumListViewController.m
//  AniLive
//
//  Created by isaoeka on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPlayerForumListViewController.h"
#import "DLAppFrameManager.h"
#import "DLAPIHelper.h"
#import "DLPlayerOptionBarView.h"
#import "DLForumTableView.h"
#import "DLForum.h"

@interface DLPlayerForumListViewController ()

@property (nonatomic, strong) DLForumTableView *forumTableView;

@end

@implementation DLPlayerForumListViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

#pragma mark - Override Methods

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Private Methods

- (void)initView {
    self.view.backgroundColor = [UIColor greenColor];
    
#pragma mark Player Option Bar View

    self.playerOptionBarView = [[DLPlayerOptionBarView alloc] initWithType:DLPlayerOptionViewForumList];
    [self.view addSubview:self.playerOptionBarView];
    
#pragma mark Forum Table View
    
    self.forumTableView = [DLForumTableView new];
    [self.view addSubview:self.forumTableView];

}

- (void)updateFrame {
#pragma mark Player Option Bar View
    self.playerOptionBarView.frame = CGRectMake(0,
                                                0,
                                                self.view.width,
                                                kPlayerOptionViewHeight);
    
#pragma mark Forum Table View
    self.forumTableView.frame = CGRectMake(0,
                                           self.playerOptionBarView.maxY,
                                           self.view.width,
                                           self.view.height - self.playerOptionBarView.maxY);
    
}

#pragma mark - Public Methods

- (void)setForums:(NSMutableArray<DLForum *> *)forums {
    [self.forumTableView loadForums:forums];
}

@end
