//
//  DLForumViewController.h
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/10/17.
//  Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLForum.h"
#import "DLNavigationBarView.h"

@class DLNavigationBarView;
@class DLForumHeaderView;

@interface DLForumViewController : UIViewController

@property(nonatomic, strong) DLForum *forum;

- (void)editComment:(DLComment *)comment;

- (void)refreshForum;

@end
