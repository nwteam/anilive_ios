//
//  DLCatalogPopularCell.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/10/01.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLSuperCollectionViewCell.h"
#import "DLWork.h"

const static CGFloat kCatalogCellSpaceMargin = 12;

@interface DLCatalogPopularCell : DLSuperCollectionViewCell

+ (CGSize)getCellSizeWithParantWidth:(CGFloat)parantWidth;

- (void)setCellData:(DLWork *)workData;

@end
