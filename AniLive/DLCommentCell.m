//
//  DLCommentCell.m
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/10/24.
//  Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLCommentCell.h"

const static CGFloat kCellHeight = 50;

@interface DLCommentCell ()
@property (weak, nonatomic) IBOutlet UIView *commentBackView;
@property(weak, nonatomic) IBOutlet UILabel *lbBody;
@property(weak, nonatomic) IBOutlet UILabel *lbTimeStamp;
@end

@implementation DLCommentCell

- (void)setComment:(DLComment *)comment {
    _comment = comment;
    if (!comment) {
        return;
    }
    NSString *user = comment.userName;
    if (comment.userName) {
        user =  [comment.userName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    NSString *newStr = [NSString stringWithFormat:@"%@: %@",user,comment.body];
    NSMutableAttributedString *newText = [[NSMutableAttributedString alloc] initWithString:newStr];
    [newText addAttribute:NSForegroundColorAttributeName
                    value:[UIColor colorWithHex:DLConstColorCodeYellow]
                    range:NSMakeRange(0, user.length)];
    self.lbBody.attributedText = newText;
    // TODO 以下どこかに共通関数として切り出す。
    NSTimeInterval interval = -[comment.created timeIntervalSinceNow];
    int showInt = 0;
    NSString *format = nil;
    if (interval < 60) {
        showInt = (int) interval;
        format = @"%d sec";
    } else if (interval < 3600) {
        showInt = (int) (interval / 60);
        format = @"%d min";
    } else if (interval < 86400) {
        showInt = (int) (interval / 3600);
        format = @"%d hour";
    } else if (interval < 31536000) {
        showInt = (int) (interval / 86400);
        format = @"%d day";
    } else {
        showInt = (int) (interval / 31536000);
        format = @"%d year";
    }
    self.lbTimeStamp.text = [NSString stringWithFormat:format, showInt];
    self.commentBackView.layer.cornerRadius = 8;
}

+ (UINib *)getDefaultNib {
    return [UINib nibWithNibName:@"DLCommentCell" bundle:nil];
}

+ (CGFloat)getCellHeight {
    return kCellHeight;
}

@end
