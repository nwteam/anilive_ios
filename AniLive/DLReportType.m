//
//  DLReportType.m
//  AniLive
//
//  Created by mac on 2017/1/9.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLReportType.h"

@interface DLReportType ()

@property(readwrite, nonatomic, strong) NSNumber *categoryID;
@property(readwrite, nonatomic, strong) NSString *name;

@end

@implementation DLReportType

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.categoryID = CHECK_NULL_DATA_NUMBER(attributes[@"category_id"]);
    self.name = CHECK_NULL_DATA_STRING(attributes[@"name"]);
    return self;
    
}

@end
