//
//  DLSeelaterManager.m
//  AniLive
//
//  Created by mac on 2017/1/18.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLSeelaterManager.h"
#import "DLAPIHelper.h"
#import "DLForumData.h"

@implementation DLSeelaterManager

+ (instancetype)sharedInstance {
    static DLSeelaterManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[DLSeelaterManager alloc] init];
        [manager initConfig];
    });
    
    return manager;
}

- (void)initConfig {
    
}

+ (void)loadSeelaterDatafromServerCompletion:(void(^)(BOOL success, NSArray *seelaterData))completetion {
    [DLAPIHelper fetchSeeLaterDatacallback:^(NSMutableArray *forums) {
        if (forums && forums.count > 0) {
            completetion(YES,forums);
            [DLSeelaterManager refreshLocalSeelaterDataWithSeelaterData:forums];
        }
        else {
            completetion(NO,nil);
        }
        
    }];
}

+ (void)addSeelaterWithForum:(DLForum *)forum Completion:(void(^)(BOOL success))completetion {
    [DLAPIHelper addSeelaterDataWithForum:forum callback:^(BOOL success) {
        completetion(success);
        if (success) {
            RLMRealm *realm = [RLMRealm defaultRealm];
            DLForumData *data = [[DLForumData alloc] init];
            data.forumId = forum.forumId;
            
            [realm beginWriteTransaction];
            [realm addObject:data];
            [realm commitWriteTransaction];
        }
    }];
}

+ (void)removeSeelaterWithForum:(DLForum *)forum Completion:(void(^)(BOOL success))completetion {
    [DLAPIHelper removeSeelaterDataWithForum:forum callback:^(BOOL success) {
        completetion(success);
        if (success) {
            RLMRealm *realm = [RLMRealm defaultRealm];
            RLMResults *allLoacalData = [DLForumData allObjects];
            for (NSInteger i = 0; i < allLoacalData.count; i ++) {
                DLForumData *data = allLoacalData[i];
                if ([data.forumId integerValue] == [forum.forumId integerValue]) {
                    [realm beginWriteTransaction];
                    [realm deleteObject:data];
                    [realm commitWriteTransaction];
                }
            }
        }
    }];
}

+ (void)refreshLocalSeelaterDataWithSeelaterData:(NSArray *)seelaterData {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteObjects:[DLForumData allObjects]];
    for (NSInteger i = 0; i < seelaterData.count; i ++) {
        DLForum *forum = seelaterData[i];
        DLForumData *data = [[DLForumData alloc] init];
        data.forumId = forum.forumId;
        [realm addObject:data];
    }
    [realm commitWriteTransaction];
}

+ (BOOL)checkFroumIfInSeelater:(DLForum *)forum {
    RLMResults *allLoacalData = [DLForumData allObjects];
    for (NSInteger i = 0; i < allLoacalData.count; i ++) {
        DLForumData *data = allLoacalData[i];
        if ([data.forumId integerValue] == [forum.forumId integerValue]) {
            return YES;
        }
    }
    return NO;
}

@end
