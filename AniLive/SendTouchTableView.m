//
//  SendTouchTableView.m
//  AniLive
//
//  Created by mac on 2017/1/9.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import "SendTouchTableView.h"

@implementation SendTouchTableView

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (self.superview) {
        [super.superview touchesBegan:touches withEvent:event];
    }
}

@end
