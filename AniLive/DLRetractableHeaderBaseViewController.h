//
//  DLRetractableHeaderBaseViewController.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/11/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DLRetractableHeaderDelegate;

@interface DLRetractableHeaderBaseViewController : UIViewController

@property (nonatomic, assign) CGPoint currentContentOffset;

@property (nonatomic, weak) id<DLRetractableHeaderDelegate> delegate;

- (void)setOffsetToBaseScrollView:(CGPoint)offset;

@end

@protocol DLRetractableHeaderDelegate <NSObject>

@optional
- (void)retractableScrollViewDidScroll:(UIScrollView *)scrollView;

@end
