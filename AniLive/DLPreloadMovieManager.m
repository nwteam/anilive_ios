//
//  DLPreloadMovieManager.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/10/05.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPreloadMovieManager.h"
#import "DLLiveMovieCell.h"
#import "DLNetworkManager.h"

@interface DLPreloadMovieManager ()

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *preloadIndexPathArray;

@end

@implementation DLPreloadMovieManager

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView
{
    self = [super init];
    if (self) {
        
        self.preloadIndexPathArray = [[NSMutableArray alloc]init];
        self.collectionView = collectionView;
    }
    return self;
}

- (void)setPreloadCells {
    
    if (![[DLNetworkManager sharedInstance] hasInternetConnection]) {
        [self removeAllPreloadCells];
        return;
    }
    
    NSArray *visibleCells = [NSArray arrayWithArray:self.collectionView.indexPathsForVisibleItems];
    NSIndexPath *lastIndexPath = [visibleCells lastObject];
    NSMutableArray *sameIndexPathArray = [[NSMutableArray alloc]init];
    
    for (NSIndexPath *indexPath in visibleCells) {
        
        // 関係無いセルはスルー
        if (![[self.collectionView cellForItemAtIndexPath:indexPath] isKindOfClass:[DLLiveMovieCell class]]) {
            continue;
        }
        
        DLLiveMovieCell *cell = (DLLiveMovieCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
        
        // 読み込み判定
        for (NSIndexPath *preloadIndexPath in self.preloadIndexPathArray) {
            
            if (indexPath.row == preloadIndexPath.row) {
                [sameIndexPathArray addObject:indexPath];
                continue;
            } else {
                [cell addPlayerBaseView];
            }
        }
        
        if (self.preloadIndexPathArray.count == 0) {
            [cell addPlayerBaseView];
        }
        
        // 再生判定
        UICollectionViewLayoutAttributes *attributes = [self.collectionView layoutAttributesForItemAtIndexPath:indexPath];
        
        if (self.collectionView.contentOffset.y >= self.collectionView.contentSize.height - self.collectionView.height) {
            
            if (indexPath.row == lastIndexPath.row) {
                [cell.playerBaseView play];
                if (cell.playerBaseView.isLoaded) {
                    [cell.playerBaseView setPlayerViewHidden:NO withAnimated:YES];
                } else {
                    [cell.playerBaseView startIndicator];
                }
            } else {
                [cell.playerBaseView pause];
                [cell.playerBaseView setPlayerViewHidden:YES withAnimated:YES];
            }
        } else {
            
            CGRect cellFrameInSuperview = [self.collectionView convertRect:attributes.frame toView:[self.collectionView superview]];
            
            if (cellFrameInSuperview.origin.y >= 0 && cellFrameInSuperview.origin.y <= [DLLiveMovieCell getCellSizeWithParantWidth:self.collectionView.width].height) {
                [cell.playerBaseView play];
                if (cell.playerBaseView.isLoaded) {
                    [cell.playerBaseView setPlayerViewHidden:NO withAnimated:YES];
                } else {
                    [cell.playerBaseView startIndicator];
                }
            } else {
                [cell.playerBaseView pause];
                [cell.playerBaseView setPlayerViewHidden:YES withAnimated:YES];
            }
        }
    }
    
    // 削除判定
    [self.preloadIndexPathArray removeObjectsInArray:sameIndexPathArray];
    
    for (NSIndexPath *removeIndexPath in self.preloadIndexPathArray) {
        DLLiveMovieCell *cell = (DLLiveMovieCell *)[self.collectionView cellForItemAtIndexPath:removeIndexPath];
        [cell removePlayerBaseView];
    }
    
    self.preloadIndexPathArray = [NSMutableArray arrayWithArray:[visibleCells mutableCopy]];
}

- (void)removeAllPreloadCells {
    
    for (NSIndexPath *indexPath in self.preloadIndexPathArray) {
        DLLiveMovieCell *cell = (DLLiveMovieCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
        [cell removePlayerBaseView];
    }
    
    [self.preloadIndexPathArray removeAllObjects];
}

@end
