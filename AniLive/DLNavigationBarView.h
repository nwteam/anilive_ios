//
//  DLNavigationBarView.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/11.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, DLNavigationBarViewType) {
    DLNavigationBarViewTypeLogo = 0,
    DLNavigationBarViewTypeTitle,
    DLNavigationBarViewTypeMoviePlay,
    DLNavigationBarViewTypeForumList,
    DLNavigationBarViewTypeMyPage,
    DLNavigationBarViewTypeSettings,
    DLNavigationBarViewTypeWeb
};

@protocol DLNavigationBarViewDelegate;

@class DLMovie, DLLive;
@interface DLNavigationBarView : UIView

@property (nonatomic, weak) id<DLNavigationBarViewDelegate> delegate;

- (instancetype)initWithType:(DLNavigationBarViewType)type;
- (void)setTitle:(NSString *)title;
- (void)setNavigationBarMovieData:(DLMovie *)movieData;
- (void)setNavigationBarLiveData:(DLLive *)liveData;

@end

@protocol DLNavigationBarViewDelegate <NSObject>

@optional
- (void)navigationBarViewDidTapLeftButton:(UIButton *)button;

- (void)navigationBarViewDidTapRightButton:(UIButton *)button;

@end
