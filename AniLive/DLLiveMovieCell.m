//
//  DLLiveMovieCell.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/17.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLLiveMovieCell.h"
#import "DLTimeLabelView.h"
#import "UIImageView+Category.h"
#import "NSString+Category.h"

const static CGFloat kFooterViewHeight = 50;

@interface DLLiveMovieCell () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIImageView *thumbnailImageView;
@property (nonatomic, strong) UIImageView *liveIconImageView;
@property (nonatomic, strong) UIView *footerBaseView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *playCountIconImageView;
@property (nonatomic, strong) UILabel *playCountLabel;
@property (nonatomic, strong) UIImageView *commentCountIconImageView;
@property (nonatomic, strong) UILabel *commentCountLabel;
@property (nonatomic, strong) DLTimeLabelView *timeLabelView;
@property (nonatomic, strong) DLMovie *movieData;
@property (nonatomic, strong) DLLive *liveData;
@property (nonatomic, strong) UITapGestureRecognizer *cellTapGesture;
@property (nonatomic, assign) BOOL isLive;

@end

@implementation DLLiveMovieCell

#pragma mark - ORVSuperCollectionViewCellProtocol

- (void)initView {
    [super initView];
    
#pragma mark Thumbnail ImageView
    
    self.thumbnailImageView = [[UIImageView alloc]init];
    self.thumbnailImageView.backgroundColor = [UIColor clearColor];
    self.thumbnailImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:self.thumbnailImageView];
    
    self.liveIconImageView = [[UIImageView alloc]init];
    self.liveIconImageView.backgroundColor = [UIColor clearColor];
    self.liveIconImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.liveIconImageView.image = [UIImage imageNamed:DLConstLabelLive];
    [self.contentView addSubview:self.liveIconImageView];
    
#pragma mark Footer View
    
    self.footerBaseView = [[UIView alloc]init];
    self.footerBaseView.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkBlue];
    [self.contentView addSubview:self.footerBaseView];
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = self.footerBaseView.backgroundColor;
    self.titleLabel.font = BASE_FONT(14);
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.footerBaseView addSubview:self.titleLabel];
    
    self.commentCountLabel = [[UILabel alloc]init];
    self.commentCountLabel.backgroundColor = self.footerBaseView.backgroundColor;
    self.commentCountLabel.font = BASE_FONT_BOLD(12);
    self.commentCountLabel.textColor = [UIColor colorWithHex:DLConstColorCodeGray];
    self.commentCountLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.footerBaseView addSubview:self.commentCountLabel];
    
#pragma mark Cell Tap Gesture
    
    self.cellTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                                action:@selector(didTapCell)];
    self.cellTapGesture.numberOfTapsRequired = 1;
    self.cellTapGesture.numberOfTouchesRequired = 1;
    self.cellTapGesture.delegate = self;
    [self.contentView addGestureRecognizer:self.cellTapGesture];
}

- (void)initializeCell {
    [super initializeCell];
    
    self.movieData = nil;
    self.liveData = nil;
    self.isLive = NO;
    
    self.thumbnailImageView.image = nil;
    self.playCountLabel.text = @"";
    self.commentCountLabel.text = @"";
    self.liveIconImageView.hidden = YES;
    [self removePlayerBaseView];
}

+ (NSString *)getCellIdentifier {
    
    return NSStringFromClass([self class]);
}

#pragma mark - Class Methods

+ (CGSize)getCellSizeWithParantWidth:(CGFloat)parantWidth {
    
    CGFloat thumbnailHeight = parantWidth / 16 * 9;
    
    return CGSizeMake(parantWidth, thumbnailHeight + kFooterViewHeight);
}

#pragma mark - Override Methods

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
    self.cellTapGesture.delegate = nil;
    self.delegate = nil;
}

#pragma mark - Public Methods

- (void)setCellWithMovieData:(DLMovie *)movieData {
    
    self.movieData = (DLMovie *)movieData;
    
    self.isLive = NO;
    
    self.titleLabel.text = self.movieData.title;
    
    if (NOT_NULL(self.movieData.image)) {
        [UIImageView loadImageWithURLString:self.movieData.image
                            targetImageView:self.thumbnailImageView];
    }
    
    [self updateFrame];
}

- (void)setCellWithLiveData:(DLLive *)liveData {
    
    self.liveData = liveData;
    
    self.isLive = YES;
    
    self.titleLabel.text = self.liveData.title;
    
    if (NOT_NULL(self.liveData.image)) {
        [UIImageView loadImageWithURLString:self.liveData.image
                            targetImageView:self.thumbnailImageView];
    }
    
    self.commentCountLabel.text = [NSString stringWithFormat:@"%@ topics", [NSString commasFormatedStringFromNumber:self.liveData.totalForumsCount]];
    
    
    self.liveIconImageView.hidden = ![self.liveData isOnAir];
    
    [self updateFrame];
}

- (void)addPlayerBaseView {
    
    if (self.isLive) {
        if (!NOT_EMPTY(self.liveData.highlightMovieUrl) || NOT_NULL(self.playerBaseView)) {
            return;
        }
        
        self.playerBaseView = [[DLPlayerBaseView alloc]initWithLiveData:self.liveData withType:DLPlayerBaseViewTypeAutoPlay];
    } else {
        if (!NOT_EMPTY(self.movieData.image) || NOT_NULL(self.playerBaseView)) {
            return;
        }
        
        self.playerBaseView = [[DLPlayerBaseView alloc]initWithMovieData:self.movieData withType:DLPlayerBaseViewTypeAutoPlay];
    }
    
    self.playerBaseView.playerView.alpha = 0;
    [self.playerBaseView setPlayerMute:YES];
    [self.playerBaseView setPlayerRepeat:YES];
    [self.contentView insertSubview:self.playerBaseView belowSubview:self.liveIconImageView];
    [self updateFrame];
}

- (void)removePlayerBaseView {
    
    if (NOT_NULL(self.playerBaseView)) {
        [self.playerBaseView pause];
        [self.playerBaseView removeFromSuperview];
        self.playerBaseView = nil;
    }
}

#pragma mark - Private Methods

- (void)updateFrame {
    
    const CGFloat kIconSide = 10;
    
    CGFloat thumbnailHeight = self.width / 16 * 9;
    
    self.thumbnailImageView.frame = CGRectMake(0, 0, self.width, thumbnailHeight);
    
    if (NOT_NULL(self.playerBaseView)) {
        self.playerBaseView.frame = self.thumbnailImageView.frame;
    }
    
    self.liveIconImageView.frame = CGRectMake(DLConstDefaultMargin, DLConstDefaultMargin, 35, 18);
    
    self.footerBaseView.frame = CGRectMake(0, self.thumbnailImageView.maxY, self.width, kFooterViewHeight);
    [self.titleLabel sizeToFit];
    self.titleLabel.frame = CGRectMake(DLConstDefaultMargin, 8, self.width - (DLConstDefaultMargin * 2), self.titleLabel.height);
    
    self.commentCountLabel.frame = CGRectMake(DLConstDefaultMargin, self.footerBaseView.height - 9 - kIconSide, self.width - DLConstDefaultMargin, kIconSide);
}

- (void)didTapCell {
    
    if (self.isLive) {
        if ([self.delegate respondsToSelector:@selector(didTapLiveMovieCell:withLiveData:)]) {
            [self.delegate didTapLiveMovieCell:self withLiveData:self.liveData];
        }
    } else {
        if ([self.delegate respondsToSelector:@selector(didTapLiveMovieCell:withMovieData:)]) {
            [self.delegate didTapLiveMovieCell:self withMovieData:self.movieData];
        }
    }
}

#pragma mark - UIGestureRecognizer Delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if ([self.delegate respondsToSelector:@selector(scrollViewIsScrolling)]) {
        return ![self.delegate scrollViewIsScrolling];
    }
    
    return YES;
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([self.delegate respondsToSelector:@selector(scrollViewIsScrolling)]) {
        return ![self.delegate scrollViewIsScrolling];
    }
    
    return YES;
}

@end
