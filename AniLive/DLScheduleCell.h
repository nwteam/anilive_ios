//
//  DLScheduleCell.h
//  AniLive
//
//  Created by Loyal Lauzier on 12/21/16.
//  Copyright © 2016 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DLScheduleCellDelegate <NSObject>

- (void)didTapSelectButtonItemID:(NSInteger)itemID IsNoti:(BOOL)isNoti;
//- (void)didTapNotiButtonItemID:(NSInteger)itemID;
- (void)didTapNotiButtonItemID:(NSIndexPath *)itemPosition;
- (void)didTapButtonItemID:(NSInteger)itemID;
@end

@interface DLScheduleCell : UITableViewCell

@property (strong, nonatomic) id <DLScheduleCellDelegate>delegate;

@property (weak, nonatomic) IBOutlet UIImageView *imageThumb;
@property (weak, nonatomic) IBOutlet UIImageView *imageMark;
@property (weak, nonatomic) IBOutlet UILabel *lblStartTime;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnNoti;

@property (nonatomic, assign) NSInteger itemIdx;
@property (nonatomic, assign) BOOL isNoti;
@property (nonatomic, assign) NSIndexPath *itemPosition;

- (IBAction)tapBtnNoti:(id)sender;
- (IBAction)tapBtnBG:(id)sender;

@end
