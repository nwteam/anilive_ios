//
//  DLAppFrameManager.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>

// TODO: iPad対応時に必要になる
typedef NS_ENUM(NSUInteger, DLAppFrameType) {
    DLAppFrameTypePortraitFull = 0,
    DLAppFrameTypePortraitSplitLeft,
    DLAppFrameTypePortraitSplitRight,
    DLAppFrameTypeLandscapeFull,
    DLAppFrameTypeLandscapeSplitLeft,
    DLAppFrameTypeLandscapeSplitRight,
    DLAppFrameTypeLandscapeSplitCenter,
    DLAppFrameTypeOther
};

@interface DLAppFrameManager : NSObject

@property (readonly, nonatomic, assign) DLAppFrameType appFrameType;

@property (readonly, nonatomic, assign) CGRect applicationFrame;

@property (nonatomic, assign) BOOL updateEnabled;

+ (instancetype)sharedInstance;

- (void)updateApplicationFrame;

- (void)setCurrentOrientationForce:(UIInterfaceOrientation)orientation;

- (NSInteger)getNumberOfCellPerRowForCatalog;

@end
