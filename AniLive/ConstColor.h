//
//  ConstColor.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/17.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConstColor : NSObject

extern NSString * const DLConstColorCodeGray;
extern NSString * const DLConstColorCodeDarkGray;
extern NSString * const DLConstColorCodeThinGray;
extern NSString * const DLConstColorCodeDarkBlue;
extern NSString * const DLConstColorCodeYellow;
extern NSString * const DLConstColorLightBlue;
extern NSString * const DLConstColorLightWhite;
extern NSString * const DLConstColorDarkBlue;

@end
