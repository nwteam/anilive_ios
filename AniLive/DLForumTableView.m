//
//  DLForumTableView.m
//  AniLive
//
//  Created by isaoeka on 2016/12/30.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLForumTableView.h"
#import "DLForumCell.h"
#import "DLForum.h"

#import "DLAppFrameManager.h"

@interface DLForumTableView () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray<DLForum *> *forums;

@end

@implementation DLForumTableView

#pragma mark - Initializer

- (instancetype)init {
    self = [super init];
    if (self) {
        self.forums = [NSMutableArray<DLForum *> array];
        [self initView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

#pragma mark - Dealloc

- (void)dealloc {
}

#pragma mark - Private Methods

- (void)initView {
    self.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkGray];
    self.delegate   = self;
    self.dataSource = self;
    
    [self registerNib:[DLForumCell getDefaultNib] forCellReuseIdentifier:@"forum-cell"];
}

- (void)updateFrame {
    
}

#pragma mark - Public Methods

- (void)loadForums:(NSMutableArray<DLForum *> *)forums{
    self.forums = forums;
    [self reloadData];
}

#pragma mark - Table View Delegate

#pragma mark Section

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

#pragma mark Cell

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [DLForumCell getCellHeight];
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.forums count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DLForumCell *cell = [tableView dequeueReusableCellWithIdentifier:@"forum-cell"
                                                        forIndexPath:indexPath];
    [cell setForum:self.forums[indexPath.row]];
    
    return cell;
}

@end
