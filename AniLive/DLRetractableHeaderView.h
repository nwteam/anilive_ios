//
//  DLRetractableHeaderView.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/11/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DLRetractableHeaderViewDelegate;

@class DLWork;
@interface DLRetractableHeaderView : UIView

@property (nonatomic, weak) id<DLRetractableHeaderViewDelegate> delegate;

- (instancetype)initWithWorkData:(DLWork *)workData withStartHeight:(CGFloat)startHeight;

- (void)updateWorkData:(DLWork *)workData;

@end

@protocol DLRetractableHeaderViewDelegate <NSObject>

@optional
- (void)backButtonDidTap;
- (void)showInfoButtonDidTap;

@end
