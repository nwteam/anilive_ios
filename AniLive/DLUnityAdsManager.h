//
//  DLUnityAdsManager.h
//  AniLive
//
//  Created by 辰己佳祐 on 2017/01/14.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UnityAds/UnityAds.h>

typedef void (^DLUnityAdsManagerDidReadyBlock)(NSString *placementId);
typedef void (^DLUnityAdsManagerDidErrorBlock)(NSString *errorMessage);
typedef void (^DLUnityAdsManagerDidStartBlock)(NSString *placementId);
typedef void (^DLUnityAdsManagerDidFinishBlock)(NSString *placementId, UnityAdsFinishState state);

static NSString *kAdsManagerReadyToPlayKeyPath = @"isAdReadyToPlay";
static NSString *kAdsManagerIsPlayingKeyPath = @"isPlaying";

@interface DLUnityAdsManager : NSObject

@property (nonatomic, assign, readonly) BOOL isAdReadyToPlay;
@property (nonatomic, assign, readonly) BOOL isPlaying;

- (instancetype)initTargetViewController:(UIViewController *)viewController;
- (void)addObserverWithTarget:(id)target;
- (void)removeObserver;
- (void)updateTargetViewController:(UIViewController *)viewController;
- (void)showAd;

// Wrapped UnityAdsDelegate Methods

- (void)adDidReadyToPlay:(DLUnityAdsManagerDidReadyBlock)completion;
- (void)adDidError:(DLUnityAdsManagerDidErrorBlock)completion;
- (void)adDidStart:(DLUnityAdsManagerDidStartBlock)completion;
- (void)adDidFinish:(DLUnityAdsManagerDidFinishBlock)completion;

@end
