//
//  DLTabBarController.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLInAppPurchaseManager.h"

@interface DLMainTabBarController : UITabBarController <InAppPurchaseManagerDelegate>

- (void)setTabBarViewIsHidden:(BOOL)isHidden;

@end
