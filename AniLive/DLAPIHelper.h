//
// Created by Kotaro Itoyama on 2016/10/09.
// Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

//#import "AFHTTPRequestOperationManager.h"
#import <AFNetworking/AFNetworking.h>

#import "DLForumGroupListViewController.h"
#import "DLMyPageGroupListViewController.h"

@class AFHTTPSessionManager;
@class DLMovie;
@class DLForum;
@class DLComment;
@class DLWatch;
@class DLReportType;
@class DLWork;
@class DLLive;
@class DLAuthcode;
@class DLUserDevice;
@class DLPromotion;
@class DLPoint;

typedef NS_ENUM(NSInteger, DLOrderType) {
    DLOrderTypeAscending = 346,
    DLOrderTypeDescending
};

@interface DLAPIHelper : NSObject
+ (instancetype)sharedManager;

+ (AFHTTPSessionManager *)sesmgr;

+ (NSInteger)myUserId;

+ (NSDate *)dateFromString:(NSString *)dateString;
+ (NSTimeInterval)timeIntervalFromString:(NSString *)string;

#pragma mark /Work
// GET /works
+ (void)fetchWorksWithPageNumber:(NSInteger)pageNumber
                         perPage:(NSInteger)perPage
                        sortType:(NSString *)sortType
                        callback:(void (^)(NSMutableArray *works, NSError *error))callback;
// GET /works/{work_id}
+ (void)fetchWorksByWorkID:(NSInteger)workId
                  callback:(void (^)(DLWork *work, NSError *error))callback;
// GET /works/{work_id}/forums
+ (void)fetchForumsByWorkID:(NSInteger)workId
                 pageNumber:(NSInteger)pageNumber
                    perPage:(NSInteger)perPage
                   callback:(void (^)(NSMutableArray *forums, NSError *error))callback;
// GET /works/{work_id}/movies
+ (void)fetchMovieByWorkID:(NSInteger)workId
                pageNumber:(NSInteger)pageNumber
                   perPage:(NSInteger)perPage
                  callback:(void (^)(NSMutableArray *movies, NSError *error))callback;

#pragma mark /Movie
// GET /movies/{movie_id}/forums
+ (void)fetchMoviesForumsByMovieID:(NSInteger)movieId
                          callback:(void (^)(NSMutableArray<DLForum *> *forums, NSError *error))callback;
// POST /movies/{movie_id}/forums
+ (void)postForumsByMovieID:(NSInteger)movieId
                 forumTitle:(NSString *)forumTitle
            forumPinnedTime:(NSString *)forumPinnedTime
                   callback:(void (^)(DLForum *forum, NSError *error))callback;
// GET /movies/{movie_id}/watch
+ (void)fetchWatch:(NSInteger)movieId
            liveId:(NSInteger)liveId
          callback:(void (^)(DLWatch *watch, NSError *error))callback;
// GET /users/movies
+ (void)fetchVideoHistoryByPageNumber:(NSInteger)pageNumber
                              perPage:(NSInteger)perPage
                            orderType:(DLOrderType)orderType
                             callback:(void (^)(NSMutableArray *movies, NSError *error))callback;
// GET /movies/{movie_id}/point/used
+ (void)fetchPointUsedHistory:(NSInteger)movieId
                     callback:(void (^)(NSObject *point, NSError *error))callback;

#pragma mark /Live
// GET /lives
+ (void)fetchLives:(void (^)(NSMutableArray *lives))callback;

+ (void)fetchLive: (NSInteger) liveId callback:(void (^)(DLLive *live))callback;

// GET /lives/{live_id}/movies
+ (void)fetchMoviesWithLiveId:(NSInteger)liveId
                     callback:(void (^)(NSMutableArray<DLMovie *> *movies, NSError *error))callback;
// GET /lives/promotions
+ (void)fetchPromotionMoviesWithQuantity:(NSInteger)quantity
                                callback:(void (^)(NSMutableArray<DLPromotion*> *promotions, NSError *error))callback;
// POST /lives/reserve
+ (void)postLiveReserve:(NSInteger)liveId
               callback:(void (^)(NSObject *live, NSError *error))callback;
// GET /lives/reserves
+ (void)fetchLivesReserve:(void (^)(NSMutableArray *lives, NSError *error))callback;
// GET /lives/timeschedules
+ (void)fetchTimeSchedules:(void (^)(NSMutableArray *lives))callback;

#pragma mark /Forum
// DELETE /comments/{comment_id}
+ (void)deleteComment:(DLComment *)comment;
// PATCH /comments/{comment_id}
+ (void)editComment:(DLComment *)comment
           withBody:(NSString *)body
           callback:(void (^)(BOOL flag))callback;
// GET /forums
+ (void)fetchForumsWithPageNumber:(NSInteger)pageNumber
                         SortType:(DLForumGroupSortType)sortType
                         callback:(void (^)(NSMutableArray<DLForum *> *forums))callback;
// DELETE /forums/{forum_id}
+ (void)deleteForumByForumID:(NSInteger)forumId
                    callback:(void (^)(BOOL flag))callback;
// GET /forums/{forum_id}
+ (void)fetchForumByForumID:(NSInteger)forumId
                   callback:(void (^)(NSObject *forum, NSError *error))callback;
// GET /forums/{forum_id}/comments
+ (void)fetchCommentsByForumID:(NSInteger)forumId
                    pageNumber:(NSInteger)pageNumber
                      callback:(void (^)(NSMutableArray *comments, NSError *error))callback;
// POST /forums/{forum_id}/comments
+ (void)postComment:(NSString *)comment
      targetForumId:(NSInteger)forumId
           callback:(void (^)(BOOL flag , id responseObject))callback;

#pragma mark /User
// GET /users
+ (void)fetchUserWithCallback:(void (^)(NSError *error))callback;
+ (void)postUserWithCallback:(void (^)(NSError *error))callback;
+ (void)patchUser:(NSString *)name callback:(void (^)(NSError *error))callback;

// PATCH /users/svod/ios
+ (void)patchSubscriptionVideoOnDemand:(NSString *)receipt
                      trialEndDateTime:(NSString *)trialEndDateTime
                              callback:(void (^)(NSInteger statusCode, NSError *error))callback;
// GET /users/creates
+ (void)fetchMyForumByPageNumber:(NSInteger)pageNumber
                         perPage:(NSInteger)perPage
                        callback:(void (^)(NSMutableArray *forums, NSError *error))callback;
// GET /users/participations
+ (void)fetchParticipateForumByPageNumber:(NSInteger)pageNumber
                                  perPage:(NSInteger)perPage
                                 callback:(void (^)(NSMutableArray *forums, NSError *error))callback;

#pragma mark /Device
+ (void)fetchUsersWithDeviceID:(NSInteger)deviceID
                          UUID:(NSString *)UUID
                      callback:(void (^)(NSMutableArray *users))callback;
+ (void)issueAuthcode: (NSNumber*) code
             callback: (void (^)(DLAuthcode *authcode))callback;
+ (void)checkAuthcode: (NSString*) code
               userId: (NSString*) userId
             callback:(void (^)(NSMutableArray<DLUserDevice*> *userDevices, NSError *error))callback;
+ (void)revisionDevicesRelation: (NSString*) code
                         userId: (NSString*) userId
                releaseDeviceId: (NSString*) releaseDeviceId
                       callback: (void (^)(BOOL isSuccess)) callback;

//POST /restore/check/ios
+ (void)checkRestoreSubscription:(NSString *)receipt
                        callback:(void (^)(NSInteger statusCode, NSError *error))callback;

#pragma mark /SeeLater
+ (void)fetchSeeLaterDatacallback:(void (^)(NSMutableArray *forums))callback
;

+ (void)addSeelaterDataWithForum:(DLForum *)forum callback:(void (^)(BOOL success))callback;
+ (void)removeSeelaterDataWithForum:(DLForum *)forum callback:(void (^)(BOOL success))callback;
#pragma mark /Point

+ (void)usePointWithMovieId:(NSNumber *)movieID reason:(NSString *)reason callback:(void (^)(BOOL flag))callback;

+ (void)checkAdsShowingTimesCallback:(void (^)(BOOL flag))callback;

+ (void)addPoint:(void (^)(DLPoint *point))callback;

#pragma mark /Participation
#pragma mark /Other
+ (void)loadReportTypescallback:(void (^)(NSMutableArray *types))callback;
+ (void)reportComment:(DLComment *)comment reportType:(DLReportType *)type fronm:(DLForum *)froum callback:(void (^)(BOOL flag))callback;

+ (void)fetchPointsUsed:(void (^)(NSMutableArray *points))callback;

//TODO: 消す予定
+ (void)fetchMovies:(NSArray*) movieIds
           callback:(void (^)(NSMutableArray *movies))callback;

+ (void)validateReceiptURL:(NSURL *)url;

@end
