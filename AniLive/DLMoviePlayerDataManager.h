//
//  DLMoviePlayerDataManager.h
//  AniLive
//
//  Created by ykkc on 2017/01/24.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

@class DLForum, DLComment;
@protocol DLMoviePlayerDataManagerDelegate;

@interface DLMoviePlayerDataManager : NSObject

@property (nonatomic, weak) id<DLMoviePlayerDataManagerDelegate> delegate;

- (DLForum *)getCurrentForum;
- (NSMutableArray<DLForum *> *)getForumList;

- (void)movePreviousForum;
- (void)moveNextForum;
- (void)updateForums:(NSMutableArray<DLForum *> *)forums;

@end

@protocol DLMoviePlayerDataManagerDelegate <NSObject>

// 基本的にはMoviePlayViewControllerに書く画面の通知をさせるので、requiredでよさそう。
@required
- (void)onUpdateCurrentForum:(DLForum *)forum
               existPrevious:(BOOL)existPrevious
                   existNext:(BOOL)existNext;
- (void)onUpdateCurrentForumComments:(NSMutableArray<DLComment *> *)comments;

@end
