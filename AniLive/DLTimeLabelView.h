//
//  DLTimeLabelView.h
//  AniLive
//
//  Created by isaoeka on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DLTimeLabelView : UIView

- (void)sizeToFit;

- (void)setText:(NSString *)text;
- (void)setTimeStr:(NSString *)time;

- (CGSize)getTimeLabelSize;

@end
