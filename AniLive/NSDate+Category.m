#import "NSDate+Category.h"
#import "NSString+Category.h"

@implementation NSDate (Category)

- (NSString*) genPastTimeString {

    double diff = ([[NSDate date] timeIntervalSince1970] - [self timeIntervalSince1970]);

    NSString *format;
    NSInteger representValue;
    if (diff < 0) {
        return @"Just now";
    } else if (diff < 60) {
        format = @"second";
        representValue = diff;
    } else if (diff < 3600) {
        format = @"minute";
        representValue = diff / 60;
    } else if (diff < 86400) {
        format = @"hour";
        representValue = diff / 3600;
    } else if (diff < 2592000) {
        format = @"day";
        representValue = diff / 86400;
    } else if (diff < 31536000) {
        format = @"month";
        representValue = diff / 2592000;
    } else {
        format = @"year";
        representValue = diff / 31536000;
    }
    return [NSString calcMultiple:representValue unit:format];
}

@end