//
//  DLSeeLaterButton.h
//  AniLive
//
//  Created by isaoeka on 2017/01/01.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DLSeeLaterButtonDelegate;

@interface DLSeeLaterButton : UIButton

@property (nonatomic, weak) id<DLSeeLaterButtonDelegate> delegate;
@property (nonatomic, assign) BOOL isNoNeedHighlight;

- (BOOL)isSeeLater;
- (void)setStarImageOfHighlighted:(BOOL)highlighted;

@end


#pragma mark - DLSeeLaterButtonDelegate

@protocol DLSeeLaterButtonDelegate <NSObject>

@optional

- (BOOL)seeLaterButtonDidTap:(DLSeeLaterButton *)button;

@end
