//
//  DLPlayerViewPromotionAd.h
//  AniLive
//
//  Created by ykkc on 2017/01/17.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPlayerView.h"

@protocol DLPlayerViewPromotionAdDelegate;

@interface DLPlayerViewPromotionAd : DLPlayerView

@property (nonatomic, weak) id<DLPlayerViewPromotionAdDelegate> delegate;

- (instancetype)initPromotionAds:(NSInteger)playAdTime;

@end

@protocol DLPlayerViewPromotionAdDelegate <NSObject>

- (void)onAdMovieStarted;
- (void)onAdMovieEnded:(NSInteger)remainingTime;
- (void)onAdMovieError;

@end
