//
//  UILabel+Category.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/17.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "UILabel+Category.h"

@implementation UILabel (Category)

// 文字が欠ける場合があるので、高さを調整
- (void)sizeToFit {
    [super sizeToFit];
    
    self.height += 4;
}

@end
