//
// Created by akasetaichi on 2017/01/20.
// Copyright (c) 2017 AnimeConsortiumJapan. All rights reserved.
//

#import "DLUserDevice.h"
#import "DLAPIHelper.h"

@interface DLUserDevice()
@property(readwrite, nonatomic, strong) NSNumber *deviceId;
@property(readwrite, nonatomic, strong) NSString *device;
@property(readwrite, nonatomic, strong) NSDate *created;
@end

@implementation DLUserDevice

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (self) { return nil; }
    self.deviceId = CHECK_NULL_DATA_NUMBER(attributes[@"device_id"]);
    self.device = CHECK_NULL_DATA_STRING(attributes[@"device"]);
    self.created = [DLAPIHelper dateFromString:attributes[@"created"]];

    return self;
}

@end
