//
//  DLSettingsViewController.h
//  AniLive
//
//  Created by Isao Kono on 2016/11/09.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DLSettingsViewController : UIViewController

- (instancetype)initWithIsModal:(BOOL)isModal;

@end
