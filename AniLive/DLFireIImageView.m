//
//  DLFireIImageView.m
//  AniLive
//
//  Created by isaoeka on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLFireIImageView.h"

@interface DLFireIImageView ()

@property (nonatomic, strong) UIImageView *fire1;
@property (nonatomic, strong) UIImageView *fire2;
@property (nonatomic, strong) UIImageView *fire3;
@property (nonatomic, assign) NSInteger fireCount;

@end

@implementation DLFireIImageView

- (instancetype)initWithFireCount:(NSInteger)count {
    self = [super init];
    if (self) {
        self.fireCount = count <= 3 ? count : 3; // 上限は3
        
        [self initView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
}

#pragma mark - Private Methods


- (void)initView {
    self.backgroundColor = [UIColor clearColor];
    
    UIImage *fireImage = [UIImage imageNamed:DLConstIconWhatsHot];
    
    self.fire1 = [[UIImageView alloc] initWithImage:fireImage];
    self.fire2 = [[UIImageView alloc] initWithImage:fireImage];
    self.fire3 = [[UIImageView alloc] initWithImage:fireImage];
    
    self.fire1.contentMode = UIViewContentModeScaleAspectFit;
    self.fire2.contentMode = UIViewContentModeScaleAspectFit;
    self.fire3.contentMode = UIViewContentModeScaleAspectFit;
    
    self.fire1.hidden = YES;
    self.fire2.hidden = YES;
    self.fire3.hidden = YES;
    
    [self addSubview:self.fire1];
    [self addSubview:self.fire2];
    [self addSubview:self.fire3];
    
    [self setFireCount:self.fireCount];
}

- (void)updateFrame {
    CGFloat imageWidth = 10;
    CGFloat margin     = 3;
    
    self.fire1.frame = CGRectMake(0,                        0, imageWidth, self.height);
    self.fire2.frame = CGRectMake(self.fire1.maxX + margin, 0, imageWidth, self.height);
    self.fire3.frame = CGRectMake(self.fire2.maxX + margin, 0, imageWidth, self.height);
}

#pragma mark - Public Methods

- (void)setFireCount:(NSInteger)count {
    switch (count) {
        case 0:
            self.fire1.hidden = YES;
            self.fire2.hidden = YES;
            self.fire3.hidden = YES;
            break;
        case 1:
            self.fire1.hidden = NO;
            self.fire2.hidden = YES;
            self.fire3.hidden = YES;
            break;
        case 2:
            self.fire1.hidden = NO;
            self.fire2.hidden = NO;
            self.fire3.hidden = YES;
            break;
        case 3:
            self.fire1.hidden = NO;
            self.fire2.hidden = NO;
            self.fire3.hidden = NO;
            break;
    }
}

@end
