//
//  DLForumHeaderView.m
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/10/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLForumHeaderView.h"
#import "DLForum.h"
#import "DLAppFrameManager.h"
#import <UIImageView+AFNetworking.h>
#import "UIImageView+Category.h"

@interface DLForumHeaderView ()
@property(readwrite) CGFloat residualHeight;

@end

@implementation DLForumHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        NSLog(@"frame");
        [self initView];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSLog(@"coder");
        [self initView];
    }
    return self;
}

- (instancetype)init {
    self = [super init];
    if(self){
        [self initView];
    }
    return self;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    NSLog(@"nib");
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self updateFrame];
}

- (void)initView {
    self.backgroundColor = [UIColor colorWithHex:DLConstColorDarkBlue];
    self.titleView = UILabel.new;
    self.titleView.numberOfLines = 0;
    self.titleView.textColor = [UIColor whiteColor];
    self.titleView.backgroundColor = [UIColor clearColor];
    self.titleView.userInteractionEnabled = YES;
    self.titleView.font = [UIFont systemFontOfSize:17];
    [self addSubview:self.titleView];
    
    self.movieDurationLable = UILabel.new;
    self.movieDurationLable.layer.cornerRadius = 3.0;
    self.movieDurationLable.layer.borderColor = [[UIColor colorWithHex:DLConstColorLightWhite] CGColor];
    self.movieDurationLable.layer.borderWidth = 1.0;
    self.movieDurationLable.textColor = [UIColor colorWithHex:DLConstColorLightWhite];
    self.movieDurationLable.font = [UIFont systemFontOfSize:12];
    [self addSubview:self.movieDurationLable];
    
    self.totalCommentsLable = UILabel.new;
    self.totalCommentsLable.textColor = [UIColor colorWithHex:DLConstColorLightWhite];
    self.totalCommentsLable.font = [UIFont systemFontOfSize:12];
    [self addSubview:self.totalCommentsLable];
    
    self.descriptionView = UIView.new;
    self.descriptionView.backgroundColor = [UIColor colorWithHex:DLConstColorLightBlue];
    [self addSubview:self.descriptionView];
    
    self.imageView = UIImageView.new;
    [self.descriptionView addSubview:self.imageView];
    
    self.movieTitleLable = UILabel.new;
    self.movieTitleLable.textColor = [UIColor whiteColor];
    self.movieTitleLable.font = [UIFont systemFontOfSize:14];
    self.movieTitleLable.numberOfLines = 0;
    [self.descriptionView addSubview:self.movieTitleLable]
    ;
    self.totalFroumsLable = UILabel.new;
    self.totalFroumsLable.font = [UIFont systemFontOfSize:10];
    self.totalFroumsLable.textColor = [UIColor colorWithHex:DLConstColorLightWhite];
    [self.descriptionView addSubview:self.totalFroumsLable];
    
    self.displayMovieButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.displayMovieButton setBackgroundColor:[UIColor clearColor]];
    [self.descriptionView addSubview:self.displayMovieButton];
}

- (void)updateFrame {
    
}


- (void)setForum:(DLForum *)forum {
    self.titleView.text = forum.title;
    [UIImageView loadImageWithURLString:forum.movieImage
                        targetImageView:self.imageView];
    self.movieTitleLable.text = forum.movieTitle;
    self.movieDurationLable.text = forum.forumPinnedTime;
    CGFloat kScreenWidth = CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame);
    
    self.totalFroumsLable.text = [NSString stringWithFormat:@"%@ forums",forum.totalForums];
    if ([forum.totalComments integerValue] > 0) {
        self.totalCommentsLable.text = [NSString stringWithFormat:@"%@  comments",forum.totalComments];
        self.totalCommentsLable.textColor = [UIColor colorWithHex:DLConstColorLightWhite];
    }
    else {
        self.totalCommentsLable.text = @"NEW";
        self.totalCommentsLable.textColor = [UIColor colorWithHex:DLConstColorCodeYellow];
    }
    
    if (self.titleView.text) {
        CGSize size = [self.titleView.text sizeWithFont:self.titleView.font constrainedToSize:CGSizeMake(kScreenWidth - 32, 500) lineBreakMode:NSLineBreakByWordWrapping];
        self.titleView.frame = CGRectMake(16, 15, kScreenWidth - 32, size.height);
    }
    else {
        self.titleView.frame = CGRectMake(16, 15, kScreenWidth - 32, 34);
    }
    
    self.movieDurationLable.frame = CGRectMake(16, CGRectGetMaxY(_titleView.frame) + 8, 100, 15);
    if (self.movieDurationLable.text) {
        CGSize size = [self.movieDurationLable.text sizeWithFont:self.movieDurationLable.font constrainedToSize:CGSizeMake(kScreenWidth, 21) lineBreakMode:NSLineBreakByWordWrapping];
        self.movieDurationLable.frame = CGRectMake(16, CGRectGetMaxY(_titleView.frame) + 8, size.width + 1, 15);
    }
    self.totalCommentsLable.frame = CGRectMake(CGRectGetMaxX(self.movieDurationLable.frame) + 8, self.movieDurationLable.frame.origin.y, 100, 15);
    if (self.totalCommentsLable.text) {
        CGSize size = [self.totalCommentsLable.text sizeWithFont:self.totalCommentsLable.font constrainedToSize:CGSizeMake(kScreenWidth - 32, 500) lineBreakMode:NSLineBreakByWordWrapping];
        self.totalCommentsLable.frame = CGRectMake(CGRectGetMaxX(self.movieDurationLable.frame) + 8, self.movieDurationLable.frame.origin.y, size.width + 8, 15);
    }
    NSInteger hotMarkCount;
    if ([forum.popularity intValue] >= 10) {
        hotMarkCount = 3;
    }
    else if ([forum.popularity intValue] >= 3){
        hotMarkCount = 2;
    }
    else if ([forum.popularity intValue] >= 1) {
        hotMarkCount = 1;
    }
    else {
        hotMarkCount = 0;
    }
    if (hotMarkCount > 0) {
        for (NSInteger i = 0; i < hotMarkCount; i ++) {
            UIImageView *hotmark = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_totalCommentsLable.frame) + 2 + i * (2.5 + 11), CGRectGetMidY(_totalCommentsLable.frame) - 5.5, 8, 11)];
            hotmark.image = [UIImage imageNamed:@"ic-whatshot"];
            [self addSubview:hotmark];
        }
    }
    self.imageView.frame = CGRectMake(5, 5, 106.6, 60);
    self.descriptionView.frame = CGRectMake(8, CGRectGetMaxY(self.movieDurationLable.frame) + 18, kScreenWidth - 16, 60 + 10);
    self.displayMovieButton.frame = CGRectMake(8, 8, CGRectGetWidth(_descriptionView.frame) - 16, CGRectGetHeight(_descriptionView.frame) - 16);
    self.movieTitleLable.frame = CGRectMake(CGRectGetMaxX(_imageView.frame) + 8, 8, kScreenWidth - 32 - 116, 35);
    self.totalFroumsLable.frame = CGRectMake(CGRectGetMaxX(_imageView.frame) + 8, CGRectGetMaxY(_movieTitleLable.frame) + 2, kScreenWidth - 32 - 116, 25);
    self.frame = CGRectMake(0, 0, kScreenWidth, CGRectGetMaxY(self.descriptionView.frame) + 8);
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
}

@end
