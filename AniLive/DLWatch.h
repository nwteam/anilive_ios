//
//  DLWatch.h
//  AniLive
//
//  Created by ykkc on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLModelBase.h"

@interface DLWatch : DLModelBase
@property (readonly, nonatomic, strong) NSString *ssrcd;
@property (readonly, nonatomic, strong) NSNumber *productId;
@property (readonly, nonatomic, strong) NSString *movieInitId;
@property (readonly, nonatomic, strong) NSString *playUrl;
@property (readonly, nonatomic, strong) NSString *sttmvUrl;
@property (readonly, nonatomic, strong) NSMutableArray *bwLabel;
@property (readonly, nonatomic, strong) NSString *captionUrl;
@property (readonly, nonatomic, strong) NSMutableArray *captionLang;
@property (readonly, nonatomic, strong) NSString *copyrightStr;
@property (readonly, nonatomic, strong) NSString *preimgUrl;
@property (readonly, nonatomic, assign) BOOL autoplayFlag;
@property (readonly, nonatomic, strong) NSString *worksTitle;
@property (readonly, nonatomic, strong) NSString *adId;
@property (readonly, nonatomic, strong) NSString *moviesTitle;
@property (readonly, nonatomic, strong) NSString *fov;
@property (readonly, nonatomic, strong) NSString *subscriptionStatus;
@property (readonly, nonatomic, strong) NSString *productid;
@end
