//
//  DLHighlightedButton.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/13.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLHighlightedButton.h"

@implementation DLHighlightedButton

- (void)setHighlighted:(BOOL)highlighted{
    [super setHighlighted:highlighted];
    
    if (!self.isNoNeedHighlight)  {
        if (highlighted) {
            if (NOT_NULL(self.highlightedColor)) {
                self.backgroundColor = self.highlightedColor;
            }
        } else {
            if (NOT_NULL(self.nonHighlightedColor)) {
                self.backgroundColor = self.nonHighlightedColor;
            } else {
                self.backgroundColor = [UIColor clearColor];
            }
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(highlightedButtonHighlightChanged:)]) {
        [self.delegate highlightedButtonHighlightChanged:highlighted];
    }
}

@end
