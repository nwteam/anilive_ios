//
//  DLSuperTableViewCell.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/16.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DLSuperTableViewCellProtocol <NSObject>

+ (NSString *)getCellIdentifier;

- (void)initView;

- (void)initializeCell;

@end

@interface DLSuperTableViewCell : UITableViewCell <DLSuperTableViewCellProtocol>


@end
