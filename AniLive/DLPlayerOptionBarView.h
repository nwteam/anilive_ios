//
//  DLPlayerOptionBarView.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/11/20.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

static const CGFloat kPlayerOptionViewHeight = 40;

typedef NS_ENUM(NSInteger, DLPlayerOptionBarViewType) {
    DLPlayerOptionViewForumDetail = 0,
    DLPlayerOptionViewForumList,
    DLPlayerOptionViewCreateForum,
};

@protocol DLPlayerOptionBarViewDelegate;

@interface DLPlayerOptionBarView : UIView

@property (nonatomic, weak) id<DLPlayerOptionBarViewDelegate> delegate;

- (instancetype)initWithType:(DLPlayerOptionBarViewType)type;

@end

@protocol DLPlayerOptionBarViewDelegate <NSObject>

@optional

- (void)playerOptionBarViewForumListButtonDidTap:(DLPlayerOptionBarViewType)optionBarViewType;
- (void)playerOptionBarViewBackButtonDidTap:(DLPlayerOptionBarViewType)optionBarViewType;
- (void)playerOptionBarViewNextButtonDidTap:(DLPlayerOptionBarViewType)optionBarViewType;
- (void)playerOptionBarViewPostCommentButtonDidTap:(DLPlayerOptionBarViewType)optionBarViewType;
- (void)playerOptionBarViewOtherButtonDidTap:(DLPlayerOptionBarViewType)optionBarViewType;

@optional
- (void)crereateForumButtonDidTap:(NSString *)title pinnedTime:(NSString *)pinnedTime;

@end
