//
//  DLForumListViewController.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLForumListViewController.h"
#import "DLAppFrameManager.h"
#import "DLForum.h"
#import "DLForumCell.h"
#import "DLForumViewController.h"

@interface DLForumListViewController () <UITableViewDataSource, UITableViewDelegate, DLNavigationBarViewDelegate>

@property (nonatomic, strong) DLNavigationBarView *navigationBarView;
@property (nonatomic, weak) IBOutlet UIView *titleView;
@property (nonatomic, weak) IBOutlet UILabel *titleTextView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *tableViewTopConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) NSMutableArray *forums;
@property NSUInteger pageNumber;

@end

@implementation DLForumListViewController

#pragma mark - Override Methods

- (instancetype)init {
    self = [super init];
    if (self) {
        self.pageNumber = 0;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.forums = [[NSMutableArray alloc] init];
    [self.tableView registerClass:[DLForumCell class] forCellReuseIdentifier:@"forum-cell"];
    [self loadData];
    [self initView];
    [self updateFrame];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent
                                                animated:NO];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)loadData {
    @weakify(self)
    [DLAPIHelper fetchMoviesForumsByMovieID:[self.froum.movieId integerValue] callback:^(NSMutableArray<DLForum *> *forums, NSError *error) {
        @strongify(self)
        if (!error) {
            [self.forums removeAllObjects];
            [self.forums addObjectsFromArray:forums];
            [self.tableView reloadData];
            [self updateFrame];
        }
    }];
}

#pragma mark - Private Methods

- (void)initView {

#pragma mark Navigation Bar
    self.navigationBarView = [[DLNavigationBarView alloc] initWithType:DLNavigationBarViewTypeForumList];
    self.navigationBarView.delegate = self;
    [self.navigationBarView setTitle:Localizer(@"Forum")];
    [self.view addSubview:self.navigationBarView];
    
#pragma mark UI View for title
    self.titleView.layer.borderColor = [UIColor yellowColor].CGColor;
    
    self.titleView.layer.borderWidth = 1.0f;
    
    [self setTitleText:self.group.movie.title];

#pragma mark Table View
    
}

- (void)updateFrame {
    
    self.navigationBarView.frame = CGRectMake(0,
            CGRectGetMinY([DLAppFrameManager sharedInstance].applicationFrame),
            CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame),
            DLConstNavigationBarHeight);
    
    self.titleView.frame = CGRectMake(0,
                                      self.navigationBarView.frame.size.height,
                                      CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame),
                                      40.0f);
    
    
}


#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.forums ? self.forums.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DLForumCell *cell = [tableView dequeueReusableCellWithIdentifier:@"forum-cell" forIndexPath:indexPath];
    cell.forum = self.forums[(NSUInteger) indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [DLForumCell getCellHeight];
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DLForumViewController *controller = DLForumViewController.new;
    controller.forum = self.forums[(NSUInteger) indexPath.row];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - Scroll view delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
}

#pragma mark - DLNavigation Bar View Delegate

- (void)navigationBarViewDidTapLeftButton:(UIButton *)button {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)setTitleText:(NSString*)titleText{
    self.titleTextView.text = titleText;
}
@end
