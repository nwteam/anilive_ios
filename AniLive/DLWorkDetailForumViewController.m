//
//  DLWorkDetailForumViewController.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/11/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLWorkDetailForumViewController.h"
#import "DLAPIHelper.h"
#import "DLForumCell.h"
#import "DLForumViewController.h"

@interface DLWorkDetailForumViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSNumber *workId;
@property (nonatomic, assign) CGFloat clearCellHeight;
@property (nonatomic, assign) NSInteger currentPageNumber;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) BOOL isExistNextPage;
@property (nonatomic, assign) BOOL isTransitioning; // 多重遷移防止フラグ

@end

@implementation DLWorkDetailForumViewController

- (instancetype)initWithHeaderClearCellHeight:(CGFloat)clearCellHeight  withWorkId:(NSNumber *)workId {
    self = [super init];
    if (self) {
        self.clearCellHeight = clearCellHeight;
        self.workId = workId;
    }
    return self;
}

#pragma mark - Override Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.items = [[NSMutableArray alloc]init];
    
    [self initView];
    [self updateFrame];
    [self loadData:DLLoadingTypeInitial];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.isTransitioning = NO;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.isTransitioning = NO;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)setOffsetToBaseScrollView:(CGPoint)offset {
    [super setOffsetToBaseScrollView:offset];
    self.tableView.contentOffset = offset;
}

- (void)dealloc {
    FUNC_LOG
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - DLSegmentViewControllerProtocol

- (void)setScrollsToTop:(BOOL)scrollsToTop {
    
    self.tableView.scrollsToTop = scrollsToTop;
}

#pragma mark - Private Methods

- (void)initView {
    
#pragma mark Table View
    
    self.tableView = [[UITableView alloc]init];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.scrollsToTop = NO;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[UITableViewCell class]
           forCellReuseIdentifier:DLConstClearCellIdentifier];
    [self.tableView registerNib:[DLForumCell getDefaultNib] forCellReuseIdentifier:[DLForumCell getCellIdentifier]];
    [self.view addSubview:self.tableView];
}

- (void)updateFrame {
    self.tableView.frame = self.view.bounds;
}

- (void)loadData:(DLLoadingType)loadingType {
    
    if (self.isLoading) {
        return;
    }
    
    if (loadingType == DLLoadingTypeInitial || loadingType == DLLoadingTypePullRefresh) {
        self.currentPageNumber = 1;
        self.isExistNextPage = YES;
    }
    
    if (!self.isExistNextPage) {
        return;
    }
    
    self.isLoading = YES;
    
    @weakify(self)
    [DLAPIHelper fetchForumsByWorkID:[self.workId integerValue]
                         pageNumber:self.currentPageNumber
                            perPage:DLConstPerPageCatalogDetailForums
                           callback:^(NSMutableArray *works, NSError *error) {
                               @strongify(self)
                               
                               if (NOT_NULL(error)) {
                                   // TODO: エラー時のポップアップなどを表示する
                               } else {
                                   if (loadingType == DLLoadingTypeInitial || loadingType == DLLoadingTypePullRefresh) {
                                       [self.items removeAllObjects];
                                   }
                                   
                                   if (works.count > 0) {
                                       [self.items addObjectsFromArray:works];
                                       self.currentPageNumber += 1;
                                   } else {
                                       self.isExistNextPage = NO;
                                   }
                               }
                               
                               [self.tableView reloadData];
                               
                               self.isLoading = NO;
                           }];
}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        return 1;
    } else {
        return self.items.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DLConstClearCellIdentifier
                                                                forIndexPath:indexPath];
        cell.userInteractionEnabled = NO;
        cell.backgroundColor = [UIColor clearColor];
        
        return cell;
    } else {
        DLForumCell *cell = [tableView dequeueReusableCellWithIdentifier:[DLForumCell getCellIdentifier] forIndexPath:indexPath];
        if (indexPath.row < self.items.count) {
            DLForum *forumData = self.items[indexPath.row];
            [cell setForum:forumData];
        }
        return cell;
    }
    return  nil;
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        return self.clearCellHeight;
    } else {
        return [DLForumCell getCellHeight];
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.isTransitioning && indexPath.row < self.items.count) {
        self.isTransitioning = YES;
        
        // フォーラム詳細へ遷移
        DLForumViewController *controller = [[DLForumViewController alloc]init];
        controller.forum = (DLForum *)self.items[indexPath.row];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    self.currentContentOffset = scrollView.contentOffset;
    
    if ([self.delegate respondsToSelector:@selector(retractableScrollViewDidScroll:)]) {
        [self.delegate retractableScrollViewDidScroll:scrollView];
    }
    
    CGFloat offsetY = scrollView.contentOffset.y;
    
    if (offsetY > self.tableView.contentSize.height - self.tableView.height - DLConstPagingBottomMargin) {
        [self loadData:DLLoadingTypePaging];
    }
}

@end
