//
//  DLRetractableHeaderView.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/11/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLRetractableHeaderView.h"
#import "DLWork.h"
#import "DLHighlightedButton.h"
#import "UIImageView+Category.h"

const CGFloat ktopImageViewHeight = 130;
const CGFloat kWorkImageViewWidth = 80;
const CGFloat kBackButtonSide = 40;
const CGFloat kStartMovingTitleLabelPadding = 160;
const CGFloat kMinMaskViewAlpha = 0;
const CGFloat kMaxMaskViewAlpha = 0.7;

@interface DLRetractableHeaderView ()

@property (nonatomic, strong) UIView *maskView;
@property (nonatomic, strong) UIImageView *backImageView;
@property (nonatomic, strong) DLHighlightedButton *backButton;
@property (nonatomic, strong) UILabel *navigationTitleLabel;
@property (nonatomic, strong) UIImageView *topImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *workImageView;
@property (nonatomic, strong) UILabel *synopsisLabel;
@property (nonatomic, strong) DLHighlightedButton *showInfoLabelButton;
@property (nonatomic, strong) DLHighlightedButton *showInfoButton;
@property (nonatomic, strong) DLWork *workData;
@property (nonatomic, assign) CGFloat startHeight;

@end

@implementation DLRetractableHeaderView

- (instancetype)initWithWorkData:(DLWork *)workData withStartHeight:(CGFloat)startHeight
{
    self = [super init];
    if (self) {
        self.workData = workData;
        self.startHeight = startHeight;
        [self initProcess];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
}

#pragma mark - Public Methods

- (void)updateWorkData:(DLWork *)workData {
    
    self.workData = workData;
    [self setViewData];
}

- (void)setViewData {
    
    // TOPの画像は一旦なしにする
//    if (NOT_NULL(self.workData.image2)) {
//        [UIImageView loadImageWithURLString:self.workData.image2
//                            targetImageView:self.topImageView];
//    }
    
    if (NOT_NULL(self.workData.image1)) {
        [UIImageView loadImageWithURLString:self.workData.image1
                            targetImageView:self.workImageView];
    }
    
    self.navigationTitleLabel.text = self.workData.title;
    self.titleLabel.text = self.workData.title;
    self.synopsisLabel.text = self.workData.synopsis;
    
    if (self.workData.synopsis.length < 3) {
        self.showInfoLabelButton.hidden = YES;
    } else {
        self.showInfoLabelButton.hidden = NO;
    }
    
    [self updateFrame];
}

#pragma mark - Private Methods

- (void)initProcess {
    
    [self initView];
    [self setViewData];
}

- (void)initView {
    
    self.backgroundColor = [UIColor clearColor];
    self.userInteractionEnabled = YES;
    self.clipsToBounds = YES;
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = BASE_FONT(22);
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self addSubview:self.titleLabel];
    
    self.topImageView = [[UIImageView alloc]init];
    self.topImageView.backgroundColor = [UIColor colorWithHex:DLConstColorDarkBlue];
    self.topImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.topImageView.clipsToBounds = YES;
    [self addSubview:self.topImageView];
    
    self.maskView = [[UIView alloc]init];
    self.maskView.backgroundColor = [UIColor blackColor];
    self.maskView.alpha = kMinMaskViewAlpha;
    [self.topImageView addSubview:self.maskView];
    
    self.navigationTitleLabel = [[UILabel alloc]init];
    self.navigationTitleLabel.backgroundColor = [UIColor clearColor];
    self.navigationTitleLabel.font = BASE_FONT(22);
    self.navigationTitleLabel.textColor = [UIColor whiteColor];
    self.navigationTitleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.topImageView addSubview:self.navigationTitleLabel];
    
    self.workImageView = [[UIImageView alloc]init];
    self.workImageView.backgroundColor = [UIColor lightGrayColor];
    self.workImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.workImageView.clipsToBounds = YES;
    self.workImageView.x = DLConstDefaultMargin;
    self.workImageView.width = kWorkImageViewWidth;
    self.workImageView.height = kWorkImageViewWidth / 2 * 3;
    [self addSubview:self.workImageView];
    
    self.backImageView = [[UIImageView alloc]init];
    self.backImageView.backgroundColor = [UIColor clearColor];
    self.backImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.backImageView.image = [UIImage imageNamed:DLConstIconBack];
    [self addSubview:self.backImageView];
    
    self.synopsisLabel = [[UILabel alloc]init];
    self.synopsisLabel.backgroundColor = [UIColor clearColor];
    self.synopsisLabel.font = BASE_FONT(12);
    self.synopsisLabel.textColor = [UIColor whiteColor];
    self.synopsisLabel.numberOfLines = 3;
    self.synopsisLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self addSubview:self.synopsisLabel];
    
    self.showInfoLabelButton = [DLHighlightedButton buttonWithType:UIButtonTypeSystem];
    self.showInfoLabelButton.backgroundColor = [UIColor clearColor];
    self.showInfoLabelButton.isNoNeedHighlight = YES;
    [self.showInfoLabelButton addTarget:self
                            action:@selector(showInfoButtonDidTap)
                  forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.showInfoLabelButton];
    
    self.showInfoButton = [DLHighlightedButton buttonWithType:UIButtonTypeCustom];
    self.showInfoButton.backgroundColor = [UIColor clearColor];
    [self.showInfoButton setImage:[UIImage imageNamed:DLConstIconInfoOutline] forState:UIControlStateNormal];
    self.showInfoButton.isNoNeedHighlight = YES;
    [self.showInfoButton addTarget:self
                            action:@selector(showInfoButtonDidTap)
                  forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.showInfoButton];
    
    self.backButton = [DLHighlightedButton buttonWithType:UIButtonTypeSystem];
    self.backButton.backgroundColor = [UIColor clearColor];
    self.backButton.isNoNeedHighlight = YES;
    [self.backButton addTarget:self
                        action:@selector(backButtonDidTap)
              forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.backButton];
}

- (void)backButtonDidTap {
    
    if ([self.delegate respondsToSelector:@selector(backButtonDidTap)]) {
        [self.delegate backButtonDidTap];
    }
}

- (void)showInfoButtonDidTap {
    
    if ([self.delegate respondsToSelector:@selector(showInfoButtonDidTap)]) {
        [self.delegate showInfoButtonDidTap];
    }
}

- (void)updateFrame {
    
    CGFloat maskViewAlpha = ((self.startHeight - self.height) / (self.startHeight - DLConstNavigationBarHeight)) * kMaxMaskViewAlpha;
    
    if (maskViewAlpha < kMinMaskViewAlpha) {
        maskViewAlpha = kMinMaskViewAlpha;
    }
    
    self.maskView.alpha = maskViewAlpha;
    
    CGFloat workImageViewScale = ((self.startHeight - self.height) / (self.startHeight - ktopImageViewHeight));
    
    if (workImageViewScale < 0) {
        workImageViewScale = 0;
    } else if (workImageViewScale > 0.99) {
        // 倍率が0倍になってしまうと戻って来れなくなるため
        workImageViewScale = 0.99;
    }
    workImageViewScale = 1.0 - workImageViewScale;
    
    self.workImageView.transform = CGAffineTransformMakeScale(workImageViewScale, workImageViewScale);
    self.workImageView.alpha = pow(workImageViewScale, 5);
    
    CGFloat topImageViewHeight = ktopImageViewHeight - MAX(ktopImageViewHeight - self.height,0);
    
    self.topImageView.frame = CGRectMake(0, 0, self.width, topImageViewHeight);
    self.maskView.frame = self.topImageView.bounds;
    
    [self.titleLabel sizeToFit];
    
    if (self.titleLabel.width > self.width - kWorkImageViewWidth - (DLConstDefaultMargin * 3)) {
        self.titleLabel.width = self.width - kWorkImageViewWidth - (DLConstDefaultMargin * 3);
    }
    self.titleLabel.x = kWorkImageViewWidth + (DLConstDefaultMargin * 2);
    
    [self.navigationTitleLabel sizeToFit];
    if (self.navigationTitleLabel.width > self.width - ((DLConstDefaultMargin + kBackButtonSide) * 2)) {
        self.navigationTitleLabel.width = self.width - ((DLConstDefaultMargin + kBackButtonSide) * 2);
    }
    self.navigationTitleLabel.midX = self.width / 2;
    
    CGFloat titleLabelY = self.topImageView.maxY + 12 - ((self.startHeight - (self.height + kStartMovingTitleLabelPadding)));
    
    if (titleLabelY < self.topImageView.height - 37) {
        titleLabelY = self.topImageView.height - 37;
    } else if (titleLabelY > self.topImageView.maxY + 12) {
        titleLabelY = self.topImageView.maxY + 12;
    }
    
    self.titleLabel.y = titleLabelY;
    self.workImageView.midY = self.titleLabel.midY;
    self.navigationTitleLabel.y = titleLabelY;
    
    self.synopsisLabel.frame = CGRectMake(DLConstDefaultMargin, self.titleLabel.maxY + 40 + DLConstDefaultMargin, self.width - (DLConstDefaultMargin * 2),60);
    
    self.showInfoLabelButton.frame = self.synopsisLabel.frame;
    
    self.showInfoButton.frame = CGRectMake(self.width - 20 - DLConstDefaultMargin, self.synopsisLabel.maxY + 14, 20, 20);
    
    self.backImageView.width = self.backImageView.height = 20;
    self.backButton.width = self.backButton.height = kBackButtonSide;
    self.backButton.x = DLConstDefaultMargin;
    self.backButton.y = DLConstNavigationBarHeight - self.backButton.height;
    self.backImageView.center = self.backButton.center;
}

@end
