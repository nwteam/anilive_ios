//
//  DLNavigationBarView.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/11.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLNavigationBarView.h"
#import "NSString+Category.h"
#import "DLNavigationBarItemView.h"
#import "DLMovie.h"
#import "DLLive.h"

@interface DLNavigationBarView ()

@property (nonatomic, strong) UIImageView *logoImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *counterBaseView;
@property (nonatomic, strong) UIImageView *playCountIconImageView;
@property (nonatomic, strong) UILabel *playCountLabel;
@property (nonatomic, strong) UIImageView *commentCountIconImageView;
@property (nonatomic, strong) UILabel *commentCountLabel;
@property (nonatomic, strong) DLNavigationBarItemView *leftItemView;
@property (nonatomic, strong) DLNavigationBarItemView *rightItemView;
@property (nonatomic, assign) DLNavigationBarViewType type;

@end

@implementation DLNavigationBarView

- (instancetype)initWithType:(DLNavigationBarViewType)type {
    self = [super init];
    if (self) {
        
        self.type = type;
        [self initView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

#pragma mark - Public Methods

- (void)setTitle:(NSString *)title {
    
    self.titleLabel.text = title;
    [self updateFrame];
}

- (void)setNavigationBarMovieData:(DLMovie *)movieData {
    
    self.titleLabel.text = movieData.title;
    
    [self updateFrame];
}

- (void)setNavigationBarLiveData:(DLLive *)liveData {
    
    self.titleLabel.text = liveData.title;
    self.playCountLabel.text = [NSString commasFormatedStringFromNumber:liveData.totalViews];
    self.commentCountLabel.text = [NSString commasFormatedStringFromNumber:liveData.totalForumsCount];
    
    [self updateFrame];
}

#pragma mark - Private Methods

- (void)initView {
    
    switch (self.type) {
        case DLNavigationBarViewTypeLogo:
        {
            self.backgroundColor = [UIColor colorWithHex:DLConstColorLightBlue];
            [self createLogoImageView];
            break;
        }
        case DLNavigationBarViewTypeTitle:
        {
            self.backgroundColor = [UIColor colorWithHex:DLConstColorLightBlue];
            [self createTitleLabelWithFont:BASE_FONT(16)];
            break;
        }
        case DLNavigationBarViewTypeMoviePlay:
        {
            self.backgroundColor = [UIColor colorWithHex:DLConstColorLightBlue];
            [self createTitleLabelWithFont:BASE_FONT(14)];
            [self createCounterView];
            [self createItemView];
            break;
        }
        case DLNavigationBarViewTypeForumList: {
            self.backgroundColor = [UIColor colorWithHex:DLConstColorLightBlue];
            [self createTitleLabelWithFont:BASE_FONT(16)];
            [self createItemView];
            break;
        }
        case DLNavigationBarViewTypeMyPage:
        {
            self.backgroundColor = [UIColor colorWithHex:DLConstColorLightBlue];
            [self createTitleLabelWithFont:BASE_FONT(16)];
            [self createCounterView];
            [self createItemView];
            break;
        }
        case DLNavigationBarViewTypeSettings:
        {
            self.backgroundColor = [UIColor colorWithHex:DLConstColorLightBlue];
            [self createTitleLabelWithFont:BASE_FONT(14)];
            [self createCounterView];
            [self createItemView];
            break;
        }
        case DLNavigationBarViewTypeWeb:
        {
            self.backgroundColor = [UIColor colorWithHex:DLConstColorLightBlue];
            [self createTitleLabelWithFont:BASE_FONT(14)];
            [self createItemView];
        }
    }
}

- (void)updateFrame {
    
    switch (self.type) {
        case DLNavigationBarViewTypeLogo:
        {
            self.logoImageView.frame = CGRectMake((self.width - 250) / 2, self.height - 45, 250, 40);
            break;
        }
        case DLNavigationBarViewTypeTitle:
        {
            self.titleLabel.frame = CGRectMake(0, self.height - 40, self.width, 40);
            break;
        }
        case DLNavigationBarViewTypeMoviePlay:
        {
            
            const CGFloat kIconSide = 10;
            
            self.leftItemView.frame = CGRectMake(DLConstDefaultMargin, self.height - 40, self.leftItemView.item.itemWidth, kDefaultItemViewSide);
            self.rightItemView.frame = CGRectMake(self.width - DLConstDefaultMargin - self.rightItemView.item.itemWidth, self.height - 40, self.rightItemView.item.itemWidth, kDefaultItemViewSide);
            
            [self.titleLabel sizeToFit];
            self.titleLabel.frame = CGRectMake(self.leftItemView.maxX + DLConstDefaultMargin, self.height - 40, self.width - ((self.leftItemView.maxX + DLConstDefaultMargin) * 2), self.titleLabel.height);
            
            self.playCountIconImageView.frame = CGRectMake(0, 0, kIconSide, kIconSide);
            [self.playCountLabel sizeToFit];
            self.playCountLabel.frame = CGRectMake(self.playCountIconImageView.maxX + 4, self.playCountIconImageView.y, self.playCountLabel.width, kIconSide);
            self.commentCountIconImageView.frame = CGRectMake(self.playCountLabel.maxX + 10, self.playCountIconImageView.y, kIconSide, kIconSide);
            [self.commentCountLabel sizeToFit];
            self.commentCountLabel.frame = CGRectMake(self.commentCountIconImageView.maxX + 4, self.playCountIconImageView.y, self.commentCountLabel.width, kIconSide);
            
            self.counterBaseView.width = self.commentCountLabel.maxX;
            self.counterBaseView.height = kIconSide;
            self.counterBaseView.midX = self.width / 2;
            self.counterBaseView.y = self.titleLabel.maxY + 4;
            
            break;
        }
        case DLNavigationBarViewTypeForumList:{
            self.titleLabel.frame = CGRectMake(0, self.height - 40, self.width, 40);
            self.leftItemView.frame = CGRectMake(DLConstDefaultMargin, self.height - 40, self.leftItemView.item.itemWidth, kDefaultItemViewSide);
            self.leftItemView.imageView.contentMode = UIViewContentModeScaleAspectFit;
            break;
        }
        case DLNavigationBarViewTypeMyPage:
        {
            self.titleLabel.frame = CGRectMake(0, self.height - 40, self.width, 40);
            self.rightItemView.frame = CGRectMake(self.width - DLConstDefaultMargin - self.rightItemView.item.itemWidth,
                                                  self.height - 40,
                                                  self.rightItemView.item.itemWidth,
                                                  kDefaultItemViewSide);
            break;
        }
        case DLNavigationBarViewTypeSettings:
        {
            self.titleLabel.frame = CGRectMake(0, self.height - 40, self.width, 40);
            self.leftItemView.frame = CGRectMake(DLConstDefaultMargin,
                                                 self.height - 40,
                                                 self.leftItemView.item.itemWidth,
                                                 kDefaultItemViewSide);
            break;
        }
        case DLNavigationBarViewTypeWeb:
        {
            self.leftItemView.frame = CGRectMake(DLConstDefaultMargin,
                                                 self.height - 40,
                                                 self.leftItemView.item.itemWidth,
                                                 kDefaultItemViewSide);
            self.titleLabel.frame = CGRectMake(self.leftItemView.maxX + DLConstDefaultMargin, self.height - 40, self.width - ((self.leftItemView.maxX + DLConstDefaultMargin) * 2), 40);
        }
    }
}

- (void)createLogoImageView {
    self.logoImageView = [[UIImageView alloc]init];
    self.logoImageView.backgroundColor = [UIColor clearColor];
    self.logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.logoImageView.image = [UIImage imageNamed:DLConstImageTopLogo];
    [self addSubview:self.logoImageView];
}

- (void)createTitleLabelWithFont:(UIFont *)font {
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = font;
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self addSubview:self.titleLabel];
}

- (void)createCounterView {
    
    self.counterBaseView = [[UIView alloc]init];
    self.counterBaseView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.counterBaseView];
    
    self.playCountIconImageView = [[UIImageView alloc]init];
    self.playCountIconImageView.backgroundColor = self.counterBaseView.backgroundColor;
    self.playCountIconImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.playCountIconImageView.image = [UIImage imageNamed:DLConstIconTotalView];
    [self.counterBaseView addSubview:self.playCountIconImageView];
    
    self.playCountLabel = [[UILabel alloc]init];
    self.playCountLabel.backgroundColor = self.counterBaseView.backgroundColor;
    self.playCountLabel.font = BASE_FONT_BOLD(9);
    self.playCountLabel.textColor = [UIColor colorWithHex:DLConstColorCodeGray];
    self.playCountLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.counterBaseView addSubview:self.playCountLabel];
    
    self.commentCountIconImageView = [[UIImageView alloc]init];
    self.commentCountIconImageView.backgroundColor = self.counterBaseView.backgroundColor;
    self.commentCountIconImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.commentCountIconImageView.image = [UIImage imageNamed:DLConstIconTotalComment];
    [self.counterBaseView addSubview:self.commentCountIconImageView];
    
    self.commentCountLabel = [[UILabel alloc]init];
    self.commentCountLabel.backgroundColor = self.counterBaseView.backgroundColor;
    self.commentCountLabel.font = BASE_FONT_BOLD(9);
    self.commentCountLabel.textColor = [UIColor colorWithHex:DLConstColorCodeGray];
    self.commentCountLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.counterBaseView addSubview:self.commentCountLabel];
}

- (void)createItemView {
    
    switch (self.type) {
        case DLNavigationBarViewTypeForumList:
        {
            DLNavigationBarItem *leftItem = [[DLNavigationBarItem alloc] initWithImage:[UIImage imageNamed:DLConstIconArrowBack]];
            self.leftItemView = [[DLNavigationBarItemView alloc] initWithItem:leftItem];
            [self addSubview:self.leftItemView];
            break;
        }
        case DLNavigationBarViewTypeMoviePlay:
        {
            DLNavigationBarItem *leftItem = [[DLNavigationBarItem alloc]initWithImage:[UIImage imageNamed:DLConstIconBack]];
            self.leftItemView = [[DLNavigationBarItemView alloc]initWithItem:leftItem];
            [self addSubview:self.leftItemView];
            
            DLNavigationBarItem *rightItem = [[DLNavigationBarItem alloc]initWithImage:[UIImage imageNamed:DLConstIconAddForum]];
            self.rightItemView = [[DLNavigationBarItemView alloc]initWithItem:rightItem];
            [self addSubview:self.rightItemView];
            
            break;
        }
        case DLNavigationBarViewTypeMyPage:
        {
            DLNavigationBarItem *rightItem = [[DLNavigationBarItem alloc] initWithImage:[UIImage imageNamed:DLConstIconSetting]];
            self.rightItemView = [[DLNavigationBarItemView alloc] initWithItem:rightItem];
            [self addSubview:self.rightItemView];
            
            break;
        }
        case DLNavigationBarViewTypeSettings:
        {
            DLNavigationBarItem *leftItem = [[DLNavigationBarItem alloc] initWithImage:[UIImage imageNamed:DLConstIconBack]];
            self.leftItemView = [[DLNavigationBarItemView alloc] initWithItem:leftItem];
            [self addSubview:self.leftItemView];
            
            break;
        }
        case DLNavigationBarViewTypeWeb:
        {
            DLNavigationBarItem *leftItem = [[DLNavigationBarItem alloc] initWithImage:[UIImage imageNamed:DLConstIconClose]];
            self.leftItemView = [[DLNavigationBarItemView alloc] initWithItem:leftItem];
            [self addSubview:self.leftItemView];
        }
        default:
            break;
    }
    
    if (NOT_NULL(self.leftItemView)) {
        [self.leftItemView.button addTarget:self
                                     action:@selector(leftButtonDidTapped:)
                           forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (NOT_NULL(self.rightItemView)) {
        [self.rightItemView.button addTarget:self
                                      action:@selector(rightButtonDidTapped:)
                            forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)leftButtonDidTapped:(UIButton *)button {
    
    if ([self.delegate respondsToSelector:@selector(navigationBarViewDidTapLeftButton:)]) {
        [self.delegate navigationBarViewDidTapLeftButton:button];
    }
}

- (void)rightButtonDidTapped:(UIButton *)button {
    
    if ([self.delegate respondsToSelector:@selector(navigationBarViewDidTapRightButton:)]) {
        [self.delegate navigationBarViewDidTapRightButton:button];
    }
}

@end
