//
//  DLPromotion.m
//  AniLive
//
//  Created by ykkc on 2016/12/30.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPromotion.h"

@interface DLPromotion ()
@property (readwrite, nonatomic, strong) NSNumber *liveId;
@property (readwrite, nonatomic, strong) NSNumber *promotionId;
@property (readwrite, nonatomic, strong) NSString *promotionTitle;
@property (readwrite, nonatomic, strong) NSString *moviePath;
@end

@implementation DLPromotion

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.liveId = CHECK_NULL_DATA_NUMBER(attributes[@"live_id"]);
    self.promotionId = CHECK_NULL_DATA_NUMBER(attributes[@"promotion_id"]);
    self.promotionTitle = CHECK_NULL_DATA_STRING(attributes[@"promotion_title"]);
    self.moviePath = CHECK_NULL_DATA_STRING(attributes[@"movie_path"]);
    
    return self;
}
@end
