//
//  DLForumHeaderView.h
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/10/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

@class DLForum;

@interface DLForumHeaderView : UIView
@property(readonly) CGFloat residualHeight;

@property(nonatomic, strong) UILabel *titleView;
@property(nonatomic, strong) UILabel *movieDurationLable;
@property(nonatomic, strong) UILabel *totalCommentsLable;
@property(nonatomic, strong) UIView *descriptionView;
@property(nonatomic, strong) UIImageView *imageView;
@property(nonatomic, strong) UILabel *movieTitleLable;
@property(nonatomic, strong) UILabel *totalFroumsLable;
@property(nonatomic, strong) UIButton *displayMovieButton;


- (void)setForum:(DLForum *)forum;
@end
