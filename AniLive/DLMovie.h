//
//  DLMovie.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/17.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLModelBase.h"

@class DLLive;
typedef NS_ENUM(NSInteger, DLMovieType) {
    UNSPECIFIED,
    PV,
    NORMAL,
    CAMPAIGN
};

@interface DLMovie : DLModelBase

@property (readonly, nonatomic, strong) NSNumber *workId;
@property (readonly, nonatomic, strong) NSNumber *movieId;
@property (readonly, nonatomic, strong) NSString *title;
@property (readonly, nonatomic, strong) NSNumber *priority;
@property (readonly, nonatomic, strong) NSNumber *chapter;
@property (readonly, nonatomic, strong) NSString *youtubeEmbed;
@property (readonly, nonatomic, strong) NSDate *releaseOpen;
@property (readonly, nonatomic, strong) NSDate *releaseClose;
@property (readonly, nonatomic, strong) NSString *image;
@property (readonly, nonatomic, strong) NSNumber *salesType;
@property (readonly, nonatomic, assign) NSNumber *pointFlag;
@property (readonly, nonatomic) DLMovieType movieType;
@property (readonly, nonatomic, strong) NSNumber *totalForums;
@property (readonly, nonatomic, assign) BOOL isWatched;
@property (readonly, nonatomic, strong) NSDate *watched;
@property (readonly, nonatomic, strong) NSNumber *playTime;

- (instancetype) initWithForum: (DLForum *) forum;
- (NSUInteger)getPointNeedToView;
@end
