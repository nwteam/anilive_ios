//
//  DLUserDataManager.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLUserDataManager.h"

@interface DLUserDataManager ()

@property (readwrite, nonatomic, strong) DLUser *user;

@end

@implementation DLUserDataManager

+ (instancetype)sharedInstance {
    static DLUserDataManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[DLUserDataManager alloc] init];
    });
    
    return manager;
}

#pragma mark - Public Methods

- (void)saveUserData:(NSDictionary *)dictionary {
    self.user = [[DLUser alloc]initWithAttributes:dictionary];
}

- (void)saveUserSessionToken:(NSDictionary *)dictionary {
    [self.user updateUserSessionToken:dictionary];
}

- (void)saveUserPointData:(NSDictionary *)dictionary {
    [self.user updateUserPointData:dictionary];
}

@end
