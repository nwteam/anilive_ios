//
//  ConstOther.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/11/05.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, DLLoadingType) {
    DLLoadingTypeInitial = 346,
    DLLoadingTypePullRefresh,
    DLLoadingTypePaging
};

// ヘッダーの透明セル用のIdentifier
static NSString *DLConstClearCellIdentifier = @"DLConstClearCellIdentifier";

@interface ConstOther : NSObject

// ページングする際の1ページの要素数
extern NSInteger const DLConstPerPageCatalogPopular;
extern NSInteger const DLConstPerPageCatalogAlphabetical;
extern NSInteger const DLConstPerPageCatalogDetailMovies;
extern NSInteger const DLConstPerPageCatalogDetailForums;
extern NSInteger const DLConstPerPageComments;
extern NSInteger const DLConstPerPageMyPageVideoHistory;
extern NSInteger const DLConstPerPageMyPageMyForums;
extern NSInteger const DLConstPerPageMyPageParticipatingForums;

// ページングが始まる時の下部マージン
extern CGFloat const DLConstPagingBottomMargin;

// Adobe Video Analytics
extern NSString * const DLConstAdobeAnalyticsTrackingServer;
extern NSString * const DLConstAdobeAnalyticsPublisher;
extern NSString * const DLConstAdobeAnalyticsPlayerName;

// Adobe Video Advertisement
extern NSString * const DLConstAdobeAdvertisementDomain;
extern NSInteger const DLConstAdobeAdvertisementZoneId;

// 動画広告の再生時間
extern NSInteger const DLConstPlayAdTime;

// MyPageのニックネームの最小文字数
extern NSInteger const DLConstNicknameWordCountMin;
// MyPageのニックネームの最大文字数
extern NSInteger const DLConstNicknameWordCountMax;

// Video Proxy Server
extern NSUInteger const DLConstVideoProxyServerHostingPort;
extern NSString * const DLConstVideoProxyServerLocalHost;
extern NSString * const DLConstVideoProxyServerTargetURLString;

// 初回起動判定用キー
extern NSString * const DLConstKeyIsFirstLaunch;

// Keychain UUID保存用キー
extern NSString * const DLConstKeyUUIDForKeyChain;

// Keychain UIID保存用キー
extern NSString * const DLConstKeyUIIDForKeyChain;

//UserDefaultsのキー
extern NSString * const kUserDefaultsKey_SettingNewShowAlert;
extern NSString * const kUserDefaultsKey_SettingNewLiveAlert;
extern NSString * const kUserDefaultsKey_SettingNewCommentsAlert;

// URL
extern NSString * const DLConstURLStringTerms;
extern NSString * const DLConstURLStringPrivacyPolicy;
extern NSString * const DLConstURLStringFaq;
extern NSString * const DLConstURLStringLicense;
extern NSString * const DLConstURLStringForumRules;
extern NSString * const DLConstURLStringContactUs;
extern NSString * const DLConstURLStringCopyrights;

// 動画視聴の際に要求されるポイント量
extern NSUInteger const DLConstNecessaryPointAmountOfWatchMovie;

@end
