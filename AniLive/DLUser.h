//
//  DLUser.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/12/28.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLModelBase.h"

// ユーザーの課金状態
typedef NS_ENUM(NSInteger, DLUserBillingType) {
    DLUserBillingTypeFree       = 1,
    DLUserBillingTypePremium    = 2,
    DLUserBillingTypeTrial      = 4
};

@interface DLUser : DLModelBase
@property(readonly, nonatomic, strong) NSString *name;
@property(readonly, nonatomic, strong) NSString *sessionToken;
@property(readonly, nonatomic, strong) NSNumber *deviceId;
@property(readonly, nonatomic, strong) NSNumber *userStatus;
@property(readonly, nonatomic, strong) NSNumber *billingType;
@property(readonly, nonatomic, strong) NSDate *trialEndDatetime;
@property(readonly, nonatomic, strong) NSNumber *point;
@property(readonly, nonatomic, strong) NSDate *pointLimit;

- (void)updateUserSessionToken:(NSDictionary *)attributes;
- (void)updateUserPointData:(NSDictionary *)attributes;
- (void)updateName:(NSString *)name;

// ユーザーの課金状態を欲しい場合はこちらを使ってくださいmm
- (DLUserBillingType)getBillingType;
@end
