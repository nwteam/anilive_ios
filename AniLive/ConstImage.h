//
//  ConstImage.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConstImage : NSObject


#pragma mark - Images

extern NSString * const DLConstImageComment;
extern NSString * const DLConstImageMovieLoading;
extern NSString * const DLConstImagePremium;
extern NSString * const DLConstImageSwipeBig;
extern NSString * const DLConstImageSwipeSmall;
extern NSString * const DLConstImageTap;

#pragma mark - Labels

extern NSString * const DLConstLabelLive;
extern NSString * const DLConstLabelPremium;

#pragma mark - Icons

extern NSString * const DLConstIconArrowBack;

extern NSString * const DLConstIconCheckBoxOff;
extern NSString * const DLConstIconCheckBoxOn;

extern NSString * const DLConstIconChevronLeftDisable;
extern NSString * const DLConstIconChevronLeft;
extern NSString * const DLConstIconChevronRightDisable;
extern NSString * const DLConstIconChevronRight;

extern NSString * const DLConstIconClose;

extern NSString * const DLConstIconFullScreenExit;
extern NSString * const DLConstIconFullScreen;

extern NSString * const DLConstIconKeyboadArrowDown;

extern NSString * const DLConstIconList;

extern NSString * const DLConstIconMakeForum;

extern NSString * const DLConstIconModeEditDisable;

extern NSString * const DLConstIconMoreVert;

extern NSString * const DLConstIconNotificationsOff;
extern NSString * const DLConstIconNotificationsOn;

extern NSString * const DLConstIconPause;

extern NSString * const DLConstIconPlayArrow;

extern NSString * const DLConstIconSchedule;

extern NSString * const DLConstIconStarOff;
extern NSString * const DLConstIconStarOn;

extern NSString * const DLConstIconTabBarCatalogOff;
extern NSString * const DLConstIconTabBarCatalogOn;
extern NSString * const DLConstIconTabBarForumOff;
extern NSString * const DLConstIconTabBarForumOn;
extern NSString * const DLConstIconTabBarLiveOff;
extern NSString * const DLConstIconTabBarLiveOn;
extern NSString * const DLConstIconTabBarNoticeOff;
extern NSString * const DLConstIconTabBarNoticeOn;
extern NSString * const DLConstIconTabBarMyPageOff;
extern NSString * const DLConstIconTabBarMyPageOn;

extern NSString * const DLConstIconWhatsHot;

extern NSString * const DLConstIconInfoOutline;


// ---------------------------------------------------- //
//                xxx Deprecated xxx
// ---------------------------------------------------- //

extern NSString * const DLConstImageTopLogo;
extern NSString * const DLConstImageSplashGIF;

extern NSString * const DLConstIconTotalView;
extern NSString * const DLConstIconTotalComment;
extern NSString * const DLConstIconBack;
extern NSString * const DLConstIconWhiteClose;
extern NSString * const DLConstIconLiveOff;
extern NSString * const DLConstIconAddForum;
extern NSString * const DLConstIconPlay;
extern NSString * const DLConstIconShrink;
extern NSString * const DLConstIconSetting;

extern NSString * const DLConstIconForumList;
extern NSString * const DLConstIconForumNext;
extern NSString * const DLConstIconForumPrev;
extern NSString * const DLConstIconForumPostComment;
extern NSString * const DLConstIconForumOther;

@end
