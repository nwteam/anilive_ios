//
//  DLInAppPurchaseManager.m
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/11/06.
//  Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import <StoreKit/StoreKit.h>
#import "DLInAppPurchaseManager.h"
#import "DLAPIHelper.h"
#import "AFHTTPSessionManager.h"


@interface DLInAppPurchaseManager () <SKProductsRequestDelegate, SKPaymentTransactionObserver, SKRequestDelegate, UIAlertViewDelegate>
@property(nonatomic) SKProductsRequest *myProductRequest;
@property(nonatomic) NSUInteger receiptRefreshCount;
@end

@implementation DLInAppPurchaseManager
+ (instancetype)sharedManager {
    static DLInAppPurchaseManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = self.new;
    });

    return _sharedManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.receiptRefreshCount = 0;
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    }
    return self;
}


+ (BOOL)canMakePurchases {
    return [SKPaymentQueue canMakePayments];
}

+ (void)requestProductData:(NSString *)pID {
    DLInAppPurchaseManager.sharedManager.myProductRequest = [[SKProductsRequest alloc]
            initWithProductIdentifiers:[NSSet setWithObject:pID]];
    DLInAppPurchaseManager.sharedManager.myProductRequest.delegate = DLInAppPurchaseManager.sharedManager;
    [DLInAppPurchaseManager.sharedManager.myProductRequest start];
}

+ (void)requestProductDatas {
    DLInAppPurchaseManager.sharedManager.myProductRequest = [[SKProductsRequest alloc]
            initWithProductIdentifiers:[NSSet setWithObjects:PURCHASE_ID_1, PURCHASE_ID_2, PURCHASE_ID_3, PURCHASE_ID_4, nil]];
    DLInAppPurchaseManager.sharedManager.myProductRequest.delegate = DLInAppPurchaseManager.sharedManager;
    [DLInAppPurchaseManager.sharedManager.myProductRequest start];
}


+ (void)setDelegate:(id <InAppPurchaseManagerDelegate>)delegate {
    DLInAppPurchaseManager.sharedManager.delegate = delegate;
}

+ (void)restore {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}


//Alert
- (void)showAlert:(NSString *)msg {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                    message:msg
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    NSLog(@"alert dismiss index:%ld", (long)buttonIndex);
}

+ (void)purchaseDone {
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    if ([[NSFileManager defaultManager] fileExistsAtPath:[receiptURL path]]) {
        NSLog(@"processing purchaseDone");
//        [DLAPIHelper validateReceiptURL:receiptURL];
        [DLInAppPurchaseManager validateByApple:receiptURL];
    } else if([DLInAppPurchaseManager sharedManager].receiptRefreshCount == 0) {
        NSLog(@"AppReceipt not found, refreshing...");
        [DLInAppPurchaseManager sharedManager].receiptRefreshCount++;
        SKReceiptRefreshRequest *refreshReceiptRequest = [[SKReceiptRefreshRequest alloc] initWithReceiptProperties:@{}];
        refreshReceiptRequest.delegate = DLInAppPurchaseManager.sharedManager;
        [refreshReceiptRequest start];
    }else{
        [DLInAppPurchaseManager sharedManager].receiptRefreshCount = 0;
        [DLInAppPurchaseManager requestProductData:PURCHASE_ID_1];
    }
}

+ (void)validateByApple:(NSURL *)receiptURL {
    NSData *receiptData = [NSData dataWithContentsOfURL:receiptURL];
    if (receiptData) {
        NSString *base64receiptString = [receiptData base64EncodedStringWithOptions:0];
//    NSString *serverURLString = @"https://buy.itunes.apple.com/verifyReceipt";
        NSString *serverURLString = @"https://sandbox.itunes.apple.com/verifyReceipt";
        NSDictionary *params = @{
                @"receipt-data": base64receiptString,
                @"password": @"646bb14ed6e7428c8e17e255cd32770c"
//                @"password": @"88fde3bf84ab4b328724c973c903f8aa"
        };
        AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
        mgr.responseSerializer = [AFJSONResponseSerializer serializer];
        mgr.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/json", @"text/plain"]];
        mgr.requestSerializer = [AFJSONRequestSerializer serializer];
        [mgr.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

        [mgr POST:serverURLString
       parameters:params
          success:^(NSURLSessionDataTask *task, id responseObject) {
              if ([responseObject isKindOfClass:[NSDictionary class]]) {
                  NSDictionary *dic = responseObject;
                  NSNumber *statusCode = dic[@"status"];
                  if (statusCode) {
                      NSString *desc;
                      switch (statusCode.longValue) {
                          case 0:
                          {
                              desc = @"receipt is valid";
                              [[DLInAppPurchaseManager sharedManager].delegate succeedInAppPurchaseReceipt:base64receiptString];
                              break;
                          }
                          case 21000:
                              desc = @"json could not read";
                              break;
                          case 21002:
                              desc = @"json format malformed";
                              break;
                          case 21003:
                              desc = @"receipt could not be authenticated";
                              break;
                          case 21004:
                              desc = @"password not match";
                              break;
                          case 21005:
                              desc = @"validation server 500";
                              break;
                          case 21006:
                              desc = @"receipt valid but expired";
                              break;
                          case 21007:
                              desc = @"you send sandbox receipt to production";
                              break;
                          case 21008:
                              desc = @"you send production receipt to sandbox";
                              break;
                          default:
                              desc = @"unexpected status code";
                      }
                      NSLog(@"validation call get response");
                      NSLog(@"%@", desc);
//                      [[DLInAppPurchaseManager sharedManager] showAlert:desc];
                  }
              }
          }
          failure:^(NSURLSessionDataTask *task, NSError *error) {
              NSLog(@"validation call fail");
          }];
    }
}

# pragma mark - SKProductsRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request
     didReceiveResponse:(SKProductsResponse *)response {

    if (response == nil) {
        [self showAlert:@"responseなし"];
        return;
    }

    // 確認できなかったidentifierをログに記録
    for (NSString *identifier in response.invalidProductIdentifiers) {
        [self.delegate
                failedInAppPurchase:identifier
                        withMessage:[NSString stringWithFormat:@"不正な購入項目です。：%@", identifier]];
    }
    if (response.products == 0) {
        return;
    }

    for (SKProduct *product in response.products) {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }

}

#pragma mark - SKPaymentTransactionObserver

- (void)paymentQueue:(SKPaymentQueue *)queue
 updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
            {
                [self.delegate purchasing];
                break;
            }
            case SKPaymentTransactionStatePurchased: {
                [self.delegate succeedInAppPurchase:transaction.payment.productIdentifier];
                [queue finishTransaction:transaction];
                [DLInAppPurchaseManager purchaseDone];
                NSLog(@"SKPaymentTransactionStatePurchased");
                break;
            }
            case SKPaymentTransactionStateFailed:
            {
                [queue finishTransaction:transaction];
                [self.delegate
                 failedInAppPurchase:transaction.payment.productIdentifier
                 withMessage:[NSString stringWithFormat:@"エラーコード：%ld 原因：%@", (long) transaction.error.code, transaction.error.localizedDescription]];
                
                break;
            }
            case SKPaymentTransactionStateRestored:
            {
                [self.delegate succeedInAppPurchase:transaction.payment.productIdentifier];
                [queue finishTransaction:transaction];
            }
            default:
                break;
        }
    }
}

#pragma mark - SKPaymentDelegate

- (void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions {

}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    [DLInAppPurchaseManager purchaseDone];
}

#pragma mark - SKRequestDelegate

- (void)requestDidFinish:(SKRequest *)request {
    [DLInAppPurchaseManager purchaseDone];
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"%@", error);
}
@end
