//
//  DLWatchingPlanViewController.m
//  AniLive
//
//  Created by volcano on 1/19/17.
//  Copyright © 2017 AnimeConsortiumJapan. All rights reserved.
//

#import "DLWatchingPlanViewController.h"
#import "DLAppFrameManager.h"
#import "DLNavigationBarView.h"
#import "DLUserDataManager.h"
#import "DLInAppPurchaseManager.h"

typedef NS_ENUM(NSInteger, DLSVODStoreAccessType) {
    DLSVODStoreAccessTypeDefault    = 0,
    DLSVODStoreAccessTypePurchase   = 1,
    DLSVODStoreAccessTypeRestore    = 2
};

@interface DLWatchingPlanViewController ()<DLNavigationBarViewDelegate, UIScrollViewDelegate, InAppPurchaseManagerDelegate>

@property (nonatomic, strong) DLNavigationBarView *navigationBarView;
@property (nonatomic, strong) UIScrollView        *mainScrollView;
@property (nonatomic, strong) UIImageView         *premiumImageView;
@property (nonatomic, strong) UILabel             *premiumTitleLabel;
@property (nonatomic, strong) UILabel             *premiumDescriptionLabel;
@property (nonatomic, strong) UILabel             *premiumOptionLabel_0;
@property (nonatomic, strong) UILabel             *premiumOptionLabel_1;
@property (nonatomic, strong) UILabel             *premiumOptionLabel_2;
@property (nonatomic, strong) UILabel             *premiumOptionLabel_3;
@property (nonatomic, strong) UIButton            *applyButton;
@property (nonatomic, strong) UIButton            *restoreButton;

@property (nonatomic, strong) UILabel             *currentPlanLabel;
@property (nonatomic, strong) UIButton            *checkPlanButton;
@property (nonatomic, strong) UILabel             *premiumPrefixLabel;
@property (nonatomic, strong) UILabel             *premiumDetailLabel;
@property (nonatomic, strong) UILabel             *nextUpdateLabel;
@property (nonatomic, assign) DLSVODStoreAccessType storeAccessType;


@end

@implementation DLWatchingPlanViewController


#pragma mark - Override Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;

    [self initView];
    [self updateFrame];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Private Methods

- (void)initView {

    // self.view.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkGray];
    self.view.backgroundColor = [UIColor colorWithHex:@"141723"];

#pragma mark Scroll View

    self.mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];

    [self.mainScrollView setShowsVerticalScrollIndicator:NO];

    [self.view addSubview:self.mainScrollView];

#pragma mark Navigation Bar

    self.navigationBarView = [[DLNavigationBarView alloc] initWithType:DLNavigationBarViewTypeSettings];

    self.navigationBarView.delegate = self;

    // TODO: Localize化
    [self.navigationBarView setTitle:Localizer(@"Watching Plan")];

    [self.view addSubview:self.navigationBarView];

    DLUser* user = [DLUserDataManager sharedInstance].user;

    if ([user getBillingType] == DLUserBillingTypeFree)
    {
#pragma mark Premium Image View

        self.premiumImageView = [[UIImageView alloc] initWithFrame:CGRectZero];

        [self.premiumImageView setImage:[UIImage imageNamed:DLConstImagePremium]];

        [self.mainScrollView addSubview:self.premiumImageView];

#pragma mark Premium Title Label

        self.premiumTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];

        [self.premiumTitleLabel setText:@"Join the premium plan!"];

        self.premiumTitleLabel.textAlignment = NSTextAlignmentCenter;

        self.premiumTitleLabel.textColor = [UIColor whiteColor];

        self.premiumTitleLabel.font = [self.premiumTitleLabel.font fontWithSize:24];

        [self.mainScrollView addSubview:self.premiumTitleLabel];

#pragma mark Premium Description Label

        self.premiumDescriptionLabel = [[UILabel alloc] initWithFrame:CGRectZero];

        [self.premiumDescriptionLabel setText:@"When you register for the premium plan..."];

        self.premiumDescriptionLabel.textColor = [UIColor darkGrayColor];

        self.premiumDescriptionLabel.font = [self.premiumTitleLabel.font fontWithSize:14];

        [self.mainScrollView addSubview:self.premiumDescriptionLabel];

#pragma mark Premuim Option Label

        self.premiumOptionLabel_0 = [[UILabel alloc] initWithFrame:CGRectZero];

        NSMutableAttributedString* attrStr0 = [[NSMutableAttributedString alloc] initWithString:@"- Watch the fastest and newest story!"];

        NSInteger attrLen = attrStr0.length;

        [attrStr0 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHex:@"ffffff"] range:NSMakeRange(0, attrLen)];

        [attrStr0 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHex:@"ffc900"] range:NSMakeRange(12, 8)];

        [attrStr0 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:12.0f] range:NSMakeRange(0, attrLen)];

        self.premiumOptionLabel_0.attributedText = attrStr0;

        [self.mainScrollView addSubview:self.premiumOptionLabel_0];

        self.premiumOptionLabel_1 = [[UILabel alloc] initWithFrame:CGRectZero];

        NSMutableAttributedString* attrStr1 = [[NSMutableAttributedString alloc] initWithString:@"- Watch all animation including premium limit!"];

        attrLen = attrStr1.length;

        [attrStr1 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHex:@"ffffff"] range:NSMakeRange(0, attrLen)];

        [attrStr1 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHex:@"ffc900"] range:NSMakeRange(attrLen-14, 14)];

        [attrStr1 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:12.0f] range:NSMakeRange(0, attrLen)];

        self.premiumOptionLabel_1.attributedText = attrStr1;

        [self.mainScrollView addSubview:self.premiumOptionLabel_1];

        self.premiumOptionLabel_2 = [[UILabel alloc] initWithFrame:CGRectZero];

        NSMutableAttributedString* attrStr2 = [[NSMutableAttributedString alloc] initWithString:@"- Watch all the special programs of each work!"];

        attrLen = attrStr2.length;

        [attrStr2 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHex:@"ffffff"] range:NSMakeRange(0, attrLen)];

        [attrStr2 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHex:@"ffc900"] range:NSMakeRange(16, 16)];

        [attrStr2 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:12.0f] range:NSMakeRange(0, attrLen)];

        self.premiumOptionLabel_2.attributedText = attrStr2;

        [self.mainScrollView addSubview:self.premiumOptionLabel_2];

        self.premiumOptionLabel_3 = [[UILabel alloc] initWithFrame:CGRectZero];

        NSMutableAttributedString* attrStr3 = [[NSMutableAttributedString alloc] initWithString:@"- Watch anime without almost advertising!"];

        attrLen = attrStr3.length;

        [attrStr3 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHex:@"ffffff"] range:NSMakeRange(0, attrLen)];

        [attrStr3 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHex:@"ffc900"] range:NSMakeRange(attrLen - 28, 28)];

        [attrStr3 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:12.0f] range:NSMakeRange(0, attrLen)];

        self.premiumOptionLabel_3.attributedText = attrStr3;

        [self.mainScrollView addSubview:self.premiumOptionLabel_3];


#pragma mark Apply Button

        self.applyButton = [[UIButton alloc] initWithFrame:CGRectZero];

        NSMutableAttributedString* ButtonAttrStr;

        if (user.trialEndDatetime == nil)
        {
            ButtonAttrStr = [[NSMutableAttributedString alloc] initWithString:@"START FREE TRIAL"];

            [self.applyButton addTarget:self action:@selector(onBtnStartFreeTrial) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            ButtonAttrStr = [[NSMutableAttributedString alloc] initWithString:@"GO PREMIUM"];

            [self.applyButton addTarget:self action:@selector(onBtnGoPremium) forControlEvents:UIControlEventTouchUpInside];
        }

        [ButtonAttrStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHex:@"ffc900" ] range:NSMakeRange(0, ButtonAttrStr.length)];

        UIFont* font_hel = [UIFont fontWithName:@"HelveticaNeue" size:18.0f];

        [ButtonAttrStr addAttribute:NSFontAttributeName value:font_hel range:NSMakeRange(0, ButtonAttrStr.length)];

        [self.applyButton setAttributedTitle:ButtonAttrStr forState:UIControlStateNormal];



        [self.mainScrollView addSubview:self.applyButton];

#pragma mark Restore Button

        self.restoreButton = [[UIButton alloc] initWithFrame:CGRectZero];

        NSMutableAttributedString* RestoreAttr = [[NSMutableAttributedString alloc] initWithString:@"If you purchased before, restore from here."];

        [RestoreAttr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHex:@"ffffff" ] range:NSMakeRange(0, RestoreAttr.length)];

        [RestoreAttr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:14.0f] range:NSMakeRange(0, RestoreAttr.length)];

        [RestoreAttr addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, RestoreAttr.length)];

        [self.restoreButton setAttributedTitle:RestoreAttr forState:UIControlStateNormal];

        [self.restoreButton addTarget:self action:@selector(onBtnRestore) forControlEvents:UIControlEventTouchUpInside];

        [self.mainScrollView addSubview:self.restoreButton];
    }
    else if ([user getBillingType] == DLUserBillingTypePremium ||
             [user getBillingType] == DLUserBillingTypeTrial)
    {

#pragma mark Current Plan Label

        self.currentPlanLabel = [[UILabel alloc] initWithFrame:CGRectZero];

        self.currentPlanLabel.text = @"Current plan";

        self.currentPlanLabel.textColor = [UIColor darkGrayColor];

        self.currentPlanLabel.font = [self.currentPlanLabel.font fontWithSize:14];

        [self.mainScrollView addSubview:self.currentPlanLabel];

#pragma mark Check Button

        self.checkPlanButton = [[UIButton alloc] initWithFrame:CGRectZero];

        self.checkPlanButton.backgroundColor = [UIColor darkGrayColor];

        [self.mainScrollView addSubview:self.checkPlanButton];

        [self.checkPlanButton addTarget:self action:@selector(onBtnCheckPlan) forControlEvents:UIControlEventTouchUpInside];

#pragma mark Prefix Label

        self.premiumPrefixLabel = [[UILabel alloc] initWithFrame:CGRectZero];

        self.premiumPrefixLabel.text = @"Premium";

        self.premiumPrefixLabel.textColor = [UIColor whiteColor];

        [self.mainScrollView addSubview:self.premiumPrefixLabel];

#pragma mark Price Label

        self.premiumDetailLabel = [[UILabel alloc] initWithFrame:CGRectZero];

        self.premiumDetailLabel.text = @"(6.99$/month)";

        self.premiumDetailLabel.textColor = [UIColor lightTextColor];

        [self.mainScrollView addSubview:self.premiumDetailLabel];

#pragma mark Next Update Label

        self.nextUpdateLabel = [[UILabel alloc] initWithFrame:CGRectZero];

        self.nextUpdateLabel.text = @"Next update Apr. 13, 2016 14:00";

        self.nextUpdateLabel.textColor = [UIColor whiteColor];

        [self.mainScrollView addSubview:self.nextUpdateLabel];

    }
}

- (void)updateFrame {

    CGRect applicationFrame = [DLAppFrameManager sharedInstance].applicationFrame;

    self.navigationBarView.frame = CGRectMake(0,
                                              CGRectGetMinX(applicationFrame),
                                              CGRectGetWidth(applicationFrame),
                                              DLConstNavigationBarHeight);

    self.mainScrollView.frame = self.view.bounds;

    NSInteger screenHeight = self.view.frame.size.height;

    screenHeight = screenHeight >= 568 ? screenHeight : 568;

    self.mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width, screenHeight);

    DLUser* user = [DLUserDataManager sharedInstance].user;

    if ([user getBillingType] == DLUserBillingTypeFree)
    {
#pragma mark Premium Image View
        // self.ScrollView.contentSize.height
        self.premiumImageView.frame = CGRectMake((self.mainScrollView.contentSize.width - 224) / 2,
                                                 100,
                                                 224,
                                                 174);

#pragma mark Premium Title Label

        self.premiumTitleLabel.frame = CGRectMake(20,
                                                  self.premiumImageView.frame.origin.y + self.premiumImageView.frame.size.height + 20,
                                                  self.mainScrollView.contentSize.width - 40,
                                                  40);

        self.premiumDescriptionLabel.frame = CGRectMake(20,
                                                        self.premiumTitleLabel.frame.origin.y + self.premiumTitleLabel.frame.size.height + 10,
                                                        self.mainScrollView.contentSize.width - 40,
                                                        16);

        self.premiumOptionLabel_0.frame = CGRectMake(40,
                                                     self.premiumDescriptionLabel.frame.origin.y + self.premiumDescriptionLabel.frame.size.height + 10,
                                                     self.mainScrollView.contentSize.width - 40,
                                                     16);

        self.premiumOptionLabel_1.frame = CGRectMake(40,
                                                     self.premiumOptionLabel_0.frame.origin.y + self.premiumOptionLabel_0.frame.size.height + 10,
                                                     self.mainScrollView.contentSize.width - 40,
                                                     16);

        self.premiumOptionLabel_2.frame = CGRectMake(40,
                                                     self.premiumOptionLabel_1.frame.origin.y + self.premiumOptionLabel_1.frame.size.height + 10,
                                                     self.mainScrollView.contentSize.width - 40,
                                                     16);

        self.premiumOptionLabel_3.frame = CGRectMake(40,
                                                     self.premiumOptionLabel_2.frame.origin.y + self.premiumOptionLabel_2.frame.size.height + 10,
                                                     self.mainScrollView.contentSize.width - 40,
                                                     16);
#pragma mark Apply Button

        self.applyButton.frame = CGRectMake(20,
                                            self.mainScrollView.contentSize.height - 90,
                                            self.mainScrollView.contentSize.width - 40,
                                            48);

        self.applyButton.layer.borderColor = [UIColor colorWithHex:@"ffc900"].CGColor;

        self.applyButton.layer.borderWidth = 1;

        self.applyButton.layer.cornerRadius = 5;

#pragma mark Restore Button

        self.restoreButton.frame = CGRectMake(20,
                                              self.applyButton.frame.origin.y + 60,
                                              self.mainScrollView.contentSize.width - 40,
                                              16);
    }
    else if ([user getBillingType] == DLUserBillingTypePremium || [user getBillingType] == DLUserBillingTypeTrial)
    {

        self.currentPlanLabel.frame = CGRectMake(20,
                                                 _navigationBarView.frame.origin.y + _navigationBarView.frame.size.height + 20,
                                                 self.mainScrollView.contentSize.width - 40,
                                                 16);

        self.checkPlanButton.frame = CGRectMake(0,
                                                self.currentPlanLabel.frame.origin.y + self.currentPlanLabel.frame.size.height + 10,
                                                self.mainScrollView.contentSize.width,
                                                60);
        self.premiumPrefixLabel.frame = CGRectMake(20, self.checkPlanButton.frame.origin.y + 10, 100, 16);

        self.premiumDetailLabel.frame = CGRectMake(120, self.checkPlanButton.frame.origin.y + 10, 200, 16);

        self.nextUpdateLabel.frame = CGRectMake(20, self.premiumPrefixLabel.frame.origin.y + self.premiumPrefixLabel.frame.size.height + 10, 300, 16);

    }


}
#pragma mark - DLNavigationBarViewDelegate

- (void)navigationBarViewDidTapLeftButton:(UIButton *)button {

    [self dismissViewControllerAnimated:YES completion:nil];

}
#pragma mark Action
-(void)onBtnCheckPlan{
    NSLog(@"Check Plan");

    UIAlertController * alert =   [UIAlertController
                                   alertControllerWithTitle:@""
                                   message:@"You can go to AppStore > Subscription to check the current auto update setting. To cancel your subscription, please switch auto update to OFF at least 24 hours prior to the update time."
                                   preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];

                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];

                             }];

    [alert addAction:ok];
    [alert addAction:cancel];

    [self presentViewController:alert animated:YES completion:nil];
}

-(void)onBtnStartFreeTrial{
    NSLog(@"onBtnStartFreeTrial");
    DLInAppPurchaseManager.delegate = self;
    self.storeAccessType = DLSVODStoreAccessTypePurchase;
    [DLInAppPurchaseManager requestProductData:PURCHASE_ID_3];
}

-(void)onBtnGoPremium{
    NSLog(@"onBtnGoPremium");
    DLInAppPurchaseManager.delegate = self;
    self.storeAccessType = DLSVODStoreAccessTypePurchase;
    [DLInAppPurchaseManager requestProductData:PURCHASE_ID_3];
}

-(void)onBtnRestore {
    NSLog(@"onBtnRestore");
    DLInAppPurchaseManager.delegate = self;
    self.storeAccessType = DLSVODStoreAccessTypeRestore;
    [DLInAppPurchaseManager purchaseDone];
}

- (void)succeedInAppPurchase:(NSString *)productId {
    NSLog(@"succeedInAppPurchase");
}

- (void)succeedInAppPurchaseReceipt:(NSString *)receipt {
    NSLog(@"succeedInAppPurchaseReceipt");
    switch (self.storeAccessType) {
        case DLSVODStoreAccessTypePurchase:
        {
            NSString *limitDateString = [[NSDate dateWithTimeIntervalSinceNow:7*24*60*60] description];
            NSString* dateString = limitDateString;
            if([limitDateString length] > 6) {
                dateString = [limitDateString substringToIndex:[limitDateString length]-6];
            }

            @weakify(self)
            [DLAPIHelper patchSubscriptionVideoOnDemand:receipt
                                       trialEndDateTime:dateString
                                               callback:^(NSInteger statusCode, NSError *error) {
                                                   NSLog(@"Result: %ld", (long)statusCode);
                                                   @strongify(self)
                                                   [self dismissViewControllerAnimated:NO completion:nil];
                                               }];
            break;
        }
        case DLSVODStoreAccessTypeRestore:
        {
            @weakify(self)
            [DLAPIHelper checkRestoreSubscription:receipt
                                         callback:^(NSInteger statusCode, NSError *error) {
                                             NSLog(@"Result: %ld", (long)statusCode);
                                             @strongify(self)
                                             [self dismissViewControllerAnimated:NO completion:nil];
                                         }];

            break;
        }
        case DLSVODStoreAccessTypeDefault:
        {
            [self dismissViewControllerAnimated:NO completion:nil];
            break;
        }
        default:
            break;
    }
}

- (void)failedInAppPurchase:(NSString *)productId withMessage:(NSString *)message {
    NSLog(@"failedInAppPurchase:%@", message);
//    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)purchasing {
    NSLog(@"purchasing");
}

@end

