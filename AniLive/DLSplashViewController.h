//
//  DLSplashViewController.h
//  AniLive
//
//  Created by Loyal Lauzier on 12/27/16.
//  Copyright © 2016 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DLSplashViewController : UIViewController

/* nilでない時、splash終了後DLNavigationControllerが起動され、
 *          notificationTransitionDictに応じた画面に遷移する
 *              DLWorkの場合: 作品詳細画面
 *              DLForumの場合: フォーラム個別画面
 *              DLLiveの場合: ライブ画面 */
@property (strong, nonatomic) NSDictionary *notificationTransitionDict;

@end
