//
//  DLForumData.h
//  AniLive
//
//  Created by mac on 2017/1/18.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import <Realm/Realm.h>

@interface DLForumData : RLMObject

@property(nonatomic, strong) NSNumber<RLMInt> *forumId;

@end
