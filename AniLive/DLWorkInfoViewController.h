//
//  DLWorkInfoViewController.h
//  AniLive
//
//  Created by 辰己佳祐 on 2017/01/14.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DLWork;
@interface DLWorkInfoViewController : UIViewController

- (instancetype)initWithWorkData:(DLWork *)workData;

@end
