//
//  DLPointHistoryCell.h
//  AniLive
//
//  Created by 市川 俊介 on 2016/12/24.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLSuperTableViewCell.h"
#import "DLPoint.h"

@interface DLPointHistoryCell : DLSuperTableViewCell

- (void)setPoint:(DLPoint *)point;
+ (UINib *)getDefaultNib;
+ (CGFloat)getCellHeight;

@property(nonatomic, strong) DLPoint *point;

@end
