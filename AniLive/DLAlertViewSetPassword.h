//
//  DLAlertViewSetPassword.h
//  AniLive
//
//  Created by suisun on 1/13/17.
//  Copyright © 2017 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DLAlertViewSetPasswordDelegate <NSObject>
@optional
- (void)didTapButtonDLAlertViewSetPassword:(NSInteger)index;
@end

@interface DLAlertViewSetPassword : UIView <UITextFieldDelegate>

/** delegate */
@property (nonatomic, weak) UIViewController<DLAlertViewSetPasswordDelegate> *delegate;
/** 確認メッセージラベル */

@property (weak, nonatomic) IBOutlet UIView *messageView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *msgLabel;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnOK;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

/** 初期化 */
- (id)initDLAlertViewSetPassword;
- (IBAction)tapBtnDLAlertViewSetPassword:(UIButton *)sender;

@end
