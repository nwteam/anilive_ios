//
//  DLNetworkManager.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/19.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *kNetworkStatusIdentifier = @"networkStatus";

typedef NS_ENUM(NSInteger, ORVNetworkType) {
    ORVNetworkTypeNotReachable = 0,
    ORVNetworkTypeWiFi,
    ORVNetworkType3G,
    ORVNetworkTypeLTE,
};

@interface DLNetworkManager : NSObject

@property (nonatomic, assign) ORVNetworkType networkStatus;

+ (instancetype)sharedInstance;

- (BOOL)hasInternetConnection;
- (ORVNetworkType)getNetworkStatus;

@end
