//
//  DLNotice.h
//  AniLive
//
//  Created by volcano on 12/22/16.
//  Copyright © 2016 AnimeConsortiumJapan. All rights reserved.
//

#import <Realm/Realm.h>

@interface DLNotice : RLMObject


@property NSInteger id;
@property NSString *title;
@property NSDate *createdAt;
@property BOOL isRead;
@property NSString *thumbImage;
@property NSInteger transitionItemId;
@property NSInteger transitionView;

extern const int kDLNoticeTransactionView_PlayMovie;
extern const int kDLNoticeTransactionView_Forum;
extern const int kDLNoticeTransactionView_Work;

+ (DLNotice*)initWithAttributes:(NSDictionary *)attributes;
+ (NSMutableArray *) getAll;
+ (void)changeToReadAll;
+ (BOOL)isExistYetRead;

@end
RLM_ARRAY_TYPE(DLNotice)
