//
//  DLCatalogPopularViewController.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/10/01.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLCatalogPopularViewController.h"
#import "DLCatalogPopularCell.h"
#import "DLTransitionManager.h"

@interface DLCatalogPopularViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) UICollectionViewFlowLayout *collectionViewFlowLayout;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, assign) NSInteger currentPageNumber;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) BOOL isExistNextPage;

@end

@implementation DLCatalogPopularViewController

#pragma mark - Override Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.items = [[NSMutableArray alloc]init];
    
    [self initView];
    [self updateFrame];
    [self loadData:DLLoadingTypeInitial];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - DLSegmentViewControllerProtocol

- (void)setScrollsToTop:(BOOL)scrollsToTop {
    
    self.collectionView.scrollsToTop = scrollsToTop;
}

#pragma mark - Private Methods

- (void)initView {
    
#pragma mark Collection View
    
    self.collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc]init];
    self.collectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero
                                            collectionViewLayout:self.collectionViewFlowLayout];
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.scrollsToTop = NO;
    [self.collectionView registerClass:[DLCatalogPopularCell class] forCellWithReuseIdentifier:[DLCatalogPopularCell getCellIdentifier]];
    [self.view addSubview:self.collectionView];
}

- (void)updateFrame {
    self.collectionView.frame = self.view.bounds;
}

- (void)loadData:(DLLoadingType)loadingType {
    
    if (self.isLoading) {
        return;
    }
    
    if (loadingType == DLLoadingTypeInitial || loadingType == DLLoadingTypePullRefresh) {
        self.currentPageNumber = 1;
        self.isExistNextPage = YES;
    }
    
    if (!self.isExistNextPage) {
        return;
    }
    
    self.isLoading = YES;
    
    @weakify(self)
    [DLAPIHelper fetchWorksWithPageNumber:self.currentPageNumber
                                  perPage:DLConstPerPageCatalogPopular
                                 sortType:@"popular"
                                 callback:^(NSMutableArray *works, NSError *error) {
                                     @strongify(self)
                                     
                                     if (NOT_NULL(error)) {
                                         // TODO: エラー時のポップアップなどを表示する
                                     } else {
                                         if (loadingType == DLLoadingTypeInitial || loadingType == DLLoadingTypePullRefresh) {
                                             [self.items removeAllObjects];
                                         }
                                         
                                         if (works.count > 0) {
                                             [self.items addObjectsFromArray:works];
                                             self.currentPageNumber += 1;
                                         } else {
                                             self.isExistNextPage = NO;
                                         }
                                     }
                                     
                                     [self.collectionView reloadData];
                                     
                                     self.isLoading = NO;
                                 }];
}

#pragma mark - UICollectionView DataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        DLCatalogPopularCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[DLCatalogPopularCell getCellIdentifier]
                                                                        forIndexPath:indexPath];
        
        if (indexPath.row < self.items.count) {
            DLWork *workData = self.items[indexPath.row];
            [cell setCellData:workData];
        }
        return cell;
    }
    return  nil;
}

#pragma mark - UICollectionViewDelegateFlowLayout Protocol

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section==0) {
        return [DLCatalogPopularCell getCellSizeWithParantWidth:self.collectionView.width];
    }
    return CGSizeZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    if (section == 0) {
        return kCatalogCellSpaceMargin;
    }
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if (section == 0) {
        return kCatalogCellSpaceMargin;
    }
    return 0;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return UIEdgeInsetsMake(kCatalogCellSpaceMargin, kCatalogCellSpaceMargin, kCatalogCellSpaceMargin, kCatalogCellSpaceMargin);
    }
    return UIEdgeInsetsZero;
}

#pragma mark - UICollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0 && indexPath.row < self.items.count) {
        DLWork *workData = self.items[indexPath.row];
        [DLTransitionManager showWorkDetailViewControllerWithWorkData:workData withTarget:nil];
    }
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat offsetY = scrollView.contentOffset.y;
    
    if (offsetY > self.collectionView.contentSize.height - self.collectionView.height - DLConstPagingBottomMargin) {
        [self loadData:DLLoadingTypePaging];
    }
}

@end
