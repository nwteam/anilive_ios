//
//  DLForumListViewController.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//


#import "DLNavigationBarView.h"
#import "DLForumGroup.h"
#import "DLForum.h"

@interface DLForumListViewController : UIViewController

@property(strong, nonatomic) DLForumGroup *group;
@property(strong, nonatomic) DLForum *froum;

@end
