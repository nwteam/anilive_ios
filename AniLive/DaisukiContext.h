//
//  DaisukiContext.h
//  drmtest
//
//  Created by MacBookAir on 13/07/18.
//  Copyright (c) 2013年 agyou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XmlDic.h"
#import "HLSCache.h"
#import "BGNCrypt.h"

// ret 1:success 2:id/pw error 3: networkerror 4: maintenance 5:unknown
typedef enum 
{
    DAISUKI_SUCCESS = 0,
    DAISUKI_IDPW_ERROR,
    DAISUKI_NETWORK_ERROR,
    DAISUKI_MAINTENANCE,
    DAISUKI_UNKNOWN_ERROR,
    DAISUKI_INVALID_ACCESS,
    DAISUKI_GEO_BLOCKED,
    DAISUKI_READY_NEW_VER,
    DAISUKI_MOVIE_FORMAT_ERROR,
    DAISUKI_MOVIE_NEED_BUY,
    DAISUKI_NEED_UPDATE_SERIES,
    DAISUKI_AKAMAI_ERROR
} DAISUKI_API_RESULT;

@interface DaisukiContext : NSObject
{
    NSMutableArray* _moviekeys;
    BGNCrypt*       _bgnCrypt;
    NSString*       _ipAddressCache;
}

//Movie Data
@property(  retain ) NSString*	productID;
@property(  retain ) NSString*	seriesTitle;
@property(  retain ) NSString*	productTitle;
@property(  retain ) NSString*  subscriptionStatus;
@property(  retain ) NSString*  movieProductId;
@property(  retain ) NSString*  adMovieName;
@property(  retain ) NSString*	captionURL;
@property(  retain ) XmlDic*	captions;  //	字幕
@property(  retain ) NSMutableArray*	movieAdCueuePoint;
@property(  retain ) NSString*	vastURL;
@property(  retain ) NSString*  fov; //動画画角

//BGN拡張
@property(  retain ) NSMutableArray*  renditionLabels; //複数Rendition用表示ラベル
@property(  retain ) NSString* playURL;
@property(  retain ) NSMutableArray*	m3u8URLs; //複数Rendition用m3u8URLリスト
@property(  retain ) NSMutableArray*    m3u8Rendition;
@property(  retain ) NSString*	bgnSessionID;          //効果測定用BGNセッションID
@property(  retain ) NSString*	bgnLogURL;             //効果測定用BGNURL
@property(  assign ) int        bgnLogCount;           //効果測定用ログ送信カウント
@property(  retain ) NSDate*    bgnPlayStartTime;      //効果測定用再生開始時刻
@property(  retain ) NSString*	nextProductID;
@property(  retain ) NSString*	prevProductID;
@property(  retain  ) NSMutableArray*    captionLangs;

//Movie制御
@property( atomic, retain ) HLSCache*       hlsCache;

//UI
@property(  retain ) XmlDic*	selectedSeries;
@property(  retain ) NSMutableArray*	seriesTitles;
@property(  retain ) NSMutableArray*	seriesImages;
@property(  retain ) NSMutableArray*	seriesOrigin;
@property(  retain ) NSMutableArray*	seriesOriginImages;
@property(  retain ) NSMutableArray*	searchWordStdios;
@property(  retain ) NSMutableArray*	searchWordCategories;
@property(  retain ) NSMutableArray*	searchSeries;

//Movie Player Setting
@property (retain, nonatomic) NSString*			movieClosedCaptionLang;
@property (retain, nonatomic) NSString*			movieRendition;
@property (assign, nonatomic) BOOL              autoSeriesPlayEnable;
@property (retain, nonatomic) NSString*			movieURL;

//UserInfo
@property(  assign ) BOOL       isFirstExec;
@property(  retain ) NSString*  userID;
@property(  retain ) NSString*  userPassword;
@property(  retain ) NSString*	seriesHash;		//saved

@property(  retain ) NSString*  memberID;       //not saved
@property(  retain ) NSString*  memberName;     //not saved
@property(  retain ) NSString*	appVersion;		//not saved
@property(  retain ) NSString*	country;		//not saved
@property(  assign ) int		bannerCount;    //not saved	バナー数
@property(  assign ) BOOL		isFirstHelpTop;		//	トップのヘルプ
@property(  assign ) BOOL		isFirstHelpMovie;	//	動画プレイヤーのヘルプ
@property(  assign ) BOOL		isFirstHelpMenu;	//	メニューのヘルプ
@property(  assign ) BOOL		isNeedToUpdateSeriesList;	//ログイン後トップリストを更新する必要があるか？
@property(  assign ) BOOL		isNeedUpdate;		//	要アプリアップデート？
@property(  assign ) int		lastError;          //	最後のエラーコード

//	スキーム起動
@property( assign ) NSString*	schemeMovieID;	//	スキーム起動での動画 ID


//Initialize
- (void)loadSetting;
- (void)saveSetting;

//WebAPI
- (int)apiLogin : (NSString*)user password:(NSString*)password; //Login ret 1:success 2:id/pw error 3: networkerror 4: maintenance
- (BOOL)apiLogout ;
- (BOOL)apiTop;
//- (int)apiGetLicense : (NSString*)product_id movie_id:(NSString*)movie_id;
//- (NSData*)apiGetKey : (NSString*)product_id movie_id:(NSString*)movie_id level:(NSString*)level keyNo:(int)keyNo;
//- (NSData*)apiGetAllKeys : (NSString*)product_id movie_id:(NSString*)movie_id level:(NSString*)level;
//- (int)apiGetProperty : (NSString*)product_id movie_id:(NSString*)movie_id;
- (NSString*)apiRequestToken;
- (XmlDic*)apiSeriesDetail:(NSString*)series_id;
- (XmlDic*)apiProductDetail:(NSString*)product_id;
- (XmlDic*)apiAllTitles;
- (BOOL)apiGetSearchWords;
- (BOOL)apiSearch:(NSString*)mode stdio:(NSString*)stdio genre:(NSString*)genre keyword:(NSString*)keyword page:(int)page;
- (XmlDic*)apiVideoHistory;
- (XmlDic*)apiVideoPurchase;


//BGN
- (int)apiBGNInit : (NSString*)product_id;
- (void)apiBGNLog : (int)videoPlayTimeMSec;

- (NSString*) getM3U8URL : (NSString*)rendition;


//Programming Interface
//- (int)initializeMovieLicense : (NSString*)product_id movie_id:(NSString*)movie_id level:(NSString*)level;
//- (int)getDecodedKeyCount;
//- (NSData*)getDecodedKey :(int) keyNo;

//字幕ロード
- (void)loadCaption;

//	表示シリーズタイトルを切り替える
- (void)switchSimulcast:(BOOL)onoff;

//その他ユーティリティー
- (NSString *)md5:(NSString*)str;
- (NSString *)urlDecode : (NSString*)uri;
- (NSString *)getIPAddress; //IPアドレスの取得
- (void)updateIPAddressCache; //IPアドレスキャッシュの内部更新
- (NSString *)getLocalURL; //ローカルURLの取得

//
- (NSString *)subscriptionStatusFreeCaseString:(NSString *)freeCase chargeCase:(NSString *)chargeCase;


@end
