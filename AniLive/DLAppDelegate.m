//
//  DLAppDelegate.m
//  AniLive
//
//  Created by ykkc on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <Growthbeat/Growthbeat.h>
#import <Realm/Realm.h>

#import "DLAppDelegate.h"
#import "DLNavigationController.h"
#import <UserNotifications/UserNotifications.h>
#import <PSDKLibrary/PSDKLibrary.h>
#import "DLNotice.h"
#import "DLNoticeViewController.h"
#import "DLNotificationService.h"
#import "HTTPServer.h"
#import "DRMHttpConnection.h"
#import "DLOpenPlayMovieService.h"
#import <SmartBeatFramework/SmartBeat.h>

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface DLAppDelegate () <
UNUserNotificationCenterDelegate,
UINavigationControllerDelegate,
UITabBarControllerDelegate,
UITabBarDelegate
>

@end

@implementation DLAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //-------------- For PrimeTime x Akamai -----------------
    _httpServer = [[HTTPServer alloc] init];
    [_httpServer setType:@"_http._tcp."];
    [_httpServer setPort:(UInt16) DLConstVideoProxyServerHostingPort];
    [_httpServer setDocumentRoot:[[NSBundle mainBundle] resourcePath]];
    [_httpServer setConnectionClass:[DRMHttpConnection class]];
    [_httpServer start:nil];
    [PTMediaPlayer enableDebugLog:YES];
    self.context = [[DaisukiContext alloc] init];
    [self.context loadSetting];

    //-------------- APNS(Push通知から起動時のハンドリング) -----------------
    NSDictionary *userInfo = [launchOptions objectForKey:
                              UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if(userInfo != nil)
    {
        NSLog(@"Push通知から起動");
        //[self application:application didFinishLaunchingWithOptions:userInfo];
        [DLNotificationService onLaunchFromNotification:userInfo];
    } else {
        [self openSplashViewController:nil];
    }
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound |
                                                 UNAuthorizationOptionAlert |
                                                 UNAuthorizationOptionBadge)
                              completionHandler:^(BOOL granted, NSError * _Nullable error){
                                  if( !error ){
                                      [[UIApplication sharedApplication] registerForRemoteNotifications];
                                  }
                              }];  
    }
    
    /*
     * Push通知
     */
    [[GrowthPush sharedInstance] initializeWithApplicationId:@"Q0kIG7LNCGwcfKG6" credentialId:@"fwS93Rc9LQePVjXogq84zNzVGitKmsCv" environment:kGrowthPushEnvironment];
    
    [[GrowthPush sharedInstance] requestDeviceToken];

    [DLNotificationService reflectPushSettingAsTagToServer];
    
    // SmartBeatの初期化
    [SmartBeat startWithApiKey:SMARTBEAT_APIKEY];

    // UnityAdsの初期化
    [DLOpenPlayMovieService sharedInstance];

    /*
     * シミュレータでのテスト用
     */
    /*
    DLNotice *notice = [DLNotice initWithAttributes:@{
            @"thumb_image": @"http://www.daisuki.net/content/dam/pim/daisuki/en/DRAGONBALLSUPERChampa/14922/movie.jpg",
            @"transition": @{
                    @"item_id": [NSNumber numberWithLong:169],
                    @"view":[NSNumber numberWithLong:2]
            }
    }];
    RLMRealm *realm = RLMRealm.defaultRealm;
    [realm beginWriteTransaction];
    notice.id = [[NSDate date] timeIntervalSince1970];
    [realm addObject:notice];
    [realm commitWriteTransaction];*/

    return YES;
}

- (void)openSplashViewController: (NSDictionary*) notificationTransitionDict {
    /*
     * splashViewを起動する
     * :param notificationTransitionObject:
     *          nilでない時、Splash画面のからDLNavigationControllerが起動され、
     *          notificationTransitionObjectに応じた画面に遷移する
     *              DLWorkの場合: 作品詳細画面
     *              DLForumの場合: フォーラム個別画面
     *              DLLiveの場合: 動画再生画面
     */
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    DLSplashViewController *splashViewController = [[DLSplashViewController alloc] initWithNibName:@"DLSplashViewController" bundle:nil];
    splashViewController.notificationTransitionDict = notificationTransitionDict;
    self.window.rootViewController = splashViewController;
    [self.window makeKeyAndVisible];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings // NS_AVAILABLE_IOS(8_0);
{
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken{
     NSString *sDeviceToken = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    NSLog(@"deviceToken:%@", sDeviceToken);
     sDeviceToken = [sDeviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    [[GrowthPush sharedInstance] setDeviceToken:deviceToken];
}

- (void) application:(UIApplication* )application didFailToRegisterForRemoteNotificationsWithError:(NSError* )error {
    NSLog(@"didFailToRegisterForRemoteNotification : %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"Push通知から起動");
    [DLNotificationService sendNotification: userInfo];
    if (application.applicationState == UIApplicationStateInactive) {
        [DLNotificationService onLaunchFromNotification:userInfo];
    }
}

- (void)parseNotification:(NSDictionary *)notification {
    // message
    NSString *message = [notification objectForKey:@"message"];
    NSLog(@"message:%@", message);

    // post notification with notification center
    
    
    [DLNotificationService sendNotification:notification];
    // show total badge count on app icon
    // TODO アプリケーションバッヂ数はRealmよりNotificationテーブルでのonUpdate等でキャッチして変更する
//    [UIApplication sharedApplication].applicationIconBadgeNumber = ++badgeNum;
}

- (void)setTabBarViewIsHidden:(BOOL)isHidden {
    [self.rootTabBarController setTabBarViewIsHidden:isHidden];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
