//
//  DLContinueToPlaySeeLaterCell.m
//  AniLive
//
//  Created by volcano on 1/4/17.
//  Copyright © 2017 AnimeConsortiumJapan. All rights reserved.
//

#import "DLContinueToPlaySeeLaterCell.h"
#import "DLSeelaterManager.h"

const static CGFloat kDLContinueToPlaySeeLaterCellHeight = 80.0f;

@interface DLContinueToPlaySeeLaterCell()

@property (weak, nonatomic) IBOutlet UILabel *commentText;
@property (weak, nonatomic) IBOutlet UIImageView *rateImage;
@property (weak, nonatomic) IBOutlet UILabel *commentTimeText;
@property (weak, nonatomic) IBOutlet UILabel *commentCountText;
@property (weak, nonatomic) IBOutlet UIImageView *commentIconImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *durationLabelWidth;
@property (weak, nonatomic) IBOutlet UILabel *commentsCountLabelWidth;
@property (weak, nonatomic) IBOutlet UIImageView *popularMarkA;
@property (weak, nonatomic) IBOutlet UIImageView *polularMarkB;
@property (weak, nonatomic) IBOutlet UIImageView *popularMarkC;
@property (weak, nonatomic) IBOutlet UIButton *seelatterButton;
@property (strong, nonatomic) DLForum *forum;
@end

@implementation DLContinueToPlaySeeLaterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self updateFrame];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateFrame{
    self.commentTimeText.layer.borderWidth = 1.0f;
    
    self.commentTimeText.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.commentTimeText.layer.cornerRadius = 5.0f;
}

+ (UINib *)getDefaultNib {    
    return [UINib nibWithNibName:@"DLContinueToPlaySeeLaterCell" bundle:nil];
}

+ (CGFloat)getCellHeight {
    return kDLContinueToPlaySeeLaterCellHeight;
}

- (void)dispalyForum:(DLForum *)forum {
    _forum = forum;
    _commentText.text = forum.title;
    _commentTimeText.text = forum.forumPinnedTime;
    
    if (self.commentTimeText.text) {
        CGSize size = [self.commentTimeText.text sizeWithFont:self.commentTimeText.font constrainedToSize:CGSizeMake(200, 21) lineBreakMode:NSLineBreakByWordWrapping];
        self.durationLabelWidth.constant = size.width + 2;
    }
    
    if ([forum.totalComments integerValue] > 0) {
        self.commentCountText.text = [NSString stringWithFormat:@"%@  comments",forum.totalComments];
        self.commentCountText.textColor = [UIColor colorWithHex:DLConstColorLightWhite];
    }
    else {
        self.commentCountText.text = @"NEW";
        self.commentCountText.textColor = [UIColor colorWithHex:DLConstColorCodeYellow];
    }
    
    self.seelatterButton.selected = [DLSeelaterManager checkFroumIfInSeelater:forum];
    
    _popularMarkC.hidden = [forum.popularity intValue] >= 10 ? NO : YES;
    _polularMarkB.hidden = [forum.popularity intValue] >= 3 ? NO : YES;
    _popularMarkA.hidden = [forum.popularity intValue] >= 1 ? NO : YES;
}

- (IBAction)seeLatterButtonClicked:(UIButton *)sender {
    @weakify(sender)
    if (sender.selected) {
        [DLSeelaterManager removeSeelaterWithForum:_forum Completion:^(BOOL success) {
            @strongify(sender)
            if (success) {
                sender.selected = NO;
            }
        }];
    }
    else {
        [DLSeelaterManager addSeelaterWithForum:_forum Completion:^(BOOL success) {
            @strongify(sender)
            if (success) {
                sender.selected = YES;
            }
        }];
    }
}

@end
