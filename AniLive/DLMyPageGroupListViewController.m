//
//  DLMyPageGroupListViewController.m
//  AniLive
//
//  Created by Isao Kono on 2016/11/04.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLAPIHelper.h"

#import "DLMyPageGroupListViewController.h"
#import "DLForumCell.h"
#import "DLVideoHistoryCell.h"

@interface DLMyPageGroupListViewController () <UITableViewDelegate, UITableViewDataSource, DLSegmentViewControllerProtocol>

@property (nonatomic, assign) DLMyPageSegmentType type;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, assign) NSInteger currentPageNumber;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) BOOL isExistNextPage;

@end

@implementation DLMyPageGroupListViewController

#pragma mark - Override Methods

- (instancetype)initWithType:(DLMyPageSegmentType)type {
    if (self = [super init]) {
        self.type = type;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.items = [NSMutableArray new];
    
    [self initView];
    [self updateFrame];
    [self loadData:DLLoadingTypeInitial];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - DLSegmentViewControllerProtocol

- (void)setScrollsToTop:(BOOL)scrollsToTop {
    self.tableView.scrollsToTop = scrollsToTop;
}

#pragma mark - Private Methods

- (void)initView {
    
    self.view.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.scrollsToTop = NO;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    switch (self.type) {
        case DLMyPageSegumentTypeVideoHistory: {
            [self.tableView registerClass:[DLVideoHistoryCell class]
                   forCellReuseIdentifier:[DLVideoHistoryCell getCellIdentifier]];
            
            break;
        }
        case DLMyPageSegumentTypeMyForum: {
            [self.tableView registerNib:[DLForumCell getDefaultNib]
                 forCellReuseIdentifier:[DLForumCell getCellIdentifier]];
            
            break;
        }
        case DLMyPageSegumentTypeParticipateForum: {
            [self.tableView registerNib:[DLForumCell getDefaultNib]
                 forCellReuseIdentifier:[DLForumCell getCellIdentifier]];
            
            break;
        }
    }
}

- (void)updateFrame {
    self.tableView.frame = self.view.bounds;
}

- (void)loadData:(DLLoadingType)loadingType {
    
    if (self.isLoading) {
        return;
    }
    
    if (loadingType == DLLoadingTypeInitial || loadingType == DLLoadingTypePullRefresh) {
        self.currentPageNumber = 1;
        self.isExistNextPage = YES;
    }
    
    if (!self.isExistNextPage) {
        return;
    }
    
    self.isLoading = YES;
    
    @weakify(self)
    [self fetchByPageNumber:self.currentPageNumber
                   callback:^(NSMutableArray *items, NSError *error) {
                       @strongify(self)
                       
                       if (NOT_NULL(error)) {
                           // TODO: エラー時のポップアップなどを表示する
                       } else {
                           if (loadingType == DLLoadingTypeInitial || loadingType == DLLoadingTypePullRefresh) {
                               [self.items removeAllObjects];
                           }
                           
                           if (items.count > 0) {
                               [self.items addObjectsFromArray:items];
                               self.currentPageNumber += 1;
                           } else {
                               self.isExistNextPage = NO;
                           }
                       }
                       
                       [self.tableView reloadData];
                       self.isLoading = NO;
                   }];
}

- (void)fetchByPageNumber:(NSInteger)pageNumber callback:(void (^)(NSMutableArray *items, NSError *error))callback {
    
    switch (self.type) {
        case DLMyPageSegumentTypeVideoHistory:
        {
            [DLAPIHelper fetchVideoHistoryByPageNumber:pageNumber
                                               perPage:DLConstPerPageMyPageVideoHistory
                                             orderType:DLOrderTypeDescending
                                              callback:callback];
            break;
        }
        case DLMyPageSegumentTypeMyForum:
        {
            [DLAPIHelper fetchMyForumByPageNumber:pageNumber
                                          perPage:DLConstPerPageMyPageMyForums
                                         callback:callback];
            
            break;
        }
        case DLMyPageSegumentTypeParticipateForum:
        {
            [DLAPIHelper fetchParticipateForumByPageNumber:pageNumber
                                                   perPage:DLConstPerPageMyPageParticipatingForums
                                                  callback:callback];
            break;
        }
    }
}


#pragma mark - Table View Delegate

#pragma mark Section

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

#pragma mark Cell

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (self.type) {
        case DLMyPageSegumentTypeVideoHistory:
            return [DLVideoHistoryCell getCellHeight];
            
        case DLMyPageSegumentTypeMyForum: 
        case DLMyPageSegumentTypeParticipateForum:
            return [DLForumCell getCellHeight];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (self.type) {
        case DLMyPageSegumentTypeVideoHistory: {
            // TODO DLOpenPlayMovieService
            break;
        }
        case DLMyPageSegumentTypeMyForum:
            break;
        case DLMyPageSegumentTypeParticipateForum:
            break;
    }
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items ? self.items.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (self.type) {
        case DLMyPageSegumentTypeVideoHistory: {
            
            DLVideoHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:[DLVideoHistoryCell getCellIdentifier]
                                                                       forIndexPath:indexPath];
            [cell setCellData:self.items[(NSUInteger) indexPath.row]];
            
            return cell;
        }
        case DLMyPageSegumentTypeMyForum: {
            
            DLForumCell *cell = [tableView dequeueReusableCellWithIdentifier:[DLForumCell getCellIdentifier]
                                                                forIndexPath:indexPath];
            [cell setForum:self.items[(NSUInteger) indexPath.row]];
            
            return cell;
        }
        case DLMyPageSegumentTypeParticipateForum: {
            
            DLForumCell *cell = [tableView dequeueReusableCellWithIdentifier:[DLForumCell getCellIdentifier]
                                                                forIndexPath:indexPath];
            [cell setForum:self.items[(NSUInteger) indexPath.row]];
            
            return cell;
        }
    }
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat offsetY = scrollView.contentOffset.y;
    
    if (offsetY > self.tableView.contentSize.height - self.tableView.height - DLConstPagingBottomMargin) {
        [self loadData:DLLoadingTypePaging];
    }
}

@end

