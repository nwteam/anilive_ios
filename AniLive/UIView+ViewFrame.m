//
//  UIView+ViewFrame.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "UIView+ViewFrame.h"

@implementation UIView (ViewFrame)

- (CGPoint)origin
{
    return self.frame.origin;
}

- (CGFloat)x
{
    return self.origin.x;
}

- (CGFloat)y
{
    return self.origin.y;
}

- (CGFloat)minX
{
    return CGRectGetMinX(self.frame);
}

- (CGFloat)midX
{
    return CGRectGetMidX(self.frame);
}

- (CGFloat)maxX
{
    return CGRectGetMaxX(self.frame);
}

- (CGFloat)minY
{
    return CGRectGetMinY(self.frame);
}

- (CGFloat)midY
{
    return CGRectGetMidY(self.frame);
}

- (CGFloat)maxY
{
    return CGRectGetMaxY(self.frame);
}

- (CGSize)size
{
    return self.frame.size;
}

- (CGFloat)width
{
    return self.size.width;
}

- (CGFloat)height
{
    return self.size.height;
}

- (void)setOrigin:(CGPoint)p
{
    CGRect r = self.frame;
    r.origin = p;
    self.frame = r;
}

- (void)setX:(CGFloat)x
{
    CGRect r = self.frame;
    r.origin.x = x;
    self.frame = r;
}

- (void)setY:(CGFloat)y
{
    CGRect r = self.frame;
    r.origin.y = y;
    self.frame = r;
}

- (void)setMidX:(CGFloat)x
{
    CGPoint center = self.center;
    center.x = x;
    self.center = center;
}

- (void)setMidY:(CGFloat)y
{
    CGPoint center = self.center;
    center.y = y;
    self.center = center;
}

- (void)setSize:(CGSize)s
{
    CGRect r = self.frame;
    r.size = s;
    self.frame = r;
}

- (void)setWidth:(CGFloat)w
{
    CGRect r = self.frame;
    r.size.width = w;
    self.frame = r;
}

- (void)setHeight:(CGFloat)h
{
    CGRect r = self.frame;
    r.size.height = h;
    self.frame = r;
}

@end
