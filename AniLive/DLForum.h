//
//  DLForum.h
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/10/08.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLModelBase.h"
#import "DLMovie.h"

@interface DLForum : DLModelBase

@property(nonatomic, readonly, strong) NSNumber *forumId;
@property(nonatomic, readonly, strong) NSNumber *movieId;
@property(nonatomic, readonly, strong) NSString *title;
@property(nonatomic, readonly, strong) NSString *movieTitle;
@property(nonatomic, readonly, strong) NSNumber *totalForums;
@property(nonatomic, readonly, strong) NSString *forumPinnedTime;
@property(nonatomic, readonly, strong) NSNumber *totalComments;
@property(nonatomic, readonly, strong) NSNumber *popularity;
@property(nonatomic, readonly, strong) NSNumber *forumOwnerUserId;
@property(nonatomic, readonly, strong) NSString *forumOwnerUserName;
@property(nonatomic, readonly, strong) NSDate *created;
@property(nonatomic, readonly, strong) NSDate *modified;
@property(nonatomic, readonly, strong) NSString *movieImage;
@property(nonatomic, readonly, strong) NSNumber *chapter;

//only forum detail
@property(nonatomic, readonly, strong) NSNumber *pointFlag;
@property(nonatomic, readonly, strong) NSNumber *salesType;
@property(nonatomic, readonly, strong) NSNumber *workID;
@property(nonatomic, readonly, strong) NSString *workTitle;

@end
