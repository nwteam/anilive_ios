//
//  DLNoticeCell.h
//  AniLive
//
//  Created by volcano on 12/22/16.
//  Copyright © 2016 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLNotice.h"

@interface DLNoticeCell : UITableViewCell
@property(strong, nonatomic) DLNotice *notice;
@property(nonatomic) BOOL isRead;
-(void)setRead:(BOOL)isRead;

+ (UINib *)getDefaultNib;
+ (CGFloat)getCellHeight;

@end
