//
// Created by Kotaro Itoyama on 2016/10/09.
// Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLModelBase.h"

@interface DLLive : DLModelBase
@property(readonly, nonatomic, strong) NSNumber *liveId;
@property(readonly, nonatomic, strong) NSString *title;
@property(readonly, nonatomic, strong) NSDate *broadcastOpen;
@property(readonly, nonatomic, strong) NSDate *broadcastClose;
@property(readonly, nonatomic, strong) NSString *image;
@property(readonly, nonatomic, strong) NSString *highlightMovieUrl;
@property(readonly, nonatomic, strong) NSNumber *totalViews;
@property(readonly, nonatomic, strong) NSNumber *totalForumsCount;
@property(readonly, nonatomic, assign) BOOL isReserved;

- (BOOL)isOnAir;

@end
