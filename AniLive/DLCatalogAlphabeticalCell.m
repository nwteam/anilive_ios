//
//  DLCatalogAlphabeticalCell.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/11/05.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLCatalogAlphabeticalCell.h"
#import "UIImageView+Category.h"
#import "NSString+Category.h"

const static CGFloat kCellHeight = 110;

@interface DLCatalogAlphabeticalCell ()

@property (nonatomic, strong) UIImageView *thumbnailImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *synopsisLabel;
@property (nonatomic, strong) UIView *separatorLineView;

@end

@implementation DLCatalogAlphabeticalCell

#pragma mark - ORVSuperCollectionViewCellProtocol

- (void)initView {
    [super initView];
    
    self.backgroundColor = [UIColor clearColor];
    
    self.thumbnailImageView = [[UIImageView alloc]init];
    self.thumbnailImageView.backgroundColor = [UIColor lightGrayColor];
    self.thumbnailImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.thumbnailImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.thumbnailImageView];
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = BASE_FONT(15);
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.numberOfLines = 1;
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.contentView addSubview:self.titleLabel];
    
    self.synopsisLabel = [[UILabel alloc]init];
    self.synopsisLabel.backgroundColor = [UIColor clearColor];
    self.synopsisLabel.font = BASE_FONT(13);
    self.synopsisLabel.textColor = [UIColor lightGrayColor];
    self.synopsisLabel.numberOfLines = 2;
    self.synopsisLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.contentView addSubview:self.synopsisLabel];
    
    self.separatorLineView = [[UIView alloc]init];
    self.separatorLineView.backgroundColor = [UIColor blackColor];
    [self.contentView addSubview:self.separatorLineView];
}

- (void)initializeCell {
    [super initializeCell];
    
    self.titleLabel.text = @"";
    self.synopsisLabel.text = @"";
    self.thumbnailImageView.image = nil;
    self.separatorLineView.hidden = NO;
}

+ (NSString *)getCellIdentifier {
    
    return NSStringFromClass([self class]);
}

#pragma mark - Class Methods

+ (CGFloat)getCellHeight {
    return kCellHeight;
}

#pragma mark - Override Methods

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
}

#pragma mark - Public Methods

- (void)setCellData:(DLWork *)workData {
    
    if (NOT_NULL(workData.image1)) {
        [UIImageView loadImageWithURLString:workData.image1
                            targetImageView:self.thumbnailImageView];
    }
    
    self.titleLabel.attributedText = [workData.title getAdjustLineSpaceStringWithLineSpacing:0 withLineBreakMode:NSLineBreakByTruncatingTail];
    
    self.synopsisLabel.attributedText = [workData.synopsis getAdjustLineSpaceStringWithLineSpacing:0 withLineBreakMode:NSLineBreakByTruncatingTail];
    
    [self updateFrame];
}

- (void)setSeparatorLineIsHidden:(BOOL)isHidden {
    
    self.separatorLineView.hidden = isHidden;
}

#pragma mark - Private Methods

- (void)updateFrame {
    
    CGFloat thumbnailWidth = kCellHeight / 3 * 2;
    
    self.thumbnailImageView.frame = CGRectMake(0, 0, thumbnailWidth, kCellHeight);
    
    [self.titleLabel sizeToFit];
    self.titleLabel.width = self.width - thumbnailWidth - (DLConstDefaultMargin * 2);
    self.titleLabel.x = self.thumbnailImageView.maxX + DLConstDefaultMargin;
    self.titleLabel.y = 12;
    
    [self.synopsisLabel sizeToFit];
    self.synopsisLabel.width = self.width - thumbnailWidth - (DLConstDefaultMargin * 2);
    self.synopsisLabel.x = self.titleLabel.x;
    self.synopsisLabel.y = self.titleLabel.maxY + 5;
    
    self.separatorLineView.frame = CGRectMake(self.thumbnailImageView.maxX, self.height - 0.5, self.width - self.thumbnailImageView.maxX, 0.5);
}

@end
