//
//  DLTabBarItemView.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLTabBarItemView.h"
#import "DLHighlightedButton.h"

@implementation DLTabBarItem

@end

@interface DLTabBarItemView () <DLHighlightedButtonDelegate>

@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) DLHighlightedButton *tabBarButton;
@property (readwrite, nonatomic, strong) DLTabBarItem *tabBarItem;
@property (nonatomic, assign) BOOL isSelected;

@end

@implementation DLTabBarItemView

- (instancetype)initWithTabBarItem:(DLTabBarItem *)tabBarItem
{
    self = [super init];
    if (self) {
        
        self.tabBarItem = tabBarItem;
        [self initView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
    self.tabBarButton.delegate = nil;
}

#pragma mark - Public Methods

- (void)setButtonSelected:(BOOL)isSelected {
    
    self.isSelected = isSelected;
    
    [self changeColorWithHighlighted:isSelected];
}

#pragma mark - Private Methods

- (void)initView {
    
#pragma mark Icon ImageView
    
    self.iconImageView = [[UIImageView alloc]init];
    self.iconImageView.backgroundColor = [UIColor clearColor];
    self.iconImageView.image = self.tabBarItem.normalIconImage;
    self.iconImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:self.iconImageView];
    
#pragma mark Badge ImageView
    
    self.badgeImageView = [[UIImageView alloc]init];
    self.badgeImageView.backgroundColor = [UIColor clearColor];
    self.badgeImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:self.badgeImageView];
    
#pragma mark Title Label
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textColor = self.tabBarItem.normalTitleColor;
    self.titleLabel.font = BASE_FONT(11);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    self.titleLabel.text = self.tabBarItem.title;
    [self addSubview:self.titleLabel];
    
#pragma mark TabBar Button
    
    self.tabBarButton = [DLHighlightedButton buttonWithType:UIButtonTypeSystem];
    self.tabBarButton.backgroundColor = [UIColor clearColor];
    self.tabBarButton.tag = self.tabBarItem.tag;
    self.tabBarButton.delegate = self;
    [self.tabBarButton addTarget:self
                          action:@selector(buttonDidTap:)
                forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.tabBarButton];
}

- (void)updateFrame {
    
    const CGFloat kIconImageViewSide = 20;
    const CGFloat kBadgeImageViewSide = 20;
    
    self.iconImageView.frame = CGRectMake((self.width - kIconImageViewSide) / 2, 10, kIconImageViewSide, kIconImageViewSide);
    self.badgeImageView.frame = CGRectMake((self.width - kBadgeImageViewSide + 16) / 2, 8, kBadgeImageViewSide, kBadgeImageViewSide);
    self.titleLabel.frame = CGRectMake(0, self.iconImageView.maxY, self.width, self.height - self.iconImageView.maxY);
    self.tabBarButton.frame = self.bounds;
}

- (void)changeColorWithHighlighted:(BOOL)highlighted {
    
    if (highlighted) {
        
        if (NOT_NULL(self.tabBarItem.highlightedIconImage)) {
            self.iconImageView.image = self.tabBarItem.highlightedIconImage;
        } else {
            self.iconImageView.image = self.tabBarItem.normalIconImage;
        }
        
        if (NOT_NULL(self.tabBarItem.highlightedTitleColor)) {
            self.titleLabel.textColor = self.tabBarItem.highlightedTitleColor;
        } else {
            self.titleLabel.textColor = self.tabBarItem.normalTitleColor;
        }
        
    } else {
        self.iconImageView.image = self.tabBarItem.normalIconImage;
        self.titleLabel.textColor = self.tabBarItem.normalTitleColor;
    }
}

#pragma mark - Button Action

- (void)buttonDidTap:(UIButton *)button {
    
    if ([self.delegate respondsToSelector:@selector(tabBarItemViewDidSelect:)]) {
        [self.delegate tabBarItemViewDidSelect:self];
    }
}

#pragma mark - DLHighlightedButtonDelegate

- (void)highlightedButtonHighlightChanged:(BOOL)highlighted {
    
    if (self.isSelected) {
        return;
    }
    
    [self changeColorWithHighlighted:highlighted];
}

@end
