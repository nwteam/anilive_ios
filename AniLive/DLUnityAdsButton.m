//
//  DLUnityAdsButton.m
//  AniLive
//
//  Created by 辰己佳祐 on 2017/01/14.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLUnityAdsButton.h"
#import "DLHighlightedButton.h"

@interface DLUnityAdsButton ()

@property (nonatomic, assign, readwrite) BOOL enabled;

@property (nonatomic, strong) UIImageView *playMarkImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) DLHighlightedButton *button;
@property (nonatomic, copy) DLUnityButtonDidTapBlock buttonDidTapBlock;

@end

@implementation DLUnityAdsButton

#pragma mark - Class Methods

+ (CGFloat)buttonHeight {
    return 40;
}

+ (CGFloat)buttonWidth {
    return 175;
}

#pragma mark - Init Methods

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.userInteractionEnabled = YES;
        
        [self initView];
        [self updateFrame];
    }
    return self;
}

#pragma mark - Override Methods

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

#pragma mark - Dealloc

- (void)dealloc {
    FUNC_LOG
    self.buttonDidTapBlock = nil;
}

#pragma mark - Public Methods

- (void)buttonDidTap:(DLUnityButtonDidTapBlock)completion {
    self.buttonDidTapBlock = [completion copy];
}

- (void)setIsEnabled:(BOOL)isEnabled {
    self.enabled = isEnabled;
    self.button.enabled = isEnabled;
}

- (void)setButtonTitle:(NSString *)title {
    self.titleLabel.text = title;
}

#pragma mark - Private Methods

- (void)initView {
    
    self.backgroundColor = [UIColor colorWithHex:DLConstColorCodeYellow];
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 2;
    
    self.playMarkImageView = [[UIImageView alloc]init];
    self.playMarkImageView.backgroundColor = [UIColor clearColor];
    self.playMarkImageView.contentMode = UIViewContentModeScaleAspectFit;
    // TODO: 後で画像差し替え必要
    self.playMarkImageView.image = [UIImage imageNamed:DLConstIconTabBarLiveOff];
    [self addSubview:self.playMarkImageView];
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.font = BASE_FONT(12);
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.numberOfLines = 2;
    [self addSubview:self.titleLabel];
    
    self.button = [[DLHighlightedButton alloc]init];
    self.button.isNoNeedHighlight = YES;
    [self.button addTarget:self
                    action:@selector(buttonTapAction)
          forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.button];
}

- (void)updateFrame {
    
    CGFloat innerMargin = 8;
    
    self.button.frame = self.bounds;
    self.playMarkImageView.frame = CGRectMake(innerMargin, 10, 20, 20);
    self.titleLabel.frame = CGRectMake(self.playMarkImageView.maxX + innerMargin, 0, self.width - (self.playMarkImageView.maxX + (innerMargin * 2)), self.height);
}

- (void)buttonTapAction {
    self.buttonDidTapBlock(self.button);
}

@end
