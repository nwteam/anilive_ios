//
//  DLCatalogAlphabeticalCell.m
//  AniLive
//
//  Created by isaoeka on 2016/11/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "UIImageView+Category.h"
#import "NSString+Category.h"

#import "DLVideoHistoryCell.h"

const static CGFloat kCellHeight = 100;

@interface DLVideoHistoryCell ()

@property (nonatomic, strong) UIImageView *thumbnailImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *separatorLineView;

@end

@implementation DLVideoHistoryCell

#pragma mark - ORVSuperCollectionViewCellProtocol

- (void)initView {
    [super initView];
    
    self.backgroundColor = [UIColor clearColor];
    
    self.thumbnailImageView = [[UIImageView alloc]init];
    self.thumbnailImageView.backgroundColor = [UIColor clearColor];
    self.thumbnailImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.thumbnailImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.thumbnailImageView];
    
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = BASE_FONT_BOLD(14);
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.contentView addSubview:self.titleLabel];
    
    self.separatorLineView = [[UIView alloc]init];
    self.separatorLineView.backgroundColor = [UIColor blackColor];
    [self.contentView addSubview:self.separatorLineView];
}

- (void)initializeCell {
    [super initializeCell];
    
    self.thumbnailImageView.image = nil;
    self.separatorLineView.hidden = NO;
}

+ (NSString *)getCellIdentifier {
    
    return NSStringFromClass([self class]);
}

#pragma mark - Class Methods

+ (CGFloat)getCellHeight {
    return kCellHeight;
}

#pragma mark - Override Methods

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
}

#pragma mark - Public Methods

- (void)setCellData:(DLMovie *)movieData {
    
    if (NOT_NULL(movieData.image)) {
        [UIImageView loadImageWithURLString:movieData.image
                            targetImageView:self.thumbnailImageView];
    }
    
    self.titleLabel.attributedText = [movieData.title getAdjustLineSpaceStringWithLineSpacing:6
                                                                            withLineBreakMode:NSLineBreakByTruncatingTail];
    
    [self updateFrame];
}

- (void)setSeparatorLineIsHidden:(BOOL)isHidden {
    
    self.separatorLineView.hidden = isHidden;
}

#pragma mark - Private Methods

- (void)updateFrame {
    
    CGFloat thumbnailWidth = kCellHeight * 1.4;
    
    self.thumbnailImageView.frame = CGRectMake(0, 0, thumbnailWidth, kCellHeight);
    
    [self.titleLabel sizeToFit];
    self.titleLabel.width = self.width - thumbnailWidth - (DLConstDefaultMargin * 2);
    self.titleLabel.x = self.thumbnailImageView.maxX + DLConstDefaultMargin;
    self.titleLabel.y = DLConstDefaultMargin;
    
    self.separatorLineView.frame = CGRectMake(
                                              self.thumbnailImageView.maxX,
                                              self.height - 1,
                                              self.width - self.thumbnailImageView.maxX,
                                              1);
}

@end
