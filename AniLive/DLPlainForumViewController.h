//
//  DLPlainForumViewController.h
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/11/06.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLNavigationBarView.h"

@class DLForum;

@interface DLPlainForumViewController : UIViewController

@property(nonatomic, strong) DLForum *forum;

@end
