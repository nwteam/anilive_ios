//
// Created by Kotaro Itoyama on 2016/10/08.
// Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLComment.h"

@interface DLComment()
@property(readwrite, nonatomic, strong) NSNumber *commentId;
@property(readwrite, nonatomic, strong) NSString *body;
@property(readwrite, nonatomic, strong) NSString *userName;
@property(readwrite, nonatomic, strong) NSNumber *userId;
@property(readwrite, nonatomic, strong) NSDate *created;
@property(readwrite, nonatomic, strong) NSDate *modified;
@property(readwrite, nonatomic, strong) NSNumber *replyToUserId;
@end

@implementation DLComment

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }

    self.commentId = CHECK_NULL_DATA_NUMBER(attributes[@"comment_id"]);
    self.body = CHECK_NULL_DATA_STRING(attributes[@"body"]);
    self.userName = CHECK_NULL_DATA_STRING(attributes[@"user_name"]);
    self.userId = CHECK_NULL_DATA_NUMBER(attributes[@"user_id"]);
    self.replyToUserId = CHECK_NULL_DATA_NUMBER(attributes[@"reply_to_user_id"]);

    self.created = [DLAPIHelper dateFromString: attributes[@"created"]];
    self.modified = [DLAPIHelper dateFromString: attributes[@"modified"]];

    return self;

}

-(BOOL)isMine{
    return self.userId.integerValue == DLAPIHelper.myUserId;
}
@end
