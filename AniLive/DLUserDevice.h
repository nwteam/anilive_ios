//
// Created by akasetaichi on 2017/01/20.
// Copyright (c) 2017 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DLModelBase.h"


@interface DLUserDevice : DLModelBase

@property(readonly, nonatomic, strong) NSNumber *deviceId;
@property(readonly, nonatomic, strong) NSString *device;
@property(readonly, nonatomic, strong) NSDate *created;

@end
