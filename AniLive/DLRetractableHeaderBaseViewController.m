//
//  DLRetractableHeaderBaseViewController.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/11/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLRetractableHeaderBaseViewController.h"

@interface DLRetractableHeaderBaseViewController ()

@end

@implementation DLRetractableHeaderBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

// 子クラスでオーバーライドして使用する
- (void)setOffsetToBaseScrollView:(CGPoint)offset {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
