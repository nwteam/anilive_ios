//
//  DLForumGroupCell.h
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/10/23.
//  Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLSuperTableViewCell.h"

@class DLForumGroup;
@class DLForumGroupListViewController;
@class DLForum;


@interface DLForumGroupCell : DLSuperTableViewCell
@property (nonatomic, weak) DLForumGroup *group;
@property (nonatomic, weak) DLForum *froum;
@property(nonatomic, weak) DLForumGroupListViewController *delegate;

- (void)loadImage;
@end
