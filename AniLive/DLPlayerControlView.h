//
//  DLPlayerControlView.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/24.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@protocol DLPlayerControlViewDelegate;

@class DLMovie, DLLive, AVPlayerItem, PTMediaPlayerItem;

@interface DLPlayerControlView : UIView

@property (nonatomic, weak) id<DLPlayerControlViewDelegate> delegate;
@property (nonatomic, strong) PTMediaPlayerItem *mediaPlayerItem;
@property (nonatomic, assign, readonly) BOOL isLoaded;
@property (nonatomic, assign, readonly) BOOL isPlaying;
@property (nonatomic, assign, readonly) BOOL isFullScreen;

// initializer
- (instancetype)initWithMovieData:(DLMovie *)movieData
                   withPlayerItem:(AVPlayerItem *)playerItem;
- (instancetype)initWithMovieData:(DLMovie *)movieData
              withMediaPlayerItem:(PTMediaPlayerItem *)mediaPlayerItem;
- (instancetype)initWithLiveData:(DLLive *)liveData
                  withPlayerItem:(AVPlayerItem *)playerItem;
- (instancetype)initWithLiveData:(DLLive *)liveData
             withMediaPlayerItem:(PTMediaPlayerItem *)mediaPlayerItem;
- (instancetype)initIsLive:(BOOL)flag;

- (void)updateIsLoaded:(BOOL)isLoaded;
- (void)updateIsPlaying:(BOOL)isPlaying;
- (void)updateIsFullScreen:(BOOL)isFullScreen;
- (void)updateCurrentTime;
- (void)updateTimeView:(NSInteger)currentTime duration:(NSInteger)duration;
- (void)updateCaptionView:(NSString *)captionMessage;

@end

#pragma mark - DLPlayerControlViewDelegate

@protocol DLPlayerControlViewDelegate <NSObject>

@optional

- (void)backButtonDidTap;
- (void)forumButtonDidTap;
- (void)pauseButtonDidTap;
- (void)playButtonDidTap;
- (void)fullScreenButtonDidTap;
- (void)shrinkButtonDidTap;
- (void)sliderChangedWithSeekTime:(CMTime)seekTime;

@end
