//
//  DLForumViewController.m
//  AniLive
//
//  Created by Kotaro Itoyama on 2016/11/06.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLForumViewController.h"
#import "DLForumHeaderView.h"
#import "DLCommentCell.h"
#import "DLAppFrameManager.h"
#import "DLForumView.h"
#import "DLReportType.h"
#import "DLUserDataManager.h"
#import "DLPayingPointManager.h"
#import "DLMovie.h"
#import "DLTransitionManager.h"
#import "DLOpenPlayMovieService.h"

#define MAX_INPUT_TEXT_NUMBER  140

@interface DLForumViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, DLNavigationBarViewDelegate,UITextViewDelegate>

@property(nonatomic, strong) DLNavigationBarView *navigationBarView;
@property(nonatomic, strong) DLForumHeaderView *headerView;
@property(nonatomic, strong) DLForumHeaderView *dummyHeaderView;
@property(nonatomic, strong) DLForumView *forumView;
@property(nonatomic, strong) DLComment *editTargetComment;
@property(nonatomic, strong) NSMutableArray *comments;
@property(nonatomic, strong) NSArray *reportTypes;
@property NSInteger pageNumber;

@end

@implementation DLForumViewController

#pragma mark - View Controller Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    [self loadData:NO];
    [self updateFrame];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self showDebug:@"viewWillAppear"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self showDebug:@"viewWillLayoutSubviews"];
}

- (void)updateViewConstraints {
    [self showDebug:@"updateViewConstraints before"];

    [super updateViewConstraints];
    [self showDebug:@"updateViewConstraints after"];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self showDebug:@"viewDidLayoutSubviews"];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self showDebug:@"viewDidAppear"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - Private Methods

- (void)initView {
    self.pageNumber = 1;
    self.view.backgroundColor = [UIColor colorWithHex:DLConstColorDarkBlue];
    self.view.clipsToBounds = YES;

    self.forumView = DLForumView.new;
    self.forumView.tableView.dataSource = self;
    self.forumView.tableView.delegate = self;
    self.forumView.tableView.backgroundColor = [UIColor colorWithHex:DLConstColorDarkBlue];
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(cellLongPressed:)];
    [self.forumView.tableView addGestureRecognizer:longPressGestureRecognizer];
    self.forumView.tfComment.delegate = self;
    [self.view addSubview:self.forumView];

    self.headerView = DLForumHeaderView.new;
    self.forumView.tableView.tableHeaderView = self.headerView;
    self.headerView.forum = self.forum;
    [self.headerView.displayMovieButton addTarget:self action:@selector(displayMovieButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(titleLongPressed)];
    [self.headerView.titleView addGestureRecognizer:longPress];

    self.dummyHeaderView = DLForumHeaderView.new;
    self.dummyHeaderView.hidden = YES;
//    [self.view addSubview:self.dummyHeaderView];
    self.dummyHeaderView.forum = self.forum;

#pragma mark Navigation Bar
    self.navigationBarView = [[DLNavigationBarView alloc] initWithType:DLNavigationBarViewTypeForumList];
    self.navigationBarView.delegate = self;
    [self.navigationBarView setTitle:Localizer(@"Forum")];
    [self.view addSubview:self.navigationBarView];
    [DLPayingPointManager sharedInstance];
}

- (void)loadData:(BOOL) shouldScrollToBottom{
    
    @weakify(self)
    [DLAPIHelper fetchCommentsByForumID:[self.forum.forumId integerValue]
                             pageNumber:self.pageNumber
                               callback:^(NSMutableArray<DLComment *> *comments, NSError *error) {
                                   @strongify(self)
                                   self.comments = comments;
                                   
                                   [self.forumView.tableView reloadData];
                                   if (shouldScrollToBottom && self.comments.count > 0) {
                                       [self.forumView.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.comments.count - 1
                                                                                                           inSection:0]
                                                                       atScrollPosition:UITableViewScrollPositionBottom
                                                                               animated:YES];
                                   }
                               }];
}

- (void)updateFrame {

    CGRect appFrame = DLAppFrameManager.sharedInstance.applicationFrame;
    CGFloat w = CGRectGetWidth(appFrame);
    CGFloat headerHeight = w * 0.75f;
    self.navigationBarView.frame = CGRectMake(0,
            CGRectGetMinY(appFrame),
            w,
            DLConstNavigationBarHeight);
    CGFloat tableTopY = CGRectGetMaxY(self.navigationBarView.frame);
    self.dummyHeaderView.frame = CGRectMake(0,
            tableTopY + self.headerView.residualHeight - headerHeight,
            w,
            headerHeight);

    self.forumView.frame = CGRectMake(0,
            tableTopY,
            w,
            CGRectGetHeight(appFrame) - tableTopY);


    self.forumView.tableView.scrollIndicatorInsets
//            = self.forumView.tableView.contentInset
            = (UIEdgeInsets) {.top=CGRectGetMaxY([self.headerView.superview convertRect:self.headerView.frame toView:self.forumView])};
}

- (void)refreshForum {
    @weakify(self)
    [DLAPIHelper fetchForumByForumID:[_forum.forumId integerValue] callback:^(NSObject *forum, NSError *error) {
        @strongify(self)
        if(!NOT_NULL(error)){
            _forum = (DLForum *)forum;
            _headerView.forum = self.forum;
        }
    }];
}

- (void)displayMovieButtonClicked {
    [DLOpenPlayMovieService tryPlayMovie:[[DLMovie alloc] initWithForum:_forum]
                                    live:nil
                      fromViewController:self
                                callback:^{

                                }];
}

- (void)titleLongPressed {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                                 message:nil
                                                                          preferredStyle:UIAlertControllerStyleActionSheet];
        @weakify(self)

        [alertController addAction:[UIAlertAction actionWithTitle:Localizer(@"違反を報告")
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              @strongify(self)
                                                              [self startReportComment:nil];
                                                          }]];

        
        [alertController addAction:[UIAlertAction actionWithTitle:Localizer(@"キャンセル")
                                                            style:UIAlertActionStyleCancel
                                                          handler:nil]];
        
        [self presentViewController:alertController animated:YES completion:nil];
}

- (void)cellLongPressed:(UILongPressGestureRecognizer *)recognizer {
    
    CGPoint p = [recognizer locationInView:self.forumView.tableView];
    NSIndexPath *indexPath = [self.forumView.tableView indexPathForRowAtPoint:p];
    
    if (indexPath && recognizer.state == UIGestureRecognizerStateBegan) {
        
        DLComment *comment = self.comments[(NSUInteger) indexPath.row];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                                 message:nil
                                                                          preferredStyle:UIAlertControllerStyleActionSheet];
        @weakify(self, comment)
//        if (comment.isMine) {
//            
//            [alertController addAction:[UIAlertAction actionWithTitle:Localizer(@"Edit")
//                                                                style:UIAlertActionStyleDefault
//                                                              handler:^(UIAlertAction *action) {
//                                                                  @strongify(self, comment)
//                                                                  [self editComment:comment];
//                                                              }]];
//            [alertController addAction:[UIAlertAction actionWithTitle:Localizer(@"Delete")
//                                                                style:UIAlertActionStyleDestructive
//                                                              handler:^(UIAlertAction *action) {
//                                                                  @strongify(comment)
//                                                                  [DLAPIHelper deleteComment:comment];
//                                                              }]];
//        } else {
        
            [alertController addAction:[UIAlertAction actionWithTitle:Localizer(@"違反を報告")
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction *action) {
                                                                  @strongify(self,comment)
                                                                             [self startReportComment:comment];
                                                              }]];
//        }
        
        [alertController addAction:[UIAlertAction actionWithTitle:Localizer(@"キャンセル")
                                                            style:UIAlertActionStyleCancel
                                                          handler:nil]];

        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)startReportComment:(DLComment *)comment {
    if (self.reportTypes && self.reportTypes.count > 0) {
        [self showReportCommentActionSheetWithTypes:_reportTypes comment:comment];
    }
    else {
        @weakify(self,comment)
        [DLAPIHelper loadReportTypescallback:^(NSMutableArray *types) {
            if (types && types.count > 0) {
                @strongify(self ,comment)
                self.reportTypes = types;
                [self showReportCommentActionSheetWithTypes:types comment:comment];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Sorry, there come some problems, please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
}

- (void)showReportCommentActionSheetWithTypes:(NSArray *)types comment:(DLComment *)comment {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"報告理由を選択してください"
                                                                             message:nil
                                                                    preferredStyle:UIAlertControllerStyleActionSheet];
    @weakify(self,comment)
    for (NSInteger i = 0; i < types.count; i ++) {
        DLReportType *type = types[i];
        [alertController addAction:[UIAlertAction actionWithTitle:type.name
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              @strongify(self,comment)
                                                              [self sendReportRequestWithComment:comment type:type];
                                                          }]];
    }

    
    [alertController addAction:[UIAlertAction actionWithTitle:Localizer(@"Cancel")
                                                        style:UIAlertActionStyleCancel
                                                      handler:nil]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)sendReportRequestWithComment:(DLComment *)comment type:(DLReportType *)type {
    [DLAPIHelper reportComment:comment reportType:type fronm:_forum callback:^(BOOL flag) {
        if (flag) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"違反を報告しました。" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"通信エラー" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }];
}

- (void)editComment:(DLComment *)comment {
    self.editTargetComment = comment;
    self.forumView.tfComment.text = comment.body;
    [self.forumView.tfComment becomeFirstResponder];
}

#pragma mark Debug Function

- (void)showDebug:(NSString *)tag {
    NSLog(@"%@", tag);
    NSLog(@" root %@", NSStringFromCGRect(self.view.frame));
    NSLog(@" header %@", NSStringFromCGRect(self.headerView.frame));
    NSLog(@" forum %@", NSStringFromCGRect(self.forumView.frame));
}

#pragma mark Keyboard Event Handler

- (void)keyboardWillShown:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
    [UIView setAnimationBeginsFromCurrentState:YES];

    CGRect f = self.view.frame;
    CGRect keyboardRect = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    f.origin.y = -CGRectGetHeight(keyboardRect);
    self.view.frame = f;

    [UIView commitAnimations];
}

- (void)keyboardWillHidden:(NSNotification *)notification {
    NSLog(@"hidden");
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    [UIView setAnimationCurve:[notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];
    [UIView setAnimationBeginsFromCurrentState:YES];

    CGRect f = self.view.frame;
    f.origin.y = 0;
    self.view.frame = f;

    [UIView commitAnimations];
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.comments ? self.comments.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DLCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"comment-cell" forIndexPath:indexPath];
    cell.comment = self.comments[(NSUInteger) indexPath.row];
    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}


#pragma mark - Scroll View Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

    CGFloat actualOffset = CGRectGetHeight(self.headerView.frame) - scrollView.contentOffset.y;
    self.dummyHeaderView.hidden = actualOffset > self.headerView.residualHeight;
    scrollView.scrollIndicatorInsets = (UIEdgeInsets) {.top=self.dummyHeaderView.hidden ? actualOffset : self.headerView.residualHeight};
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    float offset = (scrollView.contentOffset.y - (scrollView.contentSize.height - scrollView.frame.size.height));
    if (offset >= -1.6 && offset <= 5) {
        [self additionalLoad];
    }
}

#pragma mark - DLNavigation Bar View Delegate

- (void)navigationBarViewDidTapLeftButton:(UIButton *)button {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Text Field Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason {
    NSLog(@"textFieldDidEndEditing reason");
    @weakify(self)
    if(self.editTargetComment){
        // TODO empty string だった場合はコメント削除？
        [DLAPIHelper editComment:self.editTargetComment withBody:textField.text callback:^(BOOL flag){
            @strongify(self)
            [self loadData:YES];
        }];
        self.editTargetComment = nil;
    }else {
        if(textField.text && textField.text.length > 0) {
            [DLAPIHelper postComment:textField.text targetForumId:[self.forum.forumId integerValue] callback:^(BOOL flag , id responseObject) {
                if (flag) {
                    @strongify(self)
                    [self loadData:YES];
                }
            }];
        }
    }
    textField.text = nil;
}

- (void)additionalLoad {
    
    @weakify(self)
    [DLAPIHelper fetchCommentsByForumID:[self.forum.forumId integerValue]
                             pageNumber:++self.pageNumber
                               callback:^(NSMutableArray<DLComment *> *comments, NSError *error) {
                                   @strongify(self)
                                   [self.comments addObjectsFromArray:comments];
                                   
                                   [self.forumView.tableView reloadData];
                               }];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -- textView delegate

- (void)textViewDidEndEditing:(UITextView *)textView {
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        NSLog(@"textFieldDidEndEditing reason");
        [self.forumView.tfComment resignFirstResponder];
        @weakify(self)
        if(self.editTargetComment){
            // TODO empty string だった場合はコメント削除？
            [DLAPIHelper editComment:self.editTargetComment withBody:textView.text callback:^(BOOL flag){
                @strongify(self)
                [self loadData:YES];
            }];
            self.editTargetComment = nil;
        }else {
            if(textView.text && textView.text.length > 0) {
                [DLAPIHelper postComment:textView.text targetForumId:[self.forum.forumId integerValue] callback:^(BOOL flag ,id responseObject) {
                    if (flag) {
                        @strongify(self)
                        DLComment *myComments = [DLComment instanceFromDictionary:responseObject[@"comment"]];
                        if (self.comments && _comments.count > 0) {
                            [self.comments insertObject:myComments atIndex:0];
                            [self.forumView.tableView reloadData];
                        }
                        else {
                            [self loadData:NO];
                        }
                        self.forumView.tfComment.text = nil;
                        [self.forumView displayNewInputText:nil];
                    }
                    else {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Send message fail, please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
                    }
                }];
            }
        }
        return NO;
    }
    NSString *newStr = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if (newStr.length <= 140) {
        [self.forumView displayNewInputText:newStr];
        return YES;
    }
    return NO;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.forumView.tfComment resignFirstResponder];
    [super touchesBegan:touches withEvent:event];
}

@end

