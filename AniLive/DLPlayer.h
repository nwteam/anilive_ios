//
//  DLPlayer.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/17.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface DLPlayer : AVPlayer

- (instancetype)initWithPlayerItem:(AVPlayerItem *)item withAllowsExternalPlayback:(BOOL)allowsExternalPlayback;

@end
