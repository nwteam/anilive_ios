//
//  Macro.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/10.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#define APPFRAME_RECT  [UIScreen mainScreen].applicationFrame
#define SCREEN_RECT  [UIScreen mainScreen].bounds
#define STATUSBAR_RECT  [UIApplication sharedApplication].statusBarFrame
#define DL_APPFRAME_RECT  CGRectMake(APPFRAME_RECT.origin.x, APPFRAME_RECT.origin.y - (STATUSBAR_RECT.size.height > 0 ? 20 : 0), APPFRAME_RECT.size.width, APPFRAME_RECT.size.height + (STATUSBAR_RECT.size.height > 0 ? 20 : 0))

// keyWindowの取得
#define WINDOW  [[UIApplication sharedApplication]keyWindow]

// APIの結果用のNull判定
#define CHECK_NULL_DATA_STRING(string) NOT_NULL(string) ? string : @""
#define CHECK_NULL_DATA_NUMBER(number) NOT_NULL(number) ? [NSNumber numberWithLongLong:[number longLongValue]] : @0

// Null判定
#define NOT_NULL(_instance) (_instance&&![_instance isKindOfClass:[NSNull class]])
// 空文字判定
#define NOT_EMPTY(_instance) (_instance && [_instance isKindOfClass:[NSString class]] && ![_instance isEqualToString:@""])

// iOSのバージョン情報
#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]

// iPadかどうかの判定
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

// 横向きかどうかの判定
#define IS_LANDSCAPE ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight)

#define IS_LANDSCAPE_RIGHT ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight)
#define IS_LANDSCAPE_LEFT ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft)

// 縦向きかどうかの判定
#define IS_PORTRAIT ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)

// 多言語対応用
#define Localizer(key) NSLocalizedString(key, @"")

// フォント
#define BASE_FONT(textSize) [UIFont fontWithName:@"Futura-Medium" size:textSize]
#define BASE_FONT_BOLD(textSize) [UIFont fontWithName:@"Futura-CondensedExtraBold" size:textSize]

// メソッド用のログ（Deallocに仕込んで解放チェックに使う）
#define FUNC_LOG NSLog(@"%s", __PRETTY_FUNCTION__);

//ローカルスロレジーに
#define GET_LS_VALUE(PARAM_KEY)                [[NSUserDefaults standardUserDefaults] stringForKey:PARAM_KEY]
#define SET_LS_VALUE(PARAM_KEY, VALUE)         [[NSUserDefaults standardUserDefaults] setValue:VALUE forKey:PARAM_KEY]

// APIへのURL生成
// マクロの挙動を変える定数はそのマクロの近くに記載する
#define kAPIProtocol @"https"
#define kAPIRootHostName @"next.daisuki.net"
#define kAPIPrefix @"api/v1"
#define API_STRING_BUILD(a) kAPIProtocol @"://" kAPIRootHostName @"/" kAPIPrefix @"/" a
#define API_SUFFIX_ADD(base, suffix) [NSString stringWithFormat:@"%@/%@",base,suffix]


// 課金まわり
#define PURCHASE_ID_1 @"com.example.anilive.testautorenewal"
#define PURCHASE_ID_2 @"net.acj.daisukinext.svod"
#define PURCHASE_ID_3 @"daisuki_next_subscription"
#define PURCHASE_ID_4 @"net.acj.daisukinext.test_consumable"

#define ud NSUserDefaults.standardUserDefaults

// 以下UserDefault用キー
#define KEY_USER_ID @"whatamidetect"
// IAPをリストア済みかどうか
#define KEY_RESTORATION @"rieieralgualgv"
#define KEY_IS_SUBSCRIPTION_VALID @"onthecontinuous"

// 通知名称定数
#define NOTIFICATION_KEY_RELOAD @"apidoneandshouldreload"


// ログ出力
#ifndef HIDDEN_LOG
#define NSLog(...) NSLog(__VA_ARGS__)
#else
#define NSLog(...) {}
#endif

#define APP_STORE_ID_FOR_UNITY_ADS @"1200290"

// 各APIエンドポイントURL
//#define LOCAL_MOCK 1
#ifdef LOCAL_MOCK //----------------- Fake Service ------------------
#define kAPIForumSuffix @"forums.json"
#define kAPIForum API_STRING_BUILD(kAPIForumSuffix)

#define kAPICommentSuffix @"comments.json"
#define kAPIComment API_STRING_BUILD(kAPICommentSuffix)

#define kAPILiveSuffix @"lives.json"
#define kAPILive API_STRING_BUILD(kAPILiveSuffix)

#define kAPITimeScheduleSuffix @"lives/timeschedules"
#define kAPITimeSchedule API_STRING_BUILD(kAPITimeScheduleSuffix)

#define kAPIWorkSuffix @"works.json"
#define kAPIWork API_STRING_BUILD(kAPIWorkSuffix)

#define kAPIReport API_STRING_BUILD(@"report.json")
#else   //----------------- Real Service ------------------

#define kAPICommentSuffix @"comments.json"
#define kAPIComment API_STRING_BUILD(kAPICommentSuffix)

#define kAPILiveSuffix @"lives"
#define kAPILive API_STRING_BUILD(kAPILiveSuffix)

#define kAPITimeScheduleSuffix @"lives/timeschedules"
#define kAPITimeSchedule API_STRING_BUILD(kAPITimeScheduleSuffix)

#define kAPIWorkSuffix @"works"
#define kAPIWork API_STRING_BUILD(kAPIWorkSuffix)

#define kAPIReport API_STRING_BUILD(@"report.json")

#define kAPIPointSuffix @"point"
#define kAPIPoint API_STRING_BUILD(kAPIPointSuffix)

#define kAPIPointsUsedSuffix @"points/used"
#define kAPIPointsUsed API_STRING_BUILD(kAPIPointsUsedSuffix)

#define kAPIUsersMoviesSuffix @"users/movies"
#define kAPIUsersMovies API_STRING_BUILD(kAPIUsersMoviesSuffix)

#define kAPIUsersCreatesSuffix @"users/creates"
#define kAPIUsersCreates API_STRING_BUILD(kAPIUsersCreatesSuffix)

#define kAPIUsersParticipationsSuffix @"users/participations"
#define kAPIUsersParticipations API_STRING_BUILD(kAPIUsersParticipationsSuffix)


#define kAPILiveSuffix @"lives"
#define kAPILive API_STRING_BUILD(kAPILiveSuffix)

#define kAPIForumSuffix @"forums"
#define kAPIForum API_STRING_BUILD(kAPIForumSuffix)

#define kAPISeeLatersSuffix @"see_laters"
#define kAPISeeLaters API_STRING_BUILD(kAPISeeLatersSuffix)

#define kAPISeeLaterSuffix @"see_later"
#define kAPISeeLater API_STRING_BUILD(kAPISeeLaterSuffix)


#endif

#define kAPIMovieSuffix @"movies"
#define kAPIMovie API_STRING_BUILD(kAPIMovieSuffix)

#define kAPIUserSuffix @"users"
#define kAPIUser API_STRING_BUILD(kAPIUserSuffix)

#define kAPIAuthcodeIssueSuffix @"authcode/issue"
#define kAPIAuthcodeIssue API_STRING_BUILD(kAPIAuthcodeIssueSuffix)

#define kAPIAuthcodeCheckSuffix @"authcode/check"
#define kAPIAuthcodeCheck API_STRING_BUILD(kAPIAuthcodeCheckSuffix)

#define kAPIDevicesRelationRevisionSuffix @"devices/relation/revision"
#define kAPIDevicesRelationRevision API_STRING_BUILD(kAPIDevicesRelationRevisionSuffix)

#define kAPISVODSuffix @"users/svod/ios"
#define kAPISVOD API_STRING_BUILD(kAPISVODSuffix)

#define kAPICheckRestoreSuffix @"restore/check/ios"
#define kAPICheckRestore API_STRING_BUILD(kAPICheckRestoreSuffix)

#define kAPIReportTypesSuffix @"report_categories"
#define kAPIReportTypes API_STRING_BUILD(kAPIReportTypesSuffix)

#define kAPIPointCheckSuffix @"point/check"
#define kAPIPointCheck API_STRING_BUILD(kAPIPointCheckSuffix)

#define kAPIValidateSuffix @"validate.json"
#define kAPIValidate API_STRING_BUILD(kAPIValidateSuffix)

