//
//  DLPoint.h
//  AniLive
//
//  Created by ykkc on 2016/12/30.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//


#import "DLModelBase.h"

@interface DLPoint : DLModelBase
@property (readonly, nonatomic, strong) NSDate *pointDate;
@property (readonly, nonatomic, strong) NSNumber *point;
@property (readonly, nonatomic, strong) NSString *reasonText;
@property (readonly, nonatomic, strong) NSDate *viewLimit;
@end
