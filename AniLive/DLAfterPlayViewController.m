//
//  DLAfterPlayViewController.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/10/22.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLAfterPlayViewController.h"
#import "DLAppFrameManager.h"
#import "DLHighlightedButton.h"

@interface DLAfterPlayViewController ()

@property (nonatomic, strong) DLHighlightedButton *closeButton;
@property (nonatomic, strong) UIImageView *nextShowPlayIcon;
@property (nonatomic, strong) UILabel *nextShowLabel;
@property (nonatomic, strong) UIImageView *nextShowImageView;
@property (nonatomic, strong) UIButton *nextShowButton;
@property (nonatomic, strong) UILabel *nextShowDetailLabel;
@property (nonatomic, strong) UILabel *adsTitleLabel;
@property (nonatomic, strong) UILabel *unityAdsArea;
@property (nonatomic, strong) UILabel *seeLaterLabel;

@end

@implementation DLAfterPlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initView];
    [self updateFrame];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [[DLAppFrameManager sharedInstance] updateApplicationFrame];
    [self updateFrame];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    FUNC_LOG
}

#pragma mark - Private Methods

- (void)initView {
    
    self.view.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkGray alpha:1];
    
#pragma mark Close Button
    
    self.closeButton = [DLHighlightedButton buttonWithType:UIButtonTypeSystem];
    self.closeButton.isNoNeedHighlight = YES;
    self.closeButton.width = self.closeButton.height = 36;
    [self.closeButton setImage:[UIImage imageNamed:DLConstIconWhiteClose] forState:UIControlStateNormal];
    self.closeButton.tintColor = [UIColor whiteColor];
    [self.closeButton addTarget:self
                         action:@selector(closeButtonTapped)
               forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.closeButton];
    
#pragma mark Next Show Label
    
    self.nextShowPlayIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:DLConstIconLiveOff]];
    [self.view addSubview:_nextShowPlayIcon];
    
    self.nextShowLabel = [[UILabel alloc]init];
    self.nextShowLabel.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkGray alpha:0.8];
    self.nextShowLabel.font = BASE_FONT_BOLD(14);
    self.nextShowLabel.textColor = [UIColor colorWithHex:DLConstColorCodeYellow];
    self.nextShowLabel.text = Localizer(@"NextShow");
    [self.view addSubview:self.nextShowLabel];
    
    self.nextShowImageView = [[UIImageView alloc] init];
    [self.view addSubview:_nextShowImageView];
    
    self.nextShowDetailLabel = [[UILabel alloc] init];
    self.nextShowDetailLabel.backgroundColor = [UIColor clearColor];
    self.nextShowDetailLabel.font = BASE_FONT(15);
    self.nextShowDetailLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:_nextShowDetailLabel];
    
    self.nextShowButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:_nextShowButton];
#pragma mark Ads Title Label
    
    self.adsTitleLabel = [[UILabel alloc]init];
    self.adsTitleLabel.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkGray alpha:0.8];
    self.adsTitleLabel.font = BASE_FONT_BOLD(12);
    self.adsTitleLabel.textColor = [UIColor whiteColor];
    self.adsTitleLabel.numberOfLines = 0;
    self.adsTitleLabel.text = Localizer(@"ToWatchFree");
    [self.view addSubview:self.adsTitleLabel];
    
#pragma mark Unity Ads Area
    
    self.unityAdsArea = [[UILabel alloc]init];
    self.unityAdsArea.backgroundColor = [UIColor greenColor];
    self.unityAdsArea.font = BASE_FONT_BOLD(14);
    self.unityAdsArea.textColor = [UIColor whiteColor];
    self.unityAdsArea.text = @"UnityAds";
    [self.view addSubview:self.unityAdsArea];
    
#pragma mark See Later Label
    
    self.seeLaterLabel = [[UILabel alloc]init];
    self.seeLaterLabel.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkGray alpha:0.8];
    self.seeLaterLabel.font = BASE_FONT_BOLD(14);
    self.seeLaterLabel.textColor = [UIColor whiteColor];
    self.seeLaterLabel.text = Localizer(@"SeeLaterList");
    [self.view addSubview:self.seeLaterLabel];
}

- (void)updateFrame {
    
    self.closeButton.x = DLConstDefaultMargin;
    self.closeButton.y = 24;
    
    self.adsTitleLabel.frame = CGRectMake(DLConstDefaultMargin, 200, 200, 40);
    
    if (IS_PORTRAIT) {
        self.nextShowPlayIcon.frame = CGRectMake((SCREEN_RECT.size.width - 185) / 2.0, 66, 20, 20);
        self.nextShowLabel.frame = CGRectMake(CGRectGetMaxX(_nextShowPlayIcon.frame), 66, 150, 20);
        self.nextShowImageView.frame = CGRectMake((SCREEN_RECT.size.width - 185) / 2.0, 66, 185, 105);
        self.nextShowLabel.frame = CGRectMake(DLConstDefaultMargin, 60, 200, 40);
        self.unityAdsArea.frame = CGRectMake(0, self.adsTitleLabel.maxY, 320, 100);
        self.unityAdsArea.midX = self.view.width / 2;
        
        self.seeLaterLabel.frame = CGRectMake(DLConstDefaultMargin, self.unityAdsArea.maxY + 20, 200, 40);
    } else {
        
        self.unityAdsArea.frame = CGRectMake(DLConstDefaultMargin, self.adsTitleLabel.maxY, 320, 100);
        
        self.seeLaterLabel.frame = CGRectMake(self.unityAdsArea.maxX + DLConstDefaultMargin, 20, 200, 40);
    }
}

- (void)closeButtonTapped {
    
    if ([self.delegate respondsToSelector:@selector(afterPlayViewControllerTappedCloseButton)]) {
        [self.delegate afterPlayViewControllerTappedCloseButton];
    }
}

@end
