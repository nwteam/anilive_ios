//
//  ConstOther.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/11/05.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "ConstOther.h"

@implementation ConstOther

// ページングする際の1ページの要素数
NSInteger const DLConstPerPageCatalogPopular = 24;
NSInteger const DLConstPerPageCatalogAlphabetical = 24;
NSInteger const DLConstPerPageCatalogDetailMovies = 24;
NSInteger const DLConstPerPageCatalogDetailForums = 24;
NSInteger const DLConstPerPageComments = 30;
NSInteger const DLConstPerPageMyPageVideoHistory = 20;
NSInteger const DLConstPerPageMyPageMyForums = 20;
NSInteger const DLConstPerPageMyPageParticipatingForums = 20;

// ページングが始まる時の下部マージン
CGFloat const DLConstPagingBottomMargin = 100;

// Adobe Video Analytics
NSString * const DLConstAdobeAnalyticsTrackingServer = @"animeconsortiumjapan.hb.omtrdc.net";
NSString * const DLConstAdobeAnalyticsPublisher = @"4DDA41F155D33DF07F000101@AdobeOrg";
NSString * const DLConstAdobeAnalyticsPlayerName = @"APP(iOS)";

// Adobe Video Advertisement
NSString * const DLConstAdobeAdvertisementDomain = @"auditude.com";
NSInteger const DLConstAdobeAdvertisementZoneId = 267291;

// 動画広告の再生時間
NSInteger const DLConstPlayAdTime = 60;

// MyPageのニックネームの最小文字数
NSInteger const DLConstNicknameWordCountMin = 3;
// MyPageのニックネームの最大文字数
NSInteger const DLConstNicknameWordCountMax = 20;

// Video Proxy Server
NSUInteger const DLConstVideoProxyServerHostingPort = 11059;
NSString * const DLConstVideoProxyServerLocalHost = @"http://127.0.0.1:%zd";
NSString * const DLConstVideoProxyServerTargetURLString = @"https://bngn-vh.akamaihd.net";

// 初回起動判定用キー
NSString * const DLConstKeyIsFirstLaunch = @"DLConstKeyIsFirstLaunch";

// Keychain UUID保存用キー
NSString * const DLConstKeyUUIDForKeyChain = @"DLConstKeyUUIDForKeyChain";

// Keychain UIID保存用キー
NSString * const DLConstKeyUIIDForKeyChain = @"DLConstKeyUIIDForKeyChain";

//UserDefaultsのキー
NSString * const kUserDefaultsKey_SettingNewShowAlert = @"SETTING_NEW_SHOW_ALERTS";
NSString * const kUserDefaultsKey_SettingNewLiveAlert = @"SETTING_NEW_LIVE_ALERTS";
NSString * const kUserDefaultsKey_SettingNewCommentsAlert = @"SETTING_NEW_COMMENTS_ALERTS";

// URL
NSString * const DLConstURLStringTerms = @"https://dev-next.daisuki.net/web/?page=term";
NSString * const DLConstURLStringPrivacyPolicy = @"https://www.daisuki.net/static/privacypolicy.html";
NSString * const DLConstURLStringFaq = @"https://dev-next.daisuki.net/web/?page=faq";
NSString * const DLConstURLStringLicense = @"https://dev-next.daisuki.net/web/?page=license";
NSString * const DLConstURLStringForumRules = @"https://dev-next.daisuki.net/web/?page=rule";
NSString * const DLConstURLStringContactUs = @"https://dev-next.daisuki.net/web/contact.php";
NSString * const DLConstURLStringCopyrights = @"https://www.daisuki.net/copyright.html";

// 動画視聴の際に要求されるポイント量
NSUInteger const DLConstNecessaryPointAmountOfWatchMovie = 25;


@end
