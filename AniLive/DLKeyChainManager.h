//
//  DLKeyChainManager.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DLKeyChainManager : NSObject

+ (instancetype)sharedInstance;

- (NSString *)getUUID;
- (NSString *)getUIID;

- (void)saveUUID:(NSString *)uuid;
- (void)saveUIID:(NSString *)uiid;

@end
