//
//  DLCatalogAlphabeticalCell.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/11/05.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLSuperTableViewCell.h"
#import "DLWork.h"

@interface DLCatalogAlphabeticalCell : DLSuperTableViewCell

+ (CGFloat)getCellHeight;

- (void)setCellData:(DLWork *)workData;
- (void)setSeparatorLineIsHidden:(BOOL)isHidden;

@end
