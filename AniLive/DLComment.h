//
// Created by Kotaro Itoyama on 2016/10/08.
// Copyright (c) 2016 AnimeConsortiumJapan. All rights reserved.
//

#import "DLModelBase.h"


@interface DLComment : DLModelBase
@property(readonly, nonatomic, strong) NSNumber *commentId;
@property(readonly, nonatomic, strong) NSString *body;
@property(readonly, nonatomic, strong) NSString *userName;
@property(readonly, nonatomic, strong) NSNumber *userId;
@property(readonly, nonatomic, strong) NSDate *created;
@property(readonly, nonatomic, strong) NSDate *modified;
@property(readonly, nonatomic, strong) NSNumber *replyToUserId;

- (BOOL)isMine;
@end
