//
//  DLPlayerCreateForumViewController.m
//  AniLive
//
//  Created by isaoeka on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLPlayerCreateForumViewController.h"
#import "DLPlayerOptionBarView.h"
#import "DLHighlightedButton.h"
#import "DLAppFrameManager.h"
#import "DLAPIHelper.h"
#import "DLForum.h"
#import "UITextView+Placeholder.h"

@interface DLPlayerCreateForumViewController ()

@property (nonatomic, strong) UITextView *forumTitleTextView;

@property (nonatomic, strong) DLHighlightedButton *sendButton;

@end

@implementation DLPlayerCreateForumViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

#pragma mark - Override Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Public Methods

- (void)sendForumByMovieID:(NSInteger)movieID
           forumPinnedTime:(nonnull NSString *)forumPinnedTime {
    
    NSString *forumTitle = self.forumTitleTextView.text;
    if ( !NOT_NULL(forumTitle) ) {
        return;
    }

    @weakify(self)
    [DLAPIHelper postForumsByMovieID:movieID
                          forumTitle:forumTitle
                     forumPinnedTime:forumPinnedTime
                            callback:^(DLForum *forum, NSError *error) {
                                @strongify(self)
                                self.forumTitleTextView.text = @"";
                                if (error) {
                                    return;
                                }
//                                self.forumTitleTextView.text = @"";
                                NSLog(@"%@", forum);
                            }];
}

#pragma mark - Private Methods

- (void)initView {
    self.view.backgroundColor = [UIColor colorWithHex:DLConstColorCodeDarkGray];
    
#pragma mark Player Option Bar View

    self.playerOptionBarView = [[DLPlayerOptionBarView alloc] initWithType:DLPlayerOptionViewCreateForum];
    [self.view addSubview:self.playerOptionBarView];
    
#pragma mark Text View
    self.forumTitleTextView = [UITextView new];
    self.forumTitleTextView.backgroundColor = [UIColor whiteColor];
    self.forumTitleTextView.placeholder = Localizer(@"topic_placeholder");
    self.forumTitleTextView.returnKeyType = UIReturnKeySend;
    [self.view addSubview:self.forumTitleTextView];
    
    
#pragma mark Send Button
    self.sendButton = [DLHighlightedButton buttonWithType:UIButtonTypeCustom];
    self.sendButton.isNoNeedHighlight = YES;
    self.sendButton.backgroundColor = [UIColor clearColor];
    [self.sendButton setImage:[UIImage imageNamed:DLConstImageComment]
                     forState:UIControlStateNormal];
    [self.sendButton addTarget:self
                        action:@selector(buttonDidTap:)
              forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.sendButton];
}

- (void)updateFrame {
    
    self.playerOptionBarView.frame = CGRectMake(0,
                                                0,
                                                self.view.width,
                                                kPlayerOptionViewHeight);
    
    self.forumTitleTextView.frame = CGRectMake(DLConstDefaultMargin,
                                               self.playerOptionBarView.maxY,
                                               self.view.width - (DLConstDefaultMargin * 2),
                                               self.view.height - self.playerOptionBarView.height);
    
    float tmp = 50;
    self.sendButton.frame = CGRectMake(self.view.width - DLConstDefaultMargin - tmp,
                                       self.playerOptionBarView.maxY,
                                       tmp,
                                       tmp);
    
}

- (void)buttonDidTap:(UIButton *)button {
    FUNC_LOG
    //TODO: 正しい時間を送る(Forum作成ボタンを押したタイミングで時間を覚えておく)
    [self.playerOptionBarView.delegate crereateForumButtonDidTap:self.forumTitleTextView.text
                                                      pinnedTime:@"00:00:00"];
}

@end
