//
//  DLWorkDetailViewController.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/11/12.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <Toast/UIView+Toast.h>
#import "DLWorkDetailViewController.h"
#import "DLAppFrameManager.h"
#import "DLRetractableHeaderView.h"
#import "DLSegmentView.h"
#import "DLWorkDetailVideoViewController.h"
#import "DLWorkDetailForumViewController.h"
#import "DLWork.h"
#import "DLUnityAdsButton.h"
#import "DLUnityAdsManager.h"
#import "DLTransitionManager.h"
#import "DLPoint.h"

const static NSInteger kSegmentStartRow = 0;
const static CGFloat kSegmentViewHeight = 40;
const static CGFloat kHeaderViewHeight = 340;

@interface DLWorkDetailViewController () <UIScrollViewDelegate, DLSegmentViewDelegate, UIGestureRecognizerDelegate, DLRetractableHeaderDelegate, DLRetractableHeaderViewDelegate>

@property (nonatomic, strong) DLRetractableHeaderView *headerView;
@property (nonatomic, strong) DLSegmentView *segmentView;
@property (nonatomic, strong) UIScrollView *baseScrollView;
@property (nonatomic, strong) DLWorkDetailVideoViewController *videoViewController;
@property (nonatomic, strong) DLWorkDetailForumViewController *forumViewController;
@property (nonatomic, strong) DLUnityAdsButton *unityAdsButton;
@property (nonatomic, strong) DLWork *workData;
@property (nonatomic, strong) NSArray *segmentTitleArray;
@property (nonatomic, strong) NSMutableArray *segmentControllerArray;
@property (nonatomic, assign) CGFloat scrollViewOffsetY;
@property (nonatomic, assign) BOOL isfixedOffsetContradiction;

@end

@implementation DLWorkDetailViewController

- (instancetype)initWithWorkData:(DLWork *)workData
{
    self = [super init];
    if (self) {
        self.workData = workData;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.segmentTitleArray = @[Localizer(@"Videos"), Localizer(@"Forums")];
    self.segmentControllerArray = [[NSMutableArray alloc]init];
    
    // エッジスワイプで前画面に戻れるようにする
    self.navigationController.interactivePopGestureRecognizer.delegate = self;

    [self initView];
    [self updateFrame];
    
    // 初期表示タブに移動させておく
    [self.baseScrollView setContentOffset:CGPointMake(self.baseScrollView.width * kSegmentStartRow, 0)];
    [self setScrollsToTopWithCurrentRow:kSegmentStartRow];
    
    // エッジスワイプでのバックのときはタブ切り替えでのパンジェスチャーが干渉しないようにする
    [self.baseScrollView.panGestureRecognizer requireGestureRecognizerToFail:self.navigationController.interactivePopGestureRecognizer];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [UnityAds setDelegate:self];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self syncAdButtonState];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
    self.segmentView.delegate = nil;
    self.headerView.delegate = nil;
    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initView {
    
    self.view.backgroundColor = [UIColor colorWithHex:DLConstColorDarkBlue];
    
#pragma mark Base Scroll View
    
    self.baseScrollView = [[UIScrollView alloc]init];
    self.baseScrollView.backgroundColor = [UIColor clearColor];
    self.baseScrollView.scrollsToTop = NO;
    self.baseScrollView.pagingEnabled = YES;
    self.baseScrollView.alwaysBounceHorizontal = YES;
    self.baseScrollView.showsHorizontalScrollIndicator = NO;
    self.baseScrollView.delegate = self;
    [self.view addSubview:self.baseScrollView];
    
#pragma mark Video View Controller
    
    self.videoViewController = [[DLWorkDetailVideoViewController alloc]initWithHeaderClearCellHeight:kHeaderViewHeight + kSegmentViewHeight withWorkId:self.workData.workId];
    self.videoViewController.delegate = self;
    [self.baseScrollView addSubview:self.videoViewController.view];
    [self.segmentControllerArray addObject:self.videoViewController];
    
#pragma mark Forum View Controller
    
    self.forumViewController = [[DLWorkDetailForumViewController alloc]initWithHeaderClearCellHeight:kHeaderViewHeight + kSegmentViewHeight withWorkId:self.workData.workId];
    self.forumViewController.delegate = self;
    [self.baseScrollView addSubview:self.forumViewController.view];
    [self.segmentControllerArray addObject:self.forumViewController];
    
#pragma mark Header View
    
    self.headerView = [[DLRetractableHeaderView alloc]initWithWorkData:self.workData withStartHeight:kHeaderViewHeight];
    self.headerView.delegate = self;
    [self.view addSubview:self.headerView];
    
#pragma mark Segment View
    
    self.segmentView = [[DLSegmentView alloc]initWithTitles:self.segmentTitleArray startRow:kSegmentStartRow];
    self.segmentView.delegate = self;
    [self.view addSubview:self.segmentView];
    
#pragma mark Unity Ads Button
    
    self.unityAdsButton = [[DLUnityAdsButton alloc]init];
    @weakify(self)
    [self.unityAdsButton buttonDidTap:^(UIButton *button) {
        @strongify(self)
        [self.unityAdsButton setIsEnabled:NO];
        [UnityAds show:self];
    }];
    [self.view addSubview:self.unityAdsButton];
}

- (void) syncAdButtonState{
    if([UnityAds isReady]){
        [self.unityAdsButton setIsEnabled:YES];
        [self.unityAdsButton setButtonTitle:Localizer(@"point_ad_title")];
    }else{
        [self.unityAdsButton setIsEnabled:NO];
        [self.unityAdsButton setButtonTitle:Localizer(@"point_ad_loading")];
    }
}

- (void)updateFrame {
    
    CGFloat currentHeaderViewHeight = kHeaderViewHeight - self.scrollViewOffsetY;
    
    if (currentHeaderViewHeight <= DLConstNavigationBarHeight) {
        currentHeaderViewHeight = DLConstNavigationBarHeight;
        
        if (!self.isfixedOffsetContradiction) {
            
            self.isfixedOffsetContradiction = YES;
            
            @weakify(self)
            [self.segmentControllerArray enumerateObjectsUsingBlock:^(DLRetractableHeaderBaseViewController *viewController, NSUInteger index, BOOL *stop) {
                @strongify(self)
                if (index != self.segmentView.currentRow) {
                    CGFloat currentOffsetY = viewController.currentContentOffset.y;
                    if (currentOffsetY != kHeaderViewHeight - DLConstNavigationBarHeight) {
                        [viewController setOffsetToBaseScrollView:CGPointMake(0, kHeaderViewHeight - DLConstNavigationBarHeight)];
                    }
                }
            }];
        }
    } else {
        
        self.isfixedOffsetContradiction = NO;
        
        DLRetractableHeaderBaseViewController *controller = self.segmentControllerArray[self.segmentView.currentRow];
        CGPoint currentOffset = controller.currentContentOffset;
        @weakify(self)
        [self.segmentControllerArray enumerateObjectsUsingBlock:^(DLRetractableHeaderBaseViewController *viewController, NSUInteger index, BOOL *stop) {
            @strongify(self)
            [viewController setOffsetToBaseScrollView:currentOffset];
        }];
    }
    
    self.headerView.frame = CGRectMake(0, 0, CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame), currentHeaderViewHeight);
    
    self.segmentView.frame = CGRectMake(0, self.headerView.maxY, CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame), kSegmentViewHeight);
    
    self.baseScrollView.frame = CGRectMake(0, 0, CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame), CGRectGetHeight([DLAppFrameManager sharedInstance].applicationFrame));
    self.baseScrollView.contentSize = CGSizeMake(self.baseScrollView.width * self.segmentTitleArray.count, 0);
    
    self.baseScrollView.contentOffset = CGPointMake(self.baseScrollView.width * (self.segmentView.currentRow), self.baseScrollView.contentOffset.y);
    
    @weakify(self)
    [self.segmentControllerArray enumerateObjectsUsingBlock:^(UIViewController *viewController, NSUInteger index, BOOL *stop) {
        @strongify(self)
        
        viewController.view.frame = self.baseScrollView.bounds;
        viewController.view.x = self.baseScrollView.width * index;
    }];
    
    self.unityAdsButton.frame = CGRectMake(CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame) - [DLUnityAdsButton buttonWidth] - DLConstDefaultMargin, CGRectGetHeight([DLAppFrameManager sharedInstance].applicationFrame) - [DLUnityAdsButton buttonHeight] - DLConstDefaultMargin, [DLUnityAdsButton buttonWidth], [DLUnityAdsButton buttonHeight]);
}

- (void)setScrollsToTopWithCurrentRow:(NSInteger)currentRow {
    
    [self.segmentControllerArray enumerateObjectsUsingBlock:^(UIViewController *viewController, NSUInteger index, BOOL *stop) {
        
        if ([viewController conformsToProtocol:@protocol(DLSegmentViewControllerProtocol)]) {
            
            UIViewController <DLSegmentViewControllerProtocol> *segmentViewController = (UIViewController <DLSegmentViewControllerProtocol> *)viewController;
            
            if (index == currentRow) {
                [segmentViewController setScrollsToTop:YES];
            } else {
                [segmentViewController setScrollsToTop:NO];
            }
        }
    }];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [self.segmentView setLineViewCenterWithOffset:scrollView.contentOffset];
    
    CGFloat offsetX = scrollView.contentOffset.x;
    
    NSInteger currentRow = (offsetX + scrollView.width / 2) / scrollView.width;
    if (currentRow < 0) {
        currentRow = 0;
    }
    if (currentRow > self.segmentView.numberOfButtons - 1) {
        currentRow = self.segmentView.numberOfButtons - 1;
    }
    
    [_segmentView setRow:currentRow isMoveLine:NO];
    
    [self setScrollsToTopWithCurrentRow:currentRow];
}

#pragma mark - DLSegmentViewDelegate

- (void)segmentView:(DLSegmentView *)segmentView didSelectSegmentAtRow:(NSInteger)row {
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         [segmentView setRow:row isMoveLine:YES];
                         
                         [self.baseScrollView setContentOffset:CGPointMake(self.baseScrollView.width * row, 0) animated:NO];
                         
                     }completion:^(BOOL finished) {
                         
                         [self setScrollsToTopWithCurrentRow:row];
                     }];
}

#pragma mark - DLRetractableHeader Delegate

- (void)retractableScrollViewDidScroll:(UIScrollView *)scrollView {
    
    self.scrollViewOffsetY = scrollView.contentOffset.y;
    
    [self updateFrame];
}

#pragma mark - DLRetractableHeaderView Delegate

- (void)backButtonDidTap {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showInfoButtonDidTap {
    [DLTransitionManager showWorkInfoViewControllerWithWorkData:self.workData withTarget:self];
}

#pragma mark - Unity Ads Delegate
- (void)unityAdsReady:(NSString *)placementId {
    [self syncAdButtonState];
}

- (void)unityAdsDidError:(UnityAdsError)error withMessage:(NSString *)message {
    [self syncAdButtonState];
}

- (void)unityAdsDidStart:(NSString *)placementId {
    [self.unityAdsButton setIsEnabled: NO];
}

- (void)unityAdsDidFinish:(NSString *)placementId withFinishState:(UnityAdsFinishState)state {
    switch(state){
        case kUnityAdsFinishStateError:
        case kUnityAdsFinishStateSkipped:
            [self syncAdButtonState];
            break;
        case kUnityAdsFinishStateCompleted: {
            @weakify(self)
            [DLAPIHelper addPoint:^(DLPoint *point) {
                @strongify(self)
                [self syncAdButtonState];
            }];
            break;
        }
    }
}

@end
