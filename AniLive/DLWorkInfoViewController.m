//
//  DLWorkInfoViewController.m
//  AniLive
//
//  Created by 辰己佳祐 on 2017/01/14.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLWorkInfoViewController.h"
#import "DLWork.h"
#import "DLHighlightedButton.h"
#import "DLAppFrameManager.h"

@interface DLWorkInfoViewController ()

@property (nonatomic, strong) UIVisualEffectView *visualEffectView;
@property (nonatomic, strong) DLHighlightedButton *closeButton;
@property (nonatomic, strong) UIScrollView *baseScrollView;
@property (nonatomic, strong) UILabel *synopsisTitleLabel;
@property (nonatomic, strong) UILabel *synopsisDescriptionLabel;
@property (nonatomic, strong) UILabel *abstractTitleLabel;
@property (nonatomic, strong) UILabel *abstractDescriptionLabel;
@property (nonatomic, strong) UILabel *staffTitleLabel;
@property (nonatomic, strong) UILabel *staffDescriptionLabel;
@property (nonatomic, strong) UILabel *castTitleLabel;
@property (nonatomic, strong) UILabel *castDescriptionLabel;
@property (nonatomic, strong) DLWork *workData;

@end

@implementation DLWorkInfoViewController

- (instancetype)initWithWorkData:(DLWork *)workData
{
    self = [super init];
    if (self) {
        self.workData = workData;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initView];
    [self loadData];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)initView {
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    self.visualEffectView = [[UIVisualEffectView alloc]initWithEffect:blurEffect];
    [self.view addSubview:self.visualEffectView];
    
    self.closeButton = [DLHighlightedButton buttonWithType:UIButtonTypeCustom];
    self.closeButton.isNoNeedHighlight = YES;
    [self.closeButton setImage:[UIImage imageNamed:DLConstIconClose] forState:UIControlStateNormal];
    self.closeButton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    [self.closeButton addTarget:self
                         action:@selector(closeButtonDidTap)
               forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.closeButton];
    
    self.baseScrollView = [[UIScrollView alloc]init];
    self.baseScrollView.backgroundColor = [UIColor clearColor];
    self.baseScrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    [self.view addSubview:self.baseScrollView];
    
    self.synopsisTitleLabel = [self getTitleLabelWithAddTo:self.baseScrollView];
    self.synopsisDescriptionLabel = [self getDescriptionLabelWithAddTo:self.baseScrollView];
    self.abstractTitleLabel = [self getTitleLabelWithAddTo:self.baseScrollView];
    self.abstractDescriptionLabel = [self getDescriptionLabelWithAddTo:self.baseScrollView];
    self.staffTitleLabel = [self getTitleLabelWithAddTo:self.baseScrollView];
    self.staffDescriptionLabel = [self getDescriptionLabelWithAddTo:self.baseScrollView];
    self.castTitleLabel = [self getTitleLabelWithAddTo:self.baseScrollView];
    self.castDescriptionLabel = [self getDescriptionLabelWithAddTo:self.baseScrollView];
}

- (UILabel *)getTitleLabelWithAddTo:(UIView *)view {
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = [UIColor clearColor];
    label.font = BASE_FONT_BOLD(18);
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentLeft;
    label.numberOfLines = 0;
    [view addSubview:label];
    return label;
}

- (UILabel *)getDescriptionLabelWithAddTo:(UIView *)view {
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = [UIColor clearColor];
    label.font = BASE_FONT(14);
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentLeft;
    label.numberOfLines = 0;
    [view addSubview:label];
    return label;
}

- (void)updateFrame {
    
    CGFloat kTitleLabelHeight = 30;
    CGFloat kSectionMargin = 15;
    
    self.visualEffectView.frame = [DLAppFrameManager sharedInstance].applicationFrame;
    self.closeButton.frame = CGRectMake(DLConstDefaultMargin, CGRectGetHeight(STATUSBAR_RECT) + DLConstDefaultMargin, 40, 40);
    self.baseScrollView.frame = CGRectMake(DLConstDefaultMargin, self.closeButton.maxY, CGRectGetWidth([DLAppFrameManager sharedInstance].applicationFrame) - (DLConstDefaultMargin * 2), CGRectGetHeight([DLAppFrameManager sharedInstance].applicationFrame) - self.closeButton.maxY - DLConstDefaultMargin);
    
    self.synopsisTitleLabel.frame = CGRectMake(0, 0, self.baseScrollView.width, kTitleLabelHeight);
    self.synopsisDescriptionLabel.width = self.baseScrollView.width;
    [self.synopsisDescriptionLabel sizeToFit];
    self.synopsisDescriptionLabel.x = 0;
    self.synopsisDescriptionLabel.y = self.synopsisTitleLabel.maxY;
    
    self.abstractTitleLabel.frame = CGRectMake(0, self.synopsisDescriptionLabel.maxY + kSectionMargin, self.baseScrollView.width, kTitleLabelHeight);
    self.abstractDescriptionLabel.width = self.baseScrollView.width;
    [self.abstractDescriptionLabel sizeToFit];
    self.abstractDescriptionLabel.x = 0;
    self.abstractDescriptionLabel.y = self.abstractTitleLabel.maxY;
    
    self.staffTitleLabel.frame = CGRectMake(0, self.abstractDescriptionLabel.maxY + kSectionMargin, self.baseScrollView.width, kTitleLabelHeight);
    self.staffDescriptionLabel.width = self.baseScrollView.width;
    [self.staffDescriptionLabel sizeToFit];
    self.staffDescriptionLabel.x = 0;
    self.staffDescriptionLabel.y = self.staffTitleLabel.maxY;
    
    self.castTitleLabel.frame = CGRectMake(0, self.staffDescriptionLabel.maxY + kSectionMargin, self.baseScrollView.width, kTitleLabelHeight);
    self.castDescriptionLabel.width = self.baseScrollView.width;
    [self.castDescriptionLabel sizeToFit];
    self.castDescriptionLabel.x = 0;
    self.castDescriptionLabel.y = self.castTitleLabel.maxY;
    
    self.baseScrollView.contentSize = CGSizeMake(0, self.castDescriptionLabel.maxY + kSectionMargin);
}

- (void)loadData {
    
    self.synopsisTitleLabel.text = Localizer(@"title_synopsis");
    self.abstractTitleLabel.text = Localizer(@"title_abstract");
    self.staffTitleLabel.text = Localizer(@"title_staff");
    self.castTitleLabel.text = Localizer(@"title_cast");
    
    self.synopsisDescriptionLabel.text = self.workData.synopsis;
    NSMutableString *abstractString = [[NSMutableString alloc]init];
    [abstractString appendFormat:Localizer(@"description_creation_year"), self.workData.creationYear];
    [abstractString appendString:@"\n"];
    [abstractString appendFormat:Localizer(@"description_licensor"), self.workData.licensor];
    [abstractString appendString:@"\n"];
    [abstractString appendFormat:Localizer(@"description_studio"), self.workData.studioName];
    self.abstractDescriptionLabel.text = abstractString;
    self.staffDescriptionLabel.text = self.workData.staff;
    self.castDescriptionLabel.text = self.workData.cast;
    
    [self updateFrame];
}

- (void)closeButtonDidTap {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
