//
//  DLCaptionItem.h
//  AniLive
//
//  Created by HisashiOkura on 2017/01/17.
//  Copyright © 2017年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLModelBase.h"

@interface DLCaptionItem : DLModelBase

@property(readonly, nonatomic, strong) NSString *beginTimeMs;
@property(readonly, nonatomic, strong) NSString *endTimeMs;
@property(readonly, nonatomic, strong) NSString *style;
@property(readonly, nonatomic, strong) NSString *captionMessage;

@end
