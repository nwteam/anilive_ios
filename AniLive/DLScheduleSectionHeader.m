//
//  DLScheduleSectionHeader.m
//  AniLive
//
//  Created by Loyal on 2016/12/21.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLScheduleSectionHeader.h"
#import "DLAppFrameManager.h"
#import "NSString+Category.h"

const static CGFloat kSectionHeaderHeight = 40;

@interface DLScheduleSectionHeader ()

@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation DLScheduleSectionHeader

- (instancetype)init {
    self = [super init];
    if(self){
        [self initView];
    }
    return self;
}

#pragma mark - Class Methods

+ (CGFloat)getSectionHeaderHeight {
    return kSectionHeaderHeight;
}

#pragma mark - Public Methods

- (void)setTitle:(NSString *)title {
    self.titleLabel.text = title;
    [self updateFrame];
}

#pragma mark - Private Methods

- (void)initView {
    
    self.backgroundColor = [UIColor darkGrayColor];
    
    self.titleLabel = [UILabel new];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = BASE_FONT(16);
    self.titleLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.titleLabel];
}

- (void)updateFrame {
    CGRect applicationFrame = [DLAppFrameManager sharedInstance].applicationFrame;
    
    self.width = CGRectGetWidth(applicationFrame);
    self.height = kSectionHeaderHeight;
    
    [self.titleLabel sizeToFit];
    self.titleLabel.frame = CGRectMake(DLConstDefaultMargin,
                                       0,
                                       self.width,
                                       self.height);
}


@end
