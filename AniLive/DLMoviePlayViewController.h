//
//  DLMoviePlayViewController.h
//  AniLive
//
//  Created by 辰己佳祐 on 2016/09/21.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLContinueToPlayView.h"

@class DLMovie, DLLive;

@interface DLMoviePlayViewController : UIViewController<NSXMLParserDelegate, DLContinueToPlayViewDelegate>

- (instancetype)initWithMovieData:(DLMovie *)movieData
                      withIsModal:(BOOL)isModal;
- (instancetype)initWithLiveData:(DLLive *)liveData
                     withIsModal:(BOOL)isModal;

@end
