//
//  DLCatalogAlphabeticalViewController.m
//  AniLive
//
//  Created by 辰己佳祐 on 2016/10/01.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLCatalogAlphabeticalViewController.h"
#import "DLCatalogAlphabeticalCell.h"
#import "DLTransitionManager.h"

@interface DLCatalogAlphabeticalViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, assign) NSInteger currentPageNumber;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) BOOL isExistNextPage;

@end

@implementation DLCatalogAlphabeticalViewController

#pragma mark - Override Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.items = [[NSMutableArray alloc]init];
    
    [self initView];
    [self updateFrame];
    [self loadData:DLLoadingTypeInitial];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    FUNC_LOG
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - DLSegmentViewControllerProtocol

- (void)setScrollsToTop:(BOOL)scrollsToTop {
    
    self.tableView.scrollsToTop = scrollsToTop;
}

#pragma mark - Private Methods

- (void)initView {
    
#pragma mark Table View
    
    self.tableView = [[UITableView alloc]init];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.scrollsToTop = NO;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[DLCatalogAlphabeticalCell class]
           forCellReuseIdentifier:[DLCatalogAlphabeticalCell getCellIdentifier]];
    [self.view addSubview:self.tableView];
}

- (void)updateFrame {
    self.tableView.frame = self.view.bounds;
}

- (void)loadData:(DLLoadingType)loadingType {
    
    if (self.isLoading) {
        return;
    }
    
    if (loadingType == DLLoadingTypeInitial || loadingType == DLLoadingTypePullRefresh) {
        self.currentPageNumber = 1;
        self.isExistNextPage = YES;
    }
    
    if (!self.isExistNextPage) {
        return;
    }
    
    self.isLoading = YES;
    
    @weakify(self)
    [DLAPIHelper fetchWorksWithPageNumber:self.currentPageNumber
                                  perPage:DLConstPerPageCatalogPopular
                                 sortType:@"alphabetical"
                                 callback:^(NSMutableArray *works, NSError *error) {
                                     @strongify(self)
                                     
                                     if (NOT_NULL(error)) {
                                         // TODO: エラー時のポップアップなどを表示する
                                     } else {
                                         if (loadingType == DLLoadingTypeInitial || loadingType == DLLoadingTypePullRefresh) {
                                             [self.items removeAllObjects];
                                         }
                                         
                                         if (works.count > 0) {
                                             [self.items addObjectsFromArray:works];
                                             self.currentPageNumber += 1;
                                         } else {
                                             self.isExistNextPage = NO;
                                         }
                                     }
                                     
                                     [self.tableView reloadData];
                                     
                                     self.isLoading = NO;
                                 }];
}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        return self.items.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        DLCatalogAlphabeticalCell *cell = [tableView dequeueReusableCellWithIdentifier:[DLCatalogAlphabeticalCell getCellIdentifier]
                                                                          forIndexPath:indexPath];
        
        if (indexPath.row < self.items.count) {
            DLWork *workData = self.items[indexPath.row];
            [cell setCellData:workData];
            
            // 一番底のセルのセパレータは消す
            [cell setSeparatorLineIsHidden:(indexPath.row == self.items.count - 1)];
        }
        return cell;
    }
    return  nil;
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [DLCatalogAlphabeticalCell getCellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0 && indexPath.row < self.items.count) {
        DLWork *workData = self.items[indexPath.row];
        [DLTransitionManager showWorkDetailViewControllerWithWorkData:workData withTarget:nil];
    }
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat offsetY = scrollView.contentOffset.y;
    
    if (offsetY > self.tableView.contentSize.height - self.tableView.height - DLConstPagingBottomMargin) {
        [self loadData:DLLoadingTypePaging];
    }
}

@end
