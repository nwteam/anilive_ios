#import "DLAppDelegate.h"
#import "DRMKeyResponse.h"
#import "HTTPConnection.h"
#import "HTTPLogging.h"
#import "M3U8Parser.h"

#import <unistd.h>
#import <fcntl.h>

#if ! __has_feature(objc_arc)
#warning This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

// Log levels : off, error, warn, info, verbose
// Other flags: trace

#define NULL_FD  -1


@implementation DRMKeyResponse

- (id)initWithKeyURL:(NSString*)uri forConnection:(HTTPConnection *)parent
{
    if((self = [super init]))
	{
		connection = parent; // Parents retain children, children do NOT retain parents
		_aborted = NO;

        _keyURI = [NSString stringWithString:uri];
        NSString* hash = [THE_CONTEXT() md5:_keyURI];
        _keyData = [THE_CONTEXT().hlsCache.m3u8parser.keys objectForKey:hash];
        _fileLength = _keyData.length;
		_fileOffset = 0;
	}
	return self;
}

- (void)abort
{
	[connection responseDidAbort:self];
	_aborted = YES;
}

- (UInt64)contentLength
{
	return _fileLength;
}

- (UInt64)offset
{
	return _fileOffset;
}

- (void)setOffset:(UInt64)offset
{
	_fileOffset = offset;
}

- (NSData *)readDataOfLength:(NSUInteger)length
{
	UInt64 bytesLeftInFile = _fileLength - _fileOffset;
	
	NSUInteger bytesToRead = (NSUInteger)MIN(length, bytesLeftInFile);
	
	// Make sure buffer is big enough for read request.
	// Do not over-allocate.
	
    NSData* ret = [_keyData subdataWithRange:NSMakeRange(0, bytesToRead)];
	
	if (ret.length <= 0) {
		[self abort];
		return nil;
	} else {
		_fileOffset += ret.length;
		
		return ret;
	}
}

- (BOOL)isDone
{
	BOOL result = (_fileOffset == _fileLength);
	
	return result;
}

- (void)dealloc
{
}

@end
