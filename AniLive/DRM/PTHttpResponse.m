//
//  PTHttpResponse.m
//  DAISUKI
//
//  Created by 古川　将範 on 2016/04/11.
//  Copyright © 2016年 DAISUKI Inc. All rights reserved.
//

#import "PTHttpResponse.h"

@implementation PTHttpResponse

- (id)initWithData:(NSData *)data {
    if (self = [super init]) {
        self.data = data;
        self.offset = 0;
    }
    
    return self;
}

- (UInt64)contentLength {
    return self.data.length;
}

- (NSData *)readDataOfLength:(NSUInteger)length {
    if (self.contentLength < self.offset + length) {
        length -= self.offset + length - self.contentLength;
    }
    NSRange range = NSMakeRange(self.offset, length);
    NSData *res = [self.data subdataWithRange:range];
    self.offset += length;
    
    return res;
}

- (BOOL)isDone {
    return self.data.length <= self.offset;
}

@end
