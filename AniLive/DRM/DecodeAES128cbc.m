//
//  DecodeAES128cbc.m
//  drmtest
//
//  Created by 新井 茂成 on 2013/07/16.
//  Copyright (c) 2013年 agyou. All rights reserved.
//

#import "DecodeAES128cbc.h"


@implementation DecodeAES128cbc


//	初期設定する
- (void)startCipher:(NSData*)aeskey
{
    self.aesKey = aeskey;
    unsigned char iv[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    EVP_CIPHER_CTX_init( &_de );
	EVP_DecryptInit_ex( &_de, EVP_aes_128_cbc(), NULL, [self.aesKey bytes], iv );
	EVP_CIPHER_CTX_set_padding( &_de, 0 );
	EVP_CIPHER_CTX_set_key_length( &_de, 16 );
}


//	終了設定する
- (void)stopCipher
{
    EVP_CIPHER_CTX_cleanup( &_de );
}


//	デコードバッファのクリア
- (void)clear:(NSMutableData*)read_buf
{
	self.readBuf   = read_buf;
    self.decodeBuf = [[NSMutableData alloc] initWithData:0];
	self.isDecoded = NO;
}


//	復号する(全部)
- (void)decodeBufferForAll:(BOOL)is_all;
{
	//	1032 バイトごとにデコード
	int len = (int)([self.readBuf length] - _decodeIndex);
	int result = 1;
    	
	//	1032 バイトある？
	while( 1032 < len )
	{
		result = (int)[self decodeBufferWithSize:1032];
		len    = (int)[self.readBuf length] - _decodeIndex;
	}

	//	全部デコードモード？
	if( is_all )
	{
		if( 0 < len )
		{
			result = (int)[self decodeBufferWithSize:len];
            if( !result )
            {
                
                NSLog( @"decrypto failed : (key) %@", _aesKey );
            }
		}


        self.isDecoded = YES;
		[self stopCipher];
	}
}


//	復号する(指定バイト数)
- (NSInteger)decodeBufferWithSize:(int)ilen
{
    if( 0 == ilen )
    {
        return 1;
    }
    
    int                  olen    = 0;
    int                  templen = 0;
    const unsigned char* ibuff   = (const unsigned char *)[self.readBuf bytes];
    
    EVP_DecryptUpdate( &_de, (unsigned char*)_decodeWorkBuffer, &olen, &ibuff[_decodeIndex], ilen );
    
    NSInteger final_success = EVP_DecryptFinal_ex(
		&_de, (unsigned char*)&_decodeWorkBuffer[olen], &templen );
    
    [self.decodeBuf appendData:[NSData dataWithBytes:(unsigned char*)_decodeWorkBuffer length:olen + templen]];
    _decodeIndex += ilen;
    
    return final_success;
}

- (NSMutableData*)decodeWithKey:(NSData*)aeskey input:(NSMutableData*)input
{
    NSMutableData* ret = [[NSMutableData alloc] init];

    EVP_CIPHER_CTX de;
    unsigned char iv[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    EVP_CIPHER_CTX_init( &de );
	EVP_DecryptInit_ex( &de, EVP_aes_128_cbc(), NULL, [aeskey bytes], iv );
	EVP_CIPHER_CTX_set_padding( &de, 0 );
	EVP_CIPHER_CTX_set_key_length( &de, 16 );
    
    const unsigned char* ibuff   = (const unsigned char *)[input bytes];
	unsigned char	outbuf[2048];	//	デコード用の作業バッファ
    int		p_len, f_len;
    int decodepos = 0;

    while (decodepos < input.length) {
        int datasize = 1032;
        if (datasize > input.length - decodepos) {
            datasize = (int)input.length - decodepos;
        }
        
        EVP_DecryptUpdate  (&de, outbuf, &p_len,  &ibuff[decodepos], datasize);
        if (!EVP_DecryptFinal(&de, (outbuf+p_len), &f_len)) {
            //f_len = 0;
        }
        
        [ret appendData:[NSData dataWithBytes:(unsigned char*)outbuf length:p_len + f_len]];
        decodepos += datasize;
    }

    EVP_CIPHER_CTX_cleanup(&de);
    
    return ret;
}

@end
