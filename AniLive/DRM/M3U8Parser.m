//
//  M3U8Parser.m
//  DAISUKI
//
//  Created by MacBookAir on 13/07/30.
//  Copyright (c) 2013年 DAISUKI Inc. All rights reserved.
//

#import "DLAppDelegate.h"
#import "M3U8Parser.h"

@implementation M3U8Parser

//初期化
-(id) init{
    
    self = [super init];
    
    self.items = [NSMutableArray array];
    self.lengths = [NSMutableArray array];
    self.children = [NSMutableArray array];
    self.keys = [NSMutableDictionary dictionary];
    
    return self;
    
}

- (BOOL)parseWithURI:(NSString*)uri daisukiConvert:(BOOL)convert
{
//    NSLog(@"m3u8 :%@", uri);
    
    self.items = [NSMutableArray array];
    self.lengths = [NSMutableArray array];
    self.children = [NSMutableArray array];
    self.keys = [NSMutableDictionary dictionary];

    NSFileManager* file_mgr = [NSFileManager defaultManager];

#if IS_PTPLAYER
    NSString *localUri = [uri stringByReplacingOccurrencesOfString:CDN_URL_HTTPS withString:THE_CONTEXT().getLocalURL];
    NSString* filename = [self getCacheFileName:localUri];
    if ([[filename lastPathComponent] isEqualToString:@"master.m3u8"]) {
        [file_mgr removeItemAtPath:filename error:nil];
    }
#else
    NSString* filename = [self getCacheFileName:uri];
#endif
    
    NSData* data;
    if( [file_mgr fileExistsAtPath:filename] ){
        NSLog(@"READING M3U8 From Cache %@", uri);
        data = [[NSData alloc] initWithContentsOfFile:filename];
        if( [data length] <= 0) {
            return NO;
        }
    } else {
        NSURL* url = [NSURL URLWithString:uri];
        
        //  データの受信
        NSMutableURLRequest* request = [[NSMutableURLRequest alloc]
                                        initWithURL:url
                                        cachePolicy:NSURLRequestReloadIgnoringCacheData
                                        timeoutInterval:30];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            [request setValue:PLAYER_USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
        } else {
            [request setValue:PLAYER_USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
        }
        NSURLResponse* response = nil;
        NSError*	   error	= nil;
        
        data = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:&response error:&error];
        
        if( [data length] <= 0) {
            return NO;
        }

        //NSLog(@"writing m3u8 cache");
        if (convert) {
            NSString *buf = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSString *replaced = [self daisukiURLConvert:buf];
            NSData   *replaced_data = [replaced dataUsingEncoding:NSUTF8StringEncoding];
            //NSLog(@"m3u8 buf:%@", buf);
            
            [replaced_data writeToFile:filename atomically:YES];
        } else {
            [data writeToFile:filename atomically:YES];
        }
    }
    
	//  パースの開始
	[self parseWithData:data daisukiConvert:convert];
    
	return YES;
}

- (BOOL)parseWithLocalURI:(NSString*)localuri daisukiConvert:(BOOL)convert
{
    self.items = [NSMutableArray array];
    self.lengths = [NSMutableArray array];
    self.children = [NSMutableArray array];
    self.keys = [NSMutableDictionary dictionary];

    NSString* uri = [localuri stringByReplacingOccurrencesOfString:[THE_CONTEXT() getLocalURL] withString:CDN_URL_HTTPS];
    
    NSString* filename = [self getCacheFileName:localuri];
    NSFileManager* file_mgr = [NSFileManager defaultManager];
    
    NSData* data;
    if( [file_mgr fileExistsAtPath:filename] ){
        data = [[NSData alloc] initWithContentsOfFile:filename];
        if( [data length] <= 0) {
            return NO;
        }
    } else {
        NSURL* url = [NSURL URLWithString:uri];
        
        //  データの受信
        NSMutableURLRequest* request = [[NSMutableURLRequest alloc]
                                        initWithURL:url
                                        cachePolicy:NSURLRequestReloadIgnoringCacheData
                                        timeoutInterval:30];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            [request setValue:PLAYER_USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
        } else {
            [request setValue:PLAYER_USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
        }
        NSURLResponse* response = nil;
        NSError*	   error	= nil;
        
        data = [NSURLConnection sendSynchronousRequest:request
                                     returningResponse:&response error:&error];
        
        if( [data length] <= 0) {
            return NO;
        }
        
        {//整形する
            NSString *buf = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
#if IS_PTPLAYER
            buf  = [buf stringByReplacingOccurrencesOfString:@"#EXT-X-ALLOW-CACHE:YES\n" withString:@""];
#endif
            buf  = [buf stringByReplacingOccurrencesOfString:@"#EXT-X-VERSION:2\n" withString:@"#EXT-X-VERSION:3\n"];
            data = [buf dataUsingEncoding:NSUTF8StringEncoding];
            
        }
        
        if (convert) {
            NSString *buf = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSString *replaced = [self daisukiURLConvert:buf];
#if IS_PTPLAYER
            replaced = [replaced stringByReplacingOccurrencesOfString:@"#EXT-X-ALLOW-CACHE:YES\n" withString:@""];
#endif

            NSData   *replaced_data = [replaced dataUsingEncoding:NSUTF8StringEncoding];
            
            [replaced_data writeToFile:filename atomically:YES];
        } else {
            [data writeToFile:filename atomically:YES];
        }
    }
    
	//  パースの開始
	[self parseWithData:data daisukiConvert:convert];
    
	return YES;
}

//m3u8パース
- (BOOL)parseWithData:(NSData*)data daisukiConvert:(BOOL)convert
{
    
    NSString* buf = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSMutableArray *lines = [NSMutableArray array];
    [buf enumerateLinesUsingBlock:^(NSString *line, BOOL *stop) {
        [lines addObject:line];
    }];
    
    int type = 0;
    for (int i = 0; i < lines.count; i++) {
        NSString* line = lines[i];
        if (line.length <= 0) continue;
        
        if ([[line substringToIndex:1] isEqualToString:@"#"]) {
            //TS
            if (line.length > 8) {
                if ([[line substringToIndex:8] isEqualToString:@"#EXTINF:"]) {
                    NSString* str = [NSString stringWithFormat:@"%@", line];
                    str = [str stringByReplacingOccurrencesOfString:@"#EXTINF:" withString:@""];
                    str = [str stringByReplacingOccurrencesOfString:@"," withString:@""];
                    float time = [str floatValue];
                    
                    [self.lengths addObject:[NSNumber numberWithFloat:time]];
                    type = 0;
                }
                
            }
            //キー
            if (line.length >= 11) {
                if ([[line substringToIndex:11] isEqualToString:@"#EXT-X-KEY:"]) {
                    NSRange range = [line rangeOfString:@"\".*?\"" options:NSRegularExpressionSearch];
                    if (range.location != NSNotFound) {
                        range.location++;
                        range.length-=2;
                        NSString* keyfile = [line substringWithRange:range];
                        if (convert) {
                            keyfile = [self daisukiURLConvert:keyfile];
                        }
                        [self loadKey:keyfile];
                    }
                    type = 1;
                }
            }
            //子M3U8
            if (line.length >= 18) {
                if ([[line substringToIndex:18] isEqualToString:@"#EXT-X-STREAM-INF:"]) {
                    type = 2;
#if IS_PTPLAYER
                    NSString *pattern = @"BANDWIDTH=[0-9]*";
                    NSRegularExpression *regex = [NSRegularExpression
                                                  regularExpressionWithPattern:pattern
                                                  options:NSRegularExpressionCaseInsensitive
                                                  error:nil];
                    for (NSTextCheckingResult *match in [regex
                                                         matchesInString:line
                                                         options:0
                                                         range:NSMakeRange(0, line.length)]) {
                        NSArray *bandWidth = [[line substringWithRange:[match range]] componentsSeparatedByString:@"="];
                        if (bandWidth.count == 2) {
                            [self.lengths addObject:bandWidth[1]];
                        }
                    }
#endif
                }
            }
        } else {
            if (type == 0) {
                if (convert) {
                    line = [self daisukiURLConvert:line];
                }
                [self.items addObject:line];
            } else if (type == 2) {
                line = [self daisukiURLConvert:line];
                [self.children addObject:line];
            }
        }
    }
	
    return YES;
}

- (NSString*)daisukiURLConvert:(NSString*)url
{
    NSString* ret = nil;
    ret = [url stringByReplacingOccurrencesOfString:CDN_URL withString:[THE_CONTEXT() getLocalURL]];
    ret = [ret stringByReplacingOccurrencesOfString:CDN_URL_HTTPS withString:[THE_CONTEXT() getLocalURL]];
    return ret;
}


-(NSString*)getCacheFileName:(NSString*)uri
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cache_dir = [paths objectAtIndex:0];
    NSString* hash = [THE_CONTEXT() md5:uri];
    NSString* filename = [NSString stringWithFormat:@"%@/%@.m3u8", cache_dir, hash];

#if IS_PTPLAYER
    NSURL *url = [NSURL URLWithString:uri];
    if ([[url lastPathComponent] isEqualToString:@"master.m3u8"]) {
        NSString *name = [filename lastPathComponent];
        filename = [filename stringByReplacingOccurrencesOfString:name withString:@"master.m3u8"];
    }
#endif
    
    return filename;
}

-(void)loadKey:(NSString*)uri
{
    //キーファイルはローカルから読まない
    uri = [uri stringByReplacingOccurrencesOfString:[THE_CONTEXT() getLocalURL] withString:CDN_URL_HTTPS];
    NSURL* url = [NSURL URLWithString:uri];
    
#if defined(_OFFLINE_MODE_)
    NSString* filename = [self getCacheFileName:uri];
    NSFileManager* file_mgr = [NSFileManager defaultManager];
    
    if( [file_mgr fileExistsAtPath:filename] ){
        NSData* data;
        data = [[NSData alloc] initWithContentsOfFile:filename];
        if( [data length] <= 0) {
            return;
        }
        //Dictonaryに保存 localurlをハッシュとする
        NSString* localuri = [self daisukiURLConvert:(NSString*)uri];
        NSString* hash = [THE_CONTEXT() md5:localuri];
        [_keys setObject:data forKey:hash];
        return;
    }
#endif
    //  データの受信
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc]
                                    initWithURL:url
                                    cachePolicy:NSURLRequestReloadIgnoringCacheData
                                    timeoutInterval:30];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [request setValue:PLAYER_USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
    } else {
        [request setValue:PLAYER_USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
    }
    NSURLResponse* response = nil;
    NSError*	   error	= nil;
    
    NSData* data = [NSURLConnection sendSynchronousRequest:request
                                 returningResponse:&response error:&error];
    
    if( [data length] <= 0) {
        return;
    }
    //Dictonaryに保存 localurlをハッシュとする
    NSString* localuri = [self daisukiURLConvert:(NSString*)uri];
    NSString* hash = [THE_CONTEXT() md5:localuri];
    [_keys setObject:data forKey:hash];

#if defined(_OFFLINE_MODE_)
    [data writeToFile:filename atomically:YES];
#endif
}

@end
