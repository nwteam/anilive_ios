//
//  PTHttpResponse.h
//  DAISUKI
//
//  Created by 古川　将範 on 2016/04/11.
//  Copyright © 2016年 DAISUKI Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTTPResponse.h"

@interface PTHttpResponse : NSObject <HTTPResponse>

@property (nonatomic, retain) NSData *data;

@property (nonatomic) UInt64 length;
@property (nonatomic) UInt64 offset;

- (id)initWithData:(NSData*)data;

@end
