//
//  DRMHttpConnection.m
//  drmtest
//
//  Created by MacBookAir on 13/06/14.
//  Copyright (c) 2013年 agyou. All rights reserved.
//

#import "DLAppDelegate.h"
#import "DRMHttpConnection.h"
#import "M3U8Response.h"
#import "M3U8Parser.h"
#import "DRMCacheResponse.h"
#import "DRMKeyResponse.h"
#import "CacheProxyResponse.h"
#import "HTTPMessage.h"
#import "DDRange.h"
#import "GCDAsyncSocket.h"
#import "PTHttpResponse.h"

/** @brief PEM ファイル読み込み関数有効化のため．詳細は不明． */
#undef SSLEAY_MACROS
#include <openssl/pem.h>
#include <openssl/x509v3.h>
#include <openssl/rsa.h>


@implementation DRMHttpConnection
- (id)initWithAsyncSocket:(GCDAsyncSocket *)newSocket configuration:(HTTPConfig *)aConfig
{
    return [super initWithAsyncSocket:newSocket configuration:aConfig];
}

- (NSObject<HTTPResponse> *)httpResponseForMethod:(NSString *)method URI:(NSString *)path;
{
    NSDictionary* header = [request allHeaderFields];
    
    //実行中のIP以外からのアクセスの場合
    if (![asyncSocket.connectedHost isEqualToString:[THE_CONTEXT() getIPAddress]] &&
        ![asyncSocket.connectedHost isEqualToString:@"127.0.0.1"] &&
        ![asyncSocket.connectedHost isEqualToString:@"::1"]) {

        //AppleTVだけ許可
        NSString* useragent = [header objectForKey:@"User-Agent"];
        NSRange searchResult = [useragent rangeOfString:@"Apple TV"];
        if( searchResult.location==NSNotFound ) {
            return nil;
        }
    }
    
	NSObject<HTTPResponse> *result = nil;

    if([method isEqualToString:@"GET"] ) {
        NSRange rangem3 = [path rangeOfString:@".m3u8"];
        NSRange rangets = [path rangeOfString:@".ts"];
        NSRange rangekey = [path rangeOfString:@"crypt.key"];
        
        if (rangem3.location != NSNotFound) {
#if IS_PTPLAYER
            //DEBUG_LOG(@"Proxy Donload :%@", path);
            return [self m3u8Response:path];
#endif
            
            M3U8Parser* parser = [[M3U8Parser alloc] init];
            //m3u8のダウンロード
            NSString* uri = [NSString stringWithFormat:@"%@%@", [THE_CONTEXT() getLocalURL], path];
            NSString* filename = [parser getCacheFileName:uri];

            M3U8Response* res = [[M3U8Response alloc] initWithFilePath:filename  forConnection:self];
            
            return res;

        } else if (rangekey.location != NSNotFound) {
            //キーファイルのダウンロード
#if IS_PTPLAYER
            return [self binaryResponse:path];
#endif

            NSString* uri = [NSString stringWithFormat:@"%@%@", [THE_CONTEXT() getLocalURL], path];
            return [[DRMKeyResponse alloc] initWithKeyURL:uri forConnection:self];

        } else if (rangets.location != NSNotFound) {
            //tsのダウンロード
#if IS_PTPLAYER
            //DEBUG_LOG(@"Proxy Donload :%@", path);
            return [self binaryResponse:path];
#endif
            
            NSString* uri = [NSString stringWithFormat:@"%@%@", [THE_CONTEXT() getLocalURL], path];
            return [[DRMCacheResponse alloc] initWithURL:uri forConnection:self];
        } else {
#if defined(_OFFLINE_MODE_)
            //NSLog(@"OFFLINE MODE: %@", path);
            //その他のキャッシュ付きダウンロード
            NSString* uri = [NSString stringWithFormat:@"%@%@", _OFFLINE_URL, path];
            NSURL* url = [NSURL URLWithString:uri];
            return [[CacheProxyResponse alloc] initWithURL:url forConnection:self post:@""];
#endif
        }
        
        
        return nil;
    } else {
#if defined(_OFFLINE_MODE_)
        if([method isEqualToString:@"POST"] ) {
            NSLog(@"OFFLINE MODE(POST): %@", path);
            NSString* postStr = nil;
            NSString* uri = [NSString stringWithFormat:@"%@%@", _OFFLINE_URL, path];
            if ([path rangeOfString:@"/api2/login/"].location != NSNotFound) {
                postStr = _postStr;
                uri = [NSString stringWithFormat:@"%@?%@", uri, postStr];
            } else if ([path rangeOfString:@"/api2/BGNInitSP/"].location != NSNotFound) {
                postStr = _postStr;
                uri = [NSString stringWithFormat:@"%@?%@", uri, postStr];
            }
            //その他のキャッシュ付きダウンロード
            NSURL* url = [NSURL URLWithString:uri];
            return [[CacheProxyResponse alloc] initWithURL:url forConnection:self post:postStr];
        }
#endif
    }
    
	return result;
}

- (void)processBodyData:(NSData *)postDataChunk
{
    _postStr = [[NSString alloc] initWithData:postDataChunk encoding:NSUTF8StringEncoding];
}

- (BOOL)supportsMethod:(NSString *)method atPath:(NSString *)path
{
	if ([method isEqualToString:@"GET"])
		return YES;

	if ([method isEqualToString:@"POST"])
		return YES;
	
	if ([method isEqualToString:@"HEAD"])
		return YES;
    
	return NO;
}

- (NSMutableURLRequest *)requestWithPath:(NSString *)path {
    NSString *uri = [NSString stringWithFormat:@"%@%@", CDN_URL_HTTPS, path];
    NSURL *url = [NSURL URLWithString:uri];
    
    NSMutableURLRequest* req = [[NSMutableURLRequest alloc]
                                initWithURL:url
                                cachePolicy:NSURLRequestReloadIgnoringCacheData
                                timeoutInterval:30];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [req setValue:PLAYER_USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
    } else {
        [req setValue:PLAYER_USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
    }
    
    return req;
}

- (NSObject<HTTPResponse> *)m3u8Response:(NSString *)path {
    NSURLRequest *req = [self requestWithPath:path];
    
    NSURLResponse *res;
    NSError *error;
    NSData *data = [NSURLConnection sendSynchronousRequest:req
                                         returningResponse:&res
                                                     error:&error];
    
    if (error != nil) {
        return nil;
    }

    NSString *m3u8 = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString *a = CDN_URL;
    NSString *b =[THE_CONTEXT() getLocalURL];
    m3u8 = [m3u8 stringByReplacingOccurrencesOfString:CDN_URL withString:[THE_CONTEXT() getLocalURL]];
    m3u8 = [m3u8 stringByReplacingOccurrencesOfString:CDN_URL_HTTPS withString:[THE_CONTEXT() getLocalURL]];
    
    return [[PTHttpResponse alloc] initWithData:[m3u8 dataUsingEncoding:NSUTF8StringEncoding]];
}

- (NSObject<HTTPResponse>*)binaryResponse:(NSString*)path {
    NSURLRequest *req = [self requestWithPath:path];
    
    NSURLResponse *res;
    NSError *error;
    NSData *data = [NSURLConnection sendSynchronousRequest:req
                                         returningResponse:&res
                                                     error:&error];
    
    if (error != nil) {
        return nil;
    }

    return [[PTHttpResponse alloc] initWithData:data];
}

@end
