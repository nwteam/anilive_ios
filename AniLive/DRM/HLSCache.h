//
//  HLSCache.h
//  DAISUKI
//
//  Created by MacBookAir on 13/07/31.
//  Copyright (c) 2013年 DAISUKI Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@class M3U8Parser;

@protocol HLSCacheDelegate<NSObject>

@required

@optional
-(void)onFirstFileCached:(NSObject*)hlscache;
@end


@interface HLSCache : NSObject {
    
    NSMutableData*      _buffer;
    int                 _daisukiAESKeyNo;
    NSURLConnection*    _connection;
	NSTimer*            _cachingTimer;	//	キャッシュ用タイマー
    
    BOOL        _isDownloaderActive;
    BOOL        _isFirstFileLoaded;
    int         _retryCount;
    BOOL        _isPaused;
}
@property(assign, atomic) float cachedStartTime;
@property(assign, atomic) float cachedEndTime;
@property(retain, atomic) M3U8Parser* m3u8parser;
@property(retain, atomic) NSMutableDictionary*	cacheCheck;
@property(retain, atomic) NSMutableArray*       cacheRequestQueue; //キャッシュリクエストキュー



@property(assign, atomic) BOOL isFailed;
@property(assign, atomic) BOOL isCanceled;
@property (nonatomic, assign ) id delegate; //Delegate


-(id)   init;
-(BOOL) startCachingWithURL:(NSString*)m3u8 daisukiConvert:(BOOL)convert seek:(float)seekTime;
-(void) stop;
-(void) seek:(float)seekTime;
-(BOOL) isCached:(NSString*) url;
-(BOOL) isCachedWithTime:(float)time;
-(NSString*) getCachePath:(NSString*)url;
-(BOOL) addPrimaryRequest:(NSString*)url;
-(void) checkCached;
-(void) clearCache;

-(void)pause;
-(void)resume;

@end
