//
//  DRMProxyHTTPResponse.m
//  drmtest
//
//  Created by MacBookAir on 13/06/14.
//  Copyright (c) 2013年 agyou. All rights reserved.
//

#import "DLAppDelegate.h"
#import "DRMHttpConnection.h"
#import "CacheProxyResponse.h"
#import "HTTPConnection.h"
#import "HTTPLogging.h"
#import <unistd.h>
#import <fcntl.h>

//for rsa
/*#include <openssl/rsa.h>
 #include <openssl/engine.h>
 #include <openssl/pem.h>
 #include <openssl/x509.h>*/

@implementation CacheProxyResponse

@synthesize readBuf;

- (id)initWithURL:(NSURL *)url forConnection:(HTTPConnection *)parent post:(NSString*)post;
{
	if ((self = [super init]))
	{
		_parentConnection = parent;
        
		if (url == nil)
		{
			//HTTPLogWarn(@"%@: Init failed - Nil url", THIS_FILE);
			return nil;
		}
		_url = [url copy];
        _loadedLength = 0;
        _offset = 0;
        _isReady = NO;
        _isConnectionClosed = NO;
        _isSendDone = NO;
        _isError = NO;
        
        NSString* uri = [_url absoluteString];

        if ([self isCached:uri]) {
            // データを初期化
            self.readBuf = [NSMutableData dataWithContentsOfFile:[self getCachePath:uri]];
            _contentLength = self.readBuf.length;
            _isReady = YES;
            _isConnectionClosed = YES;
            
        } else {
            if (![post isEqualToString:@""]) {
                NSMutableURLRequest* request = [[NSMutableURLRequest alloc]
                                                initWithURL:url
                                                cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
                NSURLResponse*  response = nil;
                NSError*        error    = nil;
                NSData*         data = nil;
                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                    [request setValue:USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
                } else {
                    [request setValue:USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
                }
                [request setHTTPMethod:@"POST"];	//メソッドをPOSTに指定します
                [request setHTTPBody:[post dataUsingEncoding:NSUTF8StringEncoding]];
                
                data = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:&response error:&error];
                
                [data writeToFile:[self getCachePath:uri] atomically:NO];

                
            } else {
                NSMutableURLRequest* request = [[NSMutableURLRequest alloc]
                                                initWithURL:url
                                                cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                    [request setValue:USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
                } else {
                    [request setValue:USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
                }
                NSURLResponse*  response = nil;
                NSError*        error    = nil;
                NSData*         data = nil;
                data = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:&response error:&error];
                [data writeToFile:[self getCachePath:uri] atomically:NO];
            }
            
            self.readBuf = [NSMutableData dataWithContentsOfFile:[self getCachePath:uri]];
            
            _contentLength = self.readBuf.length;
            _isReady = YES;
            _isConnectionClosed = YES;
        }
    }
	return self;
}



- (NSURL *)url
{
	return _url;
}

- (UInt64)contentLength
{
    return _contentLength;
}

/**
 * The HTTP server supports range requests in order to allow things like
 * file download resumption and optimized streaming on mobile devices.
 **/
- (UInt64)offset
{
    return _offset;
}

- (void)setOffset:(UInt64)offset
{
    _offset = offset;
}

//データロード
- (NSData *)readDataOfLength:(NSUInteger)length
{
    //	暗号化？非暗号化？
    NSMutableData* data = self.readBuf;
    
    //バッファにデータが無い場合は待つ
    int wc = 0;
    while (1) {
        if ([data length] > 0) break;
        if (_isConnectionClosed) break;
        if (_isError) break;
        wc++;
        [NSThread sleepForTimeInterval:0.1];
        //NSLog( @"readDataOfLength Idle ..." );
    }
    //    NSLog( @"...Idle Done" );
    if (_isError) return nil;
    
    NSUInteger readlen = length;
    if ([data length] < length) {
        readlen = [data length];
    }
    
    if (readlen <= 0) {
        NSLog( @"in CacheProxyResponse");
        NSLog( @"readlen <= 0" );
        NSLog( @"self.readBuf:%lu",(unsigned long)self.readBuf.length );
        NSLog( @"length:%lu", (unsigned long)length );
        NSLog( @"_loadedLength:%lu", (unsigned long)_loadedLength );
        NSLog( @"_contentLength:%lu", (unsigned long)_contentLength );
        NSData* ret = [NSData data];
        return ret;
    }
    
    NSRange range = NSMakeRange(0, readlen);
    NSData* ret = [data subdataWithRange:range];
    
    _offset += readlen;
    _loadedLength = _loadedLength + readlen;
    
    /*    if ([data length] == readlen) {
     [data setLength:0];
     if (_isConnectionClosed) {
     _isSendDone = YES;
     }
     } else if ([data length] > readlen) {
     NSRange restrange = NSMakeRange(readlen, [data length] - readlen);
     [data setData:[data subdataWithRange:restrange]];
     }
     */
    if (_loadedLength >= _contentLength) {
        [data setLength:0];
        if (_isConnectionClosed) {
            _isSendDone = YES;
        }
    } else if ([data length] > readlen) {
        NSRange restrange = NSMakeRange(readlen, [data length] - readlen);
        [data setData:[data subdataWithRange:restrange]];
    }
    
    return ret;

}

//終了か？
- (BOOL)isDone
{
    return _isSendDone;
}
- (BOOL)isChunked
{
    return YES;
}


- (BOOL)delayResponseHeaders {
    return (!_isReady);
}

- (NSDictionary *)httpHeaders
{
    return _responseHeader;
}

- (void)connectionDidClose
{
    if (!_isConnectionClosed) {
        [_nsConnection cancel];
        _isConnectionClosed = YES;
        _isSendDone = YES;
    }
}

//キャッシュファイルのパスを取得
-(NSString*) getCachePath:(NSString*)url
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cache_dir = [paths objectAtIndex:0];
    NSString* hash = [THE_CONTEXT() md5:url];
    NSString* filename = [NSString stringWithFormat:@"%@/%@.cache", cache_dir, hash];
    
    return filename;
}

//キャッシュファイルがあるか？
-(BOOL) isCached:(NSString*) url
{
    NSString* filename = [self getCachePath:url];
    
    NSFileManager* file_mgr = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cache_dir = [paths objectAtIndex:0];
    
    [file_mgr changeCurrentDirectoryPath:cache_dir];
    
    if( [file_mgr fileExistsAtPath:filename]){
        return YES;
    } else {
        return NO;
    }
    return NO;
}

@end
