//
//  DRMHttpConnection.h
//  drmtest
//
//  Created by MacBookAir on 13/06/14.
//  Copyright (c) 2013年 agyou. All rights reserved.
//

#import "HTTPConnection.h"

@interface DRMHttpConnection : HTTPConnection

@property(retain, atomic) NSString* postStr;

@end
