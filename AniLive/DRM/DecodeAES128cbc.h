//
//  DecodeAES128cbc.h
//  drmtest
//
//  Created by 新井 茂成 on 2013/07/16.
//  Copyright (c) 2013年 agyou. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <openssl/evp.h>
#include <openssl/aes.h>


@interface DecodeAES128cbc : NSObject
{
    EVP_CIPHER_CTX			_de;						//	暗号化情報
	volatile int			_decodeIndex;				//	デコード位置
	volatile unsigned char	_decodeWorkBuffer[2048];	//	デコード用の作業バッファ
}


@property (retain) NSData*			aesKey;		//	AES キー
@property (assign) NSMutableData*	readBuf;	//	デコード対象
@property (assign) BOOL				isDecoded;	//	デコード完了？
@property (retain) NSMutableData*	decodeBuf;	//	デコード後


//	初期設定する
- (void)startCipher:(NSData*)aeskey;

//	終了設定する
- (void)stopCipher;

//	デコードバッファのクリア
- (void)clear:(NSMutableData*)read_buf;

//	復号する(全部)
- (void)decodeBufferForAll:(BOOL)is_all;

//	復号する(指定バイト数)
- (NSInteger)decodeBufferWithSize:(int)ilen;

// 一括デコード
- (NSMutableData*)decodeWithKey:(NSData*)aeskey input:(NSMutableData*)input;

@end
