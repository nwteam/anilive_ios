//
//  M3U8Parser.h
//  DAISUKI
//
//  Created by MacBookAir on 13/07/30.
//  Copyright (c) 2013年 DAISUKI Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface M3U8Parser : NSObject {
}
@property( nonatomic, retain ) NSMutableArray*	items;
@property( nonatomic, retain ) NSMutableArray*	lengths;
@property( nonatomic, retain ) NSMutableArray*	children;
@property( nonatomic, retain ) NSMutableDictionary*  keys; //URL(local)をmd5した値をキーとする

- (id)  init;
- (BOOL)parseWithURI:(NSString*)uri daisukiConvert:(BOOL)convert;
- (BOOL)parseWithLocalURI:(NSString*)uri daisukiConvert:(BOOL)convert;
- (BOOL)parseWithData:(NSData*)data daisukiConvert:(BOOL)convert;
- (NSString*)daisukiURLConvert:(NSString*)url;

-(NSString*)getCacheFileName:(NSString*)uri;
@end
