#import <Foundation/Foundation.h>
#import "HTTPResponse.h"

@class HTTPConnection;


@interface DRMKeyResponse : NSObject <HTTPResponse>
{
	HTTPConnection *connection;
	
	UInt64 _fileLength;
	UInt64 _fileOffset;
	
	BOOL _aborted;
	NSString* _keyURI;
    NSData* _keyData;
}

- (id)initWithKeyURL:(NSString*)uri forConnection:(HTTPConnection *)parent;

@end
