//
//  HLSCache.m
//  DAISUKI
//
//  Created by MacBookAir on 13/07/31.
//  Copyright (c) 2013年 DAISUKI Inc. All rights reserved.
//

#import "DLAppDelegate.h"
#import "HLSCache.h"
#import "M3U8Parser.h"
#import "DecodeAES128cbc.h"

@implementation HLSCache

//初期化
-(id) init{
    
    self = [super init];
    
    _isFirstFileLoaded = NO;
    _isFailed = NO;
    _retryCount = 0;
    _isCanceled = NO;
    _cachingTimer = nil;
    _isDownloaderActive = NO;
    self.cachedStartTime = 0.0f;
    self.cachedEndTime = 0.0f;
    self.cacheRequestQueue = [NSMutableArray array];
    self.cacheCheck = [NSMutableDictionary dictionary];
    return self;
    
}

//キャッシュスタート
-(BOOL) startCachingWithURL:(NSString*)m3u8 daisukiConvert:(BOOL)convert seek:(float)seekTime
{
    //[self clearCache];
    
    _isPaused = NO;
    _isFirstFileLoaded = NO;
    self.isFailed = NO;
    _retryCount = 0;
    self.cachedStartTime = 0.0f;
    self.cachedEndTime = 0.0f;
    _isDownloaderActive = NO;

    //m3u8はメインスレッドで同期ダウンロード
    self.m3u8parser = [[M3U8Parser alloc] init];

    if (![_m3u8parser parseWithLocalURI:m3u8 daisukiConvert:convert]) {
        self.isFailed = YES;
        return NO;
    }
        
    if (_m3u8parser.items.count > 0) {
        [self seek:seekTime];
    }

    return YES;

}

//TSファイルをダウンロード
-(void) loadTSFile:(NSTimer*)timer
{
    [_cachingTimer invalidate];
    _cachingTimer = nil;
    
    if (self.cacheRequestQueue.count == 0) {
        //All File Cached
        _isDownloaderActive = NO;
        if (!_isFirstFileLoaded) {
            if ([_delegate respondsToSelector:@selector(onFirstFileCached:)]) {
                [_delegate performSelector:@selector(onFirstFileCached:) withObject:self];
            }
            _isFirstFileLoaded = YES;
        }
        return;
    }
    if (self.isCanceled) {
        _isDownloaderActive = NO;
        return;
    }
    if (_isPaused) {
        return;
    }

    
    NSString* downloadURL = self.cacheRequestQueue[0];
    
    //デバッグ用
    BOOL bFind = NO;
    for (int i = 0; i < _m3u8parser.items.count; i++) {
        NSString* tsURL = [_m3u8parser.items[i] stringByReplacingOccurrencesOfString:[THE_CONTEXT() getLocalURL] withString:CDN_URL];

        if ([downloadURL isEqualToString:tsURL]) {
            //NSLog(@"  begin cache to file %d", i);
            bFind = YES;
            break;
        }
    }
    if (!bFind) {
        NSLog(@"  INVALID Cache Request");
    }

    //NSLog(@"  LoadTS %@", downloadURL);
    
    //既にキャッシュされてたら次のファイルへ
    if ([self isCached:downloadURL]) {
        //NSLog(@"loadTSFile already cached");
        [self checkCached];

        //既にキャッシュされていた場合はデリゲート呼び出し
        if (!_isFirstFileLoaded) {
            if ([_delegate respondsToSelector:@selector(onFirstFileCached:)]) {
                [_delegate performSelector:@selector(onFirstFileCached:) withObject:self];
            }
            _isFirstFileLoaded = YES;
        }

        //キューの先頭を削除
        [self.cacheRequestQueue removeObjectAtIndex:0];
        
        //delegateを使ってコールバックする
        if (!_isFirstFileLoaded) {
            if ([_delegate respondsToSelector:@selector(onFirstFileCached:)]) {
                [_delegate performSelector:@selector(onFirstFileCached:) withObject:self];
            }
            _isFirstFileLoaded = YES;
        }
        
        [self loadNextURL];
        return;
    }

    //NSLog(@"Start loadTSFile");

    //バッファの初期化
    _buffer = [NSMutableData dataWithCapacity:0];
    
	//  TSファイルを読み込む
    NSURL* url = [NSURL URLWithString:downloadURL];
	NSMutableURLRequest* request = [[NSMutableURLRequest alloc]
                                    initWithURL:url
                                    cachePolicy:NSURLRequestReloadIgnoringCacheData
                                    timeoutInterval:30];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [request setValue:PLAYER_USER_AGENT_IPHONE forHTTPHeaderField:@"User-Agent"];
    } else {
        [request setValue:PLAYER_USER_AGENT_IPAD forHTTPHeaderField:@"User-Agent"];
    }
    
    _connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
    if (_connection==nil) {
        _isFirstFileLoaded = NO;
        self.isFailed = YES;
        return;
    }
    [_connection scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    [_connection start];
}

//次のファイルをロード
-(void) loadNextURL
{
    if (self.isCanceled) {
        return;
    }
    if (_isPaused) {
        return;
    }
    
    //次のファイルをロード
    if (_cachingTimer) {
        [_cachingTimer invalidate];
    }
    _cachingTimer = nil;

    _cachingTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f
                                                     target:self selector:@selector(loadTSFile:)
                                                   userInfo:nil repeats:NO];

}

//すでにキャッシュされているファイルを調べてcachedEndTimeを更新新
-(void) checkCached
{
    self.cachedEndTime = self.cachedStartTime;
    int lastindex = 0;

    int seekIndex = -1;
    float total=0;
    for (int i = 0; i < _m3u8parser.lengths.count; i++) {
        float from = total;
        total += [_m3u8parser.lengths[i] floatValue];
        float to   = total;
        
        if (from <= self.cachedStartTime && self.cachedStartTime < to) {
            seekIndex = i;
            break;
        }
    }
    if (seekIndex == -1) {
        return;
    }
    
    for (int i = seekIndex; i < _m3u8parser.items.count; i++) {
        NSString* tsURL = [_m3u8parser.items[i] stringByReplacingOccurrencesOfString:[THE_CONTEXT() getLocalURL] withString:CDN_URL];
        if( [self isCached:tsURL]){
            lastindex = i;
         } else {
             break;
         }
    }
    
    self.cachedEndTime = 0;
    for (int i = 0; i <= lastindex; i++) {
        if (i < _m3u8parser.lengths.count) {
            self.cachedEndTime += [_m3u8parser.lengths[i] floatValue];
        }
    }
//    NSLog(@"checkCached %f %f", self.cachedStartTime, self.cachedEndTime);
}

//キャッシュファイルがあるか？
-(BOOL) isCached:(NSString*) url
{    
    NSString* filename = [self getCachePath:url];
    
#if defined(_OFFLINE_MODE_)
    NSFileManager* file_mgr = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cache_dir = [paths objectAtIndex:0];
    
    [file_mgr changeCurrentDirectoryPath:cache_dir];
    
    if( [file_mgr fileExistsAtPath:filename]){
        return YES;
    } else {
        return NO;
    }
    return NO;
#else
    NSString *check = [self.cacheCheck objectForKey:filename];
    if (check) {
        return YES;
    }
#endif
    return NO;
}

//時間指定でキャッシュされているか確認
-(BOOL) isCachedWithTime:(float)time
{
    NSLog(@"isCachedWithTime");
    
    if (time < 0) time = 0;

    if (!_m3u8parser) {
        return NO;
    }
    
    //時間から開始位置を算出
    float total = 0;
    int seekIndex = -1;
    float seekStartTime = 0;
    for (int i = 0; i < _m3u8parser.lengths.count; i++) {
        float from = total;
        total += [_m3u8parser.lengths[i] floatValue];
        float to   = total;
        
        if (from <= time && time < to) {
            seekStartTime = from;
            seekIndex = i;
            break;
        }
    }
    
    if (seekIndex == -1) {
        return NO;
    }
    NSString* url = [_m3u8parser.items[seekIndex] stringByReplacingOccurrencesOfString:[THE_CONTEXT() getLocalURL] withString:CDN_URL];
    return [self isCached:url];
}

//キャッシュファイルのパスを取得
-(NSString*) getCachePath:(NSString*)url
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cache_dir = [paths objectAtIndex:0];
    NSString* hash = [THE_CONTEXT() md5:url];
    NSString* filename = [NSString stringWithFormat:@"%@/%@.ts", cache_dir, hash];

    return filename;
}

//キャッシュ停止
-(void) stop
{
    if (_connection) {
        [_connection cancel];
        _connection = nil;
    }
    
    self.isCanceled = YES;
    
    if (_cachingTimer) {
        [_cachingTimer invalidate];
        _cachingTimer = nil;
    }
    [self.cacheRequestQueue removeAllObjects];
    _isDownloaderActive = NO;
}

//シークに対応する
-(void) seek:(float)seekTime
{
    //NSLog(@"HLSCache::seek %f", seekTime);
    
    if (seekTime < 0) seekTime = 0;
    
    //時間から開始位置を算出
    float total = 0;
    int seekIndex = -1;
    float seekStartTime = 0;
    for (int i = 0; i < _m3u8parser.lengths.count; i++) {
        float from = total;
        total += [_m3u8parser.lengths[i] floatValue];
        float to   = total;
                
        if (from <= seekTime && seekTime < to) {
            seekStartTime = from;
            seekIndex = i;
            break;
        }
    }
    if (seekIndex == -1) {
        seekIndex = 0;
    }

    self.cachedStartTime = seekStartTime;
    self.cachedEndTime = seekStartTime;
    self.isCanceled = NO;
    
    //キューを書き換える
    if (self.cacheRequestQueue.count > 0) {
        NSString* top = [NSString stringWithString: self.cacheRequestQueue[0]];
        [self.cacheRequestQueue removeAllObjects];
        [self.cacheRequestQueue addObject:top]; //キューの先頭は残しておく
    } else {
        [self.cacheRequestQueue removeAllObjects];
    }
    for (int i = seekIndex; i <_m3u8parser.items.count; i++) {
        NSString* tsURL = [_m3u8parser.items[i] stringByReplacingOccurrencesOfString:[THE_CONTEXT() getLocalURL] withString:CDN_URL];
        [self.cacheRequestQueue addObject:tsURL];
    }
    
    [self checkCached];
    
    //タイマーが消えていたら再起動
    if (!_isDownloaderActive) {
        _isDownloaderActive = YES;
        _cachingTimer = [NSTimer scheduledTimerWithTimeInterval:0.001f
                                                         target:self selector:@selector(loadTSFile:)
                                                       userInfo:nil repeats:NO];
    }
}

//優先ダウンロードバッファに追加
-(BOOL) addPrimaryRequest:(NSString*)url
{
    if (self.isFailed) return NO;
    if (self.isCanceled) return NO;

    BOOL bFind = NO;
    
    //m3u8の中にあるファイルか？
    for (int i = 0; i < _m3u8parser.items.count; i++) {
        NSString* tsURL = [_m3u8parser.items[i] stringByReplacingOccurrencesOfString:[THE_CONTEXT() getLocalURL] withString:CDN_URL];
        if ([url isEqualToString:tsURL]) {
            //NSLog(@"HLSCache::addPrimaryRequest %d", i);
            bFind = YES;
            break;
        }
    }
/*
 {//デバッグ
        NSLog(@"Cueue dump:");
        for (int j = 0; j < self.cacheRequestQueue.count; j++) {
            NSString* cue = self.cacheRequestQueue[j];
            for (int i = 0; i < _m3u8parser.items.count; i++) {
                if ([cue isEqualToString:_m3u8parser.items[i]]) {
                    NSLog(@"  %d: %d", j, i);
                    break;
                }
            }
        }
    }
 */
    if (!bFind) {
        NSLog(@"HLSCache::addPrimaryRequest IVNALID %@", url);
        return NO;
    }
    
    if (self.cacheRequestQueue.count > 0) {
        int loop = 2;
        if (self.cacheRequestQueue.count < loop) loop = (int)self.cacheRequestQueue.count;
        BOOL bFind = NO;
        
        for (int i = 0; i < loop; i++) {
            if ([url isEqualToString:self.cacheRequestQueue[i]]) {
                bFind = YES;
            }
        }
        if (!bFind) {
            //今読み込んでいた物は破棄
            if (_connection) {
                [_connection cancel];
                _buffer = nil;
                _connection = nil;
            }
            NSLog(@"  insertRequest(1)");
            [self.cacheRequestQueue insertObject:url atIndex:0];
        }
    } else {
        if (_connection) {
            [_connection cancel];
            _buffer = nil;
            _connection = nil;
        }
        NSLog(@"  insertRequest(0)");
        [self.cacheRequestQueue insertObject:url atIndex:0];
    }
    
    //タイマーが消えていたら再起動
    if (!_isDownloaderActive || !_cachingTimer) {
        //NSLog(@"  restart timer");
        _isDownloaderActive = YES;
        _cachingTimer = [NSTimer scheduledTimerWithTimeInterval:0.001f
                                                         target:self selector:@selector(loadTSFile:)
                                                       userInfo:nil repeats:NO];
    }
    return YES;
}


//--------------------------------------------------------------------------------------------
//NSURLConnection delegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"CACHE didFailWithError");
    
    if (self.isCanceled) return;
    if (_isPaused) return;
    
    //リトライ
    _retryCount++;
    if (_retryCount < 3) {
        NSLog(@"  retry %d", _retryCount);
        [self loadNextURL];
        
    } else {
        NSLog(@"CACHE Cache Abort");
        [self stop];
        _isFailed = YES;
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (_isPaused) return;
    if (_isCanceled) return;
    if (!_buffer) return;
    if (data) {
        [_buffer appendData:data];
    }
}

//ダウンロード終了時
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (_isPaused) return;
    if (_isCanceled) return;
    if (self.cacheRequestQueue.count == 0) {
        return;
    }
    if (!_buffer) return;

    NSString* cacheURL = [NSString stringWithString: self.cacheRequestQueue[0]];
    NSString* filename = [self getCachePath:cacheURL];
    [_buffer writeToFile:filename atomically:YES];
    
    //キャッシュフラグをたてる
    [self.cacheCheck setObject:@"YES" forKey:filename];
    [self.cacheRequestQueue removeObjectAtIndex:0];
    

    [self checkCached];
    

    //delegateを使ってコールバックする
    if (!_isFirstFileLoaded) {
        if ([_delegate respondsToSelector:@selector(onFirstFileCached:)]) {
            [_delegate performSelector:@selector(onFirstFileCached:) withObject:self];
        }
        _isFirstFileLoaded = YES;
    }
    _connection = nil;
    

    //次のファイルをロード
    [self loadNextURL];
}


// キャッシュのクリア
-(void) clearCache
{
#if !defined(_OFFLINE_MODE_)
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cache_dir = [paths objectAtIndex:0];
    NSFileManager* file_mgr = [NSFileManager defaultManager];
    
    [file_mgr changeCurrentDirectoryPath:cache_dir];

    NSError *error = nil;
    NSArray *allFileName = [file_mgr contentsOfDirectoryAtPath:cache_dir error:&error];
    if (error) return;
    
    for (NSString *fileName in allFileName) {
        if ([[fileName pathExtension] isEqualToString:@"ts"]) {
            [file_mgr removeItemAtPath:fileName error:nil];
        }
        if ([[fileName pathExtension] isEqualToString:@"m3u8"]) {
            [file_mgr removeItemAtPath:fileName error:nil];
        }
    }
#endif
    [self.cacheCheck removeAllObjects];
    [self.cacheRequestQueue removeAllObjects];
    
}

// キャッシュの一時中断
-(void) pause
{
    NSLog(@"HLSCache::pause");
    _isPaused = YES;

    //今読み込んでいた物は破棄
    [_connection cancel];
    _buffer = nil; 
    _connection = nil;
    

    if (_cachingTimer) {
        [_cachingTimer invalidate];
        _cachingTimer = nil;
    }
}

// キャッシュの復帰
-(void) resume
{
    NSLog(@"HLSCache::resume");
    if (!_isCanceled && _isDownloaderActive) {
        _isFirstFileLoaded = NO;
        NSLog(@"  resuming..");
        _isPaused = NO;
        
        //次のファイルをロード
        [self loadNextURL];
    }
}



@end
