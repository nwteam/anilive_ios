//
//  BGNCrypt.h
//  DAISUKI
//
//  Created by MacBookAir on 2014/03/11.
//  Copyright (c) 2014年 DAISUKI Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#define ANGOU_RO  ("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDkMf4hywh+TyHfocJAWpzXTa53")

@interface BGNCrypt : NSObject {
    NSMutableData* _aesKey;
    NSString* _angou_ni;

}
@property (retain) NSString*    encryptedAesKey;    //エンコード済AES キー

//初期化
-(id) init;

//公開鍵の読み込み
- (BOOL)loadPublicKey;

//公開鍵での暗号化
- (NSString*)encryptByPublicKey:(NSMutableData*)data;

//AESでの暗号化
- (NSString*)encryptByAES:(NSString*)str;

//AESでの復号化
- (NSString*)decryptByAES:(NSString*)base64str;

//Base64エンコード
- (NSString *)base64Encode:(NSData*)data;

//Base64デコード
- (NSData *)base64Decode:(NSString*)base64str;


@end
