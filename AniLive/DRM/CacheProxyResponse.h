//
//  CacheProxyResponse.h
//  DAISUKI
//
//  Created by MacBookAir on 2013/10/04.
//  Copyright (c) 2013年 DAISUKI Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTTPResponse.h"
#import "DecodeAES128cbc.h"


@class HTTPConnection;


@interface CacheProxyResponse : NSObject <HTTPResponse> {
	HTTPConnection  *_parentConnection;
    NSURLConnection  *_nsConnection;
    NSURL *_url;
	NSUInteger _contentLength;      //コンテンツサイズ
	NSUInteger _loadedLength;       //読み込んだサイズ
    NSUInteger _offset;             //オフセット読み込み用
    BOOL _isReady;                  //ヘッダを読み込み準備が整っているか？
    BOOL _isConnectionClosed;       //HTTP通信が完了したか？
    BOOL _isError;                  //HTTP通信が失敗したか？
    BOOL _isSendDone;               //バッファ転送が完了したか？
    NSMutableDictionary* _responseHeader; //response
}


- (id)initWithURL:(NSURL *)url forConnection:(HTTPConnection *)parent post:(NSString*)post;
- (NSURL *)url;


@property (retain) NSMutableData* readBuf;		//thead safe read-buffer


@end
