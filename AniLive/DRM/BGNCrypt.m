//
//  BGNCrypt.m
//  DAISUKI
//
//  Created by MacBookAir on 2014/03/11.
//  Copyright (c) 2014年 DAISUKI Inc. All rights reserved.
//

#import "DLAppDelegate.h"
#import "BGNCrypt.h"
#undef SSLEAY_MACROS
#include <openssl/pem.h>
#include <openssl/x509v3.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <openssl/aes.h>
#import <CommonCrypto/CommonDigest.h>

#define ANGOU_NI  ("A5/pW0GFYhLFypzYVksc5L+X/AyoZaNJAfZdbwcJ29CHtxXfhp8H+YxzA3KBOp1i")

static char base64Table[64] = {
    'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P',
    'Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f',
    'g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v',
    'w','x','y','z','0','1','2','3','4','5','6','7','8','9','+','/' };


@interface BGNCrypt ()
{
	RSA*	_pubkey;	//	公開鍵
}
@end

@implementation BGNCrypt

//初期化
-(id) init{
    self = [super init];
    _angou_ni = [NSString stringWithFormat:@"%s", ANGOU_NI];
    return self;
    
}

//公開鍵のロード
- (BOOL)loadPublicKey
{

    _aesKey = [[NSMutableData alloc] initWithLength:16];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cache_dir = [paths objectAtIndex:0];

    NSString* fname = [NSString stringWithFormat:@"%@/%@", cache_dir, @"a078f226c177c8779cd437878a25377d3de.ts"];
    FILE* fp = fopen( [fname UTF8String], "w" );
    fprintf(fp, "%s\n", ANGOU_I);
    fprintf(fp, "%s\n", ANGOU_RO);
    fprintf(fp, "%s\n", ANGOU_HA);
    fprintf(fp, "%s\n", [_angou_ni UTF8String]);
    fprintf(fp, "%s\n", ANGOU_HO);
    fprintf(fp, "%s\n", ANGOU_HE);
    fclose(fp);

//    NSString* pubkeyFilename = [[NSBundle mainBundle] pathForResource:@"pubkey" ofType:@"pem"];
//    FILE* fp = fopen( [pubkeyFilename UTF8String], "r" );

    fp = fopen( [fname UTF8String], "r" );
    if (!fp) {
        return NO;
    }
    
    _pubkey = PEM_read_RSA_PUBKEY( fp, NULL, NULL, NULL );
    if ( _pubkey == NULL )
    {
        //remove([fname UTF8String]);
        return NO;
    }
    fclose( fp );
    
    //remove([fname UTF8String]);
    
    //AESキーをランダムで作成
#if defined(_OFFLINE_MODE_)
    unsigned char* p = (unsigned char*)[_aesKey mutableBytes];
    for (int i = 0; i < 16; i++) {
        p[i] = (unsigned char)(base64Table[0]);
    }
#else
    unsigned char* p = (unsigned char*)[_aesKey mutableBytes];
    for (int i = 0; i < 16; i++) {
        p[i] = (unsigned char)(base64Table[arc4random() % 64]);
    }
#endif

    //共通鍵を公開鍵で暗号化
    _encryptedAesKey = [self encryptByPublicKey:_aesKey];
    
    return YES;
}

//公開鍵で暗号化
- (NSString*)encryptByPublicKey:(NSMutableData*)data
{
    NSString* ret;
    int outlen;
    unsigned char outbuf[8192];

    if((outlen = RSA_public_encrypt((int)[data length], [data mutableBytes], outbuf, _pubkey, RSA_PKCS1_PADDING)) == -1) {
        NSLog(@"Encrypt error");
        return ret;
    }
    NSData* encrypted = [[NSData alloc] initWithBytes:outbuf length:outlen];
    ret = [self base64Encode:encrypted];
    
    return ret;
}

//AESで暗号化
- (NSString*)encryptByAES:(NSString*)str
{
    NSMutableData* ret = [[NSMutableData alloc] init];
    NSData* input = [str dataUsingEncoding:NSUTF8StringEncoding];
    unsigned char iv[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    unsigned char	outbuf[1032];	//	デコード用の作業バッファ
	int olen, tlen;
    const unsigned char* ibuff   = (const unsigned char *)[input bytes];
	EVP_CIPHER_CTX ctx;

	EVP_CIPHER_CTX_init(&ctx);
    EVP_EncryptInit_ex(&ctx,EVP_aes_128_cbc(), NULL,[_aesKey bytes], iv);
    int encodepos = 0;
    
    while (encodepos < input.length) {
        int datasize = 1032;
        if (datasize > input.length - encodepos) {
            datasize = (int)input.length - encodepos;
        }
        
        if (EVP_EncryptUpdate (&ctx, outbuf, &olen, ibuff, datasize) != 1) {
            NSLog(@"error in encrypt update\n");
            return 0;
        }
        
        if (EVP_EncryptFinal_ex(&ctx, outbuf + olen, &tlen) != 1) {
        }
        olen += tlen;
        
        [ret appendData:[NSData dataWithBytes:(unsigned char*)outbuf length:olen]];
        encodepos += datasize;
        ibuff += datasize;
    }
	EVP_CIPHER_CTX_cleanup (&ctx);
    
    NSString* retstr = [self base64Encode:ret];
    return retstr;
}

//AESキーで復号
- (NSString*)decryptByAES:(NSString*)base64str
{
    NSMutableData* ret = [[NSMutableData alloc] init];
    NSData* input = [self base64Decode:base64str];
    
    EVP_CIPHER_CTX de;
    unsigned char iv[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    EVP_CIPHER_CTX_init( &de );
	EVP_DecryptInit_ex( &de, EVP_aes_128_cbc(), NULL, [_aesKey bytes], iv );
	EVP_CIPHER_CTX_set_padding( &de, 0 );
	EVP_CIPHER_CTX_set_key_length( &de, 16 );
    
    const unsigned char* ibuff   = (const unsigned char *)[input bytes];
	unsigned char	outbuf[2048];	//	デコード用の作業バッファ
    int		p_len, f_len;
    int decodepos = 0;
    
    while (decodepos < input.length) {
        int datasize = 1032;
        if (datasize > input.length - decodepos) {
            datasize = (int)input.length - decodepos;
        }
        
        EVP_DecryptUpdate  (&de, outbuf, &p_len,  &ibuff[decodepos], datasize);
        if (!EVP_DecryptFinal(&de, (outbuf+p_len), &f_len)) {
            //f_len = 0;
        }
        
        for (int i = 0; i < p_len+f_len; i++) {
            if (outbuf[i] == 5) {
                outbuf[i] = '\0';
            }
        }
        
        [ret appendData:[NSData dataWithBytes:(unsigned char*)outbuf length:p_len + f_len]];
        decodepos += datasize;
    }
    EVP_CIPHER_CTX_cleanup(&de);
    
    NSString* retstr = [[NSString alloc] initWithData:ret encoding:NSUTF8StringEncoding];
    return retstr;
}

//Base64フォーマットにエンコード
- (NSString *)base64Encode:(NSData*)data
{
	const unsigned char	*bytes = [data bytes];
	NSMutableString *result = [NSMutableString stringWithCapacity:[data length]];
	unsigned long ixtext = 0;
	unsigned long lentext = [data length];
	long ctremaining = 0;
	unsigned char inbuf[3], outbuf[4];
	unsigned short i = 0;
	unsigned short charsonline = 0, ctcopy = 0;
	unsigned long ix = 0;
	
	while( YES )
	{
		ctremaining = lentext - ixtext;
		if( ctremaining <= 0 ) break;
		
		for( i = 0; i < 3; i++ ) {
			ix = ixtext + i;
			if( ix < lentext ) inbuf[i] = bytes[ix];
			else inbuf [i] = 0;
		}
		
		outbuf [0] = (inbuf [0] & 0xFC) >> 2;
		outbuf [1] = ((inbuf [0] & 0x03) << 4) | ((inbuf [1] & 0xF0) >> 4);
		outbuf [2] = ((inbuf [1] & 0x0F) << 2) | ((inbuf [2] & 0xC0) >> 6);
		outbuf [3] = inbuf [2] & 0x3F;
		ctcopy = 4;
		
		switch( ctremaining )
		{
			case 1:
				ctcopy = 2;
				break;
			case 2:
				ctcopy = 3;
				break;
		}
		
		for( i = 0; i < ctcopy; i++ )
			[result appendFormat:@"%c", base64Table[outbuf[i]]];
		
		for( i = ctcopy; i < 4; i++ )
			[result appendString:@"="];
		
		ixtext += 3;
		charsonline += 4;
	}
	
	return [NSString stringWithString:result];
}

- (NSData *)base64Decode:(NSString*)base64str
{
    NSData *input = [base64str dataUsingEncoding:NSUTF8StringEncoding];
    
	const unsigned char	*bytes = [input bytes];
	NSMutableData *result = [NSMutableData dataWithCapacity:[input length]];
	
	unsigned long ixtext = 0;
	unsigned long lentext = [input length];
	unsigned char ch = 0;
	unsigned char inbuf[4] = {0, 0, 0, 0};
	unsigned char outbuf[3] = {0, 0, 0};
	short i = 0, ixinbuf = 0;
	BOOL flignore = NO;
	BOOL flendtext = NO;
	
	while( YES )
	{
		if( ixtext >= lentext ) break;
		ch = bytes[ixtext++];
		flignore = NO;
		
		if( ( ch >= 'A' ) && ( ch <= 'Z' ) ) ch = ch - 'A';
		else if( ( ch >= 'a' ) && ( ch <= 'z' ) ) ch = ch - 'a' + 26;
		else if( ( ch >= '0' ) && ( ch <= '9' ) ) ch = ch - '0' + 52;
		else if( ch == '+' ) ch = 62;
		else if( ch == '=' ) flendtext = YES;
		else if( ch == '/' ) ch = 63;
		else flignore = YES;
		
		if( ! flignore )
		{
			short ctcharsinbuf = 3;
			BOOL flbreak = NO;
			
			if( flendtext )
			{
				if( ! ixinbuf ) break;
				if( ( ixinbuf == 1 ) || ( ixinbuf == 2 ) ) ctcharsinbuf = 1;
				else ctcharsinbuf = 2;
				ixinbuf = 3;
				flbreak = YES;
			}
			
			inbuf [ixinbuf++] = ch;
			
			if( ixinbuf == 4 )
			{
				ixinbuf = 0;
				outbuf [0] = ( inbuf[0] << 2 ) | ( ( inbuf[1] & 0x30) >> 4 );
				outbuf [1] = ( ( inbuf[1] & 0x0F ) << 4 ) | ( ( inbuf[2] & 0x3C ) >> 2 );
				outbuf [2] = ( ( inbuf[2] & 0x03 ) << 6 ) | ( inbuf[3] & 0x3F );
				
				for( i = 0; i < ctcharsinbuf; i++ )
					[result appendBytes:&outbuf[i] length:1];
			}
			
			if( flbreak )  break;
		}
	}
	
	return [NSData dataWithData:result];
}

@end
