//
//  DLTimeLabelView.m
//  AniLive
//
//  Created by isaoeka on 2016/12/29.
//  Copyright © 2016年 AnimeConsortiumJapan. All rights reserved.
//

#import "DLTimeLabelView.h"

@interface DLTimeLabelView ()

@property (nonatomic, strong) UILabel *timeLabel;

@end

@implementation DLTimeLabelView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrame];
}

- (void)dealloc {
    self.timeLabel.text = @"";
    FUNC_LOG
}

#pragma mark - Private Methods

- (void)initView {
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3f];
    self.layer.borderColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5f].CGColor;
    self.layer.borderWidth = 0.8f;
    self.layer.cornerRadius = 2.0f;
    self.layer.masksToBounds = true;
    
    self.timeLabel = [UILabel new];
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.font = BASE_FONT_BOLD(9);
    self.timeLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.timeLabel];
}

- (void)updateFrame {
    
    [self.timeLabel sizeToFit];
    self.timeLabel.center = CGPointMake(self.width / 2, self.height / 2);
    
}

#pragma mark - Private Methods

- (void)sizeToFit {
    [super sizeToFit];
    [self.timeLabel sizeToFit];
}

- (void)setText:(NSString *)text {
    self.timeLabel.text = text;
}

- (void)setTimeStr:(NSString *)time {
    // TODO: `0:00:00` と出てしまうが、時が不要であれば出ないようにする
     NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"H:mm:ss";
    
    NSDate   *date    = [dateFormatter dateFromString:time];
    NSString *dateStr = [dateFormatter stringFromDate:date];
    
    self.timeLabel.text = dateStr;
}

- (CGSize)getTimeLabelSize {
    return self.timeLabel.size;
}

@end
